/*	******************************************************************************************************************************************	*/
/*																																				*/
/*		Indicateur 02 : Exclusions pour les patients BPCO probable																 				*/
/*																																				*/
/*	******************************************************************************************************************************************	*/

*	******************************************************************************************************************************************	;
*	EXCLUSION DES PATIENTS ASTHMATIQUES																											;
*	******************************************************************************************************************************************	;

*	******************************************************************************************************************************************;
* 	Exclusion des patients en ALD asthme active au 1er septembre de l ann�e N;

proc sql;

	CREATE TABLE ALD_asthme AS
		SELECT DISTINCT
			a.BEN_IDT_ANO
		FROM T_INDI_BPCO_VACGA_&an_N. a
			INNER JOIN travail.Histo_ALD b
				ON a.BEN_IDT_ANO = b.BEN_IDT_ANO
		WHERE b.reperage = 53 AND (b.date_debut <= "01SEP&Annee_N."d <= b.date_fin OR (b.date_debut <= "01SEP&Annee_N."d AND 
			b.date_fin = "01JAN1600"d));

quit;

proc sql;

	* On ins�re l effectif dans le Flowchart;
	INSERT INTO flowch.Flow_Chart_BPCO_VACG_&an_N.
		SELECT
			5,
			COUNT(DISTINCT BEN_IDT_ANO)
	FROM T_INDI_BPCO_VACGA_&an_N.
	WHERE BEN_IDT_ANO IN (SELECT BEN_IDT_ANO FROM ALD_asthme);

	* On conserve les id des patients � supprimer;
	CREATE TABLE travail.exclus1 AS
		SELECT DISTINCT
			BEN_IDT_ANO
	FROM T_INDI_BPCO_VACGA_&an_N.
	WHERE BEN_IDT_ANO IN (SELECT BEN_IDT_ANO FROM ALD_asthme);

quit;

proc delete data = ALD_asthme;
run; quit;

*	******************************************************************************************************************************************;
* 	Exclusion des patients en ALD mucoviscidose active au 1er septembre de l ann�e N;

proc sql;

	CREATE TABLE ALD_mucoviscidose AS
		SELECT DISTINCT
			a.BEN_IDT_ANO
		FROM T_INDI_BPCO_VACGA_&an_N. a
			INNER JOIN travail.Histo_ALD b
				ON a.BEN_IDT_ANO = b.BEN_IDT_ANO
		WHERE b.reperage = 54 AND (b.date_debut <= "01SEP&Annee_N."d <= b.date_fin OR (b.date_debut <= "01SEP&Annee_N."d AND 
			b.date_fin = "01JAN1600"d));

quit;

proc sql;

	* On ins�re l effectif dans le Flowchart;
	INSERT INTO flowch.Flow_Chart_BPCO_VACG_&an_N.
		SELECT
			6,
			COUNT(DISTINCT BEN_IDT_ANO)
	FROM T_INDI_BPCO_VACGA_&an_N.
	WHERE BEN_IDT_ANO IN (SELECT BEN_IDT_ANO FROM ALD_mucoviscidose);

	* On conserve les id des patients � supprimer;
	CREATE TABLE travail.exclus2 AS
		SELECT DISTINCT
			BEN_IDT_ANO
	FROM T_INDI_BPCO_VACGA_&an_N.
	WHERE BEN_IDT_ANO IN (SELECT BEN_IDT_ANO FROM ALD_mucoviscidose);

quit;

proc delete data = ALD_mucoviscidose;
run; quit;

*	******************************************************************************************************************************************;
* 	Exclusion des patients avec au moins 3 d�livrances rembours�es de CSI seuls entre le 1er janvier de l ann�e N-1 et le 31 d�cembre de l annee N;

proc sql;

	CREATE TABLE CSI AS
		SELECT DISTINCT
			a.BEN_IDT_ANO,
			COUNT(DISTINCT b.date_debut) AS Nb_deliv length = 3
		FROM T_INDI_BPCO_VACGA_&an_N. a
			INNER JOIN travail.reperage_med_asthme_&Annee_2N._&Annee_N1. b
				ON a.BEN_IDT_ANO = b.BEN_IDT_ANO
		WHERE b.reperage = 41 AND ("01JAN&Annee_1N."d <= b.date_debut <= "31DEC&Annee_N."d)
		GROUP BY a.BEN_IDT_ANO;

quit;

proc sql;

	* On ins�re l effectif dans le Flowchart;
	INSERT INTO flowch.Flow_Chart_BPCO_VACG_&an_N.
		SELECT
			7,
			COUNT(DISTINCT BEN_IDT_ANO)
	FROM T_INDI_BPCO_VACGA_&an_N.
	WHERE BEN_IDT_ANO IN (SELECT BEN_IDT_ANO FROM CSI WHERE Nb_deliv >= 3);

	* On conserve les id des patients � supprimer;
	CREATE TABLE travail.exclus3 AS
		SELECT DISTINCT
			BEN_IDT_ANO
	FROM T_INDI_BPCO_VACGA_&an_N.
	WHERE BEN_IDT_ANO IN (SELECT BEN_IDT_ANO FROM CSI WHERE Nb_deliv >= 3);

quit;

proc delete data = CSI;
run; quit;

*	******************************************************************************************************************************************;
* 	Exclusion des patients avec au moins 1 d�livrance rembours�e d�anti IgE entre le 1er janvier de l annee N-1 et le 31 d�cembre de l annee N;

proc sql;

	CREATE TABLE anti_IGE AS
		SELECT DISTINCT
			a.BEN_IDT_ANO,
			COUNT(DISTINCT b.date_debut) AS Nb_deliv length = 3
		FROM T_INDI_BPCO_VACGA_&an_N. a
			INNER JOIN travail.reperage_med_asthme_&Annee_2N._&Annee_N1. b
				ON a.BEN_IDT_ANO = b.BEN_IDT_ANO
		WHERE b.reperage IN (37) AND ("01JAN&Annee_1N."d <= b.date_debut <= "31DEC&Annee_N."d)
		GROUP BY a.BEN_IDT_ANO;

quit;

proc sql;

	* On ins�re l effectif dans le Flowchart;
	INSERT INTO flowch.Flow_Chart_BPCO_VACG_&an_N.
		SELECT
			8,
			COUNT(DISTINCT BEN_IDT_ANO)
	FROM T_INDI_BPCO_VACGA_&an_N.
	WHERE BEN_IDT_ANO IN (SELECT BEN_IDT_ANO FROM anti_IGE WHERE Nb_deliv >= 1);

	* On conserve les id des patients � supprimer;
	CREATE TABLE travail.exclus4 AS
		SELECT DISTINCT
			BEN_IDT_ANO
	FROM T_INDI_BPCO_VACGA_&an_N.
	WHERE BEN_IDT_ANO IN (SELECT BEN_IDT_ANO FROM anti_IGE WHERE Nb_deliv >= 1);

quit;

proc delete data = anti_IGE;
run; quit;

*	******************************************************************************************************************************************;
* 	Exclusion des patients avec au moins 1 d�livrance rembours�e d�anti IL-5 entre le 1er janvier de l annee N-1 et le 31 d�cembre de l annee N;

proc sql;

	CREATE TABLE anti_IL5 AS
		SELECT DISTINCT
			a.BEN_IDT_ANO,
			COUNT(DISTINCT b.date_debut) AS Nb_deliv length = 3
		FROM T_INDI_BPCO_VACGA_&an_N. a
			INNER JOIN travail.reperage_med_asthme_&Annee_2N._&Annee_N1. b
				ON a.BEN_IDT_ANO = b.BEN_IDT_ANO
		WHERE b.reperage IN (38) AND ("01JAN&Annee_1N."d <= b.date_debut <= "31DEC&Annee_N."d)
		GROUP BY a.BEN_IDT_ANO;

quit;

proc sql;

	* On ins�re l effectif dans le Flowchart;
	INSERT INTO flowch.Flow_Chart_BPCO_VACG_&an_N.
		SELECT
			9,
			COUNT(DISTINCT BEN_IDT_ANO)
	FROM T_INDI_BPCO_VACGA_&an_N.
	WHERE BEN_IDT_ANO IN (SELECT BEN_IDT_ANO FROM anti_IL5 WHERE Nb_deliv >= 1);

	* On conserve les id des patients � supprimer;
	CREATE TABLE travail.exclus5 AS
		SELECT DISTINCT
			BEN_IDT_ANO
	FROM T_INDI_BPCO_VACGA_&an_N.
	WHERE BEN_IDT_ANO IN (SELECT BEN_IDT_ANO FROM anti_IL5 WHERE Nb_deliv >= 1);

quit;

proc delete data = anti_IL5;
run; quit;

*	******************************************************************************************************************************************;
* 	Exclusion des patients avec au moins 1 d�livrance rembours�e d�antileucotri�ne entre le 1er janvier de l annee N-1 et le 31 d�cembre de l annee N;

proc sql;

	CREATE TABLE antileucotriene AS
		SELECT DISTINCT
			a.BEN_IDT_ANO,
			COUNT(DISTINCT b.date_debut) AS Nb_deliv length = 3
		FROM T_INDI_BPCO_VACGA_&an_N. a
			INNER JOIN travail.reperage_med_asthme_&Annee_2N._&Annee_N1. b
				ON a.BEN_IDT_ANO = b.BEN_IDT_ANO
		WHERE b.reperage = 39 AND ("01JAN&Annee_1N."d <= b.date_debut <= "31DEC&Annee_N."d)
		GROUP BY a.BEN_IDT_ANO;

quit;

proc sql;

	* On ins�re l effectif dans le Flowchart;
	INSERT INTO flowch.Flow_Chart_BPCO_VACG_&an_N.
		SELECT
			10,
			COUNT(DISTINCT BEN_IDT_ANO)
	FROM T_INDI_BPCO_VACGA_&an_N.
	WHERE BEN_IDT_ANO IN (SELECT BEN_IDT_ANO FROM antileucotriene WHERE Nb_deliv >= 1);

	* On conserve les id des patients � supprimer;
	CREATE TABLE travail.exclus6 AS
		SELECT DISTINCT
			BEN_IDT_ANO
	FROM T_INDI_BPCO_VACGA_&an_N.
	WHERE BEN_IDT_ANO IN (SELECT BEN_IDT_ANO FROM antileucotriene WHERE Nb_deliv >= 1);

quit;

proc delete data = antileucotriene;
run; quit;

*	******************************************************************************************************************************************;
* 	Exclusion des patients avec au moins un acte CCAM de Thermoplastie bronchique cod� lors d�un s�-jour hospitalier en MCO termin� entre 
	le 1er janvier de l annee N-1 et le 31 d�cembre de l annee N;

proc sql;

	CREATE TABLE thermo_bronchique AS
		SELECT DISTINCT
			a.BEN_IDT_ANO,
			COUNT(DISTINCT b.date_debut) AS Nb_actes length = 3
		FROM T_INDI_BPCO_VACGA_&an_N. a
			INNER JOIN travail.reperage_thermo_bronch_&annee_1N._&annee_N1. b
				ON a.BEN_IDT_ANO = b.BEN_IDT_ANO
		WHERE "01JAN&Annee_1N."d <= b.date_fin <= "31DEC&Annee_N."d
		GROUP BY a.BEN_IDT_ANO;

quit;

proc sql;

	* On ins�re l effectif dans le Flowchart;
	INSERT INTO flowch.Flow_Chart_BPCO_VACG_&an_N.
		SELECT
			11,
			COUNT(DISTINCT BEN_IDT_ANO)
	FROM T_INDI_BPCO_VACGA_&an_N.
	WHERE BEN_IDT_ANO IN (SELECT BEN_IDT_ANO FROM thermo_bronchique WHERE Nb_actes >= 1);

	* On conserve les id des patients � supprimer;
	CREATE TABLE travail.exclus7 AS
		SELECT DISTINCT
			BEN_IDT_ANO
	FROM T_INDI_BPCO_VACGA_&an_N.
	WHERE BEN_IDT_ANO IN (SELECT BEN_IDT_ANO FROM thermo_bronchique WHERE Nb_actes >= 1);

quit;

proc delete data = thermo_bronchique;
run; quit;

*	******************************************************************************************************************************************;
* 	Exclusion des patients ayant eu au moins un diagnostic d�asthme cod� entre le 1er janvier de l annee N-1 et le 31 d�cembre de l annee N;


proc sql undo_policy = none;

	CREATE TABLE diag_asthme_MCO AS
		SELECT DISTINCT
			a.BEN_IDT_ANO
		FROM T_INDI_BPCO_VACGA_&an_N. a
			INNER JOIN travail.diag_asthme_&annee_1N._&annee_N1. b
				ON a.BEN_IDT_ANO = b.BEN_IDT_ANO
		WHERE reperage = 14 AND "01JAN&Annee_1N."d <= b.date_fin <= "31DEC&Annee_N."d AND b.domaine = "MCO";

	CREATE TABLE diag_asthme_SSR_HAD AS
		SELECT DISTINCT
			a.BEN_IDT_ANO
		FROM T_INDI_BPCO_VACGA_&an_N. a
			INNER JOIN travail.diag_asthme_&annee_1N._&annee_N1. b
				ON a.BEN_IDT_ANO = b.BEN_IDT_ANO
		WHERE reperage = 14 AND b.domaine IN ("HAD", "SSR") AND b.date_debut <= "31DEC&Annee_N."d AND b.date_fin >= "01JAN&Annee_1N."d;

quit;

data diag_asthme;
	set diag_asthme_MCO
		diag_asthme_SSR_HAD;
run;

proc sql;

	* On ins�re l effectif dans le Flowchart;
	INSERT INTO flowch.Flow_Chart_BPCO_VACG_&an_N.
		SELECT
			12,
			COUNT(DISTINCT BEN_IDT_ANO)
	FROM T_INDI_BPCO_VACGA_&an_N.
	WHERE BEN_IDT_ANO IN (SELECT BEN_IDT_ANO FROM diag_asthme);

	* On conserve les id des patients � supprimer;
	CREATE TABLE travail.exclus8 AS
		SELECT DISTINCT
			BEN_IDT_ANO
	FROM T_INDI_BPCO_VACGA_&an_N.
	WHERE BEN_IDT_ANO IN (SELECT BEN_IDT_ANO FROM diag_asthme);

quit;

proc delete data = diag_asthme diag_asthme_MCO diag_asthme_SSR_HAD;
run; quit;

*	******************************************************************************************************************************************;
* 	Exclusion des patients ayant eu au moins un diagnostic d��tat de mal asthmatique  cod� entre le 1er janvier de l annee N-1 et le 31 d�cembre de l annee N;

proc sql;

	CREATE TABLE etat_mal_asthmatique AS
		SELECT DISTINCT
			a.BEN_IDT_ANO
		FROM T_INDI_BPCO_VACGA_&an_N. a
			INNER JOIN travail.diag_asthme_&annee_1N._&annee_N1. b
				ON a.BEN_IDT_ANO = b.BEN_IDT_ANO
		WHERE reperage = 15 AND "01JAN&Annee_1N."d <= b.date_fin <= "31DEC&Annee_N."d;

quit;

proc sql;

	* On ins�re l effectif dans le Flowchart;
	INSERT INTO flowch.Flow_Chart_BPCO_VACG_&an_N.
		SELECT
			13,
			COUNT(DISTINCT BEN_IDT_ANO)
	FROM T_INDI_BPCO_VACGA_&an_N.
	WHERE BEN_IDT_ANO IN (SELECT BEN_IDT_ANO FROM etat_mal_asthmatique);

	* On conserve les id des patients � supprimer;
	CREATE TABLE travail.exclus9 AS
		SELECT DISTINCT
			BEN_IDT_ANO
	FROM T_INDI_BPCO_VACGA_&an_N.
	WHERE BEN_IDT_ANO IN (SELECT BEN_IDT_ANO FROM etat_mal_asthmatique);

quit;

proc delete data = etat_mal_asthmatique;
run; quit;
			
*	******************************************************************************************************************************************;
*	On exclut tous les patients;

data exclus;
	set	travail.exclus1-travail.exclus9;
run;

proc sql;

	* On ins�re l effectif dans le Flowchart;
	INSERT INTO flowch.Flow_Chart_BPCO_VACG_&an_N.
		SELECT
			14,
			COUNT(DISTINCT BEN_IDT_ANO)
	FROM exclus;

	DELETE FROM T_INDI_BPCO_VACGA_&an_N.
	WHERE BEN_IDT_ANO IN (SELECT BEN_IDT_ANO FROM exclus);

quit;

proc delete data = exclus;
run; quit;

proc datasets library = travail memtype = data nolist;
	delete exclus1-exclus9;
run; quit;

*	******************************************************************************************************************************************;
* 	Exclusion des patients diagnostiqu�s BPCO;

* Patients avec une ALD BPCO active au 1er septembre N;
proc sql;

	CREATE TABLE travail.exclus1 AS
		SELECT DISTINCT
			BEN_IDT_ANO
		FROM T_INDI_BPCO_VACGA_&an_N.
		WHERE BEN_IDT_ANO IN (SELECT BEN_IDT_ANO FROM res.T_INDI_BPCO_&an_N. WHERE ALD_BPCO_septembre = 1);

quit;

proc sql;

	* On ins�re l effectif dans le Flowchart;
	INSERT INTO flowch.Flow_Chart_BPCO_VACG_&an_N.
		SELECT
			15,
			COUNT(DISTINCT BEN_IDT_ANO)
	FROM travail.exclus1;

quit;

* Patients avec au moins une hospit BPCO;
proc sql;

	CREATE TABLE travail.exclus2 AS
		SELECT DISTINCT
			BEN_IDT_ANO
		FROM T_INDI_BPCO_VACGA_&an_N.
		WHERE BEN_IDT_ANO IN (SELECT BEN_IDT_ANO FROM res.T_INDI_BPCO_&an_N. WHERE Nb_hospit_BPCO_Ind02 > 0);

quit;

proc sql;

	* On ins�re l effectif dans le Flowchart;
	INSERT INTO flowch.Flow_Chart_BPCO_VACG_&an_N.
		SELECT
			16,
			COUNT(DISTINCT BEN_IDT_ANO)
	FROM travail.exclus2;

quit;
			
*	******************************************************************************************************************************************;
*	On exclut tous les patients;

data exclus;
	set	travail.exclus1-travail.exclus2;
run;

proc sql;

	* On ins�re l effectif dans le Flowchart;
	INSERT INTO flowch.Flow_Chart_BPCO_VACG_&an_N.
		SELECT
			17,
			COUNT(DISTINCT BEN_IDT_ANO)
	FROM exclus;

	DELETE FROM T_INDI_BPCO_VACGA_&an_N.
	WHERE BEN_IDT_ANO IN (SELECT BEN_IDT_ANO FROM exclus);

quit;

proc datasets library = travail memtype = data nolist;
	delete exclus1-exclus2;
run; quit;

proc delete data = exclus;
run; quit;

*	******************************************************************************************************************************************;
* 	Nombre de patients dans la population d'�tude : BPCO probable;

proc sql;

	INSERT INTO flowch.Flow_Chart_BPCO_VACG_&an_N.
		SELECT
			18,
			COUNT(DISTINCT BEN_IDT_ANO)
	FROM T_INDI_BPCO_VACGA_&an_N.;

quit;

proc sql;

	SELECT COUNT(*), COUNT(DISTINCT BEN_IDT_ANO)
	FROM T_INDI_BPCO_VACGA_&an_N.;

quit;
