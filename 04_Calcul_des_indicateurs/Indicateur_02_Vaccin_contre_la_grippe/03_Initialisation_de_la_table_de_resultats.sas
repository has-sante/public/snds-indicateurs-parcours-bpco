/*	******************************************************************************************************************************************	*/
/*																																				*/
/*		Indicateur 02 : Initialisation de la table de r�sultats contenant les BPCO probables et les patients diagnostiqu�s BPCO		 			*/
/*																																				*/
/*	******************************************************************************************************************************************	*/

proc sort data = T_INDI_BPCO_VACG_&an_N. out = res.T_INDI_BPCO_VACG_&an_N nodupkey;
	by BEN_IDT_ANO;
run;

*	******************************************************************************************************************************************;
* 	On flag les patients ayant eu au moins un diagnostic de BPCO cod� lors d�un s�jour hospitalier en MCO termin� entre le 1er janvier de
	l ann�e N et le 31 aout de l ann�e N+1 ou lors d�un s�jour hospitalier en SSR, HAD entre le 1er janvier de l ann�e N et le 31 d�cembre 
	de l ann�e N+1;

proc sql;

	CREATE TABLE diag_BPCO AS
		SELECT DISTINCT
			BEN_IDT_ANO,
			1 AS Diag_BPCO length = 3
		FROM res.T_INDI_BPCO_VACG_&an_N. a
		WHERE BEN_IDT_ANO IN (SELECT BEN_IDT_ANO FROM res.T_INDI_BPCO_&an_N. WHERE Nb_hospit_BPCO_Ind02 > 0);

quit;

data res.T_INDI_BPCO_VACG_&an_N.;
	merge	res.T_INDI_BPCO_VACG_&an_N. (in = a)
			diag_BPCO (in = b);
	by BEN_IDT_ANO;
	if a;
	if not b then
		Diag_BPCO = 0;
run;

*	******************************************************************************************************************************************;
* 	On flag les patients en ALD BPCO active le 1er septembre de l ann�e N;

proc sql;

	CREATE TABLE ALD_BCPO AS
		SELECT
			a.BEN_IDT_ANO,
			1 AS ALD_BPCO_avSep length = 3,
			MIN(date_debut) AS ALD_BPCO_Date format ddmmyy10. length = 4
		FROM res.T_INDI_BPCO_VACG_&an_N. a
			INNER JOIN travail.Histo_ALD b
				ON a.BEN_IDT_ANO = b.BEN_IDT_ANO
		WHERE b.reperage = 52 AND a.BEN_IDT_ANO IN (SELECT BEN_IDT_ANO FROM res.T_INDI_BPCO_&an_N. WHERE ALD_BPCO_septembre = 1)
		GROUP BY a.BEN_IDT_ANO
		ORDER BY a.BEN_IDT_ANO;

quit;

data res.T_INDI_BPCO_VACG_&an_N.;
	merge	res.T_INDI_BPCO_VACG_&an_N. (in = a)
			ALD_BCPO (in = b);
	by BEN_IDT_ANO;
	if a;
	if ALD_BPCO_avSep = . then
		ALD_BPCO_avSep = 0;
	if ALD_BPCO_avSep = 0 then
		ALD_BPCO_Date = .;
run;

proc delete data = ALD_BCPO;
run; quit;
