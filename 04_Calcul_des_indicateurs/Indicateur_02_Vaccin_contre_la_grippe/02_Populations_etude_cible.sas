/*	******************************************************************************************************************************************	*/
/*																																				*/
/*		Indicateur 02 : Populations d'�tude et population cible																		 			*/
/*																																				*/
/*	******************************************************************************************************************************************	*/

*	******************************************************************************************************************************************	;
*	POPULATION D �TUDE																															;
*	******************************************************************************************************************************************	;

data T_INDI_BPCO_VACG_&an_N.;
	set	T_INDI_BPCO_VACGA_&an_N. (in = a)
		T_INDI_BPCO_VACGB_&an_N. (in = b keep = BEN_IDT_ANO);
	length BPCO_VACG_PROB_CIBLE BPCO_VACG_DIAG_CIBLE BPCO_VACG_CIBLE 3.;
	BPCO_VACG_PROB_CIBLE = 0;
	BPCO_VACG_DIAG_CIBLE = 0;
	if a then
		BPCO_VACG_PROB_CIBLE = 1;
	if b then
		BPCO_VACG_DIAG_CIBLE = 1;
	BPCO_VACG_CIBLE = 1;
run;

* 	Nombre de patients dans la population d'�tude : BPCO probable ou diagnostiqu�e;

proc sql;

	INSERT INTO flowch.Flow_Chart_BPCO_VACG_&an_N.
		SELECT
			19,
			COUNT(DISTINCT BEN_IDT_ANO)
	FROM T_INDI_BPCO_VACG_&an_N.;

quit;

*	******************************************************************************************************************************************	;
*	PATIENTS POUR LESQUELS LA RECHERCHE DE VACCINATION CONTRE LA GRIPPE N EST PAS POSSIBLE														;
*	******************************************************************************************************************************************	;

*	******************************************************************************************************************************************;
* 	Exclusion des patients d�c�d�s avant la campagne;

proc sql;

	CREATE TABLE patients_decedes AS
		SELECT DISTINCT
			a.BEN_IDT_ANO
		FROM T_INDI_BPCO_VACG_&an_N. a
			INNER JOIN res.T_INDI_BPCO_&an_N. b
				ON a.BEN_IDT_ANO = b.BEN_IDT_ANO
		WHERE b.BEN_DCD_DTE < "01SEP&Annee_N."d AND b.BEN_DCD_DTE NE .;

quit;

proc sql;

	* On ins�re l effectif dans le Flowchart;
	INSERT INTO flowch.Flow_Chart_BPCO_VACG_&an_N.
		SELECT
			20,
			COUNT(DISTINCT BEN_IDT_ANO)
	FROM T_INDI_BPCO_VACG_&an_N.
	WHERE BEN_IDT_ANO IN (SELECT BEN_IDT_ANO FROM patients_decedes);

	* On conserve les id des patients � supprimer;
	CREATE TABLE travail.exclus1 AS
		SELECT DISTINCT
			BEN_IDT_ANO
	FROM T_INDI_BPCO_VACG_&an_N.
	WHERE BEN_IDT_ANO IN (SELECT BEN_IDT_ANO FROM patients_decedes);

quit;

proc delete data = patients_decedes;
run; quit;

*	******************************************************************************************************************************************;
* 	Exclusion des patients ayant eu un diagnostic de soins palliatif cod� lors d�un s�jour hospitalier MCO termin� entre le 1er septembre de l ann�e N
	et le 31 mai de l ann�e N+1, ou lors d�un s�jour hospitalier HAD, SSR entre le 1er septembre de l ann�e N et le 31 mai de l ann�e N+1;

* On exclut les s�jours avec un s�jour MCO termin� pendant la campagne;
* On exclut les s�jours avec un s�jour SSR ou HAD pendant la campagne;
proc sql undo_policy = none;

	CREATE TABLE soin_palliatif_MCO AS
		SELECT DISTINCT
			a.BEN_IDT_ANO
		FROM T_INDI_BPCO_VACG_&an_N. a
			INNER JOIN travail.soin_palliatif_&annee_2N._&annee_N1. b
				ON a.BEN_IDT_ANO = b.BEN_IDT_ANO
		WHERE "01SEP&Annee_N."d <= b.date_fin <= "31MAY&Annee_N1"d AND b.domaine = "MCO";

	CREATE TABLE soin_palliatif_SSR_HAD AS
		SELECT DISTINCT
			a.BEN_IDT_ANO
		FROM T_INDI_BPCO_VACG_&an_N. a
			INNER JOIN travail.soin_palliatif_&annee_2N._&annee_N1. b
				ON a.BEN_IDT_ANO = b.BEN_IDT_ANO
		WHERE b.domaine IN ("HAD", "SSR") AND b.date_debut <= "31MAY&Annee_N1"d AND b.date_fin >= "01SEP&Annee_N."d;

quit;

data soin_palliatif;
	set soin_palliatif_MCO
		soin_palliatif_SSR_HAD;
run;

proc sql;

	* On ins�re l effectif dans le Flowchart;
	INSERT INTO flowch.Flow_Chart_BPCO_VACG_&an_N.
		SELECT
			21,
			COUNT(DISTINCT BEN_IDT_ANO)
	FROM T_INDI_BPCO_VACG_&an_N.
	WHERE BEN_IDT_ANO IN (SELECT BEN_IDT_ANO FROM soin_palliatif);

	* On conserve les id des s�jours � supprimer;
	CREATE TABLE travail.exclus2 AS
		SELECT DISTINCT
			BEN_IDT_ANO
	FROM T_INDI_BPCO_VACG_&an_N.
	WHERE BEN_IDT_ANO IN (SELECT BEN_IDT_ANO FROM soin_palliatif);

quit;

proc datasets library = work memtype = data nolist;
	delete soin_palliatif_MCO soin_palliatif_SSR_HAD soin_palliatif ;
run; quit;

*	******************************************************************************************************************************************;
* 	Exclusion des patients hospitalis�s en MCO, SSR, psychiatrie plus de 3 mois entre le 1er septembre de l ann�e N et le 31 mai de l ann�e N+1;

data sejours_&Annee_N._&Annee_N1.;
	set travail.sejours_&Annee_N._&Annee_N1.;
run;

proc sql;

	DELETE FROM sejours_&Annee_N._&Annee_N1.
	WHERE SUBSTR(GRG_GHM, 1, 2) = "28";

quit;

proc sql;

	CREATE TABLE hospit1_3mois AS
		SELECT DISTINCT
			a.BEN_IDT_ANO
		FROM T_INDI_BPCO_VACG_&an_N. a
			INNER JOIN sejours_&Annee_N._&Annee_N1. b
				ON a.BEN_IDT_ANO = b.BEN_IDT_ANO
		WHERE b.domaine IN ("MCO", "SSR", "RIP") AND b.date_debut < "01SEP&Annee_N."d AND (b.date_fin - "01SEP&Annee_N."d) > 90;

	CREATE TABLE hospit2_3mois AS
		SELECT DISTINCT
			a.BEN_IDT_ANO
		FROM T_INDI_BPCO_VACG_&an_N. a
			INNER JOIN sejours_&Annee_N._&Annee_N1. b
				ON a.BEN_IDT_ANO = b.BEN_IDT_ANO
		WHERE b.domaine IN ("MCO", "SSR", "RIP") AND "01SEP&Annee_N."d <= b.date_debut <= "31MAY&Annee_N1"d AND 
			"01SEP&Annee_N."d <= b.date_fin <= "31MAY&Annee_N1"d AND (b.date_fin - b.date_debut) > 90;

	CREATE TABLE hospit3_3mois AS
		SELECT DISTINCT
			a.BEN_IDT_ANO
		FROM T_INDI_BPCO_VACG_&an_N. a
			INNER JOIN sejours_&Annee_N._&Annee_N1. b
				ON a.BEN_IDT_ANO = b.BEN_IDT_ANO
		WHERE b.domaine IN ("MCO", "SSR", "RIP") AND b.date_debut >= "01SEP&Annee_N."d AND b.date_fin > "31MAY&Annee_N1"d
			AND ("31MAY&Annee_N1"d - b.date_debut) > 90;

quit;

data hospit_3mois;
	set hospit1_3mois hospit2_3mois hospit3_3mois;
run;

proc sort data = hospit_3mois (keep = BEN_IDT_ANO) nodupkey;
	by BEN_IDT_ANO;
run;

proc sql;

	* On ins�re l effectif dans le Flowchart;
	INSERT INTO flowch.Flow_Chart_BPCO_VACG_&an_N.
		SELECT
			22,
			COUNT(DISTINCT BEN_IDT_ANO)
	FROM T_INDI_BPCO_VACG_&an_N.
	WHERE BEN_IDT_ANO IN (SELECT BEN_IDT_ANO FROM hospit_3mois);

	* On conserve les id des s�jours � supprimer;
	CREATE TABLE travail.exclus3 AS
		SELECT DISTINCT
			BEN_IDT_ANO
	FROM T_INDI_BPCO_VACG_&an_N.
	WHERE BEN_IDT_ANO IN (SELECT BEN_IDT_ANO FROM hospit_3mois);

quit;

proc datasets library = work memtype = data nolist;
	delete hospit_3mois hospit1_3mois hospit2_3mois hospit3_3mois sejours_&Annee_N._&Annee_N1.;
run; quit;

*	******************************************************************************************************************************************;
*	On exclut tous les patients;

data exclus;
	set	travail.exclus1-travail.exclus3;
run;

proc sql;

	* On ins�re l effectif dans le Flowchart;
	INSERT INTO flowch.Flow_Chart_BPCO_VACG_&an_N.
		SELECT
			23,
			COUNT(DISTINCT BEN_IDT_ANO)
	FROM exclus;

	DELETE FROM T_INDI_BPCO_VACG_&an_N.
	WHERE BEN_IDT_ANO IN (SELECT BEN_IDT_ANO FROM exclus);

quit;

proc delete data = exclus;
run; quit;

proc datasets library = travail memtype = data nolist;
	delete exclus1-exclus3;
run; quit;

*	******************************************************************************************************************************************;
* 	Population cible;

proc sql;

	INSERT INTO flowch.Flow_Chart_BPCO_VACG_&an_N.
		SELECT
			25,
			COUNT(DISTINCT BEN_IDT_ANO)
	FROM T_INDI_BPCO_VACG_&an_N.;

quit;

proc sql;

	SELECT COUNT(*), COUNT(DISTINCT BEN_IDT_ANO)
	FROM T_INDI_BPCO_VACG_&an_N.;

quit;
