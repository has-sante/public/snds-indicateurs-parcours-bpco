/*	******************************************************************************************************************************************	*/
/*																																				*/
/*		Table de population - Patients avec indicateur_02a = 1 ou indicateur_02b = 1													 		*/
/*																																				*/
/*	******************************************************************************************************************************************	*/

* Patients BPCO probable;
data T_INDI_BPCO_VACGA_&an_N. (keep = BEN_IDT_ANO);
	set pop.indicateurs_&an_N. (where = (indicateur_02a = 1));
run;

proc sort data = T_INDI_BPCO_VACGA_&an_N. nodupkey;
	by BEN_IDT_ANO;
run;

* Patients BPCO diagnostiqu�e;
data T_INDI_BPCO_VACGB_&an_N. (keep = BEN_IDT_ANO);
	set pop.indicateurs_&an_N. (where = (indicateur_02b = 1));
run;

proc sort data = T_INDI_BPCO_VACGB_&an_N. nodupkey;
	by BEN_IDT_ANO;
run;

*	******************************************************************************************************************************************;
*	Effectifs pour le flowchart;

*	Individus de 40 ans et plus avec une ALD BPCO active au 1er septembre de l ann�e N ou individus de 40 ans et plus hospitalis�s en MCO, SSR, 
	HAD avec un diagnostic BPCO cod�;
	
*	Individus de 40 ans et plus ayant b�n�fici� de soins rembours�s au moins une fois l ann�e N ayant eu un traitement sp�cifique de la BPCO (3 BLDLA);	

proc sql;

	CREATE TABLE flowch.Flow_Chart_BPCO_VACG_&an_N.  AS
		SELECT *
		FROM (
			SELECT 1 AS indicateur length = 3, COUNT(DISTINCT BEN_IDT_ANO) AS N length = 5 FROM T_INDI_BPCO_VACGB_&an_N. WHERE BEN_IDT_ANO IN (SELECT BEN_IDT_ANO 
				FROM res.T_INDI_BPCO_&an_N. WHERE age >= 40 AND ALD_BPCO_septembre = 1)
			UNION ALL
			SELECT 2, COUNT(DISTINCT BEN_IDT_ANO) FROM T_INDI_BPCO_VACGB_&an_N. WHERE BEN_IDT_ANO IN (SELECT BEN_IDT_ANO 
				FROM res.T_INDI_BPCO_&an_N. WHERE age >= 40 AND Nb_hospit_BPCO_Ind02 > 0)
			UNION ALL
			SELECT 3, COUNT(DISTINCT BEN_IDT_ANO) FROM T_INDI_BPCO_VACGB_&an_N.
			UNION ALL
			SELECT 4, COUNT(DISTINCT BEN_IDT_ANO) FROM T_INDI_BPCO_VACGA_&an_N.
		);

quit;
