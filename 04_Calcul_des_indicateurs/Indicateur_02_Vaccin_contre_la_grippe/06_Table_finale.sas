/*	******************************************************************************************************************************************	*/
/*																																				*/
/*		Indicateur 02 : Table finale - Ajout de formats et de labels																			*/
/*																																				*/
/*	******************************************************************************************************************************************	*/

data res.T_INDI_BPCO_VACG_&an_N.;
	set res.T_INDI_BPCO_VACG_&an_N. (keep = BEN_IDT_ANO BPCO_VACG_PROB_CIBLE Diag_BPCO ALD_BPCO_avSep ALD_BPCO_Date BPCO_VACG_DIAG_CIBLE 
		BPCO_VACG_CIBLE Nb_Sej_Diag_BPCO_MCO Nb_Sej_Diag_BPCO_SSR Nb_Sej_Diag_BPCO_HAD BPCO_VACG_PROB_OBS BPCO_VACG_DIAG_OBS BPCO_VACG_OBS 
		Date_VACG);
	label		
		BEN_IDT_ANO = "Num�ro d'individu"
		BPCO_VACG_PROB_CIBLE = "Patient correspondant aux crit�res d'inclusion et d'exclusion de la population cible probable - BPCO probable"
		Diag_BPCO = "Patient ayant eu au moins un diagnostic de BPCO cod� lors d'un s�jour hospitalier en MCO termin� entre le 1er janvier &annee_1N. et le 31 ao�t &Annee_N. ou lors d'un s�jour hospitalier en SSR, HAD entre le 1er janvier &annee_1N. et le 31 ao�t &Annee_N."
		ALD_BPCO_avSep = "Patient en ALD BPCO active le 1er septembre &Annee_N."
		ALD_BPCO_Date = "Date de d�but de mise en ALD BPCO"
		BPCO_VACG_DIAG_CIBLE = "Patient correspondant aux crit�res d'inclusion et d'exclusion de la population cible diagnostiqu�e - BPCO diagnostiqu�s"
		BPCO_VACG_CIBLE = "Patient correspondant aux crit�res d'inclusion et d'exclusion de la population cible"
		Nb_Sej_Diag_BPCO_MCO = "Nombre de s�jours avec un diagnostic BPCO termin�s entre le 1er janvier &annee_1N. et le 31 ao�t &Annee_N. en MCO"
		Nb_Sej_Diag_BPCO_SSR = "Nombre de s�jours avec un diagnostic BPCO entre le 1er janvier &annee_1N. et le 31 ao�t &Annee_N. en SSR"
		Nb_Sej_Diag_BPCO_HAD = "Nombre de s�jours avec un diagnostic BPCO entre le 1er janvier &annee_1N. et le 31 ao�t &Annee_N. en HAD"
		BPCO_VACG_PROB_OBS = "Patient de la population cible probable ayant eu une d�livrance rembours�e du vaccin contre la grippe entre le 1er septembre &Annee_N. et le 31 mai &Annee_N1."
		BPCO_VACG_DIAG_OBS = "Patient de la population cible diagnostiqu�e ayant eu une d�livrance rembours�e du vaccin contre la grippe entre le 1er septembre &Annee_N. et le 31 mai &Annee_N1."
		BPCO_VACG_OBS = "Patient de la population cible ayant eu une d�livrance rembours�e du vaccin contre la grippe entre le 1er septembre &Annee_N. et le 31 mai &Annee_N1."
		Date_VACG = "Date de la d�livrance rembours�e du vaccin contre la grippe entre le 1er septembre &Annee_N. et le 31 mai &Annee_N1."
		;
	format BPCO_VACG_PROB_CIBLE Diag_BPCO ALD_BPCO_avSep BPCO_VACG_DIAG_CIBLE BPCO_VACG_PROB_OBS BPCO_VACG_DIAG_OBS BPCO_VACG_CIBLE BPCO_VACG_OBS
		f_oui_non. ALD_BPCO_Date Date_VACG date9.;
run;
			
*	******************************************************************************************************************************************;
* 	Vaccination contre la grippe;

proc sql;

	INSERT INTO flowch.Flow_Chart_BPCO_VACG_&an_N.
		SELECT
			26,
			COUNT(DISTINCT BEN_IDT_ANO)
	FROM res.T_INDI_BPCO_VACG_&an_N.
	WHERE BPCO_VACG_OBS = 1;

	* Nombre de patients pour les num�rateur des premi�res exclusions;
	SELECT Nb_patients_ref0 into : N0 FROM travail.ref_flowcharts; 
	SELECT N into : N1 FROM flowch.Flow_Chart_BPCO_VACG_&an_N. WHERE indicateur = 1;
	SELECT N into : N4 FROM flowch.Flow_Chart_BPCO_VACG_&an_N. WHERE indicateur = 4;

	* Population d �tude;
	SELECT N into : N19 FROM flowch.Flow_Chart_BPCO_VACG_&an_N. WHERE indicateur = 19;

	* Population cible;
	SELECT N into : N25 FROM flowch.Flow_Chart_BPCO_VACG_&an_N. WHERE indicateur = 25;

quit;

data flowch.Flow_Chart_BPCO_VACG_&an_N.;
	set flowch.Flow_Chart_BPCO_VACG_&an_N.;
	if indicateur in (1, 2, 3, 4, 18, 19) then
		percent = N / &N0.;
	if indicateur in (5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17) then
		percent = N / &N4.;
	if indicateur in (20, 21, 22, 23, 25) then
		percent = N / &N19.;
	if indicateur = 26 then
		percent = N / &N25.;
	format indicateur f_ind_02_flowchart. percent percent7.1;		
run;

* V�rif;
proc sql;

	SELECT COUNT(*) AS nb_lignes, COUNT(DISTINCT BEN_IDT_ANO) AS nb_patients
	FROM res.T_INDI_BPCO_VACG_&an_N.;
	* nb_lignes = nb_patients : OK;

	SELECT COUNT(*) AS nb_lignes, COUNT(DISTINCT BEN_IDT_ANO) AS nb_patients
	FROM res.T_INDI_BPCO_VACG_&an_N.
	WHERE BPCO_VACG_OBS = 1;
	* nb_lignes = nb_patients : OK;

quit;

*	******************************************************************************************************************************************;
*	On ajoute les flag BPCO_VACG_PROB_CIBLE, BPCO_VACG_DIAG_CIBLE et BPCO_VACG_CIBLE dans la table res.T_INDI_BPCO_&an_N.;

proc sql;

	ALTER TABLE res.T_INDI_BPCO_&an_N. DROP BPCO_VACG_PROB_CIBLE, BPCO_VACG_DIAG_CIBLE, BPCO_VACG_CIBLE;

	ALTER TABLE res.T_INDI_BPCO_&an_N. ADD BPCO_VACG_PROB_CIBLE INT format = f_oui_non. length = 3
		label = "Patient appartenant � la population cible BPCO_VACG_PROB";

	ALTER TABLE res.T_INDI_BPCO_&an_N. ADD BPCO_VACG_DIAG_CIBLE INT format = f_oui_non. length = 3
		label = "Patient appartenant � la population cible BPCO_VACG_DIAG";

	ALTER TABLE res.T_INDI_BPCO_&an_N. ADD BPCO_VACG_CIBLE INT format = f_oui_non. length = 3
		label = "Patient appartenant � la population cible BPCO_VACG";

	UPDATE res.T_INDI_BPCO_&an_N.
		SET BPCO_VACG_PROB_CIBLE = CASE	WHEN BEN_IDT_ANO IN (SELECT DISTINCT BEN_IDT_ANO FROM res.T_INDI_BPCO_VACG_&an_N. WHERE 
											BPCO_VACG_PROB_CIBLE = 1) THEN 1
										ELSE 0
										END,
			BPCO_VACG_DIAG_CIBLE = CASE	WHEN BEN_IDT_ANO IN (SELECT DISTINCT BEN_IDT_ANO FROM res.T_INDI_BPCO_VACG_&an_N. WHERE 
											BPCO_VACG_DIAG_CIBLE = 1) THEN 1
										ELSE 0
										END,
			BPCO_VACG_CIBLE = CASE		WHEN BEN_IDT_ANO IN (SELECT DISTINCT BEN_IDT_ANO FROM res.T_INDI_BPCO_VACG_&an_N. WHERE 
											BPCO_VACG_CIBLE = 1) THEN 1
										ELSE 0
										END;

quit;

data res.T_INDI_BPCO_&an_N.;
	set res.T_INDI_BPCO_&an_N.;
	if BPCO_VACG_PROB_CIBLE = .
		then BPCO_VACG_PROB_CIBLE = 0;
	if BPCO_VACG_DIAG_CIBLE = .
		then BPCO_VACG_DIAG_CIBLE = 0;
	if BPCO_VACG_CIBLE = .
		then BPCO_VACG_CIBLE = 0;
run;
