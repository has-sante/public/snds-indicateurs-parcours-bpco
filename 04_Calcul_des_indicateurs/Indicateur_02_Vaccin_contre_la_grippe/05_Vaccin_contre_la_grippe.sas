/*	******************************************************************************************************************************************	*/
/*																																				*/
/*		Indicateur 02 : Vaccin contre la grippe entre le 1er septembre et le 31 mai													 			*/
/*																																				*/
/*	******************************************************************************************************************************************	*/

*	******************************************************************************************************************************************;
*	On joint avec la table res.T_INDI_BPCO_VACG_&an_N. pour vaccination des patients cibles;

proc sql;

	CREATE TABLE vaccin_grippe_campagne AS
		SELECT DISTINCT
			a.BEN_IDT_ANO,
			MAX(CASE	WHEN a.BPCO_VACG_PROB_CIBLE = 1 THEN 1
						ELSE .
						END) AS BPCO_VACG_PROB_OBS length = 3,
			MAX(CASE	WHEN a.BPCO_VACG_DIAG_CIBLE = 1 THEN 1
						ELSE .
						END) AS BPCO_VACG_DIAG_OBS length = 3,
			MIN(b.date_debut) AS Date_VACG length = 4
		FROM res.T_INDI_BPCO_VACG_&an_N. a
			INNER JOIN travail.reperage_vacc_grippe_&Annee_N._&Annee_N1. b
				ON a.BEN_IDT_ANO = b.BEN_IDT_ANO
		WHERE "01SEP&Annee_N."d <= b.date_debut <= "31MAY&Annee_N1."d
		GROUP BY a.BEN_IDT_ANO;

quit;

*	On ajoute l information dans la table de résultats;
proc sort data = res.T_INDI_BPCO_VACG_&an_N.;
	by BEN_IDT_ANO;
run;

proc sort data = vaccin_grippe_campagne;
	by BEN_IDT_ANO;
run;

data res.T_INDI_BPCO_VACG_&an_N.;
	merge	res.T_INDI_BPCO_VACG_&an_N. (in = a)
			vaccin_grippe_campagne (in = b);
	by BEN_IDT_ANO;
	if BPCO_VACG_PROB_CIBLE = 1 AND BPCO_VACG_PROB_OBS ne 1 then
		BPCO_VACG_PROB_OBS = 0;
	if BPCO_VACG_DIAG_CIBLE = 1 AND BPCO_VACG_DIAG_OBS ne 1 then
		BPCO_VACG_DIAG_OBS = 0;
	if BPCO_VACG_PROB_OBS = 1 or BPCO_VACG_DIAG_OBS = 1 then
		BPCO_VACG_OBS = 1;
	array v BPCO_VACG_PROB_CIBLE BPCO_VACG_PROB_OBS BPCO_VACG_DIAG_CIBLE BPCO_VACG_DIAG_OBS BPCO_VACG_OBS;
	do over v;
		if v = . then
			v = 0;
	end;
	if a then
		output;
run;

proc delete data = vaccin_grippe_campagne;
run; quit;
