/*	******************************************************************************************************************************************	*/
/*																																				*/
/*		Indicateur 06 : Traitement apr�s hospitalisation pour exacerbation																		 */
/*																																				*/
/*	******************************************************************************************************************************************	*/

*	******************************************************************************************************************************************;
*	On corrige les donn�es de la table travail.reperage_BDLA_&Annee_2N._&Annee_N1. qui sont incoh�rentes avec la variable nb_broncho_LDA de;
*	la table res.T_INDI_BPCO_&an_N.;
*	En effet, certaines d�livrances de m�dicaments BDLA de 2017 ne remontent que dans les flux de 2019. Ils seront donc pris en compte dans le;
*	rep�rage des d�livrances de BDLA alors qu'ils n �taient pas pris en compte dans le d�nombrement nb_broncho_LDA de la table res.T_INDI_BPCO_&an_N.;
*	On supprime les d�livrances de 2017 pour ces patients;

proc sql;

	CREATE TABLE patients_sans_BDLA AS
		SELECT
			BEN_IDT_ANO
		FROM res.T_INDI_BPCO_&an_N.
		WHERE Nb_Broncho_LDA = 0;

quit;

data reperage_BDLA_&Annee_2N._&Annee_N1.;
	set travail.reperage_BDLA_&Annee_2N._&Annee_N1.;
run;

proc sql;

	DELETE FROM reperage_BDLA_&Annee_2N._&Annee_N1.
		WHERE BEN_IDT_ANO IN (SELECT BEN_IDT_ANO FROM patients_sans_BDLA) AND YEAR(date_debut) = &Annee_N.;

quit;

proc delete data = patients_sans_BDLA;
run; quit;

*	******************************************************************************************************************************************;
*	On joint avec la table res.T_INDI_BPCO_BDLA_EA_&an_N. pour r�cup�rer les info dans les 90 jours suivant la date index;

proc sql;

	CREATE TABLE Broncho_LDA_90j AS
		SELECT DISTINCT
			a.BPCO_BDLA_EA_Sej_Index,
			1 AS BPCO_BDLA_EA_OBS length = 3,
			COUNT(DISTINCT b.date_debut) AS Nb_Broncho_LDA_90j length = 4
		FROM res.T_INDI_BPCO_BDLA_EA_&an_N. a
			INNER JOIN reperage_BDLA_&Annee_2N._&Annee_N1. b
				ON a.BEN_IDT_ANO = b.BEN_IDT_ANO
		WHERE a.BPCO_BDLA_EA_Date_index <= b.date_debut <= (a.BPCO_BDLA_EA_Date_index + 90)
		GROUP BY BPCO_BDLA_EA_Sej_Index;

quit;

*	On ajoute l information dans la table de r�sultats;
proc sort data = res.T_INDI_BPCO_BDLA_EA_&an_N.;
	by BPCO_BDLA_EA_Sej_Index;
run;

proc sort data = Broncho_LDA_90j;
	by BPCO_BDLA_EA_Sej_Index;
run;

data res.T_INDI_BPCO_BDLA_EA_&an_N.;
	merge	res.T_INDI_BPCO_BDLA_EA_&an_N. (in = a)
			Broncho_LDA_90j (in = b);
	by BPCO_BDLA_EA_Sej_Index;
	if BPCO_BDLA_EA_OBS ne 1 then
		do;
			BPCO_BDLA_EA_OBS = 0;
			Nb_Broncho_LDA_90j = 0;
		end;
	if a then
		output;
run;

proc delete data = Broncho_LDA_90j;
run; quit;

*	******************************************************************************************************************************************;
*	On joint avec la table res.T_INDI_BPCO_BDLA_EA_&an_N. pour r�cup�rer les info dans les 180 jours suivant la date index;
proc sql;

	CREATE TABLE Broncho_LDA_180j AS
		SELECT DISTINCT
			a.BPCO_BDLA_EA_Sej_Index,
			MIN(b.date_debut) AS Date_Broncho_LDA_180j length = 4
		FROM res.T_INDI_BPCO_BDLA_EA_&an_N. a
			INNER JOIN reperage_BDLA_&Annee_2N._&Annee_N1. b
				ON a.BEN_IDT_ANO = b.BEN_IDT_ANO
		WHERE a.BPCO_BDLA_EA_Date_index <= b.date_debut <= (a.BPCO_BDLA_EA_Date_index + 180)
		GROUP BY BPCO_BDLA_EA_Sej_Index;

quit;

*	On ajoute l information dans la table de r�sultats;
proc sort data = res.T_INDI_BPCO_BDLA_EA_&an_N.;
	by BPCO_BDLA_EA_Sej_Index;
run;

proc sort data = Broncho_LDA_180j;
	by BPCO_BDLA_EA_Sej_Index;
run;

data res.T_INDI_BPCO_BDLA_EA_&an_N.;
	merge	res.T_INDI_BPCO_BDLA_EA_&an_N. (in = a)
			Broncho_LDA_180j (in = b);
	by BPCO_BDLA_EA_Sej_Index;
	if a then
		output;
run;

proc delete data = Broncho_LDA_180j;
run; quit;

*	******************************************************************************************************************************************;
*	On joint avec la table res.T_INDI_BPCO_BDLA_EA_&an_N. pour r�cup�rer les info dans les 90 jours avant la date index;
proc sql;

	CREATE TABLE Broncho_LDA_90javant AS
		SELECT DISTINCT
			a.BPCO_BDLA_EA_Sej_Index,
			1 AS Broncho_LDA_90javant length = 3
		FROM res.T_INDI_BPCO_BDLA_EA_&an_N. a
			INNER JOIN reperage_BDLA_&Annee_2N._&Annee_N1. b
				ON a.BEN_IDT_ANO = b.BEN_IDT_ANO
		WHERE (a.BPCO_BDLA_EA_Date_index - 90) <= b.date_debut < a.BPCO_BDLA_EA_Date_index
		ORDER BY BPCO_BDLA_EA_Sej_Index;

quit;

proc delete data = reperage_BDLA_&Annee_2N._&Annee_N1.;
run; quit;

*	On ajoute l information dans la table de r�sultats;
proc sort data = res.T_INDI_BPCO_BDLA_EA_&an_N.;
	by BPCO_BDLA_EA_Sej_Index;
run;

proc sort data = Broncho_LDA_90javant;
	by BPCO_BDLA_EA_Sej_Index;
run;

data res.T_INDI_BPCO_BDLA_EA_&an_N.;
	merge	res.T_INDI_BPCO_BDLA_EA_&an_N. (in = a)
			Broncho_LDA_90javant (in = b);
	by BPCO_BDLA_EA_Sej_Index;
	if Broncho_LDA_90javant ne 1 then
		Broncho_LDA_90javant = 0;
	if a then
		output;
run;

proc delete data = Broncho_LDA_90javant;
run; quit;
