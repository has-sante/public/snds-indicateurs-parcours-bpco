/*	******************************************************************************************************************************************	*/
/*																																				*/
/*		Indicateur 06 : Table finale - Ajout de formats et de labels																			*/
/*																																				*/
/*	******************************************************************************************************************************************	*/

data res.T_INDI_BPCO_BDLA_EA_&an_N.;
	set res.T_INDI_BPCO_BDLA_EA_&an_N. (keep = BEN_IDT_ANO BPCO_BDLA_EA_Date_index BPCO_BDLA_EA_Sej_Index Finess_PMSI Finess_GEO Dp Dp_classe
		DA_BPCO GHM BPCO_BDLA_EA_CIBLE BPCO_BDLA_EA_OBS Nb_Sej_EA_BPCO_DP_MCO Nb_Sej_EA_BPCO_DAS_MCO Nb_Broncho_LDA_90j Date_Broncho_LDA_180j 
		Broncho_LDA_90javant);
	label		
		BEN_IDT_ANO = "Num�ro d'individu"
		BPCO_BDLA_EA_Date_index = "Date index"
		BPCO_BDLA_EA_Sej_Index = "Identifiant du s�jour index"
		Finess_PMSI = "Finess PMSI ayant r�alis� le s�jour index"
		Finess_GEO = "Finess g�ographique du premier RUM"
		Dp = "Diagnostic principal du s�jour index"
		Dp_classe = "Diagnostic principal du s�jour index"
		DA_BPCO = "BPCO cod� en diagnostic secondaire du s�jour index"
		GHM = "GHM du s�jour index"
		BPCO_BDLA_EA_CIBLE = "S�jour correspondant aux crit�res d'inclusion et d'exclusion de la population cible"
		BPCO_BDLA_EA_OBS = "S�jour de la population cible pour lesquels le patient a eu une d�livrance rembours�e de traitement bronchodilatateur b�ta-2 longue dur�e d'action dans les 90 jours suivant la date index"
		Nb_Sej_EA_BPCO_DP_MCO = "Nombre de s�jours MCO d'exacerbation de BPCO cod� en DP termin�s en &Annee_N."
		Nb_Sej_EA_BPCO_DAS_MCO = "Nombre de s�jours MCO d'exacerbation de BPCO cod� en DAS termin�s en &Annee_N."
		Nb_Broncho_LDA_90j = "Nombre de d�livrances rembours�es de bronchodilatateur anticholinergique ou B�ta-2 longue dur�e d'action dans les 90 jours suivant la date index"
		Date_Broncho_LDA_180j = "Date de la premi�re d�livrance rembours�e de bronchodilatateur anticholinergique ou B�ta-2 longue dur�e d'action dans les 180 jours suivant la date index"
		Broncho_LDA_90javant = "Patient ayant eu au moins une d�livrance rembours�e d'un bronchodilatateur anticholinergique ou B�ta-2 longue dur�e d'action dans les 90 jours pr�c�dant la date index"
		;
	format DA_BPCO BPCO_BDLA_EA_CIBLE BPCO_BDLA_EA_OBS Broncho_LDA_90javant f_oui_non. Dp_Classe f_DP_classe.
		BPCO_BDLA_EA_Date_index Date_Broncho_LDA_180j date9.;
run;
			
*	******************************************************************************************************************************************;
* 	Nombre de traitement de BDLA dans les 3 mois apr�s hospitalisation;

proc sql;

	INSERT INTO flowch.Flow_Chart_BPCO_BDLA_EA_&an_N.
		SELECT
			18,
			COUNT(DISTINCT BPCO_BDLA_EA_Sej_Index)
	FROM res.T_INDI_BPCO_BDLA_EA_&an_N.
	WHERE BPCO_BDLA_EA_OBS = 1;

	* Population d �tude;
	SELECT N into : N12 FROM flowch.Flow_Chart_BPCO_BDLA_EA_&an_N. WHERE indicateur = 12;
	* Population cible;
	SELECT N into : N17 FROM flowch.Flow_Chart_BPCO_BDLA_EA_&an_N. WHERE indicateur = 17;

quit;

data flowch.Flow_Chart_BPCO_BDLA_EA_&an_N.;
	set flowch.Flow_Chart_BPCO_BDLA_EA_&an_N.;
	if indicateur in (13, 14, 15, 17) then
		percent = N / &N12.;
	if indicateur = 18 then
		percent = N / &N17.;
	format indicateur f_ind_06_flowchart. percent percent7.1;		
run;

* V�rif;
proc sql;

	SELECT COUNT(*) AS nb_lignes, COUNT(DISTINCT BPCO_BDLA_EA_Sej_Index) AS nb_sejours
	FROM res.T_INDI_BPCO_BDLA_EA_&an_N.;
	* nb_lignes = nb_sejours : OK;

	SELECT COUNT(*) AS nb_lignes, COUNT(DISTINCT BPCO_BDLA_EA_Sej_Index) AS nb_sejours
	FROM res.T_INDI_BPCO_BDLA_EA_&an_N.
	WHERE BPCO_BDLA_EA_OBS = 1;
	* nb_lignes = nb_sejours : OK;

quit;

*	******************************************************************************************************************************************;
*	On ajoute le flag BPCO_BDLA_EA_CIBLE dans la table res.T_INDI_BPCO_&an_N.;

proc sql;

	ALTER TABLE res.T_INDI_BPCO_&an_N. DROP BPCO_BDLA_EA_CIBLE;

	ALTER TABLE res.T_INDI_BPCO_&an_N. ADD BPCO_BDLA_EA_CIBLE INT format = f_oui_non. length = 3
		label = "Patient appartenant � la population cible BPCO_BDLA_EA";

	UPDATE res.T_INDI_BPCO_&an_N.
		SET BPCO_BDLA_EA_CIBLE = CASE	WHEN BEN_IDT_ANO IN (SELECT DISTINCT BEN_IDT_ANO FROM res.T_INDI_BPCO_BDLA_EA_&an_N.) THEN 1
										ELSE 0
										END;

quit;

data res.T_INDI_BPCO_&an_N.;
	set res.T_INDI_BPCO_&an_N.;
	if BPCO_BDLA_EA_CIBLE = .
		then BPCO_BDLA_EA_CIBLE = 0;
run;
