/*	******************************************************************************************************************************************	*/
/*																																				*/
/*		Insuffisance coronarienne et/ou aortique																							 	*/
/*																																				*/
/*	******************************************************************************************************************************************	*/

*	******************************************************************************************************************************************	;
* 	Via les GHM;

* On supprime la table si elle existe;
%suppr_table(
	lib = orauser, 
	table = code_GHM
	);

data orauser.code_GHM;
	set orauser.codes_GHM_BPCO (where = (reperage in (28, 29, 30)));
run;

* On appelle la macro pour le repérage dans le PMSI;
%extract_GHM_PMSI(
	annee_deb = &annee_N.,
	annee_fin = &annee_N1., 
	tbl_codes = code_GHM, 
	tbl_out = GHM_&annee_N._&annee_N1.,
	tbl_patients = corresp_id_patient
	);

proc sql;

	DELETE FROM GHM_&annee_N._&annee_N1.
	WHERE reperage IN (28, 29) AND (SUBSTR(GRG_GHM, 1, 2) = "28" OR SEJ_NBJ = 0);

quit;

%suppr_table(
	lib = orauser, 
	table = code_GHM
	);

*	******************************************************************************************************************************************	;
* 	Via les codes CCAM;

* On supprime la table si elle existe;
%suppr_table(
	lib = orauser, 
	table = code_CCAM
	);

data orauser.code_CCAM;
	set orauser.codes_CCAM_BPCO (where = (reperage in (28, 29)));
run;

* On appelle la macro pour le repérage dans le PMSI;
%extract_CCAM_PMSI(
	annee_deb = &Annee_N., 
	annee_fin = &Annee_N1., 
	HAD = 0, MCO = 1, RIP = 0, SSR = 0, 
	tbl_out = CCAM_&annee_N._&annee_N1., 
	tbl_codes = code_CCAM, 
	tbl_patients = corresp_id_patient
	);

proc sql;

	DELETE FROM CCAM_&annee_N._&annee_N1.
	WHERE type = "PMSI ACE" 
		OR SUBSTR(GRG_GHM, 1, 2) = "28"
		OR SEJ_NBJ = 0;

quit;

* On supprime la table si elle existe;
%suppr_table(
	lib = orauser, 
	table = code_CCAM
	);

data orauser.code_CCAM;
	set orauser.codes_CCAM_BPCO (where = (reperage = 30));
run;

* On appelle la macro pour le repérage dans le PMSI;
%extract_CCAM_PMSI(
	annee_deb = &Annee_N., 
	annee_fin = &Annee_N1., 
	HAD = 0, MCO = 1, RIP = 0, SSR = 0, 
	tbl_out = CCAM2_&annee_N._&annee_N1., 
	tbl_codes = code_CCAM, 
	tbl_patients = corresp_id_patient
	);

proc sql;

	DELETE FROM CCAM2_&annee_N._&annee_N1.
	WHERE type = "PMSI ACE";

quit;

*	******************************************************************************************************************************************	;
* 	On regroupe les deux tables;

data travail.insuf_coro_aortique_&annee_N._&annee_N1.;
	set	GHM_&annee_N._&annee_N1.
		CCAM_&annee_N._&annee_N1.
		CCAM2_&annee_N._&annee_N1.;
	id_sejour = ETA_NUM||"_"||RSA_NUM||"_"||put(Annee, 4.);
run;

proc delete data = GHM_&annee_N._&annee_N1. CCAM_&annee_N._&annee_N1. CCAM2_&annee_N._&annee_N1.;
run; quit;

*	******************************************************************************************************************************************;
*	Vérifications;

* On ne passe pas par la macro car trop de variables;
proc sql;

	CREATE TABLE verif._0&tmp_num_tab._insuf_coro AS
		SELECT 
			Annee, 
			Domaine, 
			table, 
			SUBSTR(GRG_GHM, 1, 3) AS GRG_GHM3,
			type_UM, 
			type_UM2,
			reperage, 
			code_CCAM,
			COUNT(*) AS Frequency
		FROM travail.insuf_coro_aortique_&annee_N._&annee_N1.
		GROUP BY 1, 2, 3, 4, 5, 6, 7, 8;

quit;

proc sql noprint; SELECT SUM(Frequency) INTO: nb_tot FROM verif._0&tmp_num_tab._insuf_coro; quit;

data verif._0&tmp_num_tab._insuf_coro;
	set verif._0&tmp_num_tab._insuf_coro;
	length percent 4.;
	format percent nlpct7.1 Frequency commafr_0ch.;
	percent = Frequency/&nb_tot.;
run;

%let tmp_num_tab = %sysevalf(&tmp_num_tab. + 1);
