/*	******************************************************************************************************************************************	*/
/*																																				*/
/*		Pneumothorax ou infarctus																								 				*/
/*																																				*/
/*	******************************************************************************************************************************************	*/

* On supprime la table si elle existe;
%suppr_table(
	lib = orauser, 
	table = code_CCAM
	);

data orauser.code_CCAM;
	set orauser.codes_CCAM_BPCO (where = (reperage = 11));
run;

* On appelle la macro pour le repérage dans le PMSI;
%extract_CCAM_PMSI(
	annee_deb = &Annee_1N.,
	annee_fin = &annee_N1., 
	HAD = 0, 
	MCO = 1, 
	RIP = 0, 
	SSR = 1, 
	tbl_out = CCAM_pneumo_&Annee_1N._&annee_N1., 
	tbl_codes = code_CCAM, 
	tbl_patients = corresp_id_patient
	);

data CCAM_pneumo_&Annee_1N._&annee_N1.;
	set CCAM_pneumo_&Annee_1N._&annee_N1.;
	if table = "FMSTC" and date_fin = . then
		date_fin = date_debut;
	else if domaine ne "MCO" and date_fin = . then
		date_fin = mdy (12, 31, annee);
run;

* On supprime la table si elle existe;
%suppr_table(
	lib = orauser, 
	table = code_CIM
	);

data orauser.code_CIM;
	set orauser.diag_codes_CIM_BPCO (where = (reperage = 11));
run;

* On appelle la macro pour le repérage dans le PMSI;
%extract_CIM10_PMSI(
	annee_deb = &Annee_1N.,
	annee_fin = &annee_N1., 
	HAD_DP = 0,
	HAD_DAS = 0, 
	HAD_MPP = 0, 
	HAD_MPA = 0, 
	MCO_DP = 1, 
	MCO_DR = 1, 
	MCO_DAS = 1, 
	MCO_DP_UM = 1, 
	MCO_DR_UM = 1,
	SSR_FP = 0, 
	SSR_MPP = 0, 
	SSR_AE = 0, 
	SSR_DAS = 0, 
	tbl_out = pneumothorax_&Annee_1N._&annee_N1., 
	tbl_codes = code_CIM,
	tbl_patients = corresp_id_patient
	);

* On supprime la table si elle existe;
%suppr_table(
	lib = orauser, 
	table = code_CIM
	);

data orauser.code_CIM;
	set orauser.diag_codes_CIM_BPCO (where = (reperage = 12));
run;

* On appelle la macro pour le repérage dans le PMSI;
%extract_CIM10_PMSI(
	annee_deb = &Annee_1N.,
	annee_fin = &annee_N1., 
	HAD_DP = 0,
	HAD_DAS = 0, 
	HAD_MPP = 0, 
	HAD_MPA = 0, 
	MCO_DP = 1, 
	MCO_DR = 0, 
	MCO_DAS = 0, 
	MCO_DP_UM = 0, 
	MCO_DR_UM = 0,
	SSR_FP = 0, 
	SSR_MPP = 0, 
	SSR_AE = 0, 
	SSR_DAS = 0, 
	tbl_out = infarctus_&Annee_1N._&annee_N1., 
	tbl_codes = code_CIM,
	tbl_patients = corresp_id_patient
	);

data travail.pneumothorax_infarctus_&Annee_1N._&annee_N1.;
	set CCAM_pneumo_&Annee_1N._&annee_N1. pneumothorax_&Annee_1N._&annee_N1. infarctus_&Annee_1N._&annee_N1.;
	if domaine = "MCO" and SEQ_NUM = "" then
		id_sejour = ETA_NUM||"_"||RSA_NUM||"_"||put(Annee, 4.);
	else if domaine = "SSR" and SEQ_NUM = ""  then
		id_sejour = ETA_NUM||"_"||RHA_NUM||"_"||put(Annee, 4.);
	if SEQ_NUM ne "" then
		id_ACE = ETA_NUM||"_"||SEQ_NUM||"_"||put(Annee, 4.);
run;

proc delete data = orauser.code_CIM orauser.code_CCAM;
run; quit;

*	******************************************************************************************************************************************;
*	Vérifications;

* On ne passe pas par la macro car trop de variables;
proc sql;

	CREATE TABLE verif._0&tmp_num_tab._pneumo_inf AS
		SELECT 
			annee,
			table,
			reperage,
			domaine,
			variable,
			code_CIM,
			code_CCAM,
			COUNT(*) AS Frequency
		FROM travail.pneumothorax_infarctus_&Annee_1N._&annee_N1.
		GROUP BY 1, 2, 3, 4, 5, 6, 7;

quit;

proc sql noprint; SELECT SUM(Frequency) INTO: nb_tot FROM verif._0&tmp_num_tab._pneumo_inf; quit;

data verif._0&tmp_num_tab._pneumo_inf;
	set verif._0&tmp_num_tab._pneumo_inf;
	length percent 4.;
	format percent nlpct7.1 Frequency commafr_0ch.;
	percent = Frequency/&nb_tot.;
run;

%let tmp_num_tab = %sysevalf(&tmp_num_tab. + 1);
