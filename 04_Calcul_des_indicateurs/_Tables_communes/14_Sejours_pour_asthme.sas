/*	******************************************************************************************************************************************	*/
/*																																				*/
/*		S�jours avec un code diagnostic pour asthme et �tat de mal asthmatique													 				*/
/*																																				*/
/*	******************************************************************************************************************************************	*/

%suppr_table(
	lib = orauser, 
	table = codes_CIM
	);

data orauser.codes_CIM;
	set orauser.diag_codes_CIM_BPCO (where = (reperage in (14)));
run;

* On appelle la macro pour le rep�rage dans le PMSI - Asthme;
%extract_CIM10_PMSI(
	annee_deb = &annee_1N.,
	annee_fin = &annee_N1.,  
	HAD_DP = 1,
	HAD_DAS = 1, 
	HAD_MPP = 1, 
	HAD_MPA = 1, 
	MCO_DP = 1, 
	MCO_DR = 1, 
	MCO_DAS = 1, 
	MCO_DP_UM = 1, 
	MCO_DR_UM = 1,
	SSR_FP = 1, 
	SSR_MPP = 1, 
	SSR_AE = 1, 
	SSR_DAS = 1, 
	tbl_out = hospit_asthme1, 
	tbl_codes = codes_CIM,
	tbl_patients = corresp_id_patient
	);

proc delete data = orauser.codes_CIM;
run; quit;

%suppr_table(
	lib = orauser, 
	table = codes_CIM
	);

data orauser.codes_CIM;
	set orauser.diag_codes_CIM_BPCO (where = (reperage in (15)));
run;

* On appelle la macro pour le rep�rage dans le PMSI - �tat de mal asthmatique;
%extract_CIM10_PMSI(
	annee_deb = &annee_1N.,
	annee_fin = &annee_N1.,  
	HAD_DP = 0,
	HAD_DAS = 0, 
	HAD_MPP = 0, 
	HAD_MPA = 0, 
	MCO_DP = 1, 
	MCO_DR = 0, 
	MCO_DAS = 0, 
	MCO_DP_UM = 1, 
	MCO_DR_UM = 0,
	SSR_FP = 0, 
	SSR_MPP = 0, 
	SSR_AE = 0, 
	SSR_DAS = 0, 
	tbl_out = hospit_asthme2, 
	tbl_codes = codes_CIM,
	tbl_patients = corresp_id_patient
	);

proc sql;

	DELETE FROM hospit_asthme2
	WHERE SUBSTR(GRG_GHM, 1, 2) = "28" OR SEJ_NBJ = 0;

quit;

proc delete data = orauser.codes_CIM;
run; quit;

* On concat�ne les deux tables;
data travail.diag_asthme_&annee_1N._&annee_N1.;
	set hospit_asthme1 hospit_asthme2;
	if domaine in ("HAD", "RIP", "SSR") then
		do;
			if date_fin = . then
				date_fin = mdy(12, 31, annee);
		end;
run;

proc delete data = hospit_asthme1 hospit_asthme2;
run; quit;

*	******************************************************************************************************************************************;
*	V�rifications;

data verif;
	set travail.diag_asthme_&annee_1N._&annee_N1. (where = (reperage = 14));
run;

%proc_freq(
	in_tbl = verif, 
	out_tbl = _sejours_asthme14, 
	list_var_in = annee*domaine*table*variable,
	list_var_out = annee domaine table2 variable Frequency
	);

proc delete data = verif;
run; quit;

data verif;
	set travail.diag_asthme_&annee_1N._&annee_N1. (where = (reperage = 15));
run;

%proc_freq(
	in_tbl = verif, 
	out_tbl = _sejours_asthme15, 
	list_var_in = annee*domaine*table*variable,
	list_var_out = annee domaine table2 variable Frequency
	);

proc delete data = verif;
run; quit;
