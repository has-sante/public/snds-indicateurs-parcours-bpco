/*	******************************************************************************************************************************************	*/
/*																																				*/
/*		Délivrance remboursée de BDLA																							 				*/
/*																																				*/
/*	******************************************************************************************************************************************	*/

%suppr_table(
	lib = orauser, 
	table = codes_ATC
	);

data orauser.codes_ATC;
	set orauser.codes_ATC_BPCO (where = (reperage in (46, 45, 47, 48, 49)));
run;

%suppr_table(
	lib = travail, 
	table = reperage_BDLA_&Annee_2N._&Annee_N1.
	);

* Dans le DCIR - Codes CIP;
%extract_CIP(
	annee_deb = &Annee_2N., 
	annee_fin = &Annee_N1., 
	tbl_out = travail.reperage_BDLA_&Annee_2N._&Annee_N1., 
	tbl_codes = codes_ATC, 
	tbl_patients = corresp_id_patient
	);

proc delete data = orauser.codes_ATC;
run; quit;

*	******************************************************************************************************************************************;
*	Vérifications;
		
data verif;
	set travail.reperage_BDLA_&Annee_2N._&Annee_N1.;
	annee_soin = year(date_debut);
run;
		
%proc_freq(
	in_tbl = verif, 
	out_tbl = _BDLA, 
	list_var_in = annee_soin*PHA_ATC_C07*PHA_CND_TOP, 
	list_var_out = annee_soin PHA_ATC_C07 PHA_CND_TOP Frequency
	);

proc delete data = verif;
run; quit;
