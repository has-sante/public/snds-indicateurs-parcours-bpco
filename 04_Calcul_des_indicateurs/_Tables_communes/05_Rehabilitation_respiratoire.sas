/*	******************************************************************************************************************************************	*/
/*																																				*/
/*		R�habilitation respiratoire																												*/
/*																																				*/
/*	******************************************************************************************************************************************	*/

*	******************************************************************************************************************************************	;
*	1.	R�adaptation respiratoire r�alis�e lors d�un s�jour en SSR : les informations sont recherch�es dans le PMSI SSR							;
*			�	qui a commenc� au plus tard dans les 3 mois (date index incluse) suivant la  date de fin du s�jour (date index) pour 			;
*				exacerbation en MCO																												;
*			�	quel que soit la dur�e du s�jour en SSR																				 			;
*			�	et quel que soit le type d�hospitalisation (HC, HDJ, s�ance) en SSR																;
*	******************************************************************************************************************************************	;

%extract_sej_PMSI(
	annee_deb = &Annee_1N., 
	annee_fin = &Annee_N1., 
	HAD = 0, 
	MCO = 0, 
	SSR = 1, 
	RIP = 0, 
	tbl_out = travail.sejours_SSR_&Annee_1N._&Annee_N1., 
	tbl_patients = corresp_id_patient
	);

data travail.sejours_SSR_&Annee_1N._&Annee_N1.;
	set travail.sejours_SSR_&Annee_1N._&Annee_N1.;	
	if date_fin = . then
		date_fin = mdy(12, 31, annee);
run;

proc sort data = travail.sejours_SSR_&Annee_1N._&Annee_N1.;
	by id_sejour;
run;

* ********** Codes GME ********** ;

%suppr_table(
	lib = orauser, 
	table = codes_GME
	);

data orauser.codes_GME;
	set orauser.codes_GME_BPCO (where = (reperage = 7));
run;

%extract_GME_PMSI(
	annee_deb = &Annee_1N., 
	annee_fin = &Annee_N1., 
	tbl_out = reperage_CM04_&Annee_1N._&Annee_N1., 
	tbl_codes = codes_GME, 
	tbl_patients = corresp_id_patient
	);

data reperage_CM04_&Annee_1N._&Annee_N1.;
	set reperage_CM04_&Annee_1N._&Annee_N1.;
	id_sejour = ETA_NUM||"_"||RHA_NUM||"_"||put(annee, 4.);
run;

proc sql undo_policy = none;

	CREATE TABLE reperage_CM04_&Annee_1N._&Annee_N1. AS
		SELECT
			id_sejour,
			MAX(CASE	WHEN CODE_GME = "04" THEN 1
						ELSE .
						END) AS CM_04 length = 3
		FROM reperage_CM04_&Annee_1N._&Annee_N1.
		GROUP BY id_sejour
		ORDER BY id_sejour;

quit;

* ********** Codes CCAM ********** ;
%suppr_table(
	lib = orauser, 
	table = codes_CCAM
	);

data orauser.codes_CCAM;
	set orauser.codes_CCAM_BPCO (where = (reperage = 7));
run;

%extract_CCAM_PMSI(
	annee_deb = &Annee_1N., 
	annee_fin = &Annee_N1., 
	HAD = 0, MCO = 0, RIP = 0, SSR = 1,
	tbl_out = reperage_CCAM, 
	tbl_codes = codes_CCAM, 
	tbl_patients = corresp_id_patient
	);

proc delete data = orauser.codes_CCAM;
run; quit;

* ********** Codes CSARR ********** ;
%suppr_table(
	lib = orauser, 
	table = codes_CSARR
	);

data orauser.codes_CSARR;
	set orauser.codes_CSARR_BPCO (where = (reperage = 7));
run;

%extract_CSARR_PMSI(
	annee_deb = &Annee_1N., 
	annee_fin = &Annee_N1., 
	tbl_out = reperage_CSARR, 
	tbl_codes = codes_CSARR, 
	tbl_patients = corresp_id_patient
	);

proc delete data = orauser.codes_CSARR;
run; quit;

* On rep�re pour info les s�jours actes CCAM ou CSARR de la liste, peu importe la CM et le type UM;
data reperage_CCAM_CSARR;
	length type_UM $2.;
	set reperage_CSARR reperage_CCAM;
	where type = "PMSI s�jours";
	id_sejour = ETA_NUM||"_"||RHA_NUM||"_"||put(annee, 4.);
run;

* RR r�alis�e en cours d hospitalisation : on flag si pr�sence d au moins un acte CCAM ou au moins un acte CSARR;

proc sql undo_policy = none;

	CREATE TABLE reperage_CCAM_CSARR AS
		SELECT
			id_sejour,
			MAX(CASE	WHEN code_ccam NE "" THEN 1
						ELSE .
						END) AS CCAM length = 3,
			MAX(CASE	WHEN code_csarr NE "" THEN 1
						ELSE .
						END) AS CSARR length = 3
		FROM reperage_CCAM_CSARR
		GROUP BY id_sejour
		ORDER BY id_sejour;

quit;

data travail.RR_sejours_SSR_&Annee_1N._&Annee_N1.;
	merge	travail.sejours_SSR_&Annee_1N._&Annee_N1. (in = a)
			reperage_CM04_&Annee_1N._&Annee_N1.
			reperage_CCAM_CSARR;
	by id_sejour;
	if a;
run;

proc delete data = reperage_CM04_&Annee_1N._&Annee_N1. reperage_CCAM_CSARR reperage_CSARR reperage_CCAM;
run; quit;


*	******************************************************************************************************************************************;
*	V�rifications;

* On ne passe pas par la macro car trop de variables;
proc sql;

	CREATE TABLE verif._0&tmp_num_tab._CCAM_CSARR AS
		SELECT 
			Annee, 
			Type,
			Domaine, 
			type_um, 
			Ccam, 
			Csarr,
			CM_04,
			COUNT(*) AS Frequency
		FROM travail.RR_sejours_SSR_&Annee_1N._&Annee_N1.
		GROUP BY Annee, Type, Domaine, type_um, ccam, csarr, CM_04;

quit;

proc sql noprint; SELECT SUM(Frequency) INTO: nb_tot FROM verif._0&tmp_num_tab._CCAM_CSARR; quit;

data verif._0&tmp_num_tab._CCAM_CSARR;
	set verif._0&tmp_num_tab._CCAM_CSARR;
	length percent 4.;
	format percent nlpct7.1 Frequency commafr_0ch.;
	percent = Frequency/&nb_tot.;
run;

%let tmp_num_tab = %sysevalf(&tmp_num_tab. + 1);


*	******************************************************************************************************************************************	;
*	2.	R�adaptation respiratoire r�alis�e en dehors d�une hospitalisation : les informations sont recherch�es dans le DCIR et dans le PMSI		;
*		MCO et SSR au niveau des actes et consultations externes																				;
*	******************************************************************************************************************************************	;

* Dans le PMSI - ACE;
%suppr_table(
	lib = orauser, 
	table = codes_actes
	);

data orauser.codes_actes;
	set orauser.codes_actes_BPCO (where = (reperage = 7));
run;

%extract_ACT_COD_PMSI(
	annee_deb = &Annee_1N., 
	annee_fin = &Annee_N1., 
	MCO = 1, SSR = 1,
	tbl_out = travail.reperage_actes, 
	tbl_codes = codes_actes, 
	tbl_patients = corresp_id_patient
	);

data travail.reperage_actes;
	set travail.reperage_actes (where = (type = "PMSI ACE"));
run;

*	******************************************************************************************************************************************;
*	V�rifications;

%proc_freq(
	in_tbl = travail.reperage_actes, 
	out_tbl = _reperage_actes, 
	list_var_in = Annee*Type*Domaine*table*ACT_COD,
	list_var_out = Annee Type Domaine table2 ACT_COD Frequency
	);

proc delete data = orauser.codes_actes;
run; quit;

* Dans le DCIR;
%suppr_table(
	lib = orauser, 
	table = codes_presta_RR
	);

data orauser.codes_presta_RR;
	set orauser.codes_presta_BPCO (where = (reperage = 7));
run;

%extract_PRS_NAT_REF(
	annee_deb = &Annee_1N., 
	annee_fin = &Annee_N1., 
	tbl_out = travail.reperage_presta, 
	tbl_codes = codes_presta_RR, 
	tbl_patients = corresp_id_patient
	);

*	******************************************************************************************************************************************;
*	V�rifications;

data verif;
	set travail.reperage_presta;
	annee = year(date_debut);
run;

%proc_freq(
	in_tbl = verif, 
	out_tbl = _reperage_presta, 
	list_var_in = Annee*BSE_PRS_NAT*ETE_MCO_DDP,
	list_var_out = Annee BSE_PRS_NAT ETE_MCO_DDP Frequency
	);

proc delete data = orauser.codes_presta_RR verif;
run; quit;

* On concat�ne les 2 sources;
data travail.reperage_actes;
	set	travail.reperage_actes
		travail.reperage_presta;
run;

proc delete data = travail.reperage_presta;
run; quit;

*	************************************ POPULATION 1 ************************************	;
*	Tous patients et actes r�alis�s avant le 1er juillet 2018								;
*	**************************************************************************************	;


* AMS, AMC ou AMK 9,5 + AMS, AMC ou AMK 8 d�lai maximum entre les 2 actes <= 30 jours avant ou apr�s le code 9,5 mais le remboursement
ne doit pas �tre ant�rieur � la date index et les actes doivent �tre r�alis� tous les 2 soit en ville, soit en MCO, soit en SSR;

* On r�cup�re les AMS (BSE_PRS_NAT = 3125), AMC (BSE_PRS_NAT = 3121) ou AMK (BSE_PRS_NAT = 3122) avec un coefficient 9.5;
data pop1_reperage1_95;
	set travail.reperage_actes (where = (date_debut < "01JUL2018"d and ((type = "DCIR" and BSE_PRS_NAT in (3125, 3121, 3122) and 
		PRS_ACT_CFT = 9.5) or (type = "PMSI ACE" and ACT_COD in ("AMS", "AMC", "AMK") and ACT_COE = 9.5))));
	rename
		PRS_ACT_CFT = PRS_ACT_CFT_95
		ACT_COE = ACT_COE_95;
run;

* On r�cup�re les AMS (BSE_PRS_NAT = 3125), AMC (BSE_PRS_NAT = 3121) ou AMK (BSE_PRS_NAT = 3122) avec un coefficient 8;
data pop1_reperage1_8;
	set travail.reperage_actes (where = (date_debut < "01JUL2018"d and ((type = "DCIR" and BSE_PRS_NAT in (3125, 3121, 3122) and 
		PRS_ACT_CFT = 8) or (type = "PMSI ACE" and ACT_COD in ("AMS", "AMC", "AMK") and ACT_COE = 8))));
	rename
		PRS_ACT_CFT = PRS_ACT_CFT_8
		ACT_COE = ACT_COE_8;
run;

* On r�cup�re la jointure des 2 : 30 jours max entre les 2 codes;
proc sql;

	CREATE TABLE travail.pop1_reperage1 AS
		SELECT
			a.*,
			b.PRS_ACT_CFT_8,
			b.ACT_COE_8,
			b.date_debut AS date_debut2
		FROM pop1_reperage1_95 a
			INNER JOIN pop1_reperage1_8 b
				ON a.BEN_IDT_ANO = b.BEN_IDT_ANO
				AND a.type = b.type
				AND a.domaine = b.domaine
		WHERE -30 <= (a.date_debut - b.date_debut) <= 30;

quit;

data travail.pop1_reperage1;
	set travail.pop1_reperage1;
	date_RR = MIN(date_debut, date_debut2);
run;

*	******************************************************************************************************************************************;
*	V�rifications;

%proc_freq(
	in_tbl = travail.pop1_reperage1, 
	out_tbl = _RR_pop1_reperage1, 
	list_var_in = PRS_ACT_CFT_8*ACT_COE_8*PRS_ACT_CFT_95*ACT_COE_95,
	list_var_out = PRS_ACT_CFT_8 ACT_COE_8 PRS_ACT_CFT_95 ACT_COE_95 Frequency
	);

proc delete data = pop1_reperage1_95 pop1_reperage1_8;
run; quit;


* AMS, AMC ou AMK 9,5 + AMS, AMC ou AMK 8/2 et les actes doivent �tre r�alis� tous les 2 soit en ville, soit en MCO, soit en SSR et les 2 actes r�alis�s le m�me jour;

* On r�cup�re les AMS (BSE_PRS_NAT = 3125), AMC (BSE_PRS_NAT = 3121) ou AMK (BSE_PRS_NAT = 3122) avec un coefficient 9.5;
data pop1_reperage2_95;
	set travail.reperage_actes (where = (date_debut < "01JUL2018"d and ((type = "DCIR" and BSE_PRS_NAT in (3125, 3121, 3122) and 
		PRS_ACT_CFT = 9.5) or (type = "PMSI ACE" and ACT_COD in ("AMS", "AMC", "AMK") and ACT_COE = 9.5))));
	rename
		PRS_ACT_CFT = PRS_ACT_CFT_95
		ACT_COE = ACT_COE_95;
run;

* On r�cup�re les AMS (BSE_PRS_NAT = 3125), AMC (BSE_PRS_NAT = 3121) ou AMK (BSE_PRS_NAT = 3122) avec un coefficient 4 (8/2);
data pop1_reperage2_8;
	set travail.reperage_actes (where = (date_debut < "01JUL2018"d and ((type = "DCIR" and BSE_PRS_NAT in (3125, 3121, 3122) and 
		PRS_ACT_CFT = 4) or (type = "PMSI ACE" and ACT_COD in ("AMS", "AMC", "AMK") and ACT_COE = 4))));
	rename
		PRS_ACT_CFT = PRS_ACT_CFT_4
		ACT_COE = ACT_COE_4;
run;

* On r�cup�re la jointure des 2 : 30 jours max entre les 2 codes;
proc sql;

	CREATE TABLE travail.pop1_reperage2 AS
		SELECT
			a.*,
			b.PRS_ACT_CFT_4,
			b.ACT_COE_4,
			b.date_debut AS date_debut2
		FROM pop1_reperage2_95 a
			INNER JOIN pop1_reperage2_8 b
				ON a.BEN_IDT_ANO = b.BEN_IDT_ANO
				AND a.type = b.type
				AND a.domaine = b.domaine
				AND a.date_debut = b.date_debut;

quit;

data travail.pop1_reperage2;
	set travail.pop1_reperage2;
	date_RR = MIN(date_debut, date_debut2);
run;

*	******************************************************************************************************************************************;
*	V�rifications;

%proc_freq(
	in_tbl = travail.pop1_reperage2, 
	out_tbl = _RR_pop1_reperage2, 
	list_var_in = PRS_ACT_CFT_4*ACT_COE_4*PRS_ACT_CFT_95*ACT_COE_95,
	list_var_out = PRS_ACT_CFT_4 ACT_COE_4 PRS_ACT_CFT_95 ACT_COE_95 Frequency
	);

proc delete data = pop1_reperage2_95 pop1_reperage2_8;
run; quit;


* AMS, AMC ou AMK 13.5;

data travail.pop1_reperage3;
	set travail.reperage_actes (where = (date_debut < "01JUL2018"d and ((type = "DCIR" and BSE_PRS_NAT in (3125, 3121, 3122) and 
		PRS_ACT_CFT = 13.5) or (type = "PMSI ACE" and ACT_COD in ("AMS", "AMC", "AMK") and ACT_COE = 13.5))));
	length date_RR 4.;
	date_RR = date_debut;
	rename
		PRS_ACT_CFT = PRS_ACT_CFT_135
		ACT_COE = ACT_COE_135;
run;


* AMS, AMC ou AMK 8;

data travail.pop1_reperage4;
	set travail.reperage_actes (where = (date_debut < "01JUL2018"d and ((type = "DCIR" and BSE_PRS_NAT in (3125, 3121, 3122) and 
		PRS_ACT_CFT = 8) or (type = "PMSI ACE" and ACT_COD in ("AMS", "AMC", "AMK") and ACT_COE = 8))));
	length date_RR 4.;
	date_RR = date_debut;
	rename
		PRS_ACT_CFT = PRS_ACT_CFT_8
		ACT_COE = ACT_COE_8;
run;


* BPC (BSE_PRS_NAT = 9570);

data travail.pop1_reperage5;
	set travail.reperage_actes (where = (date_debut < "01JUL2018"d and ((type = "DCIR" and BSE_PRS_NAT = 9570) or (type = "PMSI ACE" 
		and ACT_COD in ("BPC")))));
	length date_RR 4.;
	date_RR = date_debut;
run;

data travail.population1;
	set travail.pop1_reperage1 travail.pop1_reperage2 (in = a) travail.pop1_reperage3 travail.pop1_reperage4 travail.pop1_reperage5 (in = b);
	length flag 3.;
	if a then 
		flag = 1;
	if b then 
		flag = 2;
run; 

proc delete data = travail.pop1_reperage1 travail.pop1_reperage2 travail.pop1_reperage3 travail.pop1_reperage4 travail.pop1_reperage5;
run; quit;


*	************************************ POPULATION 2 ************************************	;
*	Patients sans ALD (quelque soit l�ALD) et actes r�alis�s apr�s le 1er juillet 2018		;
*	**************************************************************************************	;


* AMS, AMC ou AMK 9,5 + AMS, AMC ou AMK 8 d�lai maximum entre les 2 actes <= 30 jours avant ou apr�s le code 9,5 mais le remboursement
ne doit pas �tre ant�rieur � la date index et les actes doivent �tre r�alis� tous les 2 soit en ville, soit en MCO, soit en SSR;

* On r�cup�re les AMS (BSE_PRS_NAT = 3125), AMC (BSE_PRS_NAT = 3121) ou AMK (BSE_PRS_NAT = 3122) avec un coefficient 9.5;
data pop2_reperage1_95;
	set travail.reperage_actes (where = (date_debut >= "01JUL2018"d and ((type = "DCIR" and BSE_PRS_NAT in (3125, 3121, 3122) and 
		PRS_ACT_CFT = 9.5) or (type = "PMSI ACE" and ACT_COD in ("AMS", "AMC", "AMK") and ACT_COE = 9.5))));
	rename
		PRS_ACT_CFT = PRS_ACT_CFT_95
		ACT_COE = ACT_COE_95;
run;

* On r�cup�re les AMS (BSE_PRS_NAT = 3125), AMC (BSE_PRS_NAT = 3121) ou AMK (BSE_PRS_NAT = 3122) avec un coefficient 8;
data pop2_reperage1_8;
	set travail.reperage_actes (where = (date_debut >= "01JUL2018"d and ((type = "DCIR" and BSE_PRS_NAT in (3125, 3121, 3122) and 
		PRS_ACT_CFT = 8) or (type = "PMSI ACE" and ACT_COD in ("AMS", "AMC", "AMK") and ACT_COE = 8))));
	rename
		PRS_ACT_CFT = PRS_ACT_CFT_8
		ACT_COE = ACT_COE_8;
run;

* On r�cup�re la jointure des 2 : 30 jours max entre les 2 codes;
proc sql;

	CREATE TABLE travail.pop2_reperage1 AS
		SELECT
			a.*,
			b.PRS_ACT_CFT_8,
			b.ACT_COE_8,
			b.date_debut AS date_debut2
		FROM pop2_reperage1_95 a
			INNER JOIN pop2_reperage1_8 b
				ON a.BEN_IDT_ANO = b.BEN_IDT_ANO
				AND a.type = b.type
				AND a.domaine = b.domaine
		WHERE -30 <= (a.date_debut - b.date_debut) <= 30;

quit;

data travail.pop2_reperage1;
	set travail.pop2_reperage1;
	length date_RR 4.;
	date_RR = MIN(date_debut, date_debut2);
run;

*	******************************************************************************************************************************************;
*	V�rifications;

%proc_freq(
	in_tbl = travail.pop2_reperage1, 
	out_tbl = _RR_pop2_reperage1, 
	list_var_in = PRS_ACT_CFT_8*ACT_COE_8*PRS_ACT_CFT_95*ACT_COE_95,
	list_var_out = PRS_ACT_CFT_8 ACT_COE_8 PRS_ACT_CFT_95 ACT_COE_95 Frequency
	);

proc delete data = pop2_reperage1_95 pop2_reperage1_8;
run; quit;


* AMS, AMC ou AMK 9,5 + AMS, AMC ou AMK 8/2 et les actes doivent �tre r�alis� tous les 2 soit en ville, soit en MCO, soit en SSR et les 2 actes r�alis�s le m�me jour;

* On r�cup�re les AMS (BSE_PRS_NAT = 3125), AMC (BSE_PRS_NAT = 3121) ou AMK (BSE_PRS_NAT = 3122) avec un coefficient 9.5;
data pop2_reperage2_95;
	set travail.reperage_actes (where = (date_debut >= "01JUL2018"d and ((type = "DCIR" and BSE_PRS_NAT in (3125, 3121, 3122) and 
		PRS_ACT_CFT = 9.5) or (type = "PMSI ACE" and ACT_COD in ("AMS", "AMC", "AMK") and ACT_COE = 9.5))));
	rename
		PRS_ACT_CFT = PRS_ACT_CFT_95
		ACT_COE = ACT_COE_95;
run;

* On r�cup�re les AMS (BSE_PRS_NAT = 3125), AMC (BSE_PRS_NAT = 3121) ou AMK (BSE_PRS_NAT = 3122) avec un coefficient 4 (8/2);
data pop2_reperage2_8;
	set travail.reperage_actes (where = (date_debut >= "01JUL2018"d and ((type = "DCIR" and BSE_PRS_NAT in (3125, 3121, 3122) and 
		PRS_ACT_CFT = 4) or (type = "PMSI ACE" and ACT_COD in ("AMS", "AMC", "AMK") and ACT_COE = 4))));
	rename
		PRS_ACT_CFT = PRS_ACT_CFT_4
		ACT_COE = ACT_COE_4;
run;

* On r�cup�re la jointure des 2 : 30 jours max entre les 2 codes;
proc sql;

	CREATE TABLE travail.pop2_reperage2 AS
		SELECT
			a.*,
			b.PRS_ACT_CFT_4,
			b.ACT_COE_4,
			b.date_debut AS date_debut2
		FROM pop2_reperage2_95 a
			INNER JOIN pop2_reperage2_8 b
				ON a.BEN_IDT_ANO = b.BEN_IDT_ANO
				AND a.type = b.type
				AND a.domaine = b.domaine
				AND a.date_debut = b.date_debut;

quit;

data travail.pop2_reperage2;
	set travail.pop2_reperage2;
	length date_RR 4.;
	date_RR = MIN(date_debut, date_debut2);
run;

*	******************************************************************************************************************************************;
*	V�rifications;

%proc_freq(
	in_tbl = travail.pop2_reperage2, 
	out_tbl = _RR_pop2_reperage2, 
	list_var_in = PRS_ACT_CFT_4*ACT_COE_4*PRS_ACT_CFT_95*ACT_COE_95,
	list_var_out = PRS_ACT_CFT_4 ACT_COE_4 PRS_ACT_CFT_95 ACT_COE_95 Frequency
	);

proc delete data = pop2_reperage2_95 pop2_reperage2_8;
run; quit;


* AMS, AMC ou AMK 13.5;

data travail.pop2_reperage3;
	set travail.reperage_actes (where = (date_debut >= "01JUL2018"d and ((type = "DCIR" and BSE_PRS_NAT in (3125, 3121, 3122) and 
		PRS_ACT_CFT = 13.5) or (type = "PMSI ACE" and ACT_COD in ("AMS", "AMC", "AMK") and ACT_COE = 13.5))));
	length date_RR 4.;
	date_RR = date_debut;
	rename
		PRS_ACT_CFT = PRS_ACT_CFT_135
		ACT_COE = ACT_COE_135;
run;


* AMS, AMC ou AMK 8;

data travail.pop2_reperage4;
	set travail.reperage_actes (where = (date_debut >= "01JUL2018"d and ((type = "DCIR" and BSE_PRS_NAT in (3125, 3121, 3122) and 
		PRS_ACT_CFT = 8) or (type = "PMSI ACE" and ACT_COD in ("AMS", "AMC", "AMK") and ACT_COE = 8))));
	length date_RR 4.;
	date_RR = date_debut;
	rename
		PRS_ACT_CFT = PRS_ACT_CFT_8
		ACT_COE = ACT_COE_8;
run;


* BPC (BSE_PRS_NAT = 9570);

data travail.pop2_reperage5;
	set travail.reperage_actes (where = (date_debut >= "01JUL2018"d and ((type = "DCIR" and BSE_PRS_NAT = 9570) or (type = "PMSI ACE" 
		and ACT_COD in ("BPC")))));
	length date_RR 4.;
	date_RR = date_debut;
run;

data travail.population2;
	set travail.pop2_reperage1 travail.pop2_reperage2 (in = a) travail.pop2_reperage3 travail.pop2_reperage4 travail.pop2_reperage5 (in = b);
	length flag 3.;
	if a then 
		flag = 1;
	if b then 
		flag = 2;
run; 

* On supprime les patients avec une ALD;
proc sql;

	DELETE FROM travail.population2
	WHERE BEN_IDT_ANO IN (SELECT BEN_IDT_ANO FROM res.T_INDI_BPCO_&an_N. WHERE ALD = 1);

quit;

proc delete data = travail.pop2_reperage1 travail.pop2_reperage2 travail.pop2_reperage3 travail.pop2_reperage4 travail.pop2_reperage5;
run; quit;

*	************************************ POPULATION 3 ************************************	;
*	Patients avec ALD (quelque soit l�ALD) et actes r�alis�s apr�s le 1er juillet 2018		;
*	**************************************************************************************	;


* AMS, AMC ou AMK 9,5 + AMS, AMC ou AMK 8 d�lai maximum entre les 2 actes <= 30 jours avant ou apr�s le code 9,5 mais le remboursement
ne doit pas �tre ant�rieur � la date index et les actes doivent �tre r�alis� tous les 2 soit en ville, soit en MCO, soit en SSR;

* On r�cup�re les AMS (BSE_PRS_NAT = 3125), AMC (BSE_PRS_NAT = 3121) ou AMK (BSE_PRS_NAT = 3122) avec un coefficient 9.5;
data pop3_reperage1_95;
	set travail.reperage_actes (where = (date_debut >= "01JUL2018"d and ((type = "DCIR" and BSE_PRS_NAT in (3125, 3121, 3122) and 
		PRS_ACT_CFT = 9.5) or (type = "PMSI ACE" and ACT_COD in ("AMS", "AMC", "AMK") and ACT_COE = 9.5))));
	rename
		PRS_ACT_CFT = PRS_ACT_CFT_95
		ACT_COE = ACT_COE_95;
run;

* On r�cup�re les AMS (BSE_PRS_NAT = 3125), AMC (BSE_PRS_NAT = 3121) ou AMK (BSE_PRS_NAT = 3122) avec un coefficient 8;
data pop3_reperage1_8;
	set travail.reperage_actes (where = (date_debut >= "01JUL2018"d and ((type = "DCIR" and BSE_PRS_NAT in (3125, 3121, 3122) and 
		PRS_ACT_CFT = 8) or (type = "PMSI ACE" and ACT_COD in ("AMS", "AMC", "AMK") and ACT_COE = 8))));
	rename
		PRS_ACT_CFT = PRS_ACT_CFT_8
		ACT_COE = ACT_COE_8;
run;

* On r�cup�re la jointure des 2 : 30 jours max entre les 2 codes;
proc sql;

	CREATE TABLE travail.pop3_reperage1 AS
		SELECT
			a.*,
			b.PRS_ACT_CFT_8,
			b.ACT_COE_8,
			b.date_debut AS date_debut2
		FROM pop3_reperage1_95 a
			INNER JOIN pop3_reperage1_8 b
				ON a.BEN_IDT_ANO = b.BEN_IDT_ANO
				AND a.type = b.type
				AND a.domaine = b.domaine
		WHERE -30 <= (a.date_debut - b.date_debut) <= 30;

quit;

data travail.pop3_reperage1;
	set travail.pop3_reperage1;
	length date_RR 4.;
	date_RR = MIN(date_debut, date_debut2);
run;

proc delete data = pop3_reperage1_95 pop3_reperage1_8;
run; quit;

*	******************************************************************************************************************************************;
*	V�rifications;

%proc_freq(
	in_tbl = travail.pop3_reperage1, 
	out_tbl = _RR_pop3_reperage1, 
	list_var_in = PRS_ACT_CFT_8*ACT_COE_8*PRS_ACT_CFT_95*ACT_COE_95,
	list_var_out = PRS_ACT_CFT_8 ACT_COE_8 PRS_ACT_CFT_95 ACT_COE_95 Frequency
	);


* AMS, AMC ou AMK 9,5 + AMS, AMC ou AMK 8/2 et les actes doivent �tre r�alis� tous les 2 soit en ville, soit en MCO, soit en SSR et les 2 actes r�alis�s le m�me jour ;

* On r�cup�re les AMS (BSE_PRS_NAT = 3125), AMC (BSE_PRS_NAT = 3121) ou AMK (BSE_PRS_NAT = 3122) avec un coefficient 9.5;
data pop3_reperage2_95;
	set travail.reperage_actes (where = (date_debut >= "01JUL2018"d and ((type = "DCIR" and BSE_PRS_NAT in (3125, 3121, 3122) and 
		PRS_ACT_CFT = 9.5) or (type = "PMSI ACE" and ACT_COD in ("AMS", "AMC", "AMK") and ACT_COE = 9.5))));
	rename
		PRS_ACT_CFT = PRS_ACT_CFT_95
		ACT_COE = ACT_COE_95;
run;

* On r�cup�re les AMS (BSE_PRS_NAT = 3125), AMC (BSE_PRS_NAT = 3121) ou AMK (BSE_PRS_NAT = 3122) avec un coefficient 4 (8/2);
data pop3_reperage2_8;
	set travail.reperage_actes (where = (date_debut >= "01JUL2018"d and ((type = "DCIR" and BSE_PRS_NAT in (3125, 3121, 3122) and 
		PRS_ACT_CFT = 4) or (type = "PMSI ACE" and ACT_COD in ("AMS", "AMC", "AMK") and ACT_COE = 4))));
	rename
		PRS_ACT_CFT = PRS_ACT_CFT_4
		ACT_COE = ACT_COE_4;
run;

* On r�cup�re la jointure des 2 : 30 jours max entre les 2 codes;
proc sql;

	CREATE TABLE travail.pop3_reperage2 AS
		SELECT
			a.*,
			b.PRS_ACT_CFT_4,
			b.ACT_COE_4,
			b.date_debut AS date_debut2
		FROM pop3_reperage2_95 a
			INNER JOIN pop3_reperage2_8 b
				ON a.BEN_IDT_ANO = b.BEN_IDT_ANO
				AND a.type = b.type
				AND a.domaine = b.domaine
				AND a.date_debut = b.date_debut;

quit;

data travail.pop3_reperage2;
	set travail.pop3_reperage2;
	length date_RR 4.;
	date_RR = MIN(date_debut, date_debut2);
run;

*	******************************************************************************************************************************************;
*	V�rifications;

%proc_freq(
	in_tbl = travail.pop3_reperage2, 
	out_tbl = _RR_pop3_reperage2, 
	list_var_in = PRS_ACT_CFT_4*ACT_COE_4*PRS_ACT_CFT_95*ACT_COE_95,
	list_var_out = PRS_ACT_CFT_4 ACT_COE_4 PRS_ACT_CFT_95 ACT_COE_95 Frequency
	);

proc delete data = pop3_reperage2_95 pop3_reperage2_8;
run; quit;


* AMS, AMC ou AMK 13.5;

data travail.pop3_reperage3;
	set travail.reperage_actes (where = (date_debut >= "01JUL2018"d and ((type = "DCIR" and BSE_PRS_NAT in (3125, 3121, 3122) and 
		PRS_ACT_CFT = 13.5) or (type = "PMSI ACE" and ACT_COD in ("AMS", "AMC", "AMK") and ACT_COE = 13.5))));
	length date_RR 4.;
	date_RR = date_debut;
	rename
		PRS_ACT_CFT = PRS_ACT_CFT_135
		ACT_COE = ACT_COE_135;
run;


* AMS, AMC ou AMK 8;

data travail.pop3_reperage4;
	set travail.reperage_actes (where = (date_debut >= "01JUL2018"d and ((type = "DCIR" and BSE_PRS_NAT in (3125, 3121, 3122) and 
		PRS_ACT_CFT = 8) or (type = "PMSI ACE" and ACT_COD in ("AMS", "AMC", "AMK") and ACT_COE = 8))));
	length date_RR 4.;
	date_RR = date_debut;
	rename
		PRS_ACT_CFT = PRS_ACT_CFT_8
		ACT_COE = ACT_COE_8;
run;


* BPC (BSE_PRS_NAT = 9570);

data travail.pop3_reperage5;
	set travail.reperage_actes (where = (date_debut >= "01JUL2018"d and ((type = "DCIR" and BSE_PRS_NAT = 9570) or (type = "PMSI ACE" 
		and ACT_COD in ("BPC")))));
	length date_RR 4.;
	date_RR = date_debut;
run;


* AMC ou AMK 20;

data travail.pop3_reperage6;
	set travail.reperage_actes (where = (date_debut >= "01JUL2018"d and ((type = "DCIR" and BSE_PRS_NAT in (3121, 3122) and 
		PRS_ACT_CFT = 20) or (type = "PMSI ACE" and ACT_COD in ("AMC", "AMK") and ACT_COE = 20))));
	length date_RR 4.;
	date_RR = date_debut;
run;


* AMC ou AMK 28;

data travail.pop3_reperage7;
	set travail.reperage_actes (where = (date_debut >= "01JUL2018"d and ((type = "DCIR" and BSE_PRS_NAT in (3121, 3122) and 
		PRS_ACT_CFT = 28) or (type = "PMSI ACE" and ACT_COD in ("AMC", "AMK") and ACT_COE = 28))));
	length date_RR 4.;
	date_RR = date_debut;
run;

data travail.population3;
	set travail.pop3_reperage1 travail.pop3_reperage2 (in = a) travail.pop3_reperage3 travail.pop3_reperage4 travail.pop3_reperage5 (in = b)
		travail.pop3_reperage6 (in = c) travail.pop3_reperage7 (in = d);
	length flag 3.;
	if a then 
		flag = 1;
	if b then 
		flag = 2;
	if c then
		flag = 20;
	if d then
		flag = 28;
run; 

* On supprime les patients avec une ALD;
proc sql;

	DELETE FROM travail.population3
	WHERE BEN_IDT_ANO IN (SELECT BEN_IDT_ANO FROM res.T_INDI_BPCO_&an_N. WHERE ALD = 0);

quit;

proc delete data = travail.pop3_reperage1 travail.pop3_reperage2 travail.pop3_reperage3 travail.pop3_reperage4 travail.pop3_reperage5
		travail.pop3_reperage6 travail.pop3_reperage7;
run; quit;


*	******************************************************************************************************************************************;
*	RR r�alis�e en dehors d une hospitalisation;
data travail.reperage_RR_&Annee_1N._&Annee_N1.;
	set travail.population1 (in = c)
		travail.population2 (in = d)
		travail.population3 (in = e);
	length date_RR 4. lieu_RR KINE_AMK_AMC_AMS_8 KINE_AMK_13_5 KINE_AMK_AMC_AMS_9_5_8 KINE_AMK_AMC_AMS_9_5_4 KINE_BPC 3.;
	format type_reperage $18. date_RR ddmmyy10.;
	KINE_AMK_AMC_AMS_8 = 0;
	KINE_AMK_13_5 = 0;
	KINE_AMK_AMC_AMS_9_5_8 = 0;
	KINE_AMK_AMC_AMS_9_5_4 = 0;
	KINE_BPC = 0;
	* Type de rep�rage;
	if flag = 28 then
		type_reperage = "AMK AMC 28";
	if flag = 20 then
		type_reperage = "AMK AMC 20";
	if ACT_COE_8 = 8 or PRS_ACT_CFT_8 = 8 then
		KINE_AMK_AMC_AMS_8 = 1;
	if ACT_COE_135 = 13.5 or PRS_ACT_CFT_135 = 13.5 then
		KINE_AMK_13_5 = 1;
	if date_debut2 ne . and flag = . and (ACT_COE_95 = 9.5 or PRS_ACT_CFT_95 = 9.5) and (ACT_COE_8 = 8 or PRS_ACT_CFT_8 = 8) then
		KINE_AMK_AMC_AMS_9_5_8 = 1;	
	if date_debut2 ne . and flag = 1 and (ACT_COE_95 = 9.5 or PRS_ACT_CFT_95 = 9.5) and (ACT_COE_4 = 4 or PRS_ACT_CFT_4 = 4) then
		KINE_AMK_AMC_AMS_9_5_4 = 1;
	if flag = 2 then
		KINE_BPC = 1;
	* Lieu RR;
	if type = "DCIR" then
		lieu_RR = 1;
	if domaine = "SSR" then
		lieu_RR = 3;
	if domaine = "MCO" then
		lieu_RR = 2;
run;

proc datasets library = travail memtype = data nolist;
	delete population1 population2 population3 reperage_actes;
run; quit;
