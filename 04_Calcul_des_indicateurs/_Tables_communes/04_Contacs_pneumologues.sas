/*	******************************************************************************************************************************************	*/
/*																																				*/
/*		Contacts avec un pneumologue																		 									*/
/*																																				*/
/*	******************************************************************************************************************************************	*/

* Pneumologue dans le DCIR;
%suppr_table(
	lib = orauser, 
	table = presta_pneumo
	);

data orauser.presta_pneumo;
	set orauser.codes_presta_BPCO (where = (reperage = 32));
run;

%extract_PRS_NAT_REF(
	annee_deb = &Annee_N.,
	annee_fin = &Annee_N1.,
	sel_spe = AND PSE_SPE_COD = 13,
	tbl_out = pneumo_sdv,
	tbl_codes = presta_pneumo,
	tbl_patients = corresp_MT_MG,
	regul = 1
	);

* Pneumologue dans le PMSI;
%suppr_table(
	lib = orauser, 
	table = actes_pneumo
	);

data orauser.actes_pneumo;
	set orauser.codes_actes_BPCO (where = (reperage = 32));
run;

%extract_ACT_COD_PMSI(
	annee_deb = &Annee_N.,
	annee_fin = &Annee_N1.,
	MCO = 1, 
	SSR = 1, 
	tbl_out = pneumo_PMSI, 
	tbl_codes = actes_pneumo, 
	tbl_patients = corresp_MT_MG
	);

%suppr_table(
	lib = travail, 
	table = contact_pneumo_&Annee_N._&Annee_N1.
	);

data travail.contact_pneumo_&Annee_N._&Annee_N1.;
	format type $12.;
	set	pneumo_sdv
		pneumo_PMSI (where = (EXE_SPE = "13" and index(type, "ACE") >= 1));
run;

proc delete data = pneumo_sdv pneumo_PMSI;
run; quit;

*	******************************************************************************************************************************************;
*	Vérifications;

data verif;
	set travail.contact_pneumo_&Annee_N._&Annee_N1.;
	if type = "DCIR" then
		annee = year(date_debut);
run;

%proc_freq(
	in_tbl = verif, 
	out_tbl = _pneumo, 
	list_var_in = Annee*TYPE*EXE_SPE*PSE_SPE_COD*BSE_PRS_NAT*ACT_COD,
	list_var_out = Annee TYPE EXE_SPE PSE_SPE_COD BSE_PRS_NAT ACT_COD Frequency
	);

proc delete data = verif;
run; quit;
