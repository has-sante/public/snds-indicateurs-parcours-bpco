/*	******************************************************************************************************************************************	*/
/*																																				*/
/*		Séjours en HAD, MCO, SSR ou RIP																								 			*/
/*																																				*/
/*	******************************************************************************************************************************************	*/

%extract_sej_PMSI(
	annee_deb = &annee_N., 
	annee_fin = &annee_N1., 
	tbl_out = travail.sejours_&Annee_N._&Annee_N1., 
	tbl_patients = corresp_id_patient
	);

data travail.sejours_&Annee_N._&Annee_N1.;
	set travail.sejours_&Annee_N._&Annee_N1.;
	if domaine in ("HAD", "RIP", "SSR") then
		do;
			if date_fin = . then
				date_fin = mdy(12, 31, annee);
		end;
run;

*	******************************************************************************************************************************************;
*	Vérifications;

%proc_freq(
	in_tbl = travail.sejours_&Annee_N._&Annee_N1., 
	out_tbl = _sejours_PMSI, 
	list_var_in = Annee*Domaine*Type,
	list_var_out = Annee Domaine Type Frequency
	);
