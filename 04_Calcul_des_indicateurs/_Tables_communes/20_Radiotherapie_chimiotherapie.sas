/*	******************************************************************************************************************************************	*/
/*																																				*/
/*		Radiothérapie et chimiothérapie																										 	*/
/*																																				*/
/*	******************************************************************************************************************************************	*/

*	****************************************************************************************************************************************** ;
*	Repérage via les codes CIM;

* On supprime la table si elle existe;
%suppr_table(
	lib = orauser, 
	table = code_CIM
	);

data orauser.code_CIM;
	set orauser.diag_codes_CIM_BPCO (where = (reperage in (58, 59)));
run;

* On appelle la macro pour le repérage dans le PMSI;
%extract_CIM10_PMSI(
	annee_deb = &annee_1N.,
	annee_fin = &annee_N1., 
	HAD_DP = 0,
	HAD_DAS = 0, 
	HAD_MPP = 0, 
	HAD_MPA = 0, 
	MCO_DP = 0, 
	MCO_DR = 0, 
	MCO_DAS = 1, 
	MCO_DP_UM = 0, 
	MCO_DR_UM = 0,
	SSR_FP = 0, 
	SSR_MPP = 0, 
	SSR_AE = 0, 
	SSR_DAS = 0, 
	tbl_out = CIM10&annee_1N._&annee_N1., 
	tbl_codes = code_CIM,
	tbl_patients = corresp_id_patient
	);

proc delete data = orauser.code_CIM;
run; quit;

*	****************************************************************************************************************************************** ;
*	Repérage via les codes GHM;

* On supprime la table si elle existe;
%suppr_table(
	lib = orauser, 
	table = code_GHM
	);

data orauser.code_GHM;
	set orauser.codes_GHM_BPCO (where = (reperage in (58, 59)));
run;

* On appelle la macro pour le repérage dans le PMSI;
%extract_GHM_PMSI(
	annee_deb = &annee_1N.,
	annee_fin = &annee_N1., 
	tbl_codes = code_GHM, 
	tbl_out = GHM_&annee_1N._&annee_N1.,
	tbl_patients = corresp_id_patient
	);

proc delete data = orauser.code_GHM;
run; quit;

*	****************************************************************************************************************************************** ;
*	On concatène les 2 tables;

data travail.radio_chimio_&annee_1N._&annee_N1.;
	set	CIM10&annee_1N._&annee_N1.
		GHM_&annee_1N._&annee_N1.;
	id_sejour = ETA_NUM||"_"||RSA_NUM||"_"||put(Annee, 4.);
run;

proc delete data =  CIM10&annee_1N._&annee_N1. GHM_&annee_1N._&annee_N1.;
run; quit;

*	******************************************************************************************************************************************;
*	Vérifications;

%proc_freq(
	in_tbl = travail.radio_chimio_&annee_1N._&annee_N1., 
	out_tbl = _radio_chimio, 
	list_var_in = Annee*Table*Reperage*GRG_GHM*code_CIM,
	list_var_out = Annee Table2 Reperage GRG_GHM code_CIM Frequency
	);
