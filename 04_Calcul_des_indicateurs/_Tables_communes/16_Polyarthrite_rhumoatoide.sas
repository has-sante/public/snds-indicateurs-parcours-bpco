/*	******************************************************************************************************************************************	*/
/*																																				*/
/*		Polyarthrite rhumato�de																									 				*/
/*																																				*/
/*	******************************************************************************************************************************************	*/

* On supprime la table si elle existe;
%suppr_table(
	lib = orauser, 
	table = code_CIM
	);

data orauser.code_CIM;
	set orauser.diag_codes_CIM_BPCO (where = (reperage = 26));
run;

* On appelle la macro pour le rep�rage dans le PMSI;
%extract_CIM10_PMSI(
	annee_deb = &annee_4N.,
	annee_fin = &annee_N1., 
	HAD_DP = 0,
	HAD_DAS = 0, 
	HAD_MPP = 0, 
	HAD_MPA = 0, 
	MCO_DP = 1, 
	MCO_DR = 1, 
	MCO_DAS = 1, 
	MCO_DP_UM = 1, 
	MCO_DR_UM = 1,
	SSR_FP = 0, 
	SSR_MPP = 1, 
	SSR_AE = 1, 
	SSR_DAS = 1, 
	tbl_out = travail.polyarthrite_rhum_&annee_4N._&annee_N1., 
	tbl_codes = code_CIM,
	tbl_patients = corresp_id_patient
	);

data travail.polyarthrite_rhum_&annee_4N._&annee_N1.;
	set travail.polyarthrite_rhum_&annee_4N._&annee_N1.;
	if domaine = "SSR" then
		do;
			if date_fin = . then
				date_fin = mdy(12, 31, annee);
		end;
run;

proc delete data = orauser.code_CIM;
run; quit;

*	******************************************************************************************************************************************;
*	V�rifications;

%proc_freq(
	in_tbl = travail.polyarthrite_rhum_&annee_4N._&annee_N1., 
	out_tbl = _polyarthrite_rhum, 
	list_var_in = Annee*Domaine*Table*Variable*reperage*code_CIM,
	list_var_out = Annee Domaine Table2 Variable reperage code_CIM Frequency
	);
