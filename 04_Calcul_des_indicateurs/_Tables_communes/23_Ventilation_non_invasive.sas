/*	******************************************************************************************************************************************	*/
/*																																				*/
/*		Ventilation non invasive																						 						*/
/*																																				*/
/*	******************************************************************************************************************************************	*/

* Repérage via les actes CCAM;
%suppr_table(
	lib = orauser, 
	table = codes_CCAM
	);

data orauser.codes_CCAM;
	set orauser.codes_CCAM_BPCO (where = (reperage = 6));
run;

%extract_CCAM_PMSI(
	annee_deb = &Annee_N., 
	annee_fin = &Annee_N1., 
	HAD = 1, MCO = 0, RIP = 0, SSR = 0,
	tbl_out = reperage_CCAM_PMSI, 
	tbl_codes = codes_CCAM, 
	tbl_patients = corresp_id_patient
	);

* Repérage via les codes LPP;
%suppr_table(
	lib = orauser, 
	table = codes_LPP
	);

data orauser.codes_LPP;
	set orauser.codes_LPP_BPCO (where = (reperage = 6));
run;

%extract_LPP_DCIR(
	annee_deb = &Annee_N., 
	annee_fin = &Annee_N1., 
	tbl_out = reperage_LPP_sdv, 
	tbl_codes = codes_LPP, 
	tbl_patients = corresp_id_patient
	);

* Concaténation des tables;
data travail.reperage_VNI_&Annee_N._&annee_N1.;
	set reperage_LPP_sdv (in = a drop = type)
		reperage_CCAM_PMSI (in = b drop = type);
	if a and date_fin = . then
		date_fin = date_debut;
	if b and date_fin = . then
		date_fin = mdy(12, 31, annee);
run;

proc delete data = reperage_CCAM_PMSI reperage_LPP_sdv;
run; quit;

*	******************************************************************************************************************************************;
*	Vérifications;

data verif;
	set travail.reperage_VNI_&Annee_N._&annee_N1.;
	annee_debut = year(date_debut);
run;

%proc_freq(
	in_tbl = verif, 
	out_tbl = _VNI, 
	list_var_in = Domaine*Annee*Annee_Debut*Reperage*TIP_PRS_IDE*Code_CCAM,
	list_var_out = Domaine Annee Annee_Debut Reperage TIP_PRS_IDE Code_CCAM Frequency
	);

proc delete data = verif;
run; quit;
