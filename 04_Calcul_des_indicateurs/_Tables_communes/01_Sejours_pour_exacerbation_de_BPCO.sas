/*	******************************************************************************************************************************************	*/
/*																																				*/
/*		S�jours pour exacerbation de BPCO																						 				*/
/*																																				*/
/*	******************************************************************************************************************************************	*/

*	******************************************************************************************************************************************;
*	On r�cup�re les s�jours pour exacerbation de BPCO;

%macro exacerb_BPCO(annee = );

	%suppr_table(
		lib = orauser, 
		table = diag_codes_CIM_exa_BPCO
		);

	data orauser.diag_codes_CIM_exa_BPCO;
		set orauser.diag_codes_CIM_BPCO (where = (reperage in (16, 18, 17, 19, 23, 25, 24, 20, 21, 22) or (reperage = 11 and flag_pneumo = 1)));
	run;

	* On appelle la macro pour le rep�rage dans le PMSI MCO;
	%extract_CIM10_PMSI(
		annee_deb = &annee.,
		annee_fin = &annee.,
		HAD_DP = 0,
		HAD_DAS = 0, 
		HAD_MPP = 0, 
		HAD_MPA = 0, 
		MCO_DP = 1, 
		MCO_DR = 0, 
		MCO_DAS = 1, 
		MCO_DP_UM = 0, 
		MCO_DR_UM = 0,
		SSR_FP = 0, 
		SSR_MPP = 0, 
		SSR_AE = 0, 
		SSR_DAS = 0, 
		tbl_out = travail.reperage_CIM10_exa_BPCO, 
		tbl_codes = diag_codes_CIM_exa_BPCO,
		tbl_patients = corresp_id_patient
		);

	proc sql;

		DELETE FROM travail.reperage_CIM10_exa_BPCO
		WHERE SUBSTR(GRG_GHM, 1, 2) = "28" OR SEJ_NBJ = 0;

	quit;

	proc sort data = travail.reperage_CIM10_exa_BPCO;
		by ETA_NUM RSA_NUM annee;
	run;

	*	**************************************************************************************************************************************;
	*	On r�cup�re les s�jours avec un code diagnostic de BPCO a �t� cod� en DP du s�jour;

	data sejours_exacerbation1;
		set travail.reperage_CIM10_exa_BPCO (where = (reperage = 16 and table = "B" and variable = "DGN_PAL"));
		length exacerbation_BPCO 3.;
		exacerbation_BPCO = 1;
	run;

	*	**************************************************************************************************************************************;
	*	On r�cup�re les s�jours avec un code diagnostic de BPCO a �t� cod� en DAS du s�jour;

	proc sort data = travail.reperage_CIM10_exa_BPCO (keep = ETA_NUM RSA_NUM annee reperage variable where = (reperage = 16 and variable = "ASS_DGN")) 
			out = reperage_CIM10_exa_BPCO nodupkey;
		by ETA_NUM RSA_NUM annee;
	run;

	*	**************************************************************************************************************************************;
	*	On r�cup�re les s�jours avec un code de pneumopathie lobaire en DP et un diagnostic de BPCO en DAS;

	data sejours_exacerbation2;
		merge	travail.reperage_CIM10_exa_BPCO (in = a where = (reperage = 18 and table = "B" and variable = "DGN_PAL"))
				reperage_CIM10_exa_BPCO (in = b);
		by ETA_NUM RSA_NUM annee;
		if a and b;
		length exacerbation_BPCO 3.;
		exacerbation_BPCO = 2;
	run;

	*	**************************************************************************************************************************************;
	*	On r�cup�re les s�jours avec un code d insuffisance respiratoire aig�e en DP avec un diagnostic de BPCO en DAS;

	data sejours_exacerbation3;
		merge	travail.reperage_CIM10_exa_BPCO (in = a where = (reperage = 17 and table = "B" and variable = "DGN_PAL"))
				reperage_CIM10_exa_BPCO (in = b);
		by ETA_NUM RSA_NUM annee;
		if a and b;
		length exacerbation_BPCO 3.;
		exacerbation_BPCO = 3;
	run;

	*	**************************************************************************************************************************************;
	*	On r�cup�re les s�jours avec un code de grippe en DP avec un diagnostic de BPCO en DAS;

	data sejours_exacerbation4;
		merge	travail.reperage_CIM10_exa_BPCO (in = a where = (reperage = 19 and table = "B" and variable = "DGN_PAL"))
				reperage_CIM10_exa_BPCO (in = b);
		by ETA_NUM RSA_NUM annee;
		if a and b;
		length exacerbation_BPCO 3.;
		exacerbation_BPCO = 4;
	run;

	*	**************************************************************************************************************************************;
	*	On r�cup�re les s�jours avec un code d infection des voies a�riennes en DP avec un diagnostic de BPCO en DAS;

	data sejours_exacerbation5;
		merge	travail.reperage_CIM10_exa_BPCO (in = a where = (reperage = 23 and table = "B" and variable = "DGN_PAL"))
				reperage_CIM10_exa_BPCO (in = b);
		by ETA_NUM RSA_NUM annee;
		if a and b;
		length exacerbation_BPCO 3.;
		exacerbation_BPCO = 5;
	run;

	*	**************************************************************************************************************************************;
	*	On r�cup�re les s�jours avec un code de bronchopneumopathie en DP avec un diagnostic de BPCO en DAS;

	data sejours_exacerbation6;
		merge	travail.reperage_CIM10_exa_BPCO (in = a where = (reperage = 25 and table = "B" and variable = "DGN_PAL"))
				reperage_CIM10_exa_BPCO (in = b);
		by ETA_NUM RSA_NUM annee;
		if a and b;
		length exacerbation_BPCO 3.;
		exacerbation_BPCO = 6;
	run;

	*	**************************************************************************************************************************************;
	*	On r�cup�re les s�jours avec un code de pneumonie en DP avec un diagnostic de BPCO en DAS;

	data sejours_exacerbation7;
		merge	travail.reperage_CIM10_exa_BPCO (in = a where = (reperage = 24 and table = "B" and variable = "DGN_PAL"))
				reperage_CIM10_exa_BPCO (in = b);
		by ETA_NUM RSA_NUM annee;
		if a and b;
		length exacerbation_BPCO 3.;
		exacerbation_BPCO = 7;
	run;

	*	**************************************************************************************************************************************;
	*	On r�cup�re les s�jours avec un code d embolie pulmonaire en DP avec un diagnostic de BPCO en DAS;

	data sejours_exacerbation8;
		merge	travail.reperage_CIM10_exa_BPCO (in = a where = (reperage = 20 and table = "B" and variable = "DGN_PAL"))
				reperage_CIM10_exa_BPCO (in = b);
		by ETA_NUM RSA_NUM annee;
		if a and b;
		length exacerbation_BPCO 3.;
		exacerbation_BPCO = 8;
	run;

	*	**************************************************************************************************************************************;
	*	On r�cup�re les s�jours avec un code d insuffisance cardiaque aigue en DP avec un diagnostic de BPCO en DAS;

	data sejours_exacerbation9;
		merge	travail.reperage_CIM10_exa_BPCO (in = a where = (reperage = 21 and table = "B" and variable = "DGN_PAL"))
				reperage_CIM10_exa_BPCO (in = b);
		by ETA_NUM RSA_NUM annee;
		if a and b;
		length exacerbation_BPCO 3.;
		exacerbation_BPCO = 9;
	run;

	*	**************************************************************************************************************************************;
	*	On r�cup�re les s�jours avec un code d �d�me aigu du poumon en DP avec un diagnostic de BPCO en DAS;

	data sejours_exacerbation10;
		merge	travail.reperage_CIM10_exa_BPCO (in = a where = (reperage = 22 and table = "B" and variable = "DGN_PAL"))
				reperage_CIM10_exa_BPCO (in = b);
		by ETA_NUM RSA_NUM annee;
		if a and b;
		length exacerbation_BPCO 3.;
		exacerbation_BPCO = 10;
	run;

	*	**************************************************************************************************************************************;
	*	On r�cup�re les s�jours avec un code de pneumothorax en DP avec un diagnostic de BPCO en DAS;

	data sejours_exacerbation11;
		merge	travail.reperage_CIM10_exa_BPCO (in = a where = (reperage = 11 and table = "B" and variable = "DGN_PAL"))
				reperage_CIM10_exa_BPCO (in = b);
		by ETA_NUM RSA_NUM annee;
		if a and b;
		length exacerbation_BPCO 3.;
		exacerbation_BPCO = 11;
	run;

	*	**************************************************************************************************************************************;
	*	On concat�ne toutes les tables et on compte le nombre de s�jours par patient;

	data travail.sejours_exacerbation_&annee.;
		set sejours_exacerbation1-sejours_exacerbation11;
		length id_sejour $30;
		id_sejour = ETA_NUM||"_"||RSA_NUM||"_"||put(Annee, 4.);
	run;

	data travail.sejours_cible_exacerbation_&annee.;
		set travail.sejours_exacerbation_&annee.;
	run;

%mend exacerb_BPCO;

%exacerb_BPCO(annee = &annee_1N.);
%exacerb_BPCO(annee = &annee_N.);
%exacerb_BPCO(annee = &annee_N1.);

*	**************************************************************************************************************************************;
*	On exclut les s�jours qui se chevauchent (= transferts) - Pour cela, on rep�re les s�jours de l ann�e pr�c�dente et suivante;

data sejours_exacerbation_all;
	format variable $11.;
	set travail.sejours_cible_exacerbation_&annee_1N. travail.sejours_cible_exacerbation_&annee_N. travail.sejours_cible_exacerbation_&annee_N1.;
run;

*	******************************************************************************************************************************************;
*	V�rifications;
		
%proc_freq(
	in_tbl = sejours_exacerbation_all, 
	out_tbl = _sej_exacerb, 
	list_var_in = ANNEE*DOMAINE*TABLE*REPERAGE*VARIABLE*DGN_PAL, 
	list_var_out = ANNEE DOMAINE TABLE2 REPERAGE VARIABLE DGN_PAL Frequency
	);

data verif;
	set sejours_exacerbation_all;
	annee_fin = year(date_fin);
run;
		
%proc_freq(
	in_tbl = verif, 
	out_tbl = _sej_exacerb_annee_fin, 
	list_var_in = annee_fin, 
	list_var_out = annee_fin Frequency
	);

proc delete data = verif;
run; quit;

data sejours_exacerbation_all_copie;
	format variable $11.;
	set travail.sejours_cible_exacerbation_&annee_1N. travail.sejours_cible_exacerbation_&annee_N. travail.sejours_cible_exacerbation_&annee_N1.;
run;

proc sql;

	CREATE TABLE sejours_transferts AS
		SELECT DISTINCT
			a.BEN_IDT_ANO,
			a.id_sejour,
			b.id_sejour AS index_transfert,
			a.date_debut,
			a.date_fin,
			b.date_debut as date_debut_transfert,
			b.date_fin as date_fin_transfert
		FROM sejours_exacerbation_all_copie a
			LEFT JOIN sejours_exacerbation_all b
				ON a.BEN_IDT_ANO = b.BEN_IDT_ANO
		WHERE b.date_debut >= a.date_debut AND a.id_sejour NE b.id_sejour;

quit;

data sejours_transferts;
	set sejours_transferts;
	if (date_debut_transfert < date_fin and date_fin_transfert < date_fin)
			or (date_debut_transfert < date_fin and date_fin_transfert > date_fin)
			or (date_debut_transfert < date_fin and date_fin_transfert = date_fin) then
		output;
run;

proc sql;

	SELECT COUNT(DISTINCT id_sejour) AS nb_sejours, COUNT(DISTINCT index_transfert) AS nb_transferts
	FROM sejours_transferts;

quit;

* On supprime les 2 s�jours de les tables travail.sejours_exacerbation_XXXX;
proc sql;

	DELETE FROM travail.sejours_cible_exacerbation_&annee_1N.
	WHERE id_sejour IN (SELECT id_sejour FROM sejours_transferts)
		OR id_sejour IN (SELECT index_transfert FROM sejours_transferts);

	DELETE FROM travail.sejours_cible_exacerbation_&annee_N.
	WHERE id_sejour IN (SELECT id_sejour FROM sejours_transferts)
		OR id_sejour IN (SELECT index_transfert FROM sejours_transferts);

	DELETE FROM travail.sejours_cible_exacerbation_&annee_N1.
	WHERE id_sejour IN (SELECT id_sejour FROM sejours_transferts)
		OR id_sejour IN (SELECT index_transfert FROM sejours_transferts);

quit;

proc datasets library = work memtype = data ;
	delete sejours: reperage_CIM10_exa_BPCO;
run; quit;

proc delete data = travail.reperage_CIM10_exa_BPCO;
run; quit;
