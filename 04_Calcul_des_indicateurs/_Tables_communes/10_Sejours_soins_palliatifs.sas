/*	******************************************************************************************************************************************	*/
/*																																				*/
/*		S�jours avec un diagnostic de soins palliatif																			 				*/
/*																																				*/
/*	******************************************************************************************************************************************	*/

* On r�cup�re les s�jours en MCO, SSR et HAD avec un diagnostic de soins palliatif;

* On supprime la table si elle existe;
%suppr_table(
	lib = orauser, 
	table = code_CIM_soin_palliatif
	);

data orauser.code_CIM_soin_palliatif;
	set orauser.diag_codes_CIM_BPCO (where = (reperage = 13));
run;

* On appelle la macro pour le rep�rage dans le PMSI;
%extract_CIM10_PMSI(
	annee_deb = &annee_2N.,
	annee_fin = &annee_N1., 
	HAD_DP = 0,
	HAD_DAS = 0, 
	HAD_MPP = 0, 
	HAD_MPA = 0, 
	MCO_DP = 1, 
	MCO_DR = 1, 
	MCO_DAS = 1, 
	MCO_DP_UM = 1, 
	MCO_DR_UM = 1,
	SSR_FP = 1, 
	SSR_MPP = 1, 
	SSR_AE = 1, 
	SSR_DAS = 1, 
	tbl_out = travail.soin_palliatif_&annee_2N._&annee_N1., 
	tbl_codes = code_CIM_soin_palliatif,
	tbl_patients = corresp_id_patient
	);

* Rep�rage dans le HAD avec PEC_PAL = � 04 � ou PEC_ASS = � 04 �;
%macro HAD_palliatif(annee_deb=, annee_fin=, tbl_out=, tbl_patients=);
	
	%let an_deb_pmsi = %sysevalf(&annee_deb. - 2000);
	%let an_fin_pmsi = %sysevalf(&annee_fin. - 2000);

	%do i = &an_deb_pmsi. %to &an_fin_pmsi.;

		%if &i. < 10 %then %let an = 0&i.;
		%else %let an = &i.;

		%let var_join1 = ETA_NUM_EPMSI;
		%let var_join2 = RHAD_NUM;

		proc sql;

			%connectora;

				CREATE TABLE HAD_palliatif_&an. AS 
				 	SELECT *
					FROM CONNECTION TO ORACLE (
						SELECT DISTINCT
							pop.BEN_IDT_ANO,
							c.EXE_SOI_DTD,
							c.EXE_SOI_DTF,
							c.&var_join1.,
							c.&var_join2.,
							'HAD' AS domaine,
							20&an. AS annee
						FROM &tbl_patients. pop
							INNER JOIN T_HAD&an.C c
								ON	pop.BEN_NIR_PSA = c.NIR_ANO_17
							INNER JOIN T_HAD&an.B b
								ON	c.&var_join1. = b.&var_join1.
								AND	c.&var_join2. = b.&var_join2.
						WHERE (b.PEC_PAL = '04' OR b.PEC_ASS = '04')
							AND c.NIR_ANO_17 NOT IN ('&cle_inc1.', '&cle_inc2.')
							AND NIR_RET = '0' AND NAI_RET = '0' AND SEX_RET = '0' AND SEJ_RET = '0' AND FHO_RET = '0' 
							AND PMS_RET = '0' AND DAT_RET = '0' %if &an. > 12 %then %do; AND COH_NAI_RET = '0' 
							AND COH_SEX_RET = '0' %end;
						);

			DISCONNECT FROM ORACLE;

		quit;

		data HAD_palliatif_&an.;
			set HAD_palliatif_&an.;
			length date_debut date_fin 4.;
			date_debut = datepart(EXE_SOI_DTD);
			date_fin = datepart(EXE_SOI_DTF);
			format date_debut date_fin ddmmyy10.;
			drop EXE_SOI_DTD EXE_SOI_DTF;
		run;

		%arret_erreur;

	%end;

	* Empilage de la table temporaire � la table R�sultat;
	data &tbl_out.;
		set HAD_palliatif_:;
	run;

%mend HAD_palliatif;

%HAD_palliatif(
	annee_deb = &annee_2N.,
	annee_fin = &annee_N1.,  
	tbl_out = soins_palliatif_HAD, 
	tbl_patients = corresp_id_patient
	);

* Table finale;
data travail.soin_palliatif_&annee_2N._&annee_N1.;
	set	travail.soin_palliatif_&annee_2N._&annee_N1.
		soins_palliatif_HAD;
	if domaine = "MCO" then
		id_sejour = ETA_NUM||"_"||RSA_NUM||"_"||put(Annee, 4.);
	else if domaine = "HAD" then
		id_sejour = ETA_NUM_EPMSI||"_"||RHAD_NUM||"_"||put(Annee, 4.);
	else if domaine = "SSR" then
		id_sejour = ETA_NUM||"_"||RHA_NUM||"_"||put(Annee, 4.);
	if domaine ne "MCO" and date_fin = . then
		date_fin = mdy(12, 31, annee);
run;

*	******************************************************************************************************************************************;
*	V�rifications;

%proc_freq(
	in_tbl = travail.soin_palliatif_&annee_2N._&annee_N1., 
	out_tbl = _soin_pall, 
	list_var_in = Annee*Domaine*Table*Variable*Code_CIM,
	list_var_out = Annee Domaine Table2 Variable Code_CIM Frequency
	);
