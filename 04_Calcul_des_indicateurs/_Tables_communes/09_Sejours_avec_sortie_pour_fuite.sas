/*	******************************************************************************************************************************************	*/
/*																																				*/
/*	S�jours avec un code diagnostic de sortie contre avis m�dical ou fuite cod� en DAS										 					*/
/*																																				*/
/*	******************************************************************************************************************************************	*/


* On r�cup�re les s�jours en MCO avec un DAS Z53.2;
data code_CIM_fuite;
retain code_CIM;
length code_CIM $ 4;
input code_CIM $;
datalines;
Z532
;
run;

* On supprime la table si elle existe;
%suppr_table(
	lib = orauser, 
	table = code_CIM_fuite
	);

* On ajoute la longueur de code_CIM;
data orauser.code_CIM_fuite;
	set code_CIM_fuite;
	length taille 3.;
	taille = length(compress(code_CIM));
run;

proc delete data = code_CIM_fuite;
run;

* On appelle la macro pour le rep�rage dans le PMSI MCO - DAS;
%extract_CIM10_PMSI(
	annee_deb = &annee_N.,
	annee_fin = &annee_N., 
	HAD_DP = 0,
	HAD_DAS = 0, 
	HAD_MPP = 0, 
	HAD_MPA = 0, 
	MCO_DP = 0, 
	MCO_DR = 0, 
	MCO_DAS = 1, 
	MCO_DP_UM = 0, 
	MCO_DR_UM = 0,
	SSR_FP = 0, 
	SSR_MPP = 0, 
	SSR_AE = 0, 
	SSR_DAS = 0, 
	tbl_out = travail.fuite_&annee_N., 
	tbl_codes = code_CIM_fuite,
	tbl_patients = corresp_id_patient
	);

data travail.fuite_&annee_N.;
	set travail.fuite_&annee_N. (where = (substr(GRG_GHM, 1, 2) ne "28" or SEJ_NBJ > 0));
	id_sejour = ETA_NUM||"_"||RSA_NUM||"_"||put(Annee, 4.);
run;

*	******************************************************************************************************************************************;
*	V�rifications;

%proc_freq(
	in_tbl = travail.fuite_&annee_N., 
	out_tbl = _fuite_&annee_N., 
	list_var_in = Annee*Table*Variable*Code_CIM,
	list_var_out = Annee Table2 Variable Code_CIM Frequency
	);
