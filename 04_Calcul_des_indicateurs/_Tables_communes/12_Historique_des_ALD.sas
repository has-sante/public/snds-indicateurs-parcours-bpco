/*	******************************************************************************************************************************************	*/
/*																																				*/
/*		Repérage de toutes les ALD 																												*/
/*																																				*/
/*	******************************************************************************************************************************************	*/

%extract_ALD_CIM(
	tbl_out = travail.Histo_ALD, 
	tbl_codes = ALD_codes_CIM_BPCO, 
	tbl_patients = corresp_id_patient
	);

*	******************************************************************************************************************************************;
*	Vérifications;

%proc_freq(
	in_tbl = travail.Histo_ALD, 
	out_tbl = _Histo_ALD, 
	list_var_in = IMB_ETM_NAT*MED_MTF_COD,
	list_var_out = IMB_ETM_NAT MED_MTF_COD Frequency
	);
