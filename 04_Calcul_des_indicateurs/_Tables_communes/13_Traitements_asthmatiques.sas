/*	******************************************************************************************************************************************	*/
/*																																				*/
/*		Délivrance remboursée de traitements asthmatiques :																		 				*/
/*			- SCI seuls (reperage = 41)																											*/
/*			- Traitements anti IgE (reperage = 37)																								*/
/*			- Traitements anti IL5 (reperage = 38)																								*/
/*			- Antileucotriène (reperage = 39)																									*/
/*																																				*/
/*	******************************************************************************************************************************************	*/

%suppr_table(
	lib = orauser, 
	table = codes_ATC
	);

data orauser.codes_ATC;
	set orauser.codes_ATC_BPCO (where = (reperage in (37, 38, 39, 41)));
run;

* Dans le DCIR - Codes CIP;
%extract_CIP(
	annee_deb = &Annee_2N., 
	annee_fin = &Annee_N1., 
	tbl_out = travail.reperage_med_asthme_&Annee_2N._&Annee_N1., 
	tbl_codes = codes_ATC, 
	tbl_patients = corresp_id_patient
	);

proc delete data = orauser.codes_ATC;
run; quit;

*	******************************************************************************************************************************************;
*	Vérifications;

data verif;
	set travail.reperage_med_asthme_&Annee_2N._&Annee_N1.;
	annee_debut = year(date_debut);
run;

%proc_freq(
	in_tbl = verif, 
	out_tbl = _med_asthme, 
	list_var_in = TYPE*annee_debut*PHA_ATC_C07*PHA_CND_TOP,
	list_var_out = TYPE annee_debut PHA_ATC_C07 PHA_CND_TOP Frequency
	);

proc delete data = verif;
run; quit;
