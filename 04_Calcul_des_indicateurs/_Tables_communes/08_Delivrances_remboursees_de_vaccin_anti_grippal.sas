/*	******************************************************************************************************************************************	*/
/*																																				*/
/*		Délivrance remboursée de vaccin anti grippal																			 				*/
/*																																				*/
/*	******************************************************************************************************************************************	*/

%suppr_table(
	lib = orauser, 
	table = codes_ATC
	);

data orauser.codes_ATC;
	set orauser.codes_ATC_BPCO (where = (reperage in (40)));
run;

* Dans le DCIR - Codes CIP;
%extract_CIP(
	annee_deb = &Annee_N., 
	annee_fin = &Annee_N1., 
	tbl_out = travail.reperage_vacc_grippe_&Annee_N._&Annee_N1., 
	tbl_codes = codes_ATC, 
	tbl_patients = corresp_id_patient
	);

proc sql;

	DELETE FROM travail.reperage_vacc_grippe_&Annee_N._&Annee_N1.
	WHERE code_CIP13 IN (3400939886091, 3400939886213, 3400939886152, 3400939925745, 3400939925806, 3400939925974);

quit;

proc delete data = orauser.codes_ATC;
run; quit;

*	******************************************************************************************************************************************;
*	Vérifications;

data verif;
	set travail.reperage_vacc_grippe_&Annee_N._&Annee_N1.;
	annee_debut = year(date_debut);
run;

%proc_freq(
	in_tbl = verif, 
	out_tbl = _vaccin_grippe, 
	list_var_in = TYPE*annee_debut*PHA_ATC_C07*PHA_CND_TOP,
	list_var_out = TYPE annee_debut PHA_ATC_C07 PHA_CND_TOP Frequency
	);

proc delete data = verif;
run; quit;
