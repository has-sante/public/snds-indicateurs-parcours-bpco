/*	******************************************************************************************************************************************	*/
/*																																				*/
/*		Acte CCAM de prise en charge d une prothèse de hanche ou de genou ou chirurgie du rachis												*/
/*																																				*/
/*	******************************************************************************************************************************************	*/

%suppr_table(
	lib = orauser, 
	table = codes_CCAM
	);

data orauser.codes_CCAM;
	set orauser.codes_CCAM_BPCO (where = (reperage in (8, 9, 10)));
run;

* Dans le PMSI;
%extract_CCAM_PMSI(
	annee_deb = &Annee_1N., 
	annee_fin = &Annee_N1., 
	HAD = 0, 
	MCO = 1, 
	RIP = 0, 
	SSR = 0,
	tbl_out = travail.prothese_chir_rachis_&annee_1N._&annee_N1., 
	tbl_codes = codes_CCAM, 
	tbl_patients = corresp_id_patient
	);

proc sql;

	DELETE FROM travail.prothese_chir_rachis_&annee_1N._&annee_N1.
	WHERE type = "PMSI ACE" OR SUBSTR(GRG_GHM, 1, 2) = "28";

quit;

proc delete data = orauser.codes_CCAM;
run; quit;

*	******************************************************************************************************************************************;
*	Vérifications;

%proc_freq(
	in_tbl = travail.prothese_chir_rachis_&annee_1N._&annee_N1., 
	out_tbl = _prothese_chir_rachis, 
	list_var_in = Annee*Reperage*Domaine*Table*GRG_GHM*Code_CCAM,
	list_var_out = Annee Reperage Domaine Table2 GRG_GHM Code_CCAM Frequency
	);
