/*	******************************************************************************************************************************************	*/
/*																																				*/
/*		Contacts avec un médecin généraliste ou médecin traitant											 									*/
/*																																				*/
/*	******************************************************************************************************************************************	*/

*	******************************************************************************************************************************************;
*	Table contenant les patients des indicateurs 4, 5 et 6 uniquement;

data indicateurs_04et05 (keep = BEN_IDT_ANO);
	set pop.indicateurs_&an_N. (where = (indicateur_04 = 1 or indicateur_05 = 1 or indicateur_09 = 1));
run;

%suppr_table(
	lib = orauser, 
	table = corresp_MT_MG
	);

proc sql;

	CREATE TABLE orauser.corresp_MT_MG AS
		SELECT *
		FROM travail.corresp_id_patient
		WHERE BEN_IDT_ANO IN (SELECT BEN_IDT_ANO FROM indicateurs_04et05);

quit;

proc delete data = indicateurs_04et05;
run; quit;

*	******************************************************************************************************************************************;
*	Contact avec le médecin généraliste ou le médecin traitant;

* MG dans le PMSI;
%suppr_table(
	lib = orauser, 
	table = actes_MG
	);

%suppr_table(
	lib = travail, 
	table = contact_MG_PMSI_&Annee_N._&Annee_N1.
	);

data orauser.actes_MG;
	set orauser.codes_actes_BPCO (where = (reperage = 33));
run;

%extract_ACT_COD_PMSI(
	annee_deb = &Annee_N.,
	annee_fin = &Annee_N1.,
	MCO = 1, 
	SSR = 1, 
	tbl_out = travail.contact_MG_PMSI_&Annee_N._&Annee_N1., 
	tbl_codes = actes_MG, 
	tbl_patients = corresp_MT_MG
	);

data travail.contact_MG_PMSI_&Annee_N._&Annee_N1.;
	set travail.contact_MG_PMSI_&Annee_N._&Annee_N1. (where = (index(type, "ACE") >= 1 and EXE_SPE in ("01", "22", "23")));
run;

*	******************************************************************************************************************************************;
*	Vérifications;
		
%proc_freq(
	in_tbl = travail.contact_MG_PMSI_&Annee_N._&Annee_N1., 
	out_tbl = _MG_PMSI, 
	list_var_in = Annee*Domaine*Table*EXE_SPE*ACT_COD, 
	list_var_out = Annee Domaine Table2 EXE_SPE ACT_COD Frequency
	);

* MG et MT dans le DCIR;
%suppr_table(
	lib = travail, 
	table = contact_MT_&Annee_N._&Annee_N1.
	);

%extract_MT(
	annee_deb = &Annee_N., 
	annee_fin = &Annee_N1., 
	tbl_out = travail.contact_MT_&Annee_N._&Annee_N1., 
	tbl_patients = corresp_MT_MG
	);

*	******************************************************************************************************************************************;
*	Vérifications;

data verif;
	set travail.contact_MT_&Annee_N._&Annee_N1.;
	annee = year(date_debut);
run;

%proc_freq(
	in_tbl = verif, 
	out_tbl = _MG_DCIR, 
	list_var_in = Annee*PSE_SPE_COD*BSE_PRS_NAT, 
	list_var_out = Annee PSE_SPE_COD BSE_PRS_NAT Frequency
	);

%proc_freq(
	in_tbl = verif, 
	out_tbl = _MG_DCIR_qte, 
	list_var_in = quantite, 
	list_var_out = quantite Frequency
	);

proc delete data = verif;
run; quit;
