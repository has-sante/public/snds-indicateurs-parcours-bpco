/*	******************************************************************************************************************************************	*/
/*																																				*/
/*		EFR ou spirométrie																						 								*/
/*																																				*/
/*	******************************************************************************************************************************************	*/

%suppr_table(
	lib = orauser, 
	table = codes_CCAM
	);

data orauser.codes_CCAM;
	set orauser.codes_CCAM_BPCO (where = (reperage in (1, 2)));
run;

* Dans le DCIR;
%extract_CCAM_DCIR(
	annee_deb = &Annee_2N., 
	annee_fin = &Annee_N1., 
	tbl_out = reperage_CCAM_sdv, 
	tbl_codes = codes_CCAM, 
	tbl_patients = corresp_id_patient
	);

* Dans le PMSI;
%extract_CCAM_PMSI(
	annee_deb = &Annee_2N., 
	annee_fin = &Annee_N1., 
	HAD = 0, MCO = 1, RIP = 0, SSR = 1,
	tbl_out = reperage_CCAM_PMSI, 
	tbl_codes = codes_CCAM, 
	tbl_patients = corresp_id_patient
	);

data travail.reperage_EFR_spiro_&Annee_2N._&annee_N1.;
	set reperage_CCAM_sdv (in = a drop = type)
		reperage_CCAM_PMSI (in = b drop = type);
	if a and date_fin = . then
		date_fin = date_debut;
	if b then
		do;
			if table = "FMSTC" and date_fin = . then
				date_fin = date_debut;
			else if domaine ne "MCO" and date_fin = . then
				date_fin = mdy (12, 31, annee);
			
		end;
run;

proc delete data = reperage_CCAM_sdv reperage_CCAM_PMSI;
run; quit;

*	******************************************************************************************************************************************;
*	Vérifications;

* On ne passe pas par la macro car trop de variables;
proc sql;

	CREATE TABLE verif._0&tmp_num_tab._EFR_spiro AS
		SELECT 
			Reperage,
			YEAR(date_debut) AS annee_Debut_DCIR,
			Annee,
			Table,
			Domaine,
			CAM_PRS_IDE,
			CODE_CCAM,
			COUNT(*) AS Frequency
		FROM travail.reperage_EFR_spiro_&Annee_2N._&annee_N1.
		GROUP BY 1, 2, 3, 4, 5, 6, 7;

quit;

proc sql noprint; SELECT SUM(Frequency) INTO: nb_tot FROM verif._0&tmp_num_tab._EFR_spiro; quit;

data verif._0&tmp_num_tab._EFR_spiro;
	set verif._0&tmp_num_tab._EFR_spiro;
	length percent 4.;
	format percent nlpct7.1 Frequency commafr_0ch.;
	percent = Frequency/&nb_tot.;
run;

%let tmp_num_tab = %sysevalf(&tmp_num_tab. + 1);
