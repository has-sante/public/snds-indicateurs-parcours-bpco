/*	******************************************************************************************************************************************	*/
/*																																				*/
/*		Thermoplastie bronchique en MCO																		 									*/
/*																																				*/
/*	******************************************************************************************************************************************	*/

%suppr_table(
	lib = orauser, 
	table = codes_CCAM
	);

data orauser.codes_CCAM;
	set orauser.codes_CCAM_BPCO (where = (reperage = 4));
run;

* Dans le PMSI;
%extract_CCAM_PMSI(
	annee_deb = &Annee_1N., 
	annee_fin = &Annee_N1., 
	HAD = 0, MCO = 1, RIP = 0, SSR = 0,
	tbl_out = travail.reperage_thermo_bronch_&annee_1N._&annee_N1., 
	tbl_codes = codes_CCAM, 
	tbl_patients = corresp_id_patient
	);

proc sql;

	DELETE FROM travail.reperage_thermo_bronch_&annee_1N._&annee_N1.
	WHERE type = "PMSI ACE";

quit;

proc delete data = orauser.codes_CCAM;
run; quit;

*	******************************************************************************************************************************************;
*	Vérifications;

%proc_freq(
	in_tbl = travail.reperage_thermo_bronch_&annee_1N._&annee_N1., 
	out_tbl = _thermo_bronch, 
	list_var_in = Annee*Domaine*Table*reperage*code_CCAM,
	list_var_out = Annee Domaine Table2 code_CCAM reperage Frequency
	);
