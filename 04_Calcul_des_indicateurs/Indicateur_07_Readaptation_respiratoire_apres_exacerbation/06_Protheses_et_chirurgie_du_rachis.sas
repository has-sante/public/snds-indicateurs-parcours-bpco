/*	******************************************************************************************************************************************	*/
/*																																				*/
/*		Indicateur 07 -																															*/
/*		Patients ayant eu au moins un acte CCAM de prise en charge d�une proth�se de hanche cod� lors d�un s�jour hospitalier MCO termin� 		*/
/*		dans les 90 jours pr�c�dant la date index et dans les 90 jours suivant la date index.													*/
/*		Patients ayant eu au moins un acte CCAM de prise en charge d�une proth�se de genou cod� lors d�un s�jour hospitalier MCO termin� 		*/
/*		dans les 90 jours pr�c�dant la date index et dans les 90 jours suivant la date index 													*/
/*		Patients ayant eu au moins un acte CCAM de prise en charge de la chirurgie du rachis cod� lors d�un s�jour hospitalier MCO termin� 		*/
/*		dans les 90 jours pr�c�dant la date index et dans les 90 jours suivant la date index 													*/
/*																																				*/
/*	******************************************************************************************************************************************	*/

*	******************************************************************************************************************************************	;
*	On rep�re les patients avec au moins un acte CCAM de prise en charge d�une proth�se de hanche cod� lors d�un s�jour hospitalier MCO termin�
*	dans les 90 jours pr�c�dant la date index et dans les 90 jours suivant la date index;

proc sql undo_policy = none;

	CREATE TABLE prothese_hanche AS
		SELECT DISTINCT
			a.BEN_IDT_ANO,
			1 AS Prothese_Hanche length = 3
		FROM res.T_INDI_BPCO_RR_EA_&an_N. a
			INNER JOIN travail.prothese_chir_rachis_&annee_1N._&annee_N1. b
				ON a.BEN_IDT_ANO = b.BEN_IDT_ANO
		WHERE reperage = 8 AND (a.BPCO_RR_EA_Date_index - 90) <= b.date_fin <= (a.BPCO_RR_EA_Date_index + 90)
		ORDER BY a.BEN_IDT_ANO;

quit;

* On ins�re l information dans la table de r�sultats;

data res.T_INDI_BPCO_RR_EA_&an_N.;
	merge	res.T_INDI_BPCO_RR_EA_&an_N. (in = a)
			prothese_hanche (in = b);
	by BEN_IDT_ANO;
	if Prothese_Hanche = . then
		Prothese_Hanche = 0;
run;

proc delete data = prothese_hanche;
run; quit;

*	******************************************************************************************************************************************	;
*	On rep�re les patients avec au moins un acte CCAM de prise en charge d�une proth�se de genou cod� lors d�un s�jour hospitalier MCO termin�
*	dans les 90 jours pr�c�dant la date index et dans les 90 jours suivant la date index;

proc sql undo_policy = none;

	CREATE TABLE Prothese_Genou AS
		SELECT DISTINCT
			a.BEN_IDT_ANO,
			1 AS Prothese_Genou length = 3
		FROM res.T_INDI_BPCO_RR_EA_&an_N. a
			INNER JOIN travail.prothese_chir_rachis_&annee_1N._&annee_N1. b
				ON a.BEN_IDT_ANO = b.BEN_IDT_ANO
		WHERE reperage = 9 AND (a.BPCO_RR_EA_Date_index - 90) <= b.date_fin <= (a.BPCO_RR_EA_Date_index + 90)
		ORDER BY a.BEN_IDT_ANO;

quit;

* On ins�re l information dans la table de r�sultats;

data res.T_INDI_BPCO_RR_EA_&an_N.;
	merge	res.T_INDI_BPCO_RR_EA_&an_N. (in = a)
			Prothese_Genou (in = b);
	by BEN_IDT_ANO;
	if Prothese_Genou = . then
		Prothese_Genou = 0;
run;

proc delete data = Prothese_Genou;
run; quit;

*	******************************************************************************************************************************************	;
*	On rep�re les patients avec au moins un acte CCAM de prise en charge de la chirurgie du rachis cod� lors d�un s�jour hospitalier MCO termin�
*	dans les 90 jours pr�c�dant la date index et dans les 90 jours suivant la date index;

proc sql undo_policy = none;

	CREATE TABLE Chir_Rachis AS
		SELECT DISTINCT
			a.BEN_IDT_ANO,
			1 AS Chir_Rachis length = 3
		FROM res.T_INDI_BPCO_RR_EA_&an_N. a
			INNER JOIN travail.prothese_chir_rachis_&annee_1N._&annee_N1. b
				ON a.BEN_IDT_ANO = b.BEN_IDT_ANO
		WHERE reperage = 10 AND (a.BPCO_RR_EA_Date_index - 90) <= b.date_fin <= (a.BPCO_RR_EA_Date_index + 90)
		ORDER BY a.BEN_IDT_ANO;

quit;

* On ins�re l information dans la table de r�sultats;

data res.T_INDI_BPCO_RR_EA_&an_N.;
	merge	res.T_INDI_BPCO_RR_EA_&an_N. (in = a)
			Chir_Rachis (in = b);
	by BEN_IDT_ANO;
	if Chir_Rachis = . then
		Chir_Rachis = 0;
run;

proc delete data = Chir_Rachis;
run; quit;
