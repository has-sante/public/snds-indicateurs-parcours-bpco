/*	******************************************************************************************************************************************	*/
/*																																				*/
/*		Indicateur 07 -																															*/
/*		Patients ayant eu au moins un GHM ou acte CCAM de pontage coronarien cod� lors d�un s�jour hospitalier en MCO termin� dans 				*/
/*		les 90 jours pr�c�dant la date index et dans les 90 jours suivant la date index															*/
/*		Patients ayant eu au moins un GHM ou acte CCAM de remplacement de valve cardiaque cod� lors d�un s�jour hospitalier en MCO 				*/
/*		termin� dans les 90 jours pr�c�dant la date index et dans les 90 jours suivant la date index											*/
/*		Patients ayant eu au moins un GHM ou acte CCAM de proth�se vasculaire (stents) cod� lors d�un s�jour hospitalier en MCO 				*/
/*		termin� dans les 90 jours pr�c�dant la date index et dans les 90 jours suivant la date index											*/
/*																							 													*/
/*																																				*/
/*	******************************************************************************************************************************************	*/

*	******************************************************************************************************************************************	;
*	On rep�re les patients avec au moins un GHM ou acte CCAM de pontage coronarien cod� lors d�un s�jour hospitalier en MCO termin� 
*	dans les 90 jours pr�c�dant la date index et dans les 90 jours suivant la date index;

proc sql undo_policy = none;

	CREATE TABLE Pontage_Coro AS
		SELECT DISTINCT
			a.BEN_IDT_ANO,
			1 AS Pontage_Coro length = 3
		FROM res.T_INDI_BPCO_RR_EA_&an_N. a
			INNER JOIN travail.insuf_coro_aortique_&annee_N._&annee_N1. b
				ON a.BEN_IDT_ANO = b.BEN_IDT_ANO
		WHERE reperage = 28 AND (a.BPCO_RR_EA_Date_index - 90) <= b.date_fin <= (a.BPCO_RR_EA_Date_index + 90)
		ORDER BY a.BEN_IDT_ANO;

quit;

* On ins�re l information dans la table de r�sultats;

data res.T_INDI_BPCO_RR_EA_&an_N.;
	merge	res.T_INDI_BPCO_RR_EA_&an_N. (in = a)
			Pontage_Coro (in = b);
	by BEN_IDT_ANO;
	if Pontage_Coro = . then
		Pontage_Coro = 0;
run;

proc delete data = Pontage_Coro;
run; quit;

*	******************************************************************************************************************************************	;
*	On rep�re les patients ayant eu au moins un GHM ou acte CCAM de remplacement de valve cardiaque cod� lors d�un s�jour hospitalier en MCO
*	termin� dans les 90 jours pr�c�dant la date index et dans les 90 jours suivant la date index;

proc sql undo_policy = none;

	CREATE TABLE Rempla_Valve_card AS
		SELECT DISTINCT
			a.BEN_IDT_ANO,
			1 AS Rempla_Valve_card length = 3
		FROM res.T_INDI_BPCO_RR_EA_&an_N. a
			INNER JOIN travail.insuf_coro_aortique_&annee_N._&annee_N1. b
				ON a.BEN_IDT_ANO = b.BEN_IDT_ANO
		WHERE reperage = 29 AND (a.BPCO_RR_EA_Date_index - 90) <= b.date_fin <= (a.BPCO_RR_EA_Date_index + 90)
		ORDER BY a.BEN_IDT_ANO;

quit;

* On ins�re l information dans la table de r�sultats;

data res.T_INDI_BPCO_RR_EA_&an_N.;
	merge	res.T_INDI_BPCO_RR_EA_&an_N. (in = a)
			Rempla_Valve_card (in = b);
	by BEN_IDT_ANO;
	if Rempla_Valve_card = . then
		Rempla_Valve_card = 0;
run;

proc delete data = Rempla_Valve_card;
run; quit;

*	******************************************************************************************************************************************	;
*	On rep�re les patients ayant eu au moins un GHM ou acte CCAM de proth�se vasculaire (stents) cod� lors d�un s�jour hospitalier en MCO
*	termin� dans les 90 jours pr�c�dant la date index et dans les 90 jours suivant la date index;

proc sql undo_policy = none;

	CREATE TABLE Stent AS
		SELECT DISTINCT
			a.BEN_IDT_ANO,
			1 AS Stent length = 3
		FROM res.T_INDI_BPCO_RR_EA_&an_N. a
			INNER JOIN travail.insuf_coro_aortique_&annee_N._&annee_N1. b
				ON a.BEN_IDT_ANO = b.BEN_IDT_ANO
		WHERE reperage = 30 AND (a.BPCO_RR_EA_Date_index - 90) <= b.date_fin <= (a.BPCO_RR_EA_Date_index + 90)
		ORDER BY a.BEN_IDT_ANO;

quit;

* On ins�re l information dans la table de r�sultats;

data res.T_INDI_BPCO_RR_EA_&an_N.;
	merge	res.T_INDI_BPCO_RR_EA_&an_N. (in = a)
			Stent (in = b);
	by BEN_IDT_ANO;
	if Stent = . then
		Stent = 0;
run;

proc delete data = Stent;
run; quit;
