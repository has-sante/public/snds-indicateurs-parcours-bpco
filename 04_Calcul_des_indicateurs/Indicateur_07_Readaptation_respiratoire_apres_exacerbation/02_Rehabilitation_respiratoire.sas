/*	******************************************************************************************************************************************	*/
/*																																				*/
/*		Indicateur 07 : R�habilitation respiratoire															 									*/
/*																																				*/
/*	******************************************************************************************************************************************	*/

*	******************************************************************************************************************************************;
*	On rep�re les patients avec un s�jour en SSR dans les 90 jours suivant la date index + info sur les UM 54 et les CM04;

proc sql;

	CREATE TABLE Sej_SSR_90j AS
		SELECT DISTINCT
			a.BEN_IDT_ANO,
			1 AS Sej_SSR length = 3,
			MIN(b.date_debut) AS Date_RR_SSR format ddmmyy10. length = 4,
			MAX(CASE	WHEN b.CM_04 = 1 THEN 1
						ELSE 0
						END) AS Sej_CM04 length = 3,
			MAX(CASE	WHEN b.type_UM = "54" THEN 1
						ELSE 0
						END) AS Sej_UM54 length = 3,
			MAX(CASE	WHEN b.CM_04 = 1 AND b.type_UM = "54" THEN 1
						ELSE 0
						END) AS Sej_CM04_UM54 length = 3,
			MAX(CASE	WHEN b.type_UM = "50" THEN 1
						ELSE 0
						END) AS Sej_UM50 length = 3,
			MAX(CASE	WHEN CM_04 = 1 AND b.type_UM = "50" THEN 1
						ELSE 0
						END) AS Sej_CM04_UM50 length = 3,
			MAX(CASE	WHEN CCAM = 1 OR CSARR = 1 THEN 1
						ELSE 0
						END) AS Sej_CSAR_CCAM length = 3,
			MAX(CASE	WHEN CM_04 = 1 AND (CCAM = 1 OR CSARR = 1) THEN 1
						ELSE 0
						END) AS Sej_CM04_CSAR_CCAM length = 3,
			MAX(CASE	WHEN b.type_UM = "50" AND (CCAM = 1 OR CSARR = 1) THEN 1
						ELSE 0
						END) AS Sej_UM50_CSAR_CCAM length = 3,
			MAX(CASE	WHEN b.CM_04 = 1 AND b.type_UM = "50" AND (CCAM = 1 OR CSARR = 1) THEN 1
						ELSE 0
						END) AS SEJ_CM04_UM50_CSAR_CCAM length = 3
		FROM res.T_INDI_BPCO_RR_EA_&an_N. a
			INNER JOIN travail.RR_sejours_SSR_&Annee_1N._&Annee_N1. b
				ON a.BEN_IDT_ANO = b.BEN_IDT_ANO
		WHERE a.BPCO_RR_EA_Date_index <= b.date_debut <= (a.BPCO_RR_EA_Date_index + 90)
		GROUP BY 1;

quit;
 
*	******************************************************************************************************************************************;
*	On rep�re les patient avec une RR kin�sith�rapique respiratoire pour les patients atteints de handicap respiratoire dans les 90 jours 
	suivant la date index;

proc sql;

	CREATE TABLE RR_kine_90j AS
		SELECT
			a.BEN_IDT_ANO,
			MIN(b.Date_RR) AS Date_RR_kine format ddmmyy10. length = 4,			
			MAX(CASE	WHEN b.type_reperage = "AMK AMC 28" THEN 1
						ELSE 0
						END) AS KINE_AMK_AMC_28 length = 3,
			MAX(CASE	WHEN b.type_reperage = "AMK AMC 20" THEN 1
						ELSE 0
						END) AS KINE_AMK_AMC_20 length = 3,
			MAX(CASE	WHEN b.KINE_BPC = 1 THEN 1
						ELSE 0
						END) AS KINE_BPC,
			MAX(CASE	WHEN b.KINE_AMK_AMC_AMS_8 = 1 THEN 1
						ELSE 0
						END) AS KINE_AMK_AMC_AMS_8,
			MAX(CASE	WHEN b.KINE_AMK_13_5 = 1 THEN 1
						ELSE 0
						END) AS KINE_AMS_AMC_AMK_13_5,
			MAX(CASE	WHEN  b.KINE_AMK_AMC_AMS_9_5_8 = 1 THEN 1
						ELSE 0
						END) AS KINE_AMK_AMC_AMS_9_5_8,
			MAX(CASE	WHEN b.KINE_AMK_AMC_AMS_9_5_4 = 1 THEN 1
						ELSE 0
						END) AS KINE_AMK_AMC_AMS_9_5_4
		FROM res.T_INDI_BPCO_RR_EA_&an_N. a
			INNER JOIN travail.reperage_RR_&Annee_1N._&Annee_N1. b
				ON a.BEN_IDT_ANO = b.BEN_IDT_ANO
		WHERE a.BPCO_RR_EA_Date_index <= b.Date_RR <= (a.BPCO_RR_EA_Date_index + 90)
		GROUP BY a.BEN_IDT_ANO;

quit;

* On r�cup�re le lieu de la RR pour la RR kin�sith�rapique respiratoire;
proc sql;

	* On fait un max car si DCIR + PMSI, c est un doublon mal identifi�, donc la RR a lieu dans un etb;
	CREATE TABLE lieu_RR AS
		SELECT
			a.BEN_IDT_ANO,
			MAX(b.Lieu_RR) AS Lieu_RR length = 3
		FROM RR_kine_90j a
			INNER JOIN travail.reperage_RR_&Annee_1N._&Annee_N1. b
				ON a.BEN_IDT_ANO = b.BEN_IDT_ANO
				AND a.Date_RR_kine = b.Date_RR
		GROUP BY a.BEN_IDT_ANO;

quit;

data RR_kine_90j;
	merge	RR_kine_90j
			lieu_RR;
	by BEN_IDT_ANO;
run;

proc delete data = lieu_RR;
run; quit;

*	******************************************************************************************************************************************;
*	On r�cup�re la date minimale et le lieu associ� de la premi�re RR;

data premiere_RR_90j;
	merge	Sej_SSR_90j (in = a)
			RR_kine_90j (in = b);
	by BEN_IDT_ANO;
	if Date_RR_SSR ne . then
		do;
			Date_RR = Date_RR_SSR;
			Lieu_RR = 3;
		end;
	else
		Date_RR = Date_RR_kine;
	format date_RR ddmmyy10.;
run;

*	******************************************************************************************************************************************;
*	On ajoute l information dans la table de r�sultats;

proc sort data = res.T_INDI_BPCO_RR_EA_&an_N.;
	by BEN_IDT_ANO;
run;

data res.T_INDI_BPCO_RR_EA_&an_N.;
	merge	res.T_INDI_BPCO_RR_EA_&an_N. (in = a)
			premiere_RR_90j (in = b);
	by BEN_IDT_ANO;
	array v Sej_: KINE_:;
	do over v;
		if v = . then
			v = 0;
	end;
	length BPCO_RR_EA_OBS 3.;
	BPCO_RR_EA_OBS = 0;
	if Date_RR ne . then
		BPCO_RR_EA_OBS = 1;
	if a then
		output;
run;

proc delete data = Sej_SSR_90j RR_kine_90j premiere_RR_90j;
run; quit;

*	******************************************************************************************************************************************;
*	On ajoute l information de la date de la RR pr�c�dente;

* RR en SSR;
proc sql;

	CREATE TABLE Sej_SSR_prec AS
		SELECT DISTINCT
			a.BEN_IDT_ANO,
			MAX(b.date_debut) AS Date_RR_SSR format ddmmyy10. length = 4
		FROM res.T_INDI_BPCO_RR_EA_&an_N. a
			INNER JOIN travail.RR_sejours_SSR_&Annee_1N._&Annee_N1. b
				ON a.BEN_IDT_ANO = b.BEN_IDT_ANO
		WHERE (a.BPCO_RR_EA_Date_index - 365) <= b.date_debut < a.BPCO_RR_EA_Date_index
		GROUP BY a.BEN_IDT_ANO;

quit;

* RR en kin�sith�rapique respiratoire;
proc sql;

	CREATE TABLE RR_kine_prec AS
		SELECT
			a.BEN_IDT_ANO,
			MAX(b.Date_RR) AS Date_RR_kine format ddmmyy10. length = 4
		FROM res.T_INDI_BPCO_RR_EA_&an_N. a
			INNER JOIN travail.reperage_RR_&Annee_1N._&Annee_N1. b
				ON a.BEN_IDT_ANO = b.BEN_IDT_ANO
		WHERE (a.BPCO_RR_EA_Date_index - 365) <= b.Date_RR < a.BPCO_RR_EA_Date_index
		GROUP BY a.BEN_IDT_ANO;

quit;

*	On r�cup�re la date de la derni�re RR avant inclusion (priorit� sur la date SSR);
data RR_precedente;
	merge	Sej_SSR_prec (in = a)
			RR_kine_prec (in = b);
	by BEN_IDT_ANO;
	length Date_RR_prec 4.;
	if Date_RR_SSR ne . then
		Date_RR_prec = Date_RR_SSR;
	else
		Date_RR_prec = Date_RR_kine;
	format Date_RR_prec ddmmyy10.;
run;

proc delete data = Sej_SSR_prec RR_kine_prec;
run;

proc sort data = res.T_INDI_BPCO_RR_EA_&an_N.;
	by BEN_IDT_ANO;
run;

proc sort data = RR_precedente;
	by BEN_IDT_ANO;
run;

data res.T_INDI_BPCO_RR_EA_&an_N.;
	merge	res.T_INDI_BPCO_RR_EA_&an_N. (in = a)
			RR_precedente (in = b);
	by BEN_IDT_ANO;
	if a then
		output;
run;

proc delete data = RR_precedente;
run; quit;

proc sql;

	SELECT COUNT(*), COUNT(DISTINCT BEN_IDT_ANO)
	FROM res.T_INDI_BPCO_RR_EA_&an_N.;

quit;
