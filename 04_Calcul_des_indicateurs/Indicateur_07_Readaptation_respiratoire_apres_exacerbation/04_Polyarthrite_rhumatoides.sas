/*	******************************************************************************************************************************************	*/
/*																																				*/
/*		Indicateur 07 - Patient pris en charge pour polyarthrite rhumato�des et maladies apparent�es											*/
/*		- 	Patients en ALD arthrite rhumato�de active 90 jours apr�s la date index																*/
/*		-	et/ou patients ayant eu au moins un diagnostic d�arthrite rhumato�de cod� (DP ou DR du s�jour) lors d�un s�jour hospitalier en 		*/ 
/*			MCO termin� entre le 1er janvier de l ann�e N-4 et 90 jours apr�s la date index														*/
/*		-	et/ou patients ayant eu au moins un diagnostic d�arthrite rhumato�de cod� comme complication ou morbidit� associ�e (DAS, ou DP 		*/
/*			ou DR des UM) lors d�un s�jour hospitalier MCO termin� entre 275 jours avant la date index et 90 jours apr�s la date index			*/
/*		-	et/ou patients ayant eu au moins un diagnostic d�arthrite rhumato�de cod� (MMP, AE des RHS ou DAS) lors d�un s�jour hospitalier		*/
/*			en SSR entre 275 jours avant la date index et 90 jours apr�s la date index.															*/
/*																																				*/
/*	******************************************************************************************************************************************	*/

*	******************************************************************************************************************************************;
*	On rep�re les patients avec une ALD arthrite rhumato�de active 90 jours apr�s la date index;

proc sql undo_policy = none;

	CREATE TABLE ALD_arthrite AS
		SELECT DISTINCT
			a.BEN_IDT_ANO
		FROM res.T_INDI_BPCO_RR_EA_&an_N. a
			INNER JOIN travail.histo_ALD b
				ON a.BEN_IDT_ANO = b.BEN_IDT_ANO
		WHERE b.reperage = 56 AND 
			(a.BPCO_RR_EA_Date_index <= b.date_debut <= (a.BPCO_RR_EA_Date_index + 90)
			OR a.BPCO_RR_EA_Date_index <= b.date_fin <= (a.BPCO_RR_EA_Date_index + 90)
			OR (b.date_debut <= (a.BPCO_RR_EA_Date_index + 90) AND (b.date_fin >= a.BPCO_RR_EA_Date_index + 90 OR b.date_fin = "01JAN1600"d)));

quit;

*	******************************************************************************************************************************************;
*	On rep�re les patients avec au moins un diagnostic d�arthrite rhumato�de cod� (DP ou DR du s�jour) lors d�un s�jour hospitalier en MCO 
*	termin� entre le 1er janvier &annee_4N. et 90 jours apr�s la date index;

proc sql undo_policy = none;

	CREATE TABLE hospit_arthrite1 AS
		SELECT DISTINCT
			a.BEN_IDT_ANO
		FROM res.T_INDI_BPCO_RR_EA_&an_N. a
			INNER JOIN travail.polyarthrite_rhum_&annee_4N._&annee_N1. b
				ON a.BEN_IDT_ANO = b.BEN_IDT_ANO
		WHERE domaine = "MCO" AND variable IN ("DGN_PAL", "DGN_REL") AND table NE "UM" 
			AND "01JAN&annee_4N."d <= b.date_fin <= (a.BPCO_RR_EA_Date_index + 90);

quit;

*	******************************************************************************************************************************************;
*	On rep�re les patients avec au moins un diagnostic d�arthrite rhumato�de cod� comme complication ou morbidit� associ�e (DAS, ou DP ou DR 
*	des UM) lors d�un s�jour hospitalier MCO termin� entre 275 jours avant la date index et 90 jours apr�s la date index;

proc sql undo_policy = none;

	CREATE TABLE hospit_arthrite2 AS
		SELECT DISTINCT
			a.BEN_IDT_ANO
		FROM res.T_INDI_BPCO_RR_EA_&an_N. a
			INNER JOIN travail.polyarthrite_rhum_&annee_4N._&annee_N1. b
				ON a.BEN_IDT_ANO = b.BEN_IDT_ANO
		WHERE domaine = "MCO" AND (variable IN ("DGN_PAL", "DGN_REL") AND table = "UM" OR variable = "ASS_DGN")
			AND (a.BPCO_RR_EA_Date_index - 275) <= b.date_fin <= (a.BPCO_RR_EA_Date_index + 90);

quit;

*	******************************************************************************************************************************************;
*	On rep�re les patients avec au moins un diagnostic d�arthrite rhumato�de cod� (MMP, AE des RHS ou DAS) lors d�un s�jour hospitalier 
*	en SSR entre 275 jours avant la date index et 90 jours apr�s la date index;

proc sql undo_policy = none;

	CREATE TABLE hospit_arthrite3 AS
		SELECT DISTINCT
			a.BEN_IDT_ANO
		FROM res.T_INDI_BPCO_RR_EA_&an_N. a
			INNER JOIN travail.polyarthrite_rhum_&annee_4N._&annee_N1. b
				ON a.BEN_IDT_ANO = b.BEN_IDT_ANO
		WHERE domaine = "SSR" AND
			((a.BPCO_RR_EA_Date_index - 275) <= b.date_debut <= (a.BPCO_RR_EA_Date_index + 90)
			OR (a.BPCO_RR_EA_Date_index - 275) <= b.date_fin <= (a.BPCO_RR_EA_Date_index + 90)
			OR (b.date_debut < (a.BPCO_RR_EA_Date_index - 275) AND b.date_fin > (a.BPCO_RR_EA_Date_index + 90)));

quit;

*	******************************************************************************************************************************************;
*	On concat�ne toutes les informations;

data patients_arthrite;
	set ALD_arthrite hospit_arthrite1 hospit_arthrite2 hospit_arthrite3;
	length Polyarthrite_Rhuma 3.;
	Polyarthrite_Rhuma = 1;
run;

*	******************************************************************************************************************************************;
*	On ins�re l information dans la table de r�sultats;

proc sort data = patients_arthrite nodupkey;
	by BEN_IDT_ANO;
run;

proc sort data = res.T_INDI_BPCO_RR_EA_&an_N.;
	by BEN_IDT_ANO;
run;

data res.T_INDI_BPCO_RR_EA_&an_N.;
	merge	res.T_INDI_BPCO_RR_EA_&an_N. (in = a)
			patients_arthrite (in = b);
	by BEN_IDT_ANO;
	if Polyarthrite_Rhuma = . then
		Polyarthrite_Rhuma = 0;
run;

proc delete data = ALD_arthrite hospit_arthrite1 hospit_arthrite2 hospit_arthrite3 patients_arthrite;
run; quit;
