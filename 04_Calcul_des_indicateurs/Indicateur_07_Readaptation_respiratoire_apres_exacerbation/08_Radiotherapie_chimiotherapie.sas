/*	******************************************************************************************************************************************	*/
/*																																				*/
/*		Indicateur 07 -																															*/
/*		Patients ayant eu au moins une s�ance de radioth�rapie cod� (DP du s�jour) lors d�un s�jour hospitalier en MCO termin� dans les 90		*/
/*		jours pr�c�dant la date index et dans les 90 jours suivant la date index																*/
/*		Patients ayant eu au moins une s�ance de chimioth�rapie cod� (DP du s�jour) lors d�un s�jour hospitalier en MCO termin� dans les 90 	*/
/*		jours pr�c�dant la date index et dans les 90 jours suivant la date index																*/
/*																																				*/
/*	******************************************************************************************************************************************	*/

*	******************************************************************************************************************************************	;
*	On rep�re les patients avec au moins une s�ance de radioth�rapie cod� (DP du s�jour) lors d�un s�jour hospitalier en MCO termin� dans les 90		*/
*	jours pr�c�dant la date index et dans les 90 jours suivant la date index;

proc sql undo_policy = none;

	CREATE TABLE Radiotherapie AS
		SELECT DISTINCT
			a.BEN_IDT_ANO,
			1 AS Radiotherapie length = 3
		FROM res.T_INDI_BPCO_RR_EA_&an_N. a
			INNER JOIN travail.radio_chimio_&annee_1N._&annee_N1. b
				ON a.BEN_IDT_ANO = b.BEN_IDT_ANO
		WHERE reperage = 59 AND (a.BPCO_RR_EA_Date_index - 90) <= b.date_fin <= (a.BPCO_RR_EA_Date_index + 90)
		ORDER BY a.BEN_IDT_ANO;

quit;

* On ins�re l information dans la table de r�sultats;

data res.T_INDI_BPCO_RR_EA_&an_N.;
	merge	res.T_INDI_BPCO_RR_EA_&an_N. (in = a)
			Radiotherapie (in = b);
	by BEN_IDT_ANO;
	if Radiotherapie = . then
		Radiotherapie = 0;
run;

proc delete data = Radiotherapie;
run; quit;

*	******************************************************************************************************************************************	;
*	On rep�re les patients avec au moins une s�ance de chimioth�rapie cod� (DP du s�jour) lors d�un s�jour hospitalier en MCO termin� dans les 90		*/
*	jours pr�c�dant la date index et dans les 90 jours suivant la date index;

proc sql undo_policy = none;

	CREATE TABLE Chimiotherapie AS
		SELECT DISTINCT
			a.BEN_IDT_ANO,
			1 AS Chimiotherapie length = 3
		FROM res.T_INDI_BPCO_RR_EA_&an_N. a
			INNER JOIN travail.radio_chimio_&annee_1N._&annee_N1. b
				ON a.BEN_IDT_ANO = b.BEN_IDT_ANO
		WHERE reperage = 58 AND (a.BPCO_RR_EA_Date_index - 90) <= b.date_fin <= (a.BPCO_RR_EA_Date_index + 90)
		ORDER BY a.BEN_IDT_ANO;

quit;

* On ins�re l information dans la table de r�sultats;

data res.T_INDI_BPCO_RR_EA_&an_N.;
	merge	res.T_INDI_BPCO_RR_EA_&an_N. (in = a)
			Chimiotherapie (in = b);
	by BEN_IDT_ANO;
	if Chimiotherapie = . then
		Chimiotherapie = 0;
run;

proc delete data = Chimiotherapie;
run; quit;
