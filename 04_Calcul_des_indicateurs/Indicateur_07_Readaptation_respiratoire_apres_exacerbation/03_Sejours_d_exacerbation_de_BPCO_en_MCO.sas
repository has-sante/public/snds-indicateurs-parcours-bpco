/*	******************************************************************************************************************************************	*/
/*																																				*/
/*		Indicateur 07 - Nombre de s�jours l ann�e N															 									*/
/*																																				*/
/*	******************************************************************************************************************************************	*/

*	******************************************************************************************************************************************;
*	Nombre de s�jours MCO d�exacerbation de BPCO cod� en DP ou en DAS termin�s l ann�e N;

proc sql;

	CREATE TABLE nb_sejours_exa_BPCO_DP AS
		SELECT
			BEN_IDT_ANO,
			COUNT(DISTINCT id_sejour) AS Nb_Sej_EA_BPCO_DP_MCO length = 3
		FROM travail.sejours_exacerbation_&annee_N.
		WHERE reperage = 16 AND variable = "DGN_PAL"
		GROUP BY BEN_IDT_ANO;

	CREATE TABLE nb_sejours_exa_BPCO_DAS AS
		SELECT
			BEN_IDT_ANO,
			COUNT(DISTINCT id_sejour) AS Nb_Sej_EA_BPCO_DAS_MCO length = 3
		FROM travail.sejours_exacerbation_&annee_N.
		WHERE reperage = 16 AND variable = "ASS_DGN"
		GROUP BY BEN_IDT_ANO;

quit;

*	******************************************************************************************************************************************;
*	On ajoute l'information dans la table res.T_INDI_BPCO_RR_EA_&an_N.;

proc sort data = res.T_INDI_BPCO_RR_EA_&an_N.;
	by BEN_IDT_ANO;
run;

data res.T_INDI_BPCO_RR_EA_&an_N.;
	merge	res.T_INDI_BPCO_RR_EA_&an_N. (in = a)
			nb_sejours_exa_BPCO_DP (in = b)
			nb_sejours_exa_BPCO_DAS (in = c);
	by BEN_IDT_ANO;
	if a;
	if not b then
		Nb_Sej_EA_BPCO_DP_MCO = 0;
	if not c then
		Nb_Sej_EA_BPCO_DAS_MCO = 0;
run;
