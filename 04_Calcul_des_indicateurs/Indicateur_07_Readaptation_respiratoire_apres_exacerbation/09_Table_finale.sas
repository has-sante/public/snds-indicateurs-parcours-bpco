/*	******************************************************************************************************************************************	*/
/*																																				*/
/*		Indicateur 07 : Table finale - Ajout de formats et de labels																			*/
/*																																				*/
/*	******************************************************************************************************************************************	*/

data res.T_INDI_BPCO_RR_EA_&an_N.;
	set res.T_INDI_BPCO_RR_EA_&an_N. (keep = BEN_IDT_ANO BPCO_RR_EA_Date_index BPCO_RR_EA_Sej_Index Finess_PMSI Finess_GEO Dp Dp_classe DA_BPCO
		GHM BPCO_RR_EA_CIBLE Sej_SSR Sej_CM04 Sej_UM54 Sej_UM50 Sej_CM04_UM54 SEJ_CM04_UM50_CSAR_CCAM KINE_AMK_AMC_28 KINE_AMK_AMC_20 KINE_BPC 
		KINE_AMK_AMC_AMS_8 KINE_AMS_AMC_AMK_13_5 KINE_AMK_AMC_AMS_9_5_8 KINE_AMK_AMC_AMS_9_5_4 Sej_CSAR_CCAM Sej_CM04_UM50 Sej_CM04_CSAR_CCAM
		Sej_UM50_CSAR_CCAM BPCO_RR_EA_OBS Nb_Sej_EA_BPCO_DP_MCO Nb_Sej_EA_BPCO_DAS_MCO Date_RR Lieu_RR Date_RR_prec Polyarthrite_Rhuma 
		Spondylarthirte_Anky Prothese_Hanche Prothese_Genou Chir_Rachis Pontage_Coro Rempla_Valve_card Stent Radiotherapie Chimiotherapie);
	label		
		BEN_IDT_ANO = "Num�ro d'individu"
		BPCO_RR_EA_Date_index = "Date index"
		BPCO_RR_EA_Sej_Index = "Identifiant du s�jour index"
		Finess_PMSI = "Finess PMSI ayant r�alis� le s�jour index"
		Finess_GEO = "Finess g�ographique du premier RUM"
		Dp = "Diagnostic principal du s�jour index"
		Dp_classe = "Diagnostic principal du s�jour index"
		DA_BPCO = "BPCO cod� en diagnostic secondaire du s�jour index"
		GHM = "GHM du s�jour index"
		BPCO_RR_EA_CIBLE = "Patient correspondant aux crit�res d'inclusion et d'exclusion de la population cible"
		Sej_SSR = "Patient de la population cible ayant eu un s�jour en SSR dans les 90 jours suivant la date index"
		Sej_CM04 = "Patient ayant eu un s�jour en SSR dans la CM 04 dans les 90 jours suivant la date index"
		Sej_UM54 = "Patient de la population cible ayant eu un s�jour en SSR et pris en charge dans une UM sp�cialis�e � Affections respiratoires � dans les 90 jours suivant la date index"
		Sej_UM50 = "Patient de la population cible ayant eu un s�jour en SSR et pris en charge dans une UM sp�cialis�e � SSR indiff�renci�s ou polyvalents � dans les 90 jours suivant la date index"
		SEJ_CM04_UM54 = "Patient ayant eu un s�jour en SSR dans la CM 04 et pris en charge dans une unit� m�dicale sp�cialis�e � Affections respiratoires � dans les 90 jours suivant la date index"
		SEJ_CM04_UM50_CSAR_CCAM = "Patient ayant eu un s�jour en SSR dans la CM 04 et ayant eu un s�jour en SSR dans une unit� m�dicale � SSR indiff�renci�s ou polyvalents � avec au moins un acte CSARR ou CCAM de la liste de RR dans les 90 jours suivant la date index"
		KINE_AMK_AMC_28 = "R�adaptation respiratoire kin�sith�rapique respiratoire pour les patients atteints de handicap respiratoire dans les 90 jours suivant la date index"
		KINE_AMK_AMC_20 = "R�adaptation respiratoire kin�sith�rapique respiratoire pour les patients atteints de handicap respiratoire chronique en prise en charge de groupe de 2 � 4 personnes avec r��ducation respiratoire en individuel dans les 90 jours suivant la date index"
		KINE_AMK_AMC_AMS_9_5_8 = "R�adaptation respiratoire cod�e en coefficient 9.5 avec des coefficients 8 dans les 90 jours suivant la date index"
		KINE_AMK_AMC_AMS_9_5_4 = "R�adaptation respiratoire cod�e en coefficient 9.5 avec des coefficients 8/2 (r�alis�s le m�me jour et les actes doivent �tre r�alis� tous les 2 soit en ville, soit en MCO, soit en SSR) dans les 90 jours suivant la date index"
		KINE_AMK_AMC_AMS_8 = "Patient de la population cible ayant b�n�fici� d'une r�adaptation respiratoire kin�sith�rapique respiratoire cod� en coefficient 8 dans les 90 jours suivant la date index"
		KINE_AMS_AMC_AMK_13_5 = "R��ducation respiratoire et motrice cod� avec le coefficient 13.5 dans les 90 jours suivant la date index"
		KINE_BPC = "R��ducation respiratoire et motrice cod� avec l'acte BPC dans les 90 jours suivant la date index"
		Sej_CSAR_CCAM = "Patient de la population cible ayant eu un s�jour en SSR au moins un acte CSARR ou CCAM de la liste de RR dans les 90 jours suivant la date index"
		SEJ_CM04_UM50 = "Patient ayant eu un s�jour en SSR dans la CM 04 et pris en charge dans une unit� m�dicale sp�cialis�e � SSR indiff�renci�s ou polyvalents � dans les 90 jours suivant la date index"
		Sej_CM04_CSAR_CCAM = "Patient ayant eu un s�jour en SSR dans la CM 04 avec au moins un acte CSARR ou CCAM de la liste de RR dans les 90 jours suivant la date index"
		Sej_UM50_CSAR_CCAM = "Patient de la population cible ayant eu un s�jour en SSR dans une UM � Soins de suite et de r�adaptation indiff�renci�s ou polyvalents � avec au moins un acte CSARR ou CCAM de la liste de RR dans les 90 jours suivant la date index"
		BPCO_RR_EA_OBS = "Patient de la population cible ayant b�n�fici� d'une r�adaptation respiratoire dans les 90 jours suivant la date index"
		Nb_Sej_EA_BPCO_DP_MCO = "Nombre de s�jours MCO d'exacerbation de BPCO cod� en DP termin�s en &Annee_N."
		Nb_Sej_EA_BPCO_DAS_MCO = "Nombre de s�jours MCO d'exacerbation de BPCO cod� en DAS termin�s en &Annee_N."
		Date_RR = "Date de r�alisation de la r�adaptation respiratoire dans les 90 jours suivant la sortie du s�jour index"
		Lieu_RR = "Lieu de r�alisation de la r�adaptation respiratoire dans les 90 jours suivant la sortie du s�jour index"
		Date_RR_prec = "Date de la derni�re RR dans les 365 jours pr�c�dant la date d'entr�e du s�jour index"
		Polyarthrite_Rhuma = "Patient pris en charge pour polyarthrite rhumato�des et maladies apparent�es"
		Spondylarthirte_Anky = "Patient pris en charge pour spondylarthrite ankylosante et maladies apparent�es"
		Prothese_Hanche = "Patient ayant eu au moins un acte CCAM de prise en charge d'une proth�se de hanche"
		Prothese_Genou = "Patient ayant eu au moins un acte CCAM de prise en charge d'une proth�se de genou"
		Chir_Rachis = "Patient ayant eu au moins un acte CCAM de prise en charge de la chirurgie du rachis"
		Pontage_Coro = "Patient ayant eu au moins un diagnostic de pontage coronarien"
		Rempla_Valve_card = "Patient ayant eu au moins un diagnostic de remplacement de valve cardiaque"
		Stent = "Patient ayant eu au moins un diagnostic de proth�se vasculaire (stents)"
		Radiotherapie = "Patient ayant eu au moins une s�ance de radioth�rapie"
		Chimiotherapie = "Patient ayant eu au moins une s�ance de chimioth�rapie"
		;
	format DA_BPCO BPCO_RR_EA_CIBLE Sej_CM04 Sej_UM54 Sej_UM50 Sej_CM04_UM54 SEJ_CM04_UM50_CSAR_CCAM KINE_AMK_AMC_28 KINE_AMK_AMC_20 KINE_BPC 
		KINE_AMK_AMC_AMS_8 KINE_AMS_AMC_AMK_13_5 KINE_AMK_AMC_AMS_9_5_8 KINE_AMK_AMC_AMS_9_5_4 Sej_CSAR_CCAM Sej_CM04_UM50 Sej_CM04_CSAR_CCAM
		Sej_UM50_CSAR_CCAM BPCO_RR_EA_OBS Polyarthrite_Rhuma Spondylarthirte_Anky Prothese_Hanche Prothese_Genou Chir_Rachis Pontage_Coro 
		Rempla_Valve_card Stent Radiotherapie Chimiotherapie SEJ_SSR f_oui_non. Dp_Classe f_DP_classe. BPCO_RR_EA_Date_index Date_RR 
		Date_RR_prec date9. Lieu_RR f_lieu_soin.;
run;

*	******************************************************************************************************************************************;
* 	Nombre de r�adaptation respiratoire dans les 90 jours apr�s hospitalisation;

proc sql;

	INSERT INTO flowch.Flow_Chart_BPCO_RR_EA_&an_N.
		SELECT
			19,
			COUNT(DISTINCT BEN_IDT_ANO)
	FROM res.T_INDI_BPCO_RR_EA_&an_N.
	WHERE BPCO_RR_EA_OBS = 1;

	SELECT Nb_patients_ref0 into : N0 FROM travail.ref_flowcharts;

	* Population d �tude;
	SELECT N into : N12 FROM flowch.Flow_Chart_BPCO_RR_EA_&an_N. WHERE indicateur = 12;
	* Population cible;
	SELECT N into : N18 FROM flowch.Flow_Chart_BPCO_RR_EA_&an_N. WHERE indicateur = 18;

quit;

data flowch.Flow_Chart_BPCO_RR_EA_&an_N.;
	set flowch.Flow_Chart_BPCO_RR_EA_&an_N.;
	if indicateur in (1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12) then
		percent = N / &N0.;
	if indicateur in (13, 14, 15, 16, 18) then
		percent = N / &N12.;
	if indicateur = 19 then
		percent = N / &N18.;
	format indicateur f_ind_07_flowchart. percent percent7.1;		
run;

* V�rif;
proc sql;

	SELECT COUNT(*) AS nb_lignes, COUNT(DISTINCT BEN_IDT_ANO) AS nb_patients, COUNT(DISTINCT BPCO_RR_EA_Sej_Index) AS nb_sejours
	FROM res.T_INDI_BPCO_RR_EA_&an_N.;
	* nb_lignes = nb_sejours : OK;

	SELECT COUNT(*) AS nb_lignes, COUNT(DISTINCT BEN_IDT_ANO) AS nb_patients, COUNT(DISTINCT BPCO_RR_EA_Sej_Index) AS nb_sejours
	FROM res.T_INDI_BPCO_RR_EA_&an_N.
	WHERE BPCO_RR_EA_OBS = 1;
	* nb_lignes = nb_sejours : OK;

quit;

*	******************************************************************************************************************************************;
*	On ajoute le flag BPCO_RR_EA_CIBLE dans la table res.T_INDI_BPCO_&an_N.;

proc sql;

	ALTER TABLE res.T_INDI_BPCO_&an_N. DROP BPCO_RR_EA_CIBLE;

	ALTER TABLE res.T_INDI_BPCO_&an_N. ADD BPCO_RR_EA_CIBLE INT format = f_oui_non. length = 3
		label = "Patient appartenant � la population cible BPCO_RR_EA";

	UPDATE res.T_INDI_BPCO_&an_N.
		SET BPCO_RR_EA_CIBLE = CASE	WHEN BEN_IDT_ANO IN (SELECT DISTINCT BEN_IDT_ANO FROM res.T_INDI_BPCO_RR_EA_&an_N.) THEN 1
									ELSE 0
									END;

quit;

data res.T_INDI_BPCO_&an_N.;
	set res.T_INDI_BPCO_&an_N.;
	if BPCO_RR_EA_CIBLE = .
		then BPCO_RR_EA_CIBLE = 0;
run;
