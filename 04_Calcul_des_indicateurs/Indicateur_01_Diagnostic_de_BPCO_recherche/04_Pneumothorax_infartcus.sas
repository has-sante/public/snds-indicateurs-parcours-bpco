/*	******************************************************************************************************************************************	*/
/*																																				*/
/*		Indicateur 01 : Pneumothorax ou infarctus															 									*/
/*																																				*/
/*	******************************************************************************************************************************************	*/

%macro pneumo_infar(time=);

	*** D�finition des bornes basses et bornes hautes pour les rep�rages;
	* time = 1 : Avant ou apr�s inclusion;
	%if &time. = 1 %then
		%do;

			%let bb = BPCO_DG_Date_index - 365;
			%let bh = BPCO_DG_Date_index + 365;
			%let suf = ;

		%end;

	* time = 2 : Avant inclusion;
	%if &time. = 2 %then
		%do;

			%let bb = BPCO_DG_Date_index - 365;
			%let bh = BPCO_DG_Date_index;
			%let suf = _AV_365;

		%end;

	* time = 3 : Apr�s inclusion;
	%if &time. = 3 %then
		%do;

			%let bb = BPCO_DG_Date_index + 1;
			%let bh = BPCO_DG_Date_index + 365;
			%let suf = _AP_365;

		%end;

	*	**************************************************************************************************************************************;
	*	On rep�re les patients avec une pneumothorax dans les 365 jours avant ou suivant la date index;

	proc sql;

		CREATE TABLE pneumothorax AS
			SELECT DISTINCT
				a.BEN_IDT_ANO
			FROM res.T_INDI_BPCO_DG_&an_N. a
				INNER JOIN travail.pneumothorax_infarctus_&Annee_1N._&annee_N1. b
					ON a.BEN_IDT_ANO = b.BEN_IDT_ANO
			WHERE b.reperage = 11 AND b.date_debut <= a.&bh. AND b.date_fin >= a.&bb.;

	quit;

	* On ajoute l information dans la table de r�sultats;

	proc sort data = res.T_INDI_BPCO_DG_&an_N.;
		by BEN_IDT_ANO;
	run;

	proc sort data = pneumothorax;
		by BEN_IDT_ANO;
	run;

	data res.T_INDI_BPCO_DG_&an_N.;
		merge	res.T_INDI_BPCO_DG_&an_N. (in = a)
				pneumothorax (in = b);
		by BEN_IDT_ANO;
		length pneumothorax&suf. 3.;
		pneumothorax&suf. = 0;
		if b then
			pneumothorax&suf. = 1;			
		if a then
			output;
	run;

	proc delete data = pneumothorax;
	run; quit;

	*	**************************************************************************************************************************************;
	*	On rep�re les patients avec un infarctus dans les 365 jours avant ou suivant la date index;

	proc sql;

		CREATE TABLE infarctus AS
			SELECT DISTINCT
				a.BEN_IDT_ANO
			FROM res.T_INDI_BPCO_DG_&an_N. a
				INNER JOIN travail.pneumothorax_infarctus_&Annee_1N._&annee_N1. b
					ON a.BEN_IDT_ANO = b.BEN_IDT_ANO
			WHERE b.reperage = 12 AND b.date_debut <= a.&bh. AND b.date_fin >= a.&bb.;

	quit;

	* On ajoute l information dans la table de r�sultats;

	proc sort data = res.T_INDI_BPCO_DG_&an_N.;
		by BEN_IDT_ANO;
	run;

	proc sort data = infarctus;
		by BEN_IDT_ANO;
	run;

	data res.T_INDI_BPCO_DG_&an_N.;
		merge	res.T_INDI_BPCO_DG_&an_N. (in = a)
				infarctus (in = b);
		by BEN_IDT_ANO;
		length infarctus&suf. 3.;
		infarctus&suf. = 0;
		if b then
			infarctus&suf. = 1;			
		if a then
			output;
	run;

	proc delete data = infarctus;
	run; quit;

%mend pneumo_infar;

* Avant ou apr�s inclusion : time = 1;
%pneumo_infar (time = 1);

* Avant inclusion : time = 2;
%pneumo_infar (time = 2);

* Apr�s inclusion : time = 3;
%pneumo_infar (time = 3);
