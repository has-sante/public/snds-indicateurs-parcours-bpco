/*	******************************************************************************************************************************************	*/
/*																																				*/
/*		Indicateur 01 : EFR ou Spirom�tries																	 									*/
/*																																				*/
/*	******************************************************************************************************************************************	*/

%macro EFR_Spiro(time=);

	*** D�finition des bornes basses et bornes hautes pour les rep�rages;
	* time = 1 : Avant ou apr�s inclusion;
	%if &time. = 1 %then
		%do;

			%let bb = BPCO_DG_Date_index - 365;
			%let bh = BPCO_DG_Date_index + 365;
			%let suf = ;

		%end;

	* time = 2 : Avant inclusion;
	%if &time. = 2 %then
		%do;

			%let bb = BPCO_DG_Date_index - 365;
			%let bh = BPCO_DG_Date_index;
			%let suf = _AV_365;

		%end;

	* time = 3 : Apr�s inclusion;
	%if &time. = 3 %then
		%do;

			%let bb = BPCO_DG_Date_index + 1;
			%let bh = BPCO_DG_Date_index + 365;
			%let suf = _AP_365;

		%end;

	*	**************************************************************************************************************************************;
	*	On rep�re les patients avec une spirom�trie dans les 365 jours avant ou suivant la date index;

	proc sql;

		CREATE TABLE spirometrie AS
			SELECT DISTINCT
				a.BEN_IDT_ANO,
				MIN(b.date_debut) AS date_debut length = 4,
				MIN(CASE	WHEN b.date_debut < (a.&bb.) THEN a.&bb.
							ELSE b.date_debut
							END) AS Date_Spiro&suf. length = 4,
				COUNT(DISTINCT b.date_debut) AS Nb_spiro&suf. length = 3
			FROM res.T_INDI_BPCO_DG_&an_N. a
				INNER JOIN travail.reperage_EFR_spiro_&Annee_2N._&annee_N1. b
					ON a.BEN_IDT_ANO = b.BEN_IDT_ANO
			WHERE b.reperage = 2 AND b.date_debut <= a.&bh. AND b.date_fin >= a.&bb.
			GROUP BY a.BEN_IDT_ANO;

		* On fait un max car si DCIR + PMSI, c est un doublon donc la spiro a lieu dans un etb;
		CREATE TABLE lieu_spiro AS
			SELECT DISTINCT
				a.BEN_IDT_ANO,
				MAX(CASE	WHEN b.domaine = "" THEN 1
							WHEN b.domaine = "MCO" THEN 2
							WHEN b.domaine = "SSR" THEN 3
							END) AS Lieu_Spiro&suf. length = 3
				FROM spirometrie a
					LEFT JOIN travail.reperage_EFR_spiro_&Annee_2N._&annee_N1. b
						ON a.BEN_IDT_ANO = b.BEN_IDT_ANO
						AND a.date_debut = b.date_debut
				WHERE b.reperage = 2
				GROUP BY a.BEN_IDT_ANO;

	quit;

	* On ajoute l information dans la table de r�sultats;

	proc sort data = res.T_INDI_BPCO_DG_&an_N.;
		by BEN_IDT_ANO;
	run;

	proc sort data = spirometrie;
		by BEN_IDT_ANO;
	run;

	proc sort data = lieu_spiro;
		by BEN_IDT_ANO;
	run;

	data res.T_INDI_BPCO_DG_&an_N.;
		merge	res.T_INDI_BPCO_DG_&an_N. (in = a)
				spirometrie (in = b drop = date_debut)
				lieu_spiro (in = c);
		by BEN_IDT_ANO;
		length Spiro&suf. 3.;
		Spiro&suf. = 0;
		if Nb_spiro&suf. > 0 then
			Spiro&suf. = 1;
		if Nb_spiro&suf. = . then
			Nb_spiro&suf. = 0;
		if a then
			output;
	run;

	proc delete data = spirometrie lieu_spiro;
	run; quit;

	*	**************************************************************************************************************************************;
	*	On rep�re les patients avec une EFR dans les 365 jours avant ou suivant la date index;

	proc sql;

		CREATE TABLE EFR AS
			SELECT DISTINCT
				a.BEN_IDT_ANO,
				MIN(b.date_debut) AS date_debut,
				MIN(CASE	WHEN b.date_debut < (a.&bb.) THEN a.&bb.
							ELSE b.date_debut
							END) AS Date_EFR&suf. length = 4,
				COUNT(DISTINCT b.date_debut) AS Nb_EFR&suf. length = 3
			FROM res.T_INDI_BPCO_DG_&an_N. a
				INNER JOIN travail.reperage_EFR_spiro_&Annee_2N._&annee_N1. b
					ON a.BEN_IDT_ANO = b.BEN_IDT_ANO
			WHERE b.reperage = 1 AND b.date_debut <= a.&bh. AND b.date_fin >= a.&bb.
			GROUP BY a.BEN_IDT_ANO;

		* On fait un max car si DCIR + PMSI, c est un doublon donc l EFR a lieu dans un etb;
		CREATE TABLE lieu_EFR AS
			SELECT DISTINCT
				a.BEN_IDT_ANO,
				MAX(CASE	WHEN b.domaine = "" THEN 1
							WHEN b.domaine = "MCO" THEN 2
							WHEN b.domaine = "SSR" THEN 3
							END) AS Lieu_EFR&suf. length = 3
				FROM EFR a
					LEFT JOIN travail.reperage_EFR_spiro_&Annee_2N._&annee_N1. b
						ON a.BEN_IDT_ANO = b.BEN_IDT_ANO
						AND a.date_debut = b.date_debut
				WHERE b.reperage = 1 
				GROUP BY a.BEN_IDT_ANO;

	quit;

	* On ajoute l information dans la table de r�sultats;

	proc sort data = res.T_INDI_BPCO_DG_&an_N.;
		by BEN_IDT_ANO;
	run;

	proc sort data = EFR;
		by BEN_IDT_ANO;
	run;

	proc sort data = lieu_EFR;
		by BEN_IDT_ANO;
	run;

	data res.T_INDI_BPCO_DG_&an_N.;
		merge	res.T_INDI_BPCO_DG_&an_N. (in = a)
				EFR (in = b drop = date_debut)
				lieu_EFR (in = c);
		by BEN_IDT_ANO;
		length EFR&suf. 3.;
		EFR&suf. = 0;
		if Nb_EFR&suf. > 0 then
			EFR&suf. = 1;
		if Nb_EFR&suf. = . then
			Nb_EFR&suf. = 0;
		if a then
			output;
	run;

	proc delete data = EFR lieu_EFR;
	run; quit;

%mend EFR_Spiro;

* Avant ou apr�s inclusion : time = 1;
%EFR_Spiro (time = 1);

* Avant inclusion : time = 2;
%EFR_Spiro (time = 2);

* Apr�s inclusion : time = 3;
%EFR_Spiro (time = 3);

* Flag BPCO_DG_OBS;
data res.T_INDI_BPCO_DG_&an_N.;
	set res.T_INDI_BPCO_DG_&an_N.;
	length BPCO_DG_OBS 3.;
	BPCO_DG_OBS = 0;
	if EFR = 1 or Spiro = 1 then
		BPCO_DG_OBS = 1;
run;
