/*	******************************************************************************************************************************************	*/
/*																																				*/
/*		Indicateur 01 : Inclusion & exclusions																					 				*/
/*																																				*/
/*	******************************************************************************************************************************************	*/

*	******************************************************************************************************************************************	;
*	EXCLUSION DES PATIENTS ASTHMATIQUES																											;
*	******************************************************************************************************************************************	;

*	******************************************************************************************************************************************;
* 	Exclusion des patients en ALD asthme active � la date index;

proc sql;

	CREATE TABLE ALD_asthme AS
		SELECT DISTINCT
			a.BEN_IDT_ANO
		FROM res.T_INDI_BPCO_DG_&an_N. a
			INNER JOIN travail.Histo_ALD b
				ON a.BEN_IDT_ANO = b.BEN_IDT_ANO
		WHERE b.reperage = 53 AND (b.date_debut <= a.BPCO_DG_Date_index <= b.date_fin OR (b.date_debut <= a.BPCO_DG_Date_index AND 
			b.date_fin = "01JAN1600"d));

quit;

proc sql;

	* On ins�re l effectif dans le Flowchart;
	INSERT INTO flowch.Flow_Chart_BPCO_DG_&an_N.
		SELECT
			5,
			COUNT(DISTINCT BEN_IDT_ANO)
	FROM res.T_INDI_BPCO_DG_&an_N.
	WHERE BEN_IDT_ANO IN (SELECT BEN_IDT_ANO FROM ALD_asthme);

	* On conserve les id des patients � supprimer;
	CREATE TABLE travail.exclus1 AS
		SELECT DISTINCT
			BEN_IDT_ANO
	FROM res.T_INDI_BPCO_DG_&an_N.
	WHERE BEN_IDT_ANO IN (SELECT BEN_IDT_ANO FROM ALD_asthme);

quit;

proc delete data = ALD_asthme;
run; quit;

*	******************************************************************************************************************************************;
* 	Exclusion des patients en ALD mucoviscidose active � la date index;

proc sql;

	CREATE TABLE ALD_mucoviscidose AS
		SELECT DISTINCT
			a.BEN_IDT_ANO
		FROM res.T_INDI_BPCO_DG_&an_N. a
			INNER JOIN travail.Histo_ALD b
				ON a.BEN_IDT_ANO = b.BEN_IDT_ANO
		WHERE b.reperage = 54 AND (b.date_debut <= a.BPCO_DG_Date_index <= b.date_fin OR (b.date_debut <= a.BPCO_DG_Date_index AND 
			b.date_fin = "01JAN1600"d));

quit;

proc sql;

	* On ins�re l effectif dans le Flowchart;
	INSERT INTO flowch.Flow_Chart_BPCO_DG_&an_N.
		SELECT
			6,
			COUNT(DISTINCT BEN_IDT_ANO)
	FROM res.T_INDI_BPCO_DG_&an_N.
	WHERE BEN_IDT_ANO IN (SELECT BEN_IDT_ANO FROM ALD_mucoviscidose);

	* On conserve les id des patients � supprimer;
	CREATE TABLE travail.exclus2 AS
		SELECT DISTINCT
			BEN_IDT_ANO
	FROM res.T_INDI_BPCO_DG_&an_N.
	WHERE BEN_IDT_ANO IN (SELECT BEN_IDT_ANO FROM ALD_mucoviscidose);

quit;

proc delete data = ALD_mucoviscidose;
run; quit;

*	******************************************************************************************************************************************;
* 	Exclusion des patients avec 3 d�livrances de CSI sans d�livrance le m�me jour de bronchodilatateur;

proc sql;

	CREATE TABLE delivrances_CSI AS
		SELECT DISTINCT
			a.BEN_IDT_ANO,
			b.date_debut
		FROM res.T_INDI_BPCO_DG_&an_N. a
			INNER JOIN travail.reperage_med_asthme_&Annee_2N._&Annee_N1. b
				ON a.BEN_IDT_ANO = b.BEN_IDT_ANO
		WHERE b.reperage = 41 AND (a.BPCO_DG_Date_index - 730 <= b.date_debut <= a.BPCO_DG_Date_index - 365);

quit;

* On rep�re parmi ces patients, les patients avec ou sans d�livrance de bronchodilatateurs le m�me jour;
data BDCA;
	set travail.reperage_BDCA_tabac_&Annee_2N._&Annee_N1. (where = (reperage in (42, 43, 44)));
run;

proc sql;

	CREATE TABLE delivrances_CSI_broncho AS
		SELECT
			a.*,
			CASE	WHEN b.BEN_IDT_ANO IS NOT NULL OR c.BEN_IDT_ANO IS NOT NULL THEN 1
					ELSE 0
					END AS deliv_broncho length = 3
		FROM delivrances_CSI a
			LEFT JOIN BDCA b
				ON a.BEN_IDT_ANO = b.BEN_IDT_ANO
				AND a.date_debut = b.date_debut
			LEFT JOIN travail.reperage_BDLA_&Annee_2N._&Annee_N1. c
				ON a.BEN_IDT_ANO = c.BEN_IDT_ANO
				AND a.date_debut = c.date_debut;

	DELETE FROM delivrances_CSI_broncho WHERE deliv_broncho = 1;

quit;

proc delete data = BDCA;
run; quit;

proc sql undo_policy = none;

	CREATE TABLE delivrances_CSI_broncho AS
		SELECT
			BEN_IDT_ANO,
			COUNT(DISTINCT date_debut) AS nb_deliv length = 3
		FROM delivrances_CSI_broncho
		GROUP BY BEN_IDT_ANO;

	DELETE FROM delivrances_CSI_broncho WHERE nb_deliv < 3;

quit;


proc sql;

	* On ins�re l effectif dans le Flowchart;
	INSERT INTO flowch.Flow_Chart_BPCO_DG_&an_N.
		SELECT
			7,
			COUNT(DISTINCT BEN_IDT_ANO)
	FROM res.T_INDI_BPCO_DG_&an_N.
	WHERE BEN_IDT_ANO IN (SELECT BEN_IDT_ANO FROM delivrances_CSI_broncho);

	* On conserve les id des patients � supprimer;
	CREATE TABLE travail.exclus3 AS
		SELECT DISTINCT
			BEN_IDT_ANO
	FROM res.T_INDI_BPCO_DG_&an_N.
	WHERE BEN_IDT_ANO IN (SELECT BEN_IDT_ANO FROM delivrances_CSI_broncho);

quit;

proc delete data = delivrances_CSI delivrances_CSI_broncho;
run; quit;
			
*	******************************************************************************************************************************************;
*	On exclut tous les patients;

data exclus;
	set	travail.exclus1-travail.exclus3;
run;

proc sql;

	* On ins�re l effectif dans le Flowchart;
	INSERT INTO flowch.Flow_Chart_BPCO_DG_&an_N.
		SELECT
			8,
			COUNT(DISTINCT BEN_IDT_ANO)
	FROM exclus;

	DELETE FROM res.T_INDI_BPCO_DG_&an_N.
	WHERE BEN_IDT_ANO IN (SELECT BEN_IDT_ANO FROM exclus);

	* On ins�re l effectif dans le Flowchart;
	INSERT INTO flowch.Flow_Chart_BPCO_DG_&an_N.
		SELECT
			9,
			COUNT(DISTINCT BEN_IDT_ANO)
	FROM res.T_INDI_BPCO_DG_&an_N.;

quit;

proc delete data = exclus;
run; quit;

proc datasets library = travail memtype = data nolist;
	delete exclus1-exclus3;
run; quit;

*	******************************************************************************************************************************************	;
*	EXCLUSION DES PATIENTS D�J� DIAGNOSTIQU�S																									;
*	******************************************************************************************************************************************	;

*	******************************************************************************************************************************************;
* 	Exclusion des patients en ALD BPCO active � la date index;

proc sql;

	CREATE TABLE ALD_BPCO AS
		SELECT DISTINCT
			a.BEN_IDT_ANO
		FROM res.T_INDI_BPCO_DG_&an_N. a
			INNER JOIN travail.Histo_ALD b
				ON a.BEN_IDT_ANO = b.BEN_IDT_ANO
		WHERE b.reperage = 52 AND (b.date_debut <= a.BPCO_DG_Date_index <= b.date_fin OR (b.date_debut <= a.BPCO_DG_Date_index AND 
			b.date_fin = "01JAN1600"d));

quit;

proc sql;

	* On ins�re l effectif dans le Flowchart;
	INSERT INTO flowch.Flow_Chart_BPCO_DG_&an_N.
		SELECT
			10,
			COUNT(DISTINCT BEN_IDT_ANO)
	FROM res.T_INDI_BPCO_DG_&an_N.
	WHERE BEN_IDT_ANO IN (SELECT BEN_IDT_ANO FROM ALD_BPCO);

	* On conserve les id des patients � supprimer;
	CREATE TABLE travail.exclus1 AS
		SELECT DISTINCT
			BEN_IDT_ANO
	FROM res.T_INDI_BPCO_DG_&an_N.
	WHERE BEN_IDT_ANO IN (SELECT BEN_IDT_ANO FROM ALD_BPCO);

quit;

proc delete data = ALD_BPCO;
run; quit;

*	******************************************************************************************************************************************;
* 	Exclusion des patients avec au moins une d�livrance de BDLA entre 730 jours et 365 jours pr�c�dents la date index;

proc sql;

	CREATE TABLE BDLA_avant_index AS
		SELECT DISTINCT
			a.BEN_IDT_ANO
		FROM res.T_INDI_BPCO_DG_&an_N. a
			INNER JOIN travail.reperage_BDLA_&Annee_2N._&Annee_N1. b
				ON a.BEN_IDT_ANO = b.BEN_IDT_ANO
		WHERE a.BPCO_DG_Date_index - 730 <= b.date_debut <= a.BPCO_DG_Date_index - 365;

quit;

proc sql;

	* On ins�re l effectif dans le Flowchart;
	INSERT INTO flowch.Flow_Chart_BPCO_DG_&an_N.
		SELECT
			11,
			COUNT(DISTINCT BEN_IDT_ANO)
	FROM res.T_INDI_BPCO_DG_&an_N.
	WHERE BEN_IDT_ANO IN (SELECT BEN_IDT_ANO FROM BDLA_avant_index);

	* On conserve les id des patients � supprimer;
	CREATE TABLE travail.exclus2 AS
		SELECT DISTINCT
			BEN_IDT_ANO
	FROM res.T_INDI_BPCO_DG_&an_N.
	WHERE BEN_IDT_ANO IN (SELECT BEN_IDT_ANO FROM BDLA_avant_index);

quit;

proc delete data = BDLA_avant_index;
run; quit;

*	******************************************************************************************************************************************;
* 	Exclusion des patients avec au moins un diag de BPCO cod� entre 730 jours pr�c�dents la date index;

proc sql;

	CREATE TABLE hospit_MCO AS
		SELECT
			a.BEN_IDT_ANO
		FROM res.T_INDI_BPCO_DG_&an_N. a
			INNER JOIN travail.reperage_hospit_BPCO_&annee_4N._&Annee_N1. b
				ON a.BEN_IDT_ANO = b.BEN_IDT_ANO
		WHERE domaine = "MCO" AND a.BPCO_DG_Date_index - 730 <= b.date_fin <= a.BPCO_DG_Date_index;

	CREATE TABLE hospit_SSR_HAD AS
		SELECT
			a.BEN_IDT_ANO
		FROM res.T_INDI_BPCO_DG_&an_N. a
			INNER JOIN travail.reperage_hospit_BPCO_&annee_4N._&Annee_N1. b
				ON a.BEN_IDT_ANO = b.BEN_IDT_ANO
		WHERE domaine IN ("SSR", "HAD") AND b.date_debut <= a.BPCO_DG_Date_index AND b.date_fin >= a.BPCO_DG_Date_index - 730;

quit;

proc sql;

	* On ins�re l effectif dans le Flowchart;
	INSERT INTO flowch.Flow_Chart_BPCO_DG_&an_N.
		SELECT
			12,
			COUNT(DISTINCT BEN_IDT_ANO)
	FROM res.T_INDI_BPCO_DG_&an_N.
	WHERE BEN_IDT_ANO IN (SELECT BEN_IDT_ANO FROM hospit_MCO) OR BEN_IDT_ANO IN (SELECT BEN_IDT_ANO FROM hospit_SSR_HAD);

	* On conserve les id des patients � supprimer;
	CREATE TABLE travail.exclus3 AS
		SELECT DISTINCT
			BEN_IDT_ANO
	FROM res.T_INDI_BPCO_DG_&an_N.
	WHERE BEN_IDT_ANO IN (SELECT BEN_IDT_ANO FROM hospit_MCO) OR BEN_IDT_ANO IN (SELECT BEN_IDT_ANO FROM hospit_SSR_HAD);

quit;

proc delete data = hospit_MCO hospit_SSR_HAD;
run; quit;

*	******************************************************************************************************************************************;
* 	Exclusion des patients avec une spiro ou un EFR entre 730 jours et 365 jours pr�c�dant la date index;

*	Changement ao�t 2021 - Nouvelle version des indicateurs : on modifie les exclusions;
*	On exclut entre 730 jours et 365 jours pr�c�dant la date index (avant = dans les 730 jours pr�c�dents la date index);

proc sql;

	CREATE TABLE Spiro_EFR AS
		SELECT
			a.BEN_IDT_ANO
		FROM res.T_INDI_BPCO_DG_&an_N. a
			INNER JOIN travail.reperage_EFR_spiro_&Annee_2N._&annee_N1. b
				ON a.BEN_IDT_ANO = b.BEN_IDT_ANO
		WHERE b.date_debut < a.BPCO_DG_Date_index - 365 AND a.BPCO_DG_Date_index - 730 <= b.date_fin;

quit;

proc sql;

	* On ins�re l effectif dans le Flowchart;
	INSERT INTO flowch.Flow_Chart_BPCO_DG_&an_N.
		SELECT
			13,
			COUNT(DISTINCT BEN_IDT_ANO)
	FROM res.T_INDI_BPCO_DG_&an_N.
	WHERE BEN_IDT_ANO IN (SELECT BEN_IDT_ANO FROM Spiro_EFR);

	* On conserve les id des patients � supprimer;
	CREATE TABLE travail.exclus4 AS
		SELECT DISTINCT
			BEN_IDT_ANO
	FROM res.T_INDI_BPCO_DG_&an_N.
	WHERE BEN_IDT_ANO IN (SELECT BEN_IDT_ANO FROM Spiro_EFR);

quit;

proc delete data = Spiro_EFR;
run; quit;
			
*	******************************************************************************************************************************************;
*	On exclut tous les patients;

data exclus;
	set	travail.exclus1-travail.exclus4;
run;

proc sql;

	* On ins�re l effectif dans le Flowchart;
	INSERT INTO flowch.Flow_Chart_BPCO_DG_&an_N.
		SELECT
			14,
			COUNT(DISTINCT BEN_IDT_ANO)
	FROM exclus;

	DELETE FROM res.T_INDI_BPCO_DG_&an_N.
	WHERE BEN_IDT_ANO IN (SELECT BEN_IDT_ANO FROM exclus);

quit;

proc delete data = exclus;
run; quit;

proc datasets library = travail memtype = data nolist;
	delete exclus1-exclus4;
run; quit;

* On ins�re la population d �tude dans le flowchart;
proc sql;

	INSERT INTO flowch.Flow_Chart_BPCO_DG_&an_N.
		SELECT
			15,
			COUNT(DISTINCT BEN_IDT_ANO)
	FROM res.T_INDI_BPCO_DG_&an_N.;

quit;


*	******************************************************************************************************************************************	;
*	PATIENTS POUR LESQUELS LA SPIROM�TRIE OU L EFR EST CONTRE INDIQU�																			;
*	******************************************************************************************************************************************	;

*	Changement ao�t 2021 - Nouvelle version des indicateurs : on modifie les exclusions;
*	On n'exclut plus les patients pour lesquels la spirom�trie ou l EFR est contre indiqu�;

*	******************************************************************************************************************************************	;
*	PATIENTS POUR LESQUELS LA SPIROM�TRIE OU L EFR N EST PAS R�ALISABLE																			;
*	******************************************************************************************************************************************	;

*	******************************************************************************************************************************************;
* 	Exclusion des patients en ALD maladie d Alzheimer et autres d�mences active 365 jours apr�s la date index;

proc sql;

	CREATE TABLE ALD_Alzheimer AS
		SELECT DISTINCT
			a.BEN_IDT_ANO
		FROM res.T_INDI_BPCO_DG_&an_N. a
			INNER JOIN travail.Histo_ALD b
				ON a.BEN_IDT_ANO = b.BEN_IDT_ANO
		WHERE b.reperage IN (50, 51) 
			AND (b.date_debut <= a.BPCO_DG_Date_index <= b.date_fin OR (b.date_debut <= a.BPCO_DG_Date_index AND b.date_fin = "01JAN1600"d)
			OR a.BPCO_DG_Date_index <= b.date_debut <= (a.BPCO_DG_Date_index + 365));

quit;

proc sql;

	* On ins�re l effectif dans le Flowchart;
	INSERT INTO flowch.Flow_Chart_BPCO_DG_&an_N.
		SELECT
			16,
			COUNT(DISTINCT BEN_IDT_ANO)
	FROM res.T_INDI_BPCO_DG_&an_N.
	WHERE BEN_IDT_ANO IN (SELECT BEN_IDT_ANO FROM ALD_Alzheimer);

	* On conserve les id des patients � supprimer;
	CREATE TABLE travail.exclus1 AS
		SELECT DISTINCT
			BEN_IDT_ANO
	FROM res.T_INDI_BPCO_DG_&an_N.
	WHERE BEN_IDT_ANO IN (SELECT BEN_IDT_ANO FROM ALD_Alzheimer);

quit;

proc delete data = ALD_Alzheimer;
run; quit;

*	******************************************************************************************************************************************;
* 	Exclusion des patients ayant eu un diagnostic de soins palliatif cod� lors d�un s�jour hospitalier MCO termin� dans les 365 jours apr�s
    la date index, ou lors d�un s�jour hospitalier HAD, SSR dans les 365 jours apr�s la date index;

proc sql undo_policy = none;

	CREATE TABLE soin_palliatif_MCO AS
		SELECT DISTINCT
			a.BEN_IDT_ANO
		FROM res.T_INDI_BPCO_DG_&an_N. a
			INNER JOIN travail.soin_palliatif_&annee_2N._&annee_N1. b
				ON a.BEN_IDT_ANO = b.BEN_IDT_ANO
		WHERE a.BPCO_DG_Date_index <= b.date_fin <= a.BPCO_DG_Date_index + 365 AND b.domaine = "MCO";

	CREATE TABLE soin_palliatif_SSR_HAD AS
		SELECT DISTINCT
			a.BEN_IDT_ANO
		FROM res.T_INDI_BPCO_DG_&an_N. a
			INNER JOIN travail.soin_palliatif_&annee_2N._&annee_N1. b
				ON a.BEN_IDT_ANO = b.BEN_IDT_ANO
		WHERE b.domaine IN ("HAD", "SSR") AND b.date_debut <= a.BPCO_DG_Date_index + 365 AND b.date_fin >= a.BPCO_DG_Date_index;

quit;

data soin_palliatif;
	set soin_palliatif_MCO
		soin_palliatif_SSR_HAD;
run;

proc sql;

	* On ins�re l effectif dans le Flowchart;
	INSERT INTO flowch.Flow_Chart_BPCO_DG_&an_N.
		SELECT
			17,
			COUNT(DISTINCT BEN_IDT_ANO)
	FROM res.T_INDI_BPCO_DG_&an_N.
	WHERE BEN_IDT_ANO IN (SELECT BEN_IDT_ANO FROM soin_palliatif);

	* On conserve les id des s�jours � supprimer;
	CREATE TABLE travail.exclus2 AS
		SELECT DISTINCT
			BEN_IDT_ANO
	FROM res.T_INDI_BPCO_DG_&an_N.
	WHERE BEN_IDT_ANO IN (SELECT BEN_IDT_ANO FROM soin_palliatif);

quit;

proc datasets library = work memtype = data nolist;
	delete soin_palliatif_MCO soin_palliatif_SSR_HAD soin_palliatif ;
run; quit;
			
*	******************************************************************************************************************************************;
*	On flag les patients pour lesquels la spirom�trie ou les EFR ne sont pas r�alisables;

data exclus;
	set	travail.exclus1-travail.exclus2;
run;

proc sql;

	* On ins�re l effectif dans le Flowchart;
	INSERT INTO flowch.Flow_Chart_BPCO_DG_&an_N.
		SELECT
			18,
			COUNT(DISTINCT BEN_IDT_ANO)
	FROM exclus;

	DELETE FROM res.T_INDI_BPCO_DG_&an_N.
	WHERE BEN_IDT_ANO IN (SELECT BEN_IDT_ANO FROM exclus);

quit;

proc delete data = exclus;
run; quit;

proc datasets library = travail memtype = data nolist;
	delete exclus1-exclus2;
run; quit;


*	******************************************************************************************************************************************;
* 	Population cible;

proc sql;

	INSERT INTO flowch.Flow_Chart_BPCO_DG_&an_N.
		SELECT
			20,
			COUNT(DISTINCT BEN_IDT_ANO)
	FROM res.T_INDI_BPCO_DG_&an_N.;

quit;

proc sql;

	SELECT COUNT(*), COUNT(DISTINCT BEN_IDT_ANO)
	FROM res.T_INDI_BPCO_DG_&an_N.;

quit;
