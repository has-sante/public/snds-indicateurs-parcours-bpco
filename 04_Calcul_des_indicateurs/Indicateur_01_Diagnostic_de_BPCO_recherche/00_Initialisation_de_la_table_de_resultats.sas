/*	******************************************************************************************************************************************	*/
/*																																				*/
/*		Table de population - Patients avec indicateur_01 = 1																	 				*/
/*																																				*/
/*	******************************************************************************************************************************************	*/

* Patients pour diag de BPCO recherch�;
data res.T_INDI_BPCO_DG_&an_N. (keep = BEN_IDT_ANO);
	set pop.indicateurs_&an_N. (where = (indicateur_01 = 1));
run;

proc sort data = res.T_INDI_BPCO_DG_&an_N. nodupkey;
	by BEN_IDT_ANO;
run;

*	******************************************************************************************************************************************;
*	Patients ayant eu au moins une d�livrance rembours�e d un bronchodilatateur anticholinergique longue dur�e d action ou B�ta-2 longue 
	dur�e d action l ann�e N;

proc sql;

	CREATE TABLE patients_BDLA AS
		SELECT
			BEN_IDT_ANO,
			MIN(date_debut) AS Date_index_broncho format ddmmyy10. length = 4
		FROM travail.reperage_BDLA_&Annee_2N._&Annee_N1.
		WHERE YEAR(date_debut) = &Annee_N.
		GROUP BY BEN_IDT_ANO;

quit;

*	On corrige la variable broncho_LDA, qui est incoh�rente avec la variable nb_broncho_LDA de la table res.T_INDI_BPCO_&an_N.;

proc sql;

	CREATE TABLE verif_LDA AS
		SELECT
			a.BEN_IDT_ANO,
			a.Date_index_broncho,
			b.Nb_Broncho_LDA
		FROM patients_BDLA a
			INNER JOIN res.T_INDI_BPCO_17 b
				ON a.BEN_IDT_ANO = b.BEN_IDT_ANO
		WHERE b.Nb_Broncho_LDA = 0;

quit;

proc sql; select count(distinct BEN_IDT_ANo) from verif_LDA; quit;

proc sql;

	DELETE FROM patients_BDLA
	WHERE BEN_IDT_ANO IN (SELECT BEN_IDT_ANO FROM verif_LDA);

quit;

proc delete data = verif_LDA;
run; quit;

proc sql;

	SELECT COUNT(DISTINCT BEN_IDT_ANO), COUNT(*) FROM patients_BDLA;

quit;

*	******************************************************************************************************************************************;
*	Patients ayant eu au moins une d�livrance rembours�e d antibioth�rapie pour infections respiratoires l ann�e N
*		ET pr�c�d�e d au moins une d�livrance rembours�e d antibioth�rapie pour infections respiratoires dans les 365 jours 
*		ET ayant eu au moins une d�livrance rembours�e de bronchodilatateur anti-cholinergique courte dur�e d action ou un B�ta-2 
*			courte dur�e d action, d�livr�e le m�me jour :
*				o	que la cure d antibioth�rapie r�alis�e l ann�e N
*				o	OU que la cure d antibioth�rapie d�livr�e dans les 365 jours pr�c�dant la cure d antibioth�rapie r�alis�e l ann�e N;

* On r�cup�re toutes les lignes de patients avec des d�livrances d antibio l ann�e N;
proc sql;

	CREATE TABLE patients_antibio_all AS
		SELECT DISTINCT
			BEN_IDT_ANO,
			date_debut AS date_deliv_antibio_&Annee_N. format ddmmyy10. length = 4
		FROM travail.reperage_antibio_&Annee_1N._&Annee_N1.
		WHERE YEAR(date_debut) = &Annee_N. AND reperage = 36
		ORDER BY 1, 2;

quit;

* On r�cup�re toutes les d�livrance rembours�e de BDCA le m�me jour + pr�c�d�e dans les 365 jours d au moins une antibioth�rapie;
proc sql undo_policy = none;

	* D�livrance de BDCA le m�me jour que l antibio;
	CREATE TABLE tmp_patients_BDCA_memejour AS
		SELECT
			a.*,
			b.date_debut AS Date_index_antibio_memejour format date9. length = 4
		FROM patients_antibio_all a
			INNER JOIN travail.reperage_BDCA_tabac_&Annee_2N._&Annee_N1. b
				ON a.BEN_IDT_ANO = b.BEN_IDT_ANO
		WHERE (a.date_deliv_antibio_&Annee_N. = b.date_debut) AND b.reperage in (42, 43, 44);

	* D�livrance antibio dans les 365 jours avant l antibio;
	CREATE TABLE patients_antibio_memejour AS
		SELECT
			a.*
		FROM tmp_patients_BDCA_memejour a
			INNER JOIN travail.reperage_antibio_&Annee_1N._&Annee_N1. b
				ON a.BEN_IDT_ANO = b.BEN_IDT_ANO
		WHERE b.reperage = 36 AND (a.Date_index_antibio_memejour - 365) <= b.date_debut < a.Date_index_antibio_memejour;

	* Date minimale;
	CREATE TABLE patients_antibio_memejour_min AS
		SELECT
			BEN_IDT_ANO,
			MIN(Date_index_antibio_memejour) AS Date_index_antibio_memejour format date9. length = 4
		FROM patients_antibio_memejour
		GROUP BY BEN_IDT_ANO;

quit;

proc sql;

	SELECT COUNT(DISTINCT BEN_IDT_ANO), COUNT(*) FROM patients_antibio_memejour_min;

quit;

* On r�cup�re toutes les d�livrance rembours�es d antibioth�rapie dans les 365 jours avec une d�livrance rembours�e de de BDCA le m�me jour;
proc sql undo_policy = none;

	* D�livrance de BDCA dans les 365 jours pr�c�dents;
	CREATE TABLE patients_BDCA_365jour AS
		SELECT
			a.BEN_IDT_ANO,
			a.date_deliv_antibio_&Annee_N. AS Date_index_antibio_365jour length = 4,
			b.date_debut AS deliv_BDCA length = 4
		FROM patients_antibio_all a
			INNER JOIN travail.reperage_BDCA_tabac_&Annee_2N._&Annee_N1. b
				ON a.BEN_IDT_ANO = b.BEN_IDT_ANO
		WHERE (a.date_deliv_antibio_&Annee_N. - 365) <= b.date_debut < a.date_deliv_antibio_&Annee_N. AND b.reperage in (42, 43, 44);

	* Restreint aux antibios le m�me jour;
	CREATE TABLE patients_antibio_BDCA_365jour AS
		SELECT
			a.*,
			b.date_debut length = 4
		FROM patients_BDCA_365jour a
			INNER JOIN travail.reperage_antibio_&Annee_1N._&Annee_N1. b
				ON a.BEN_IDT_ANO = b.BEN_IDT_ANO
		WHERE b.reperage = 36 AND a.deliv_BDCA = b.date_debut;

	* Date minimale;
	CREATE TABLE patients_antibio_365jour_min AS
		SELECT
			BEN_IDT_ANO,
			MIN(Date_index_antibio_365jour) AS Date_index_antibio_365jour format date9. length = 4
		FROM patients_antibio_BDCA_365jour
		GROUP BY BEN_IDT_ANO;

quit;

proc sql;

	SELECT COUNT(DISTINCT BEN_IDT_ANO), COUNT(*) FROM patients_antibio_365jour_min;

quit;

proc delete data = patients_antibio_all;
run; quit;

*	******************************************************************************************************************************************;
*	Patients ayant eu au moins une d�livrance rembours�e de substituts nicotiniques (remboursable ou forfait) ou d un traitement d arr�t du
	tabac l ann�e N;

proc sql undo_policy = none;

	CREATE TABLE patients_tabac AS
		SELECT
			BEN_IDT_ANO,
			MIN(date_debut) AS Date_index_substitut format ddmmyy10. length = 4
		FROM travail.reperage_BDCA_tabac_&Annee_2N._&Annee_N1.
		WHERE YEAR(date_debut) = &Annee_N. AND reperage = 35
		GROUP BY BEN_IDT_ANO;

quit;

proc sql;

	SELECT COUNT(DISTINCT BEN_IDT_ANO), COUNT(*) FROM patients_tabac;

quit;

*	******************************************************************************************************************************************;
*	La date index est le min des 4 dates;

proc sort data = tmp_patients_BDCA_memejour nodupkey;
	by BEN_IDT_ANO;
run;

data res.T_INDI_BPCO_DG_&an_N.;
	merge	res.T_INDI_BPCO_DG_&an_N. (in = z)
			patients_BDLA (in = a keep = BEN_IDT_ANO Date_index_broncho)
			patients_antibio_365jour_min (in = b keep = BEN_IDT_ANO Date_index_antibio_365jour)
			patients_antibio_memejour_min (in = b keep = BEN_IDT_ANO Date_index_antibio_memejour)
			patients_tabac (in = c keep = BEN_IDT_ANO Date_index_substitut)
			tmp_patients_BDCA_memejour (in = d keep = BEN_IDT_ANO);
	by BEN_IDT_ANO;
	length BPCO_DG_Date_index 4. ATB_Inf_Meme_Jour ATB_Inf_365_Jour Broncho_CDA Subs_Nico 3.;
	BPCO_DG_Date_index = min(Date_index_broncho, Date_index_antibio_memejour, Date_index_antibio_365jour, Date_index_substitut);
	ATB_Inf_Meme_Jour = 0;
	if Date_index_antibio_memejour ne . then
		ATB_Inf_Meme_Jour = 1;
	ATB_Inf_365_Jour = 0;
	if Date_index_antibio_365jour ne . then
		ATB_Inf_365_Jour = 1;
	Broncho_CDA = 0;
	if d then
		Broncho_CDA = 1;
	Subs_Nico = 0;
	if Date_index_substitut ne . then
		Subs_Nico = 1;
	format BPCO_DG_Date_index Date_index_broncho Date_index_antibio_memejour Date_index_antibio_365jour Date_index_substitut ddmmyy10.;
	if z and BPCO_DG_Date_index ne . then
		output;
run;

*	******************************************************************************************************************************************;
*	Effectifs pour le flowchart;

proc sql;

	CREATE TABLE flowch.Flow_Chart_BPCO_DG_&an_N.  AS
		SELECT *
		FROM (
			SELECT 1 AS indicateur length = 3, COUNT(DISTINCT BEN_IDT_ANO) AS N length = 5 FROM res.T_INDI_BPCO_DG_&an_N. WHERE BEN_IDT_ANO 
					IN (SELECT BEN_IDT_ANO FROM patients_BDLA)
			UNION ALL
			SELECT 2.5, COUNT(DISTINCT BEN_IDT_ANO) FROM res.T_INDI_BPCO_DG_&an_N. WHERE BEN_IDT_ANO IN (SELECT BEN_IDT_ANO FROM 
					patients_antibio_365jour_min)
			UNION ALL
			SELECT 2, COUNT(DISTINCT BEN_IDT_ANO) FROM res.T_INDI_BPCO_DG_&an_N. WHERE BEN_IDT_ANO IN (SELECT BEN_IDT_ANO FROM 
				patients_antibio_memejour_min)
			UNION ALL
			SELECT 3, COUNT(DISTINCT BEN_IDT_ANO) FROM res.T_INDI_BPCO_DG_&an_N. WHERE BEN_IDT_ANO 
					IN (SELECT BEN_IDT_ANO FROM patients_tabac)
			UNION ALL
			SELECT 4, COUNT(DISTINCT BEN_IDT_ANO) FROM res.T_INDI_BPCO_DG_&an_N.
		) ;

quit;

proc delete data = patients_BDLA patients_antibio_365jour_min patients_antibio_memejour_min patients_tabac;
run; quit;
