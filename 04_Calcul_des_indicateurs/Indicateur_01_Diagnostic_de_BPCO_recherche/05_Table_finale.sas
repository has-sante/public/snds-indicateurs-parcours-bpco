/*	******************************************************************************************************************************************	*/
/*																																				*/
/*		Indicateur 01 : Table finale - Ajout de formats et de labels																			*/
/*																																				*/
/*	******************************************************************************************************************************************	*/

data res.T_INDI_BPCO_DG_&an_N.;
	set res.T_INDI_BPCO_DG_&an_N. (keep = BEN_IDT_ANO Date_index_broncho Date_index_antibio_memejour Date_index_antibio_365jour 
		Date_index_substitut BPCO_DG_Date_index ATB_Inf_Resp_3 ATB_Inf_Resp_2 ATB_Inf_Meme_Jour ATB_Inf_365_Jour Broncho_CDA Broncho_LDA Subs_Nico 
		BPCO_DG_CIBLE Nb_ATB_Inf_Resp Nb_Broncho_CDA Nb_Subs_Nico BPCO_DG_OBS Spiro Nb_Spiro Date_Spiro Lieu_Spiro EFR Nb_EFR Date_EFR Lieu_EFR 
		Spiro_Av_365 Nb_Spiro_Av_365 Date_Spiro_Av_365 Lieu_Spiro_Av_365 EFR_Av_365 Nb_EFR_Av_365 Date_EFR_Av_365 Lieu_EFR_Av_365 
		Spiro_AP_365 Nb_Spiro_AP_365 Date_Spiro_AP_365 Lieu_Spiro_AP_365 EFR_AP_365 Nb_EFR_AP_365 Date_EFR_AP_365 Lieu_EFR_AP_365 Pneumothorax 
		Pneumothorax_AV_365 Pneumothorax_AP_365 Infarctus Infarctus_AV_365 Infarctus_AP_365 Nb_ATB_Inf_Resp_AV365J Nb_Broncho_CDA_AV365j);
	label		
		BEN_IDT_ANO = "Num�ro d'indivdu"
		Date_index_broncho = "Date de la premi�re d�livrance de bronchodilatateur anticholinergique longue dur�e d'action ou B�ta-2 longue dur�e d'action en &Annee_N."
		Date_index_antibio_memejour = "Date de la premi�re d�livrance rembours�e d'antibioth�rapie en &Annee_N. associ�e le m�me jour d'une d�livrance rembours�e de BDCA pr�c�d�e dans les 365 jours d'au moins une d�livrance rembours�e d'antibioth�rapie"
		Date_index_antibio_365jour = "Date de la premi�re d�livrance rembours�e d'antibioth�rapie en &Annee_N. pr�c�d�e dans les 365 jours d'au moins une d�livrance rembours�e d'antibioth�rapie associ�e le m�me jour d'une d�livrance rembours�e de BDCA"
		Date_index_substitut = "Date de la premi�re d�livrance de substituts nicotiniques (remboursable ou forfait) ou d'un traitement d'arr�t du tabac en &Annee_N."
		BPCO_DG_Date_index = "Date index retenue"		
		ATB_Inf_Resp_3 = "Patient ayant eu au moins une d�livrance rembours�e d'antibioth�rapie pour infections respiratoires en &Annee_N. pr�c�d�e d'au moins 2 d�livrances rembours�es d'antibioth�rapie pour infections respiratoires dans les 365 jours"
		ATB_Inf_Resp_2 = "Patient ayant eu au moins une d�livrance rembours�e d'antibioth�rapie pour infections respiratoires en &Annee_N. pr�c�d�e d'au moins une d�livrance rembours�e d'antibioth�rapie pour infections respiratoires dans les 365 jours"
		ATB_Inf_meme_jour = "Patients ayant eu au moins une d�livrance rembours�e d'antibioth�rapie &Annee_N. associ�e le m�me jour d'une d�livrance rembours�e de BDCA et pr�c�d�e dans les 365 jours d'au moins une d�livrance rembours�e d'antibioth�rapie pour infections respiratoires"
		ATB_Inf_365_Jour = "Patients ayant eu au moins une d�livrance rembours�e d'antibioth�rapie &Annee_N. pr�c�d�e dans les 365 jours d'au moins une d�livrance rembours�e d'antibioth�rapie associ�e le m�me jour � une d�livrance rembours�e de BDCA"
		Broncho_CDA = "Patient ayant eu au moins une d�livrance rembours�e en &Annee_N. d'un bronchodilatateur anticholinergique ou B�ta-2 courte dur�e d'action d�livr�e le m�me jour que la cure d'antibioth�rapie"
		Broncho_LDA = "Patient ayant eu au moins une d�livrance rembours�e en B�ta-&Annee_N. d'un bronchodilatateur anticholinergique longue dur�e d'action ou B�ta-2 longue dur�e d'action"
		Subs_Nico = "Patient ayant eu au moins une d�livrance rembours�e de substituts nicotiniques en &Annee_N."
		BPCO_DG_CIBLE = "Patient correspondant aux crit�res d'inclusion et d'exclusion de la population cible"
		Nb_ATB_Inf_Resp = "Nombre de d�livrances rembours�es d'antibioth�rapie pour infections respiratoires en &Annee_N."
		Nb_ATB_Inf_Resp_AV365J = "Nombre de d�livrances rembours�es d'antibioth�rapie pour infections respiratoires dans les 365 jours pr�c�dant la date index"
		Nb_Broncho_CDA = "Nombre de d�livrances rembours�es de bronchodilatateur anticholinergique ou B�ta-2 courte dur�e d'action en &Annee_N."
		Nb_Broncho_CDA_AV365j = "Nombre de d�livrances rembours�es de bronchodilatateur anticholinergique ou B�ta-2 courte dur�e d'action dans les 365 jours pr�c�dant la date index"
		Nb_Subs_Nico = "Nombre de d�livrances rembours�es de substituts nicotiniques en &Annee_N. "
		BPCO_DG_OBS = "Patient de la population cible pour lequel une spirom�trie ou des EFR ont �t� r�alis�s dans les 365 jours pr�c�dant la date index ou  les 365 jours suivant la date index"
		Spiro = "Spirom�trie r�alis�e dans les 365 jours avant ou suivant la date index"
		Nb_Spiro = "Nombre de spirom�tries r�alis�es dans les 365 jours avant ou suivant la date index"
		Date_Spiro = "Date de la r�alisation de la premi�re spirom�trie dans les 365 jours avant ou suivant la date index"
		Lieu_Spiro = "Lieu de r�alisation de la premi�re spirom�trie dans les 365 jours avant ou suivant la date index"
		EFR = "EFR r�alis�e dans les 365 jours avant ou suivant la date index"
		Nb_EFR = "Nombre d'EFR r�alis�es dans les 365 jours avant ou suivant la date index"
		Date_EFR = "Date de la r�alisation de la premi�re EFR dans les 365 jours avant ou suivant la date index"
		Lieu_EFR = "Lieu de r�alisation de la premi�re EFR dans les 365 jours avant ou suivant la date index"		
		Spiro_Av_365 = "Spirom�trie r�alis�e dans les 365 jours avant la date index (incluse)"
		Nb_Spiro_Av_365 = "Nombre de spirom�tries r�alis�es dans les 365 jours avant la date index (incluse)"
		Date_Spiro_Av_365 = "Date de la r�alisation de la premi�re spirom�trie dans les 365 jours avant la date index (incluse)"
		Lieu_Spiro_Av_365 = "Lieu de r�alisation de la premi�re spirom�trie avant la date index"
		EFR_Av_365 = "EFR r�alis�e dans les 365 jours avant la date index (incluse)"
		Nb_EFR_Av_365 = "Nombre d'EFR r�alis�es dans les 365 jours avant la date index (incluse)"
		Date_EFR_Av_365 = "Date de la r�alisation de la premi�re EFR dans les 365 jours avant la date index (incluse)"
		Lieu_EFR_Av_365 = "Lieu de r�alisation de la premi�re EFR dans les 365 jours avant la date index (incluse)"		
		Spiro_Ap_365 = "Spirom�trie r�alis�e dans les 365 jours suivant la date index (exclue)"
		Nb_Spiro_Ap_365 = "Nombre de spirom�tries r�alis�es dans les 365 jours suivant la date index (exclue)"
		Date_Spiro_Ap_365 = "Date de la r�alisation de la premi�re spirom�trie dans les 365 jours suivant la date index (exclue)"
		Lieu_Spiro_Ap_365 = "Lieu de r�alisation de la premi�re spirom�trie suivant la date index (exclue)"
		EFR_Ap_365 = "EFR r�alis�e dans les 365 jours suivant la date index (exclue)"
		Nb_EFR_Ap_365 = "Nombre d'EFR r�alis�es dans les 365 jours suivant la date index (exclue)"
		Date_EFR_Ap_365 = "Date de la r�alisation de la premi�re EFR dans les 365 jours suivant la date index (exclue)"
		Lieu_EFR_Ap_365 = "Lieu de r�alisation de la premi�re EFR dans les 365 jours suivant la date index (exclue)"
		Pneumothorax = "Patients ayant eu un pneumothorax dans les 365 jours suivant la date index ou dans les 365 jours pr�c�dant la date index"
		Pneumothorax_AV_365 = "Patients ayant eu un pneumothorax dans les 365 jours avant la date index (incluse)"
		Pneumothorax_AP_365 = "Patients ayant eu un pneumothorax dans les 365 jours suivant la date index (exclue)"
		Infarctus = "Patients ayant eu un diagnostic d'infarctus dans les 365 jours suivant la date index ou dans les 365 jours pr�c�dant la date index"
		Infarctus_AV_365 = "Patients ayant eu un diagnostic d'infarctus dans les 365 jours avant la date index (incluse)"
		Infarctus_AP_365 = "Patients ayant eu un diagnostic d'infarctus dans les 365 jours suivant la date index (exclue)"
		;
	format ATB_Inf_Resp_3 ATB_Inf_Resp_2 Broncho_CDA Subs_Nico BPCO_DG_CIBLE BPCO_DG_OBS Spiro EFR ATB_Inf_meme_jour ATB_Inf_365_Jour 
		Broncho_LDA Spiro_Av_365 EFR_Av_365 Spiro_Ap_365 EFR_Ap_365 Pneumothorax Pneumothorax_AV_365 Pneumothorax_AP_365 Infarctus 
		Infarctus_AV_365 Infarctus_AP_365 f_oui_non. BPCO_DG_Date_index Date_index_broncho Date_index_antibio_memejour Date_index_antibio_365jour 
		Date_index_substitut Date_Spiro Date_EFR Date_Spiro_Av_365 Date_EFR_Av_365 Date_Spiro_Ap_365 Date_EFR_Ap_365 date9. Lieu_Spiro Lieu_EFR 
		Lieu_Spiro_Av_365 Lieu_EFR_Av_365 Lieu_Spiro_Ap_365 Lieu_EFR_Ap_365 f_lieu_soin.;
run;
			
*	******************************************************************************************************************************************;
* 	R�alisation d EFR ou d une spirom�trie annuelle;

proc sql;

	INSERT INTO flowch.Flow_Chart_BPCO_DG_&an_N.
		SELECT
			21,
			COUNT(DISTINCT BEN_IDT_ANO)
	FROM res.T_INDI_BPCO_DG_&an_N.
	WHERE BPCO_DG_OBS = 1;

	* Nombre de patients pour les num�rateur des premi�res exclusions;
	SELECT Nb_patients_ref0 into : N0 FROM travail.ref_flowcharts; 
	SELECT N into : N4 FROM flowch.Flow_Chart_BPCO_DG_&an_N. WHERE indicateur = 4;
	SELECT N into : N9 FROM flowch.Flow_Chart_BPCO_DG_&an_N. WHERE indicateur = 9;

	* Population d �tude;
	SELECT N into : N15 FROM flowch.Flow_Chart_BPCO_DG_&an_N. WHERE indicateur = 15;

	* Population cible;
	SELECT N into : N20 FROM flowch.Flow_Chart_BPCO_DG_&an_N. WHERE indicateur = 20;

quit;

data flowch.Flow_Chart_BPCO_DG_&an_N.;
	set flowch.Flow_Chart_BPCO_DG_&an_N.;
	if indicateur in (1, 2, 2.5, 3, 4) then
		percent = N / &N0.;
	if indicateur in (5, 6, 7, 8, 9) then
		percent = N / &N4.;
	if indicateur in (10, 11, 12, 13, 14, 15) then
		percent = N / &N9.;
	if indicateur in (16, 17, 18, 19, 20, 21) then
		percent = N / &N15.;
	if indicateur = 21 then
		percent = N / &N20.;
	format indicateur f_ind_01_flowchart. percent percent7.1;		
run;

* V�rif;
proc sql;

	SELECT COUNT(*) AS nb_lignes, COUNT(DISTINCT BEN_IDT_ANO) AS nb_patients
	FROM res.T_INDI_BPCO_DG_&an_N.;
	* nb_lignes = nb_patients : OK;

	SELECT COUNT(*) AS nb_lignes, COUNT(DISTINCT BEN_IDT_ANO) AS nb_patients
	FROM res.T_INDI_BPCO_DG_&an_N.
	WHERE BPCO_DG_CIBLE = 1;
	* nb_lignes = nb_patients : OK;

quit;

*	******************************************************************************************************************************************;
*	On ajoute le flag BPCO_DG_CIBLE dans la table res.T_INDI_BPCO_&an_N.;

proc sql;

	ALTER TABLE res.T_INDI_BPCO_&an_N. DROP BPCO_DG_CIBLE;

	ALTER TABLE res.T_INDI_BPCO_&an_N. ADD BPCO_DG_CIBLE INT format = f_oui_non. length = 3
		label = "Patient appartenant � la population cible BPCO_DG";

	UPDATE res.T_INDI_BPCO_&an_N.
		SET BPCO_DG_CIBLE = CASE	WHEN BEN_IDT_ANO IN (SELECT DISTINCT BEN_IDT_ANO FROM res.T_INDI_BPCO_DG_&an_N. WHERE 
											BPCO_DG_CIBLE = 1) THEN 1
										ELSE 0
										END;

quit;

data res.T_INDI_BPCO_&an_N.;
	set res.T_INDI_BPCO_&an_N.;
	if BPCO_DG_CIBLE = .
		then BPCO_DG_CIBLE = 0;
run;

*	******************************************************************************************************************************************;
*	On corrige la variable broncho_LDA, qui est incoh�rente avec la variable nb_broncho_LDA de la table res.T_INDI_BPCO_&an_N.;
*	En effet, certaines d�livrances de m�dicamnts BDLA de 2017 ne remontent que dans les flux de 2019. Ils sont donc pris en compte dans le;
*	flag broncho_LDA alors qu'ils n �taient pas pris en compte dans le d�nombrement nb_broncho_LDA de la table res.T_INDI_BPCO_&an_N.;
*	On d�finit broncho_LDA � 0 pour les patients ayant nb_broncho_LDA = 0;

proc sql;

	CREATE TABLE verif_LDA AS
		SELECT
			a.BEN_IDT_ANO,
			a.Broncho_LDA,
			b.Nb_Broncho_LDA
		FROM res.T_INDI_BPCO_DG_17 a
			INNER JOIN res.T_INDI_BPCO_17 b
				ON a.BEN_IDT_ANO = b.BEN_IDT_ANO
		WHERE a.Broncho_LDA = 1 AND b.Nb_Broncho_LDA = 0;

quit;

proc sql;

	UPDATE res.T_INDI_BPCO_DG_&an_N.
		SET Broncho_LDA = 0
		WHERE BEN_IDT_ANO IN (SELECT BEN_IDT_ANO FROM verif_LDA);

quit;

proc delete data = verif_LDA;
run; quit;
