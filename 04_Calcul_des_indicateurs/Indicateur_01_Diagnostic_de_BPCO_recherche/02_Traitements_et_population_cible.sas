/*	******************************************************************************************************************************************	*/
/*																																				*/
/*		Indicateur 01 : Rep�rage des traitements															 									*/
/*																																				*/
/*	******************************************************************************************************************************************	*/

*	******************************************************************************************************************************************;
*	D�livrance rembours�e d antibioth�rapie pour infections respiratoires l ann�e N pr�c�d�e de d�livrances rembours�es d antibioth�rapie 
*	pour infections respiratoires dans les 365 jours;

proc sql;

	CREATE TABLE patients_antibio AS
		SELECT
			a.BEN_IDT_ANO,
			b.date_debut AS date_deliv_antibio_&Annee_N. format ddmmyy10. length = 4
		FROM res.T_INDI_BPCO_DG_&an_N. a
			INNER JOIN travail.reperage_antibio_&Annee_1N._&Annee_N1. b
				ON a.BEN_IDT_ANO = b.BEN_IDT_ANO
		WHERE YEAR(b.date_debut) = &Annee_N. AND b.reperage = 36
		ORDER BY 1, 2;

quit;

* On r�cup�re toutes les lignes d antibio dans les 365 jours pr�c�dants la d�livrance d antibio de l ann�e N et on compte les d�livrances;
proc sql undo_policy = none;

	CREATE TABLE patients_antibio AS
		SELECT
			a.BEN_IDT_ANO,
			a.date_deliv_antibio_&Annee_N.,
			COUNT(DISTINCT b.date_debut) AS nb_deliv length = 3
		FROM patients_antibio a
			INNER JOIN travail.reperage_antibio_&Annee_1N._&Annee_N1. b
				ON a.BEN_IDT_ANO = b.BEN_IDT_ANO
		WHERE (a.date_deliv_antibio_&Annee_N. - 365) <= b.date_debut < a.date_deliv_antibio_&Annee_N. AND b.reperage = 36
		GROUP BY a.BEN_IDT_ANO, date_deliv_antibio_&Annee_N.
		ORDER BY a.BEN_IDT_ANO, date_deliv_antibio_&Annee_N.;

	CREATE TABLE patients_antibio AS
		SELECT
			BEN_IDT_ANO,
			MAX(nb_deliv) AS nb_deliv length = 3
		FROM patients_antibio a
		GROUP BY BEN_IDT_ANO;

quit;

* On ajoute l information dans la table de r�sultats;
data res.T_INDI_BPCO_DG_&an_N.;
	merge	res.T_INDI_BPCO_DG_&an_N. (in = a)
			patients_antibio (in = b);
	by BEN_IDT_ANO;
	if a;
	length nb_deliv ATB_Inf_Resp_2 ATB_Inf_Resp_3 3.;
	if not b then
		nb_deliv = 0;
	ATB_Inf_Resp_2 = 0;
	if nb_deliv >= 1 then 
		ATB_Inf_Resp_2 = 1;
	ATB_Inf_Resp_3 = 0;
	if nb_deliv >= 2 then 
		ATB_Inf_Resp_3 = 1;
run;

proc delete data = patients_antibio;
run; quit;

*	******************************************************************************************************************************************;
*	D�livrance rembours�e de bronchodilatateur anticholinergique ou B�ta-2 longue dur�e d action d�livr�e l ann�e N;

proc sql;

	CREATE TABLE patients_LDA AS
		SELECT DISTINCT
			a.BEN_IDT_ANO,
			1 AS broncho_LDA length = 3
		FROM res.T_INDI_BPCO_DG_&an_N. a
			INNER JOIN travail.reperage_BDLA_&Annee_2N._&Annee_N1. b
				ON a.BEN_IDT_ANO = b.BEN_IDT_ANO
		WHERE YEAR(b.date_debut) = &Annee_N.
		ORDER BY 1, 2;

quit;

* On ajoute l information dans la table de r�sultats;
data res.T_INDI_BPCO_DG_&an_N.;
	merge	res.T_INDI_BPCO_DG_&an_N. (in = a)
			patients_LDA (in = b);
	by BEN_IDT_ANO;
	if a;
	if broncho_LDA = . then 
		broncho_LDA = 0;
run;

proc delete data = patients_LDA;
run; quit;

*	******************************************************************************************************************************************;
* 	Population cible;

data res.T_INDI_BPCO_DG_&an_N.;
	set res.T_INDI_BPCO_DG_&an_N.;
	BPCO_DG_CIBLE = 1;
run;

proc sort data = res.T_INDI_BPCO_DG_&an_N. nodupkey;
	by _all_;
run;

*	******************************************************************************************************************************************;
* 	Nombre de d�livrances rembours�es d antibioth�rapie pour infections respiratoires l ann�e N;

proc sql;

	CREATE TABLE nb_antibio AS
		SELECT
			a.BEN_IDT_ANO,
			COUNT(DISTINCT b.date_debut) AS Nb_ATB_Inf_Resp length = 3
		FROM res.T_INDI_BPCO_DG_&an_N. a
			INNER JOIN travail.reperage_antibio_&Annee_1N._&Annee_N1. b
				ON a.BEN_IDT_ANO = b.BEN_IDT_ANO
		WHERE YEAR(b.date_debut) = &Annee_N. AND b.reperage = 36
		GROUP BY a.BEN_IDT_ANO
		ORDER BY a.BEN_IDT_ANO;

quit;

* On ajoute l information dans la table de r�sultats;
data res.T_INDI_BPCO_DG_&an_N.;
	merge	res.T_INDI_BPCO_DG_&an_N. (in = a)
			nb_antibio (in = b);
	by BEN_IDT_ANO;
	if a;
	if Nb_ATB_Inf_Resp = . then 
		Nb_ATB_Inf_Resp = 0;
run;

proc delete data = nb_antibio;
run; quit;

*	******************************************************************************************************************************************;
* 	Nombre de d�livrances rembours�es d'antibioth�rapie pour infections respiratoires dans les 365 jours pr�c�dant la date index;

proc sql;

	CREATE TABLE nb_antibio AS
		SELECT
			a.BEN_IDT_ANO,
			COUNT(DISTINCT b.date_debut) AS Nb_ATB_Inf_Resp_AV365J length = 3
		FROM res.T_INDI_BPCO_DG_&an_N. a
			INNER JOIN travail.reperage_antibio_&Annee_1N._&Annee_N1. b
				ON a.BEN_IDT_ANO = b.BEN_IDT_ANO
		WHERE (a.BPCO_DG_Date_index - 365) <= b.date_debut < BPCO_DG_Date_index AND b.reperage = 36
		GROUP BY a.BEN_IDT_ANO
		ORDER BY a.BEN_IDT_ANO;

quit;

* On ajoute l information dans la table de r�sultats;
data res.T_INDI_BPCO_DG_&an_N.;
	merge	res.T_INDI_BPCO_DG_&an_N. (in = a)
			nb_antibio (in = b);
	by BEN_IDT_ANO;
	if a;
	if Nb_ATB_Inf_Resp_AV365J = . then 
		Nb_ATB_Inf_Resp_AV365J = 0;
run;

proc delete data = nb_antibio;
run; quit;

*	******************************************************************************************************************************************;
* 	Nombre de d�livrances rembours�es de bronchodilatateur anticholinergique ou B�ta-2 courte dur�e d action l ann�e N;

proc sql;

	CREATE TABLE nb_BDCA AS
		SELECT
			a.BEN_IDT_ANO,
			COUNT(DISTINCT b.date_debut) AS Nb_Broncho_CDA length = 3
		FROM res.T_INDI_BPCO_DG_&an_N. a
			INNER JOIN travail.reperage_BDCA_tabac_&Annee_2N._&Annee_N1. b
				ON a.BEN_IDT_ANO = b.BEN_IDT_ANO
		WHERE YEAR(b.date_debut) = &Annee_N. AND b.reperage in (42, 43, 44)
		GROUP BY a.BEN_IDT_ANO
		ORDER BY 1, 2;

quit;

* On ajoute l information dans la table de r�sultats;
data res.T_INDI_BPCO_DG_&an_N.;
	merge	res.T_INDI_BPCO_DG_&an_N. (in = a)
			nb_BDCA (in = b);
	by BEN_IDT_ANO;
	if a;
	if Nb_Broncho_CDA = . then 
		Nb_Broncho_CDA = 0;
run;

proc delete data = nb_BDCA;
run; quit;

*	******************************************************************************************************************************************;
* 	Nombre de d�livrances rembours�es de bronchodilatateur anticholinergique ou B�ta-2 courte dur�e d�action dans les 365 jours pr�c�dant la 
date index;

proc sql;

	CREATE TABLE nb_BDCA AS
		SELECT
			a.BEN_IDT_ANO,
			COUNT(DISTINCT b.date_debut) AS Nb_Broncho_CDA_AV365j length = 3
		FROM res.T_INDI_BPCO_DG_&an_N. a
			INNER JOIN travail.reperage_BDCA_tabac_&Annee_2N._&Annee_N1. b
				ON a.BEN_IDT_ANO = b.BEN_IDT_ANO
		WHERE (a.BPCO_DG_Date_index - 365) <= b.date_debut < BPCO_DG_Date_index  AND b.reperage in (42, 43, 44)
		GROUP BY a.BEN_IDT_ANO
		ORDER BY 1, 2;

quit;

* On ajoute l information dans la table de r�sultats;
data res.T_INDI_BPCO_DG_&an_N.;
	merge	res.T_INDI_BPCO_DG_&an_N. (in = a)
			nb_BDCA (in = b);
	by BEN_IDT_ANO;
	if a;
	if Nb_Broncho_CDA_AV365j = . then 
		Nb_Broncho_CDA_AV365j = 0;
run;

proc delete data = nb_BDCA;
run; quit;

*	******************************************************************************************************************************************;
* 	Nombre de d�livrances rembours�es de substitus nicotiniques l ann�e N;

proc sql;

	CREATE TABLE Nb_Subs_Nico AS
		SELECT
			a.BEN_IDT_ANO,
			COUNT(DISTINCT b.date_debut) AS Nb_Subs_Nico length = 3
		FROM res.T_INDI_BPCO_DG_&an_N. a
			INNER JOIN travail.reperage_BDCA_tabac_&Annee_2N._&Annee_N1. b
				ON a.BEN_IDT_ANO = b.BEN_IDT_ANO
		WHERE YEAR(b.date_debut) = &Annee_N. AND b.reperage = 35
		GROUP BY a.BEN_IDT_ANO
		ORDER BY 1, 2;

quit;

* On ajoute l information dans la table de r�sultats;
data res.T_INDI_BPCO_DG_&an_N.;
	merge	res.T_INDI_BPCO_DG_&an_N. (in = a)
			Nb_Subs_Nico (in = b);
	by BEN_IDT_ANO;
	if a;
	if Nb_Subs_Nico = . then 
		Nb_Subs_Nico = 0;
run;

proc delete data = Nb_Subs_Nico;
run; quit;
