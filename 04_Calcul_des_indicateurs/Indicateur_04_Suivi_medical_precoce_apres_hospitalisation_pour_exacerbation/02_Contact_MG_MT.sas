/*	******************************************************************************************************************************************	*/
/*																																				*/
/*		Indicateur 04 : Contact avec un m�decin g�n�raliste ou traitant															 				*/
/*																																				*/
/*	******************************************************************************************************************************************	*/

*	******************************************************************************************************************************************;
*	Contact avec le m�decin g�n�raliste ou le m�decin traitant;
*	On joint avec la table res.T_INDI_BPCO_SMED_P_EA_&an_N. pour r�cup�rer les info dans les 7 et 180 jours suivant la date index;

%macro contact_MG_MT(nb_jours=);

	proc sql;

		CREATE TABLE contact_MG_PMSI_&nb_jours.j AS
			SELECT DISTINCT
				a.BPCO_SMED_P_EA_Sej_Index,
				a.BPCO_SMED_P_EA_Date_index,
				b.date_debut
			FROM res.T_INDI_BPCO_SMED_P_EA_&an_N. a
				INNER JOIN travail.contact_MG_PMSI_&Annee_N._&Annee_N1. b
					ON a.BEN_IDT_ANO = b.BEN_IDT_ANO
			WHERE a.BPCO_SMED_P_EA_Date_index <= b.date_debut <= (a.BPCO_SMED_P_EA_Date_index + &nb_jours.)
			ORDER BY BPCO_SMED_P_EA_Sej_Index;

		CREATE TABLE contact_MT_&nb_jours.j AS
			SELECT DISTINCT
				a.BPCO_SMED_P_EA_Sej_Index,
				a.BPCO_SMED_P_EA_Date_index,
				b.date_debut
			FROM res.T_INDI_BPCO_SMED_P_EA_&an_N. a
				INNER JOIN travail.contact_MT_&Annee_N._&Annee_N1. b
					ON a.BEN_IDT_ANO = b.BEN_IDT_ANO
			WHERE a.BPCO_SMED_P_EA_Date_index <= b.date_debut <= (a.BPCO_SMED_P_EA_Date_index + &nb_jours.)
			ORDER BY BPCO_SMED_P_EA_Sej_Index;

	quit;

	data contact_MG_MT_&nb_jours.j;
		set	contact_MG_PMSI_&nb_jours.j contact_MT_&nb_jours.j;
	run;

	proc sql undo_policy = none;

		CREATE TABLE contact_MG_MT_&nb_jours.j AS
			SELECT DISTINCT
				BPCO_SMED_P_EA_Sej_Index,
				BPCO_SMED_P_EA_Date_index,
				1 AS Contact_Med_&nb_jours.j length = 3,
				COUNT(DISTINCT date_debut) AS Nb_Contact_Med_&nb_jours.j length = 3,
				MIN(date_debut) AS Date_Contact_Med_&nb_jours.j length = 4
			FROM contact_MG_MT_&nb_jours.j
			GROUP BY BPCO_SMED_P_EA_Sej_Index, BPCO_SMED_P_EA_Date_index;

	quit;

%mend contact_MG_MT;

%contact_MG_MT(nb_jours = 7);
%contact_MG_MT(nb_jours = 180);

*	On ajoute les informations dans la table de r�sultats;
proc sort data = res.T_INDI_BPCO_SMED_P_EA_&an_N.;
	by BPCO_SMED_P_EA_Sej_Index;
run;

* Dans les 7 jours;
proc sort data = contact_MG_MT_7j;
	by BPCO_SMED_P_EA_Sej_Index;
run;

data res.T_INDI_BPCO_SMED_P_EA_&an_N.;
	merge	res.T_INDI_BPCO_SMED_P_EA_&an_N. (in = a)
			contact_MG_MT_7j (in = b keep = BPCO_SMED_P_EA_Sej_Index Contact_Med_7j Nb_Contact_Med_7j);
	by BPCO_SMED_P_EA_Sej_Index;
	if Contact_Med_7j ne 1 then
		Contact_Med_7j = 0;
	if Nb_Contact_Med_7j = . then
		Nb_Contact_Med_7j = 0;
	if a then
		output;
run;

* Dans les 180 jours;
proc sort data = contact_MG_MT_180j;
	by BPCO_SMED_P_EA_Sej_Index;
run;

data res.T_INDI_BPCO_SMED_P_EA_&an_N.;
	merge	res.T_INDI_BPCO_SMED_P_EA_&an_N. (in = a)
			contact_MG_MT_180j (in = b keep = BPCO_SMED_P_EA_Sej_Index Date_Contact_Med_180j);
	by BPCO_SMED_P_EA_Sej_Index;
	if a then
		output;
run;

proc datasets library = work memtype = data nolist;
	delete contact_:;
run; quit;
