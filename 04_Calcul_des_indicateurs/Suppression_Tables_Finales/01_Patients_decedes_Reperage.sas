/*	******************************************************************************************************************************************	*/
/*																																				*/
/*		Exclusion des patients d�c�d�s - S�lection des patients																 					*/
/*																																				*/
/*	******************************************************************************************************************************************	*/

%macro verif_dates_deces(var=, sejour=);

	%if &i. = 1 %then
		%do;

			proc sql;

				CREATE TABLE travail.patients_deces_exclus AS
					SELECT DISTINCT
						a.BEN_IDT_ANO
					FROM res.T_INDI_BPCO_&an_N. a
						INNER JOIN res.&table. b
							ON a.BEN_IDT_ANO = b.BEN_IDT_ANO
					WHERE a.BEN_DCD_DTE < b.&var. AND a.BEN_DCD_DTE NE .;

			quit;

		%end;

	%else
		%do;

			proc sql;

				INSERT INTO travail.patients_deces_exclus
					SELECT DISTINCT
						a.BEN_IDT_ANO
					FROM res.T_INDI_BPCO_&an_N. a
						INNER JOIN res.&table. b
							ON a.BEN_IDT_ANO = b.BEN_IDT_ANO
					WHERE a.BEN_DCD_DTE < b.&var. AND a.BEN_DCD_DTE NE .;

			quit;

		%end;

%mend verif_dates_deces;

******************************************************************************;
* Table T_Indi_BPCO_DG_&an_N.;

%let table = T_Indi_BPCO_DG_&an_N.;
%let sejour_index = ;

%let i = 1;
%verif_dates_deces(var = BPCO_DG_Date_Index, sejour = 0);

%let i = %sysevalf(&i. + 1);
%verif_dates_deces(var = Date_index_broncho, sejour = 0);

%let i = %sysevalf(&i. + 1);
%verif_dates_deces(var = Date_index_antibio_memejour , sejour = 0);

%let i = %sysevalf(&i. + 1);
%verif_dates_deces(var = Date_index_antibio_365jour, sejour = 0);

%let i = %sysevalf(&i. + 1);
%verif_dates_deces(var = Date_index_substitut, sejour = 0);

%let i = %sysevalf(&i. + 1);
%verif_dates_deces(var = Date_Spiro, sejour = 0);

%let i = %sysevalf(&i. + 1);
%verif_dates_deces(var = Date_EFR, sejour = 0);

%let i = %sysevalf(&i. + 1);
%verif_dates_deces(var = Date_Spiro_Av_365, sejour = 0);

%let i = %sysevalf(&i. + 1);
%verif_dates_deces(var = Date_EFR_Av_365, sejour = 0);

%let i = %sysevalf(&i. + 1);
%verif_dates_deces(var = Date_Spiro_Ap_365, sejour = 0);

%let i = %sysevalf(&i. + 1);
%verif_dates_deces(var = Date_EFR_Ap_365, sejour = 0);

******************************************************************************;
* Table T_Indi_BPCO_VacG_&an_N.;
%let table = T_Indi_BPCO_VacG_&an_N.;
%let sejour_index = ;

%let i = %sysevalf(&i. + 1);
%verif_dates_deces(var = Date_VacG, sejour = 0);

******************************************************************************;
* Table T_Indi_BPCO_EFR_SPIRO_&an_N.;
%let table = T_Indi_BPCO_EFR_SPIRO_&an_N.;
%let sejour_index = ;

%let i = %sysevalf(&i. + 1);
%verif_dates_deces(var = Date_Spiro, sejour = 0);

%let i = %sysevalf(&i. + 1);
%verif_dates_deces(var = Date_EFR, sejour = 0);

******************************************************************************;
* Table T_Indi_Smed_P_EA_&an_N.;
%let table = T_Indi_BPCO_Smed_P_EA_&an_N.;
%let sejour_index = BPCO_Smed_P_EA_Sej_Index;

%let i = %sysevalf(&i. + 1);
%verif_dates_deces(var = BPCO_Smed_P_EA_Date_Index, sejour = 1);

%let i = %sysevalf(&i. + 1);
%verif_dates_deces(var = Date_Contact_Med_180J, sejour = 1);

%let i = %sysevalf(&i. + 1);
%verif_dates_deces(var = Date_Contact_Pneumo_180J, sejour = 1);

******************************************************************************;
* Table T_Indi_Smed_D_EA_&an_N.;
%let table = T_Indi_BPCO_Smed_D_EA_&an_N.;
%let sejour_index = BPCO_Smed_D_EA_Sej_Index;

%let i = %sysevalf(&i. + 1);
%verif_dates_deces(var = BPCO_Smed_D_EA_Date_Index, sejour = 1);

%let i = %sysevalf(&i. + 1);
%verif_dates_deces(var = Date_Contact_Pneumo_180J, sejour = 1);

******************************************************************************;
* Table T_Indi_BPCO_BDLA_EA_&an_N.;
%let table = T_Indi_BPCO_BDLA_EA_&an_N.;
%let sejour_index = BPCO_BDLA_EA_Sej_Index;

%let i = %sysevalf(&i. + 1);
%verif_dates_deces(var = BPCO_BDLA_EA_Date_Index, sejour = 1);

%let i = %sysevalf(&i. + 1);
%verif_dates_deces(var = Date_Broncho_LDA_180J, sejour = 1);

******************************************************************************;
* Table T_Indi_BPCO_RR_EA_&an_N.;
%let table = T_Indi_BPCO_RR_EA_&an_N.;
%let sejour_index = ;

%let i = %sysevalf(&i. + 1);
%verif_dates_deces(var = BPCO_RR_EA_Date_Index, sejour = 0);

%let i = %sysevalf(&i. + 1);
%verif_dates_deces(var = Date_RR, sejour = 0);

%let i = %sysevalf(&i. + 1);
%verif_dates_deces(var = Date_RR_Prec, sejour = 0);

******************************************************************************;
* Table T_Indi_Reh_EA_&an_N.;
/*
%let table = T_Indi_BPCO_Reh_EA_&an_N.;
%let sejour_index = BPCO_Reh_EA_Sej_Index;

%let i = %sysevalf(&i. + 1);
%verif_dates_deces(var = BPCO_ReH_EA_Date_Index, sejour = 1);

%let i = %sysevalf(&i. + 1);
%verif_dates_deces(var = ReH_Date, sejour = 1);

%let i = %sysevalf(&i. + 1);
%verif_dates_deces(var = Date_Contact_Med_180J, sejour = 1);

%let i = %sysevalf(&i. + 1);
%verif_dates_deces(var = Date_Contact_Pneumo_180J, sejour = 1);

%let i = %sysevalf(&i. + 1);
%verif_dates_deces(var = Date_RR_180J, sejour = 1);
*/

proc sql;

	SELECT COUNT(DISTINCT BEN_IDT_ANO)
	FROM travail.patients_deces_exclus;

quit;
