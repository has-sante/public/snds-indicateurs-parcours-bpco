/*	******************************************************************************************************************************************	*/
/*																																				*/
/*		Exclusion des patients d�c�d�s - Suppression dans les tables de r�sultat											 					*/
/*																																				*/
/*	******************************************************************************************************************************************	*/

proc sql;

	DELETE FROM res.T_INDI_BPCO_BDLA_EA_&an_N.
	WHERE BEN_IDT_ANO IN (SELECT BEN_IDT_ANO FROM travail.patients_deces_exclus);

	DELETE FROM res.T_INDI_BPCO_DG_&an_N.
	WHERE BEN_IDT_ANO IN (SELECT BEN_IDT_ANO FROM travail.patients_deces_exclus);

	DELETE FROM res.T_INDI_BPCO_EFR_SPIRO_&an_N.
	WHERE BEN_IDT_ANO IN (SELECT BEN_IDT_ANO FROM travail.patients_deces_exclus);

	/*
	DELETE FROM res.T_INDI_BPCO_ReH_EA_&an_N.
	WHERE BEN_IDT_ANO IN (SELECT BEN_IDT_ANO FROM travail.patients_deces_exclus);
	*/

	DELETE FROM res.T_INDI_BPCO_RR_EA_&an_N.
	WHERE BEN_IDT_ANO IN (SELECT BEN_IDT_ANO FROM travail.patients_deces_exclus);

	DELETE FROM res.T_INDI_BPCO_SMED_D_EA_&an_N.
	WHERE BEN_IDT_ANO IN (SELECT BEN_IDT_ANO FROM travail.patients_deces_exclus);

	DELETE FROM res.T_INDI_BPCO_SMED_P_EA_&an_N.
	WHERE BEN_IDT_ANO IN (SELECT BEN_IDT_ANO FROM travail.patients_deces_exclus);

	DELETE FROM res.T_INDI_BPCO_VACG_&an_N.
	WHERE BEN_IDT_ANO IN (SELECT BEN_IDT_ANO FROM travail.patients_deces_exclus);

quit;

proc sql;

	ALTER TABLE res.T_INDI_BPCO_&an_N. DROP exclus_DCD;

	ALTER TABLE res.T_INDI_BPCO_&an_N. ADD exclus_DCD INT length = 3;

	UPDATE res.T_INDI_BPCO_&an_N.
		SET		BPCO_DG_CIBLE = 0,
				BPCO_VACG_PROB_CIBLE = 0,
				BPCO_VACG_DIAG_CIBLE = 0,
				BPCO_VACG_CIBLE = 0,
				BPCO_RR_EA_CIBLE = 0,
				BPCO_BDLA_EA_CIBLE = 0,
				BPCO_SMED_P_EA_CIBLE = 0,
				BPCO_SMED_D_EA_CIBLE = 0,
				/*BPCO_ReH_EA_CIBLE = 0,*/
				BPCO_EFR_SPIRO_CIBLE = 0,
				exclus_DCD = 1
		WHERE BEN_IDT_ANO IN (SELECT BEN_IDT_ANO FROM travail.patients_deces_exclus);

quit;
