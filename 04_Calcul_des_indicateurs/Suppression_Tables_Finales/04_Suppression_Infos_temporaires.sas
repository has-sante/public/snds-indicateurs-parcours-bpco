/*	******************************************************************************************************************************************	*/
/*																																				*/
/*		Suppression de toutes les tables, variables et observations inutiles								 									*/
/*																																				*/
/*	******************************************************************************************************************************************	*/

* Suppression des individus non rep�r�s dans les indicateurs;
proc sql;

	DELETE FROM res.T_INDI_BPCO_&an_N.
	WHERE BPCO_DG_V1_CIBLE = 0 AND BPCO_DG_V2_CIBLE = 0 AND BPCO_VACG_PROB_CIBLE = 0 AND BPCO_VACG_DIAG_CIBLE = 0 AND BPCO_RR_EA_CIBLE = 0 AND 
		BPCO_BDLA_EA_CIBLE = 0 AND BPCO_SMED_P_EA_CIBLE = 0 AND BPCO_SMED_D_EA_CIBLE = 0 AND BPCO_ReH_EA_CIBLE = 0 AND BPCO_EFR_SPIRO_CIBLE = 0;

quit;

* Suppression des variables de pr�-s�lection des indicateurs;
proc sql;

	ALTER TABLE res.T_INDI_BPCO_&an_N.
	DROP Nb_hospit_BPCO, Nb_hospit_BPCO_Ind02, ALD_BPCO_septembre, ALD_BPCO_decembre, Nb_ATB, Nb_Tabac, indicateur_01, indicateur_02a, 
		indicateur_02b, indicateur_03, indicateur_04, indicateur_05, indicateur_06, indicateur_07, indicateur_09, CODE_INSEE, exclus_DCD;

quit;

* Suppression des tables temporaires;
proc delete data =
	pop.indicateur_&an_N.
	travail.contact_MG_PMSI_&annee_N._&annee_N1.
	travail.contact_MT_&annee_N._&annee_N1. 
	travail.contact_pneumo_&annee_N._&annee_N1.
	travail.corres_id_patient
	travail.diag_asthme_&annee_1N._&annee_N1.
	travail.fruite_&annee_N.
	travail.histo_ALD
	travail.histo_deces
	travail.insuf_coro_aortique_&annee_N._&annee_N1.
	travail.patients_deces_exclus
	travail.pneumothorax_infarctus_&Annee_1N._&annee_N1.
	travail.polyarthrite_rhum_&annee_4N._&annee_N1.
	travail.prothese_chir_rachis_&annee_1N._&annee_N1.
	travail.ref_flowcharts
	travail.reperage_antibio_&annee_1N._&annee_N1.
	travail.reperage_BDCA_tabac_&annee_2N._&annee_N1.
	travail.reperage_BDLA_&annee_2N._&annee_N1.
	travail.reperage_CCAM_CSARR
	travail.reperage_EFR_spiro_&Annee_2N._&annee_N1.
	travail.reperage_hospit_BPCO_&annee_4N._&annee_N1.
	travail.reperage_med_asthme_&annee_2N._&annee_N1.
	travail.reperage_Oxygeno_&Annee_N._&annee_N1.
	travail.reperage_RR_&annee_1N._&annee_N1.
	travail.RR_sejours_SSR_&Annee_1N._&Annee_N1.
	travail.reperage_Thermo_bronch_&annee_1N._&annee_N1.
	travail.reperage_vacc_grippe_&annee_1N._&annee_N1.
	travail.reperage_VNI_&annee_1N._&annee_N1.
	travail.sejours_&annee_N._&annee_N1.
	travail.sejours_cible_exacerbation_&annee_1N.
	travail.sejours_cible_exacerbation_&annee_N.
	travail.sejours_cible_exacerbation_&annee_N1.
	travail.sejours_exacerbation_&annee_1N.
	travail.sejours_exacerbation_&annee_N.
	travail.sejours_exacerbation_&annee_N1.
	travail.soin_palliatif_&annee_2N._&annee_N1.
	travail.spondylarthrite_&annee_4N._&annee_N1.
	;
run; quit;

proc datasets library = orauser nolist kill;
run; quit;
