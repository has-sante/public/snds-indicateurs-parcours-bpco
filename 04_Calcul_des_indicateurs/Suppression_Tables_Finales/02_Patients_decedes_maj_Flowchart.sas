/*	******************************************************************************************************************************************	*/
/*																																				*/
/*		Exclusion des patients d�c�d�s - Mise � jour des Flowchart															 					*/
/*																																				*/
/*	******************************************************************************************************************************************	*/

%macro maj_flowchart(indi_res=, indi_fl=, pop_obs=, num_pop_etude=, num_pop_DC=, num_pop_cible=, num_indic=, sejour=);

	* On compte le nombre de patients d�c�d�s;
	proc sql;

		* Nombre de patients d�c�d�s parmi la population Cible ou le nombre de s�jours;
		SELECT
			%if &sejour. = 0 %then %do; COUNT(DISTINCT BEN_IDT_ANO) INTO : nb_source %end;
			%if &sejour. = 1 %then %do; COUNT(DISTINCT BPCO_&indi_res._Sej_Index) INTO : nb_source %end;
		FROM res.T_INDI_BPCO_&indi_res._&an_N.
		WHERE BEN_IDT_ANO IN (SELECT BEN_IDT_ANO FROM travail.patients_deces_exclus);

		* Nombre de patients d�c�d�s parmi la population observ�e;
		SELECT
			%if &sejour. = 0 %then %do; COUNT(DISTINCT BEN_IDT_ANO) INTO : nb_indi %end;
			%if &sejour. = 1 %then %do; COUNT(DISTINCT BPCO_&indi_res._Sej_Index) INTO : nb_indi %end;
		FROM res.T_INDI_BPCO_&indi_res._&an_N.
		WHERE &pop_obs. = 1 AND BEN_IDT_ANO IN (SELECT BEN_IDT_ANO FROM travail.patients_deces_exclus);

		* Nombre de patients parmi la population d �tude (pour calcul des %);
		SELECT N into : nb_etude
		FROM flowch.Flow_Chart_BPCO_&indi_fl._&an_N.
		WHERE indicateur = &num_pop_etude.;

		* On ins�re le nouvel indicateur d exclusion;
		INSERT INTO flowch.Flow_Chart_BPCO_&indi_fl._&an_N.	
			SELECT
				&num_pop_DC.,
				%if &sejour. = 0 %then %do; 
					COUNT(DISTINCT BEN_IDT_ANO),
					COUNT(DISTINCT BEN_IDT_ANO)/&nb_etude.
				%end;
				%if &sejour. = 1 %then %do; 
					COUNT(DISTINCT BPCO_&indi_res._Sej_Index),
					COUNT(DISTINCT BPCO_&indi_res._Sej_Index)/&nb_etude.
				%end;
			FROM res.T_INDI_BPCO_&indi_res._&an_N.
			WHERE BEN_IDT_ANO IN (SELECT BEN_IDT_ANO FROM travail.patients_deces_exclus);

	quit;

	* On trie pour visualiser la nouvelle exclusion avant la population cible;
	proc sort data = flowch.Flow_Chart_BPCO_&indi_fl._&an_N.;
		by indicateur;
	run;

	* On met � jour les % : Pour la population Cible, on supprime les patients exclus et on recalcule le pourcentage;
	data flowch.Flow_Chart_BPCO_&indi_fl._&an_N.;
		set flowch.Flow_Chart_BPCO_&indi_fl._&an_N.;
		if indicateur = &num_pop_cible. then
			do;
				N = N - &nb_source.;
				percent = N / &nb_etude.;
			end;
	run;

	* On sauvegarde la nouvelle population cible;
	proc sql;

		SELECT N INTO : nb_cible
		FROM flowch.Flow_Chart_BPCO_&indi_fl._&an_N.
		WHERE indicateur = &num_pop_cible.;

	quit;

	* On met � jour les % : Pour la population observ�e, on supprime les patients exclus et on recalcule le pourcentage;
	data flowch.Flow_Chart_BPCO_&indi_fl._&an_N.;
		set flowch.Flow_Chart_BPCO_&indi_fl._&an_N.;
		if indicateur = &num_indic. then
			do;
				N = N - &nb_indi.;
				percent = N / &nb_cible.;
			end;
	run;

	* Sp�cifique pour l indicateur de suivi m�dical pr�coce : il faut m�j les indicateur de suivi MG et de suivi pneumo;
	%if &indi_res. = SMED_P_EA %then
		%do;

			proc sql;

				* Nombre de s�jours de patients d�c�d�s parmi la population observ�e avec suivi m�d pneumo;
				SELECT
					COUNT(DISTINCT BPCO_SMED_P_EA_Sej_Index) INTO : nb_cible_pneumo
				FROM res.T_INDI_BPCO_SMED_P_EA_&an_N.
				WHERE Contact_Pneumo_7j = 1 AND BEN_IDT_ANO IN (SELECT BEN_IDT_ANO FROM travail.patients_deces_exclus);

				* Nombre de patients d�c�d�s parmi la population observ�e avec suivi m�d MG;
				SELECT
					COUNT(DISTINCT BPCO_SMED_P_EA_Sej_Index) INTO : nb_cible_MG
				FROM res.T_INDI_BPCO_SMED_P_EA_&an_N.
				WHERE Contact_Med_7j = 1 AND BEN_IDT_ANO IN (SELECT BEN_IDT_ANO FROM travail.patients_deces_exclus);

			quit;

			data flowch.Flow_Chart_BPCO_&indi_fl._&an_N.;
				set flowch.Flow_Chart_BPCO_&indi_fl._&an_N.;
				if indicateur = 18 then
					do;
						N = N - &nb_cible_pneumo.;
						percent = N / &nb_cible.;
					end;
				if indicateur = 19 then
					do;
						N = N - &nb_cible_MG.;
						percent = N / &nb_cible.;
					end;
			run;

		%end;

%mend maj_flowchart;

* Indicateur 1 - Diagnostic de BPCO recherch�;
%maj_flowchart(
	indi_res = DG, 
	indi_fl = DG, 
	pop_obs = BPCO_DG_OBS, 
	num_pop_etude = 15, 
	num_pop_DC = 19, 
	num_pop_cible = 20, 
	num_indic = 21,
	sejour = 0
	);

* Indicateur 2 - Vaccin contre la grippe;
%maj_flowchart(
	indi_res = VACG, 
	indi_fl = VACG, 
	pop_obs = BPCO_VACG_OBS, 
	num_pop_etude = 19, 
	num_pop_DC = 24, 
	num_pop_cible = 25, 
	num_indic = 26,
	sejour = 0
	);

* Indicateur 3 - R�alisation d'EFR ou d une spirom�trie annuelle;
%maj_flowchart(
	indi_res = EFR_SPIRO, 
	indi_fl = EFR_SPIRO, 
	pop_obs = BPCO_EFR_SPIRO_OBS, 
	num_pop_etude = 15, 
	num_pop_DC = 20, 
	num_pop_cible = 21, 
	num_indic = 22,
	sejour = 0
	);

* Indicateur 4 - Suivi m�ical pr�coce apr�s hospitalisation pour exacerbation;
%maj_flowchart(
	indi_res = SMED_P_EA, 
	indi_fl = SMED_P_EA, 
	pop_obs = BPCO_SMED_P_EA_OBS, 
	num_pop_etude = 12, 
	num_pop_DC = 16, 
	num_pop_cible = 17, 
	num_indic = 20,
	sejour = 1
	);

* Indicateur 5 - Suivi m�ical � distance apr�s hospitalisation pour exacerbation;
%maj_flowchart(
	indi_res = SMED_D_EA, 
	indi_fl = SMED_D_EA, 
	pop_obs = BPCO_SMED_D_EA_OBS, 
	num_pop_etude = 12, 
	num_pop_DC = 16, 
	num_pop_cible = 17, 
	num_indic = 18,
	sejour = 1
	);

* Indicateur 6 - Traitement apr�s hospitalisation pour exacerbation;
%maj_flowchart(
	indi_res = BDLA_EA, 
	indi_fl = BDLA_EA, 
	pop_obs = BPCO_BDLA_EA_OBS, 
	num_pop_etude = 12, 
	num_pop_DC = 16, 
	num_pop_cible = 17, 
	num_indic = 18,
	sejour = 1
	);

* Indicateur 7 - R�adaptation respiratoire apr�s exacerbation;
%maj_flowchart(
	indi_res = RR_EA, 
	indi_fl = RR_EA, 
	pop_obs = BPCO_RR_EA_OBS, 
	num_pop_etude = 12, 
	num_pop_DC = 17, 
	num_pop_cible = 18, 
	num_indic = 19,
	sejour = 0
	);
/*
* Indicateur 9 - R�hospitalisation pour exacerbation � 6 mois;
%maj_flowchart(
	indi_res = ReH_EA, 
	indi_fl = ReH_EA, 
	pop_obs = BPCO_ReH_EA_OBS, 
	num_pop_etude = 12, 
	num_pop_DC = 18.5, 
	num_pop_cible = 19, 
	num_indic = 20,
	sejour = 1
	);
*/
