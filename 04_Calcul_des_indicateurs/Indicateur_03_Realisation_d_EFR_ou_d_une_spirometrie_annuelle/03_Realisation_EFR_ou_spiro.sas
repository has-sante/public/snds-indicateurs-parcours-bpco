/*	******************************************************************************************************************************************	*/
/*																																				*/
/*		Indicateur 03 : EFR ou Spirom�tries																	 									*/
/*																																				*/
/*	******************************************************************************************************************************************	*/

*	******************************************************************************************************************************************;
*	On rep�re les patients avec une spirom�trie entre le 1er janvier de l ann�e N et le 28 f�vrier de l ann�e N+1;

proc sql;

	CREATE TABLE spirometrie AS
		SELECT DISTINCT
			a.BEN_IDT_ANO,
			MIN(b.date_debut) AS date_debut length = 4,
			MIN(CASE	WHEN b.date_debut < "01JAN&Annee_N."d THEN "01JAN&Annee_N."d
						ELSE b.date_debut
						END) AS Date_Spiro length = 4,
			COUNT(DISTINCT b.date_debut) AS Nb_spiro length = 3
		FROM res.T_INDI_BPCO_EFR_SPIRO_&an_N. a
			INNER JOIN travail.reperage_EFR_spiro_&Annee_2N._&annee_N1. b
				ON a.BEN_IDT_ANO = b.BEN_IDT_ANO
		WHERE b.reperage = 2 AND (b.date_debut <= "28FEB&Annee_N1."d AND b.date_fin >= "01JAN&Annee_N."d)
		GROUP BY a.BEN_IDT_ANO;

	* On fait un max car si DCIR + PMSI, c'est un doublon donc la spiro a lieu dans un etb;
	CREATE TABLE lieu_spiro AS
		SELECT DISTINCT
			a.BEN_IDT_ANO,
			MAX(CASE	WHEN b.domaine = "" THEN 1
						WHEN b.domaine = "MCO" THEN 2
						WHEN b.domaine = "SSR" THEN 3
						END) AS Lieu_Spiro length = 3
			FROM spirometrie a
				LEFT JOIN travail.reperage_EFR_spiro_&Annee_2N._&annee_N1. b
					ON a.BEN_IDT_ANO = b.BEN_IDT_ANO
					AND a.date_debut = b.date_debut
			WHERE b.reperage = 2
			GROUP BY a.BEN_IDT_ANO;

quit;

* On ajoute l information dans la table de r�sultats;

proc sort data = res.T_INDI_BPCO_EFR_SPIRO_&an_N.;
	by BEN_IDT_ANO;
run;

proc sort data = spirometrie;
	by BEN_IDT_ANO;
run;

proc sort data = lieu_spiro;
	by BEN_IDT_ANO;
run;

data res.T_INDI_BPCO_EFR_SPIRO_&an_N.;
	merge	res.T_INDI_BPCO_EFR_SPIRO_&an_N. (in = a)
			spirometrie (in = b)
			lieu_spiro (in = c);
	by BEN_IDT_ANO;
	length Spiro 3.;
	Spiro = 0;
	if Nb_spiro > 0 then
		Spiro = 1;
	if a then
		output;
run;

proc delete data = spirometrie lieu_spiro;
run; quit;

*	******************************************************************************************************************************************;
*	On rep�re les patients avec une EFR entre le 1er janvier de l ann�e N et le 28 f�vrier de l ann�e N+1;

proc sql;

	CREATE TABLE EFR AS
		SELECT DISTINCT
			a.BEN_IDT_ANO,
			MIN(b.date_debut) AS date_debut length = 4,
			MIN(CASE	WHEN b.date_debut < "01JAN&Annee_N."d THEN "01JAN&Annee_N."d
						ELSE b.date_debut
						END) AS Date_EFR length = 4,
			COUNT(DISTINCT b.date_debut) AS Nb_EFR length = 3
		FROM res.T_INDI_BPCO_EFR_SPIRO_&an_N. a
			INNER JOIN travail.reperage_EFR_spiro_&Annee_2N._&annee_N1. b
				ON a.BEN_IDT_ANO = b.BEN_IDT_ANO
		WHERE b.reperage = 1 AND (b.date_debut <= "28FEB&Annee_N1."d AND b.date_fin >= "01JAN&Annee_N."d)
		GROUP BY a.BEN_IDT_ANO;

	* On fait un max car si DCIR + PMSI, c'est un doublon donc l EFR a lieu dans un etb;
	CREATE TABLE lieu_EFR AS
		SELECT DISTINCT
			a.BEN_IDT_ANO,
			MAX(CASE	WHEN b.domaine = "" THEN 1
						WHEN b.domaine = "MCO" THEN 2
						WHEN b.domaine = "SSR" THEN 3
						END) AS Lieu_EFR length = 3
			FROM EFR a
				LEFT JOIN travail.reperage_EFR_spiro_&Annee_2N._&annee_N1. b
					ON a.BEN_IDT_ANO = b.BEN_IDT_ANO
					AND a.date_debut = b.date_debut
			WHERE b.reperage = 1
			GROUP BY a.BEN_IDT_ANO;

quit;

* On ajoute l information dans la table de r�sultats;

proc sort data = res.T_INDI_BPCO_EFR_SPIRO_&an_N.;
	by BEN_IDT_ANO;
run;

proc sort data = EFR;
	by BEN_IDT_ANO;
run;

proc sort data = lieu_EFR;
	by BEN_IDT_ANO;
run;

data res.T_INDI_BPCO_EFR_SPIRO_&an_N.;
	merge	res.T_INDI_BPCO_EFR_SPIRO_&an_N. (in = a)
			EFR (in = b)
			lieu_EFR (in = c);
	by BEN_IDT_ANO;
	length EFR BPCO_EFR_SPIRO_OBS 3.;
	EFR = 0;
	if Nb_EFR > 0 then
		EFR = 1;
	* Patient de la population cible ayant b�n�fici� d une EFR ou une spiro;
	BPCO_EFR_SPIRO_OBS = 0;
	if EFR = 1 or Spiro = 1 then
		BPCO_EFR_SPIRO_OBS = 1;
	if a then
		output;
run;

proc delete data = EFR lieu_EFR;
run; quit;
