/*	******************************************************************************************************************************************	*/
/*																																				*/
/*		Table de population - Patients avec indicateur_03 = 1																	 				*/
/*																																				*/
/*	******************************************************************************************************************************************	*/

data T_INDI_BPCO_EFR_SPIRO_&an_N. (keep = BEN_IDT_ANO BPCO_Probable Diag_BPCO BPCO_diagnostique);
	set res.T_INDI_BPCO_&an_N. (where = (indicateur_03 = 1 and (nb_broncho_LDA >= 3 or ALD_BPCO_decembre = 1 or Nb_hospit_BPCO > 0)));
	length BPCO_Probable Diag_BPCO BPCO_diagnostique 3.;
	BPCO_Probable = 0;
	Diag_BPCO = 0;
	BPCO_diagnostique = 0;
	if nb_broncho_LDA >= 3 then
		BPCO_Probable = 1;
	if Nb_hospit_BPCO > 0 then
		Diag_BPCO = 1;
	if ALD_BPCO_decembre = 1 or Nb_hospit_BPCO > 0 then
		BPCO_diagnostique = 1;
	if BPCO_diagnostique = 1 then
		BPCO_Probable = 0;
run;

proc sort data = T_INDI_BPCO_EFR_SPIRO_&an_N. nodupkey;
	by BEN_IDT_ANO BPCO_Probable Diag_BPCO BPCO_diagnostique;
run;

*	******************************************************************************************************************************************;
*	Effectifs pour le flowchart;

* 	Individus de 40 ans et plus ayant une BPCO probable ou ayant une ALD BPCO active au 31 d�cembre de l ann�e N ou ayant eu au moins un diagnostic de 
	BPCO cod� lors d un s�jour hospitalier en MCO termin� l ann�e N ou lors d un s�jour hospitalier SSR ou HAD l ann�e N;

proc sql;

	CREATE TABLE flowch.Flow_Chart_BPCO_EFR_SPIRO_&an_N.  AS
		SELECT *
		FROM (
			SELECT 1 AS indicateur length = 3, COUNT(DISTINCT BEN_IDT_ANO) AS N length = 5 FROM T_INDI_BPCO_EFR_SPIRO_&an_N. 
				WHERE BEN_IDT_ANO IN (SELECT BEN_IDT_ANO FROM res.T_INDI_BPCO_&an_N. WHERE age >= 40 AND nb_broncho_LDA >= 3)
			UNION ALL
			SELECT 2, COUNT(DISTINCT BEN_IDT_ANO) FROM T_INDI_BPCO_EFR_SPIRO_&an_N. WHERE BEN_IDT_ANO IN (SELECT BEN_IDT_ANO 
				FROM res.T_INDI_BPCO_&an_N. WHERE age >= 40 AND ALD_BPCO_decembre > 0)
			UNION ALL
			SELECT 3, COUNT(DISTINCT BEN_IDT_ANO) FROM T_INDI_BPCO_EFR_SPIRO_&an_N. WHERE BEN_IDT_ANO IN (SELECT BEN_IDT_ANO 
				FROM res.T_INDI_BPCO_&an_N. WHERE age >= 40 AND Nb_hospit_BPCO > 0)
			UNION ALL
			SELECT 4, COUNT(DISTINCT BEN_IDT_ANO) FROM T_INDI_BPCO_EFR_SPIRO_&an_N.
		);

quit;
