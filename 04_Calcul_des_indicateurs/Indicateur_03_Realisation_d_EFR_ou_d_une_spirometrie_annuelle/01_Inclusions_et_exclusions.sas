/*	******************************************************************************************************************************************	*/
/*																																				*/
/*		Indicateur 03 : Inclusion & exclusions																					 				*/
/*																																				*/
/*	******************************************************************************************************************************************	*/

data res.T_INDI_BPCO_EFR_SPIRO_&an_N.;
	set T_INDI_BPCO_EFR_SPIRO_&an_N.;
	BPCO_EFR_SPIRO_CIBLE = 1;
run;

* On ajoute la date de mise sous ALD;

proc sql;

	CREATE TABLE ALD_BCPO AS
		SELECT
			a.BEN_IDT_ANO,
			MIN(date_debut) AS ALD_BPCO_Date format ddmmyy10. length = 4
		FROM res.T_INDI_BPCO_EFR_SPIRO_&an_N. a
			INNER JOIN travail.Histo_ALD b
				ON a.BEN_IDT_ANO = b.BEN_IDT_ANO
		WHERE b.reperage = 52 AND a.BEN_IDT_ANO IN (SELECT BEN_IDT_ANO FROM res.T_INDI_BPCO_&an_N. WHERE ALD_BPCO_decembre = 1)
			AND ((b.date_debut <= "31DEC&annee_N."d <= b.date_fin) OR (b.date_debut <= "31DEC&annee_N."d and b.date_fin = "01JAN1600"d))
		GROUP BY a.BEN_IDT_ANO
		ORDER BY a.BEN_IDT_ANO;

quit;

data res.T_INDI_BPCO_EFR_SPIRO_&an_N.;
	merge	res.T_INDI_BPCO_EFR_SPIRO_&an_N. (in = a)
			ALD_BCPO (in = b);
	by BEN_IDT_ANO;
	if a;
run;

proc delete data = ALD_BCPO;
run; quit;

*	******************************************************************************************************************************************	;
*	EXCLUSION DES PATIENTS ASTHMATIQUES																											;
*	******************************************************************************************************************************************	;

*	******************************************************************************************************************************************;
* 	Exclusion des patients en ALD asthme active au 28 f�vrier de l ann�e N+1;

proc sql;

	CREATE TABLE ALD_asthme AS
		SELECT DISTINCT
			a.BEN_IDT_ANO
		FROM res.T_INDI_BPCO_EFR_SPIRO_&an_N. a
			INNER JOIN travail.Histo_ALD b
				ON a.BEN_IDT_ANO = b.BEN_IDT_ANO
		WHERE b.reperage = 53 AND (b.date_debut <= "28FEB&Annee_N1."d <= b.date_fin OR (b.date_debut <= "28FEB&Annee_N1."d AND 
			b.date_fin = "01JAN1600"d));

quit;

proc sql;

	* On ins�re l effectif dans le Flowchart;
	INSERT INTO flowch.Flow_Chart_BPCO_EFR_SPIRO_&an_N.
		SELECT
			5,
			COUNT(DISTINCT BEN_IDT_ANO)
	FROM res.T_INDI_BPCO_EFR_SPIRO_&an_N.
	WHERE BEN_IDT_ANO IN (SELECT BEN_IDT_ANO FROM ALD_asthme);

	* On conserve les id des patients � supprimer;
	CREATE TABLE travail.exclus1 AS
		SELECT DISTINCT
			BEN_IDT_ANO
	FROM res.T_INDI_BPCO_EFR_SPIRO_&an_N.
	WHERE BEN_IDT_ANO IN (SELECT BEN_IDT_ANO FROM ALD_asthme);

quit;

proc delete data = ALD_asthme;
run; quit;

*	******************************************************************************************************************************************;
* 	Exclusion des patients en ALD mucoviscidose active au 28 f�vrier de l ann�e N+1;

proc sql;

	CREATE TABLE ALD_mucoviscidose AS
		SELECT DISTINCT
			a.BEN_IDT_ANO
		FROM res.T_INDI_BPCO_EFR_SPIRO_&an_N. a
			INNER JOIN travail.Histo_ALD b
				ON a.BEN_IDT_ANO = b.BEN_IDT_ANO
		WHERE b.reperage = 54 AND (b.date_debut <= "28FEB&Annee_N1."d <= b.date_fin OR (b.date_debut <= "28FEB&Annee_N1."d AND 
			b.date_fin = "01JAN1600"d));

quit;

proc sql;

	* On ins�re l effectif dans le Flowchart;
	INSERT INTO flowch.Flow_Chart_BPCO_EFR_SPIRO_&an_N.
		SELECT
			6,
			COUNT(DISTINCT BEN_IDT_ANO)
	FROM res.T_INDI_BPCO_EFR_SPIRO_&an_N.
	WHERE BEN_IDT_ANO IN (SELECT BEN_IDT_ANO FROM ALD_mucoviscidose);

	* On conserve les id des patients � supprimer;
	CREATE TABLE travail.exclus2 AS
		SELECT DISTINCT
			BEN_IDT_ANO
	FROM res.T_INDI_BPCO_EFR_SPIRO_&an_N.
	WHERE BEN_IDT_ANO IN (SELECT BEN_IDT_ANO FROM ALD_mucoviscidose);

quit;

proc delete data = ALD_mucoviscidose;
run; quit;

*	******************************************************************************************************************************************;
* 	Exclusion des patients avec au moins 3 d�livrances rembours�es de CSI seuls entre le 1er janvier de l ann�e N-1 et le 28 f�vrier de l ann�e
	N+1;

proc sql;

	CREATE TABLE CSI AS
		SELECT DISTINCT
			a.BEN_IDT_ANO,
			COUNT(DISTINCT b.date_debut) AS Nb_deliv length = 3
		FROM res.T_INDI_BPCO_EFR_SPIRO_&an_N. a
			INNER JOIN travail.reperage_med_asthme_&Annee_2N._&Annee_N1. b
				ON a.BEN_IDT_ANO = b.BEN_IDT_ANO
		WHERE b.reperage = 41 AND ("01JAN&Annee_1N."d <= b.date_debut <= "28FEB&Annee_N1."d)
		GROUP BY a.BEN_IDT_ANO;

quit;

proc sql;

	* On ins�re l effectif dans le Flowchart;
	INSERT INTO flowch.Flow_Chart_BPCO_EFR_SPIRO_&an_N.
		SELECT
			7,
			COUNT(DISTINCT BEN_IDT_ANO)
	FROM res.T_INDI_BPCO_EFR_SPIRO_&an_N.
	WHERE BEN_IDT_ANO IN (SELECT BEN_IDT_ANO FROM CSI WHERE Nb_deliv >= 3);

	* On conserve les id des patients � supprimer;
	CREATE TABLE travail.exclus3 AS
		SELECT DISTINCT
			BEN_IDT_ANO
	FROM res.T_INDI_BPCO_EFR_SPIRO_&an_N.
	WHERE BEN_IDT_ANO IN (SELECT BEN_IDT_ANO FROM CSI WHERE Nb_deliv >= 3);

quit;

proc delete data = CSI;
run; quit;

*	******************************************************************************************************************************************;
* 	Exclusion des patients avec au moins 1 d�livrance rembours�e d�anti IgE entre le 1er janvier de l ann�e N-1 et le 28 f�vrier de l ann�e N+1;

proc sql;

	CREATE TABLE anti_IGE AS
		SELECT DISTINCT
			a.BEN_IDT_ANO,
			COUNT(DISTINCT b.date_debut) AS Nb_deliv length = 3
		FROM res.T_INDI_BPCO_EFR_SPIRO_&an_N. a
			INNER JOIN travail.reperage_med_asthme_&Annee_2N._&Annee_N1. b
				ON a.BEN_IDT_ANO = b.BEN_IDT_ANO
		WHERE b.reperage IN (37) AND ("01JAN&Annee_1N."d <= b.date_debut <= "28FEB&Annee_N1."d)
		GROUP BY a.BEN_IDT_ANO;

quit;

proc sql;

	* On ins�re l effectif dans le Flowchart;
	INSERT INTO flowch.Flow_Chart_BPCO_EFR_SPIRO_&an_N.
		SELECT
			8,
			COUNT(DISTINCT BEN_IDT_ANO)
	FROM res.T_INDI_BPCO_EFR_SPIRO_&an_N.
	WHERE BEN_IDT_ANO IN (SELECT BEN_IDT_ANO FROM anti_IGE WHERE Nb_deliv >= 1);

	* On conserve les id des patients � supprimer;
	CREATE TABLE travail.exclus4 AS
		SELECT DISTINCT
			BEN_IDT_ANO
	FROM res.T_INDI_BPCO_EFR_SPIRO_&an_N.
	WHERE BEN_IDT_ANO IN (SELECT BEN_IDT_ANO FROM anti_IGE WHERE Nb_deliv >= 1);

quit;

proc delete data = anti_IGE;
run; quit;

*	******************************************************************************************************************************************;
* 	Exclusion des patients avec au moins 1 d�livrance rembours�e d�anti IL-5 entre le 1er janvier de l ann�e N-1 et le 28 f�vrier de l ann�e N+1;

proc sql;

	CREATE TABLE anti_IL5 AS
		SELECT DISTINCT
			a.BEN_IDT_ANO,
			COUNT(DISTINCT b.date_debut) AS Nb_deliv length = 3
		FROM res.T_INDI_BPCO_EFR_SPIRO_&an_N. a
			INNER JOIN travail.reperage_med_asthme_&Annee_2N._&Annee_N1. b
				ON a.BEN_IDT_ANO = b.BEN_IDT_ANO
		WHERE b.reperage IN (38) AND ("01JAN&Annee_1N."d <= b.date_debut <= "28FEB&Annee_N1."d)
		GROUP BY a.BEN_IDT_ANO;

quit;

proc sql;

	* On ins�re l effectif dans le Flowchart;
	INSERT INTO flowch.Flow_Chart_BPCO_EFR_SPIRO_&an_N.
		SELECT
			9,
			COUNT(DISTINCT BEN_IDT_ANO)
	FROM res.T_INDI_BPCO_EFR_SPIRO_&an_N.
	WHERE BEN_IDT_ANO IN (SELECT BEN_IDT_ANO FROM anti_IL5 WHERE Nb_deliv >= 1);

	* On conserve les id des patients � supprimer;
	CREATE TABLE travail.exclus5 AS
		SELECT DISTINCT
			BEN_IDT_ANO
	FROM res.T_INDI_BPCO_EFR_SPIRO_&an_N.
	WHERE BEN_IDT_ANO IN (SELECT BEN_IDT_ANO FROM anti_IL5 WHERE Nb_deliv >= 1);

quit;

proc delete data = anti_IL5;
run; quit;

*	******************************************************************************************************************************************;
* 	Exclusion des patients avec au moins 1 d�livrance rembours�e d�antileucotri�ne entre le 1er janvier de l ann�e N-1 et le 28 f�vrier de l ann�e N+1;

proc sql;

	CREATE TABLE antileucotriene AS
		SELECT DISTINCT
			a.BEN_IDT_ANO,
			COUNT(DISTINCT b.date_debut) AS Nb_deliv length = 3
		FROM res.T_INDI_BPCO_EFR_SPIRO_&an_N. a
			INNER JOIN travail.reperage_med_asthme_&Annee_2N._&Annee_N1. b
				ON a.BEN_IDT_ANO = b.BEN_IDT_ANO
		WHERE b.reperage = 39 AND ("01JAN&Annee_1N."d <= b.date_debut <= "28FEB&Annee_N1."d)
		GROUP BY a.BEN_IDT_ANO;

quit;

proc sql;

	* On ins�re l effectif dans le Flowchart;
	INSERT INTO flowch.Flow_Chart_BPCO_EFR_SPIRO_&an_N.
		SELECT
			10,
			COUNT(DISTINCT BEN_IDT_ANO)
	FROM res.T_INDI_BPCO_EFR_SPIRO_&an_N.
	WHERE BEN_IDT_ANO IN (SELECT BEN_IDT_ANO FROM antileucotriene WHERE Nb_deliv >= 1);

	* On conserve les id des patients � supprimer;
	CREATE TABLE travail.exclus6 AS
		SELECT DISTINCT
			BEN_IDT_ANO
	FROM res.T_INDI_BPCO_EFR_SPIRO_&an_N.
	WHERE BEN_IDT_ANO IN (SELECT BEN_IDT_ANO FROM antileucotriene WHERE Nb_deliv >= 1);

quit;

proc delete data = antileucotriene;
run; quit;

*	******************************************************************************************************************************************;
* 	Exclusion des patients avec au moins un acte CCAM de Thermoplastie bronchique cod� lors d�un s�-jour hospitalier en MCO termin� entre 
	le 1er janvier de l ann�e N-1 et le 28 f�vrier de l ann�e N+1;

proc sql;

	CREATE TABLE thermo_bronchique AS
		SELECT DISTINCT
			a.BEN_IDT_ANO,
			COUNT(DISTINCT b.date_debut) AS Nb_actes length = 3
		FROM res.T_INDI_BPCO_EFR_SPIRO_&an_N. a
			INNER JOIN travail.reperage_thermo_bronch_&annee_1N._&annee_N1. b
				ON a.BEN_IDT_ANO = b.BEN_IDT_ANO
		WHERE "01JAN&Annee_1N."d <= b.date_fin <= "28FEB&Annee_N1."d
		GROUP BY a.BEN_IDT_ANO;

quit;

proc sql;

	* On ins�re l effectif dans le Flowchart;
	INSERT INTO flowch.Flow_Chart_BPCO_EFR_SPIRO_&an_N.
		SELECT
			11,
			COUNT(DISTINCT BEN_IDT_ANO)
	FROM res.T_INDI_BPCO_EFR_SPIRO_&an_N.
	WHERE BEN_IDT_ANO IN (SELECT BEN_IDT_ANO FROM thermo_bronchique WHERE Nb_actes >= 1);

	* On conserve les id des patients � supprimer;
	CREATE TABLE travail.exclus7 AS
		SELECT DISTINCT
			BEN_IDT_ANO
	FROM res.T_INDI_BPCO_EFR_SPIRO_&an_N.
	WHERE BEN_IDT_ANO IN (SELECT BEN_IDT_ANO FROM thermo_bronchique WHERE Nb_actes >= 1);

quit;

proc delete data = thermo_bronchique;
run; quit;

*	******************************************************************************************************************************************;
* 	Exclusion des patients ayant eu au moins un diagnostic d�asthme cod� entre le 1er janvier de l ann�e N-1 et le 28 f�vrier de l ann�e N+1;

proc sql;

	CREATE TABLE diag_asthme_MCO AS
		SELECT DISTINCT
			a.BEN_IDT_ANO
		FROM res.T_INDI_BPCO_EFR_SPIRO_&an_N. a
			INNER JOIN travail.diag_asthme_&annee_1N._&annee_N1. b
				ON a.BEN_IDT_ANO = b.BEN_IDT_ANO
		WHERE b.domaine = "MCO" AND b.reperage = 14 AND "01JAN&Annee_1N."d <= b.date_fin <= "28FEB&Annee_N1."d;

	CREATE TABLE diag_asthme_SSR_HAD AS
		SELECT DISTINCT
			a.BEN_IDT_ANO
		FROM res.T_INDI_BPCO_EFR_SPIRO_&an_N. a
			INNER JOIN travail.diag_asthme_&annee_1N._&annee_N1. b
				ON a.BEN_IDT_ANO = b.BEN_IDT_ANO
		WHERE b.domaine IN ("HAD", "SSR") AND b.reperage = 14 AND b.date_debut <= "28FEB&Annee_N1."d AND b.date_fin >= "01JAN&Annee_1N."d;

quit;

data diag_asthme;
	set diag_asthme_MCO diag_asthme_SSR_HAD;
run;

proc sql;

	* On ins�re l effectif dans le Flowchart;
	INSERT INTO flowch.Flow_Chart_BPCO_EFR_SPIRO_&an_N.
		SELECT
			12,
			COUNT(DISTINCT BEN_IDT_ANO)
	FROM res.T_INDI_BPCO_EFR_SPIRO_&an_N.
	WHERE BEN_IDT_ANO IN (SELECT BEN_IDT_ANO FROM diag_asthme);

	* On conserve les id des patients � supprimer;
	CREATE TABLE travail.exclus8 AS
		SELECT DISTINCT
			BEN_IDT_ANO
	FROM res.T_INDI_BPCO_EFR_SPIRO_&an_N.
	WHERE BEN_IDT_ANO IN (SELECT BEN_IDT_ANO FROM diag_asthme);

quit;

proc delete data = diag_asthme diag_asthme_MCO diag_asthme_SSR_HAD;
run; quit;

*	******************************************************************************************************************************************;
* 	Exclusion des patients ayant eu au moins un diagnostic d��tat de mal asthmatique cod� entre le 1er janvier de l ann�e N-1 et le 28 f�vrier
	de l ann�e N+1;

proc sql;

	CREATE TABLE etat_mal_asthmatique AS
		SELECT DISTINCT
			a.BEN_IDT_ANO
		FROM res.T_INDI_BPCO_EFR_SPIRO_&an_N. a
			INNER JOIN travail.diag_asthme_&annee_1N._&annee_N1. b
				ON a.BEN_IDT_ANO = b.BEN_IDT_ANO
		WHERE reperage = 15 AND "01JAN&Annee_1N."d <= b.date_fin <= "28FEB&Annee_N1."d;

quit;

proc sql;

	* On ins�re l effectif dans le Flowchart;
	INSERT INTO flowch.Flow_Chart_BPCO_EFR_SPIRO_&an_N.
		SELECT
			13,
			COUNT(DISTINCT BEN_IDT_ANO)
	FROM res.T_INDI_BPCO_EFR_SPIRO_&an_N.
	WHERE BEN_IDT_ANO IN (SELECT BEN_IDT_ANO FROM etat_mal_asthmatique);

	* On conserve les id des patients � supprimer;
	CREATE TABLE travail.exclus9 AS
		SELECT DISTINCT
			BEN_IDT_ANO
	FROM res.T_INDI_BPCO_EFR_SPIRO_&an_N.
	WHERE BEN_IDT_ANO IN (SELECT BEN_IDT_ANO FROM etat_mal_asthmatique);

quit;

proc delete data = etat_mal_asthmatique;
run; quit;
			
*	******************************************************************************************************************************************;
*	On exclut tous les patients;

data exclus;
	set	travail.exclus1-travail.exclus9;
run;

proc sql;

	* On ins�re l effectif dans le Flowchart;
	INSERT INTO flowch.Flow_Chart_BPCO_EFR_SPIRO_&an_N.
		SELECT
			14,
			COUNT(DISTINCT BEN_IDT_ANO)
	FROM exclus;

	DELETE FROM res.T_INDI_BPCO_EFR_SPIRO_&an_N.
	WHERE BEN_IDT_ANO IN (SELECT BEN_IDT_ANO FROM exclus);

quit;

proc delete data = exclus;
run; quit;

proc datasets library = travail memtype = data nolist;
	delete exclus1-exclus9;
run; quit;

* On ins�re l effectif de la population d �tude hors asthamtiques dans le Flowchart;
proc sql;
	
	INSERT INTO flowch.Flow_Chart_BPCO_EFR_SPIRO_&an_N.
		SELECT
			15,
			COUNT(DISTINCT BEN_IDT_ANO)
	FROM res.T_INDI_BPCO_EFR_SPIRO_&an_N.;

quit;


*	******************************************************************************************************************************************	;
*	PATIENTS POUR LESQUELS LA SPIROM�TRIE OU L EFR N EST PAS R�ALISABLE																			;
*	******************************************************************************************************************************************	;

*	******************************************************************************************************************************************;
* 	Exclusion des patients en ALD maladie d Alzheimer et autres d�mences active au 28 f�vrier de l ann�e N+1;

proc sql;

	CREATE TABLE ALD_Alzheimer AS
		SELECT DISTINCT
			a.BEN_IDT_ANO
		FROM res.T_INDI_BPCO_EFR_SPIRO_&an_N. a
			INNER JOIN travail.Histo_ALD b
				ON a.BEN_IDT_ANO = b.BEN_IDT_ANO
		WHERE b.reperage IN (50, 51) AND (b.date_debut <= "28FEB&Annee_N1."d <= b.date_fin OR (b.date_debut <= "28FEB&Annee_N1."d AND 
			b.date_fin = "01JAN1600"d));

quit;

proc sql;

	* On ins�re l effectif dans le Flowchart;
	INSERT INTO flowch.Flow_Chart_BPCO_EFR_SPIRO_&an_N.
		SELECT
			16,
			COUNT(DISTINCT BEN_IDT_ANO)
	FROM res.T_INDI_BPCO_EFR_SPIRO_&an_N.
	WHERE BEN_IDT_ANO IN (SELECT BEN_IDT_ANO FROM ALD_Alzheimer);

	* On conserve les id des patients � supprimer;
	CREATE TABLE travail.exclus1 AS
		SELECT DISTINCT
			BEN_IDT_ANO
	FROM res.T_INDI_BPCO_EFR_SPIRO_&an_N.
	WHERE BEN_IDT_ANO IN (SELECT BEN_IDT_ANO FROM ALD_Alzheimer);

quit;

proc delete data = ALD_Alzheimer;
run; quit;

*	******************************************************************************************************************************************;
* 	Exclusion des patients ayant eu un diagnostic de soins palliatif cod� lors d�un s�jour hospitalier MCO termin� entre le 1er janvier de l ann�e N
	et le 28 f�vrier de l ann�e N+1, ou lors d�un s�jour hospitalier HAD, SSR entre le 1er janvier de l ann�e N et le 28 f�vrier de l ann�e N+1;

* On exclut les s�jours avec un s�jour MCO termin� pendant la campagne;
* On exclut les s�jours avec un s�jour SSR ou HAD pendant la campagne;
proc sql undo_policy = none;

	CREATE TABLE soin_palliatif_MCO AS
		SELECT DISTINCT
			a.BEN_IDT_ANO
		FROM res.T_INDI_BPCO_EFR_SPIRO_&an_N. a
			INNER JOIN travail.soin_palliatif_&annee_2N._&annee_N1. b
				ON a.BEN_IDT_ANO = b.BEN_IDT_ANO
		WHERE "01JAN&Annee_N."d <= b.date_fin <= "28FEB&Annee_N1"d AND b.domaine = "MCO";

	CREATE TABLE soin_palliatif_SSR_HAD AS
		SELECT DISTINCT
			a.BEN_IDT_ANO
		FROM res.T_INDI_BPCO_EFR_SPIRO_&an_N. a
			INNER JOIN travail.soin_palliatif_&annee_2N._&annee_N1. b
				ON a.BEN_IDT_ANO = b.BEN_IDT_ANO
		WHERE b.domaine IN ("HAD", "SSR") AND b.date_debut <= "28FEB&Annee_N1."d AND b.date_fin >= "01JAN&Annee_N."d;

quit;

data soin_palliatif;
	set soin_palliatif_MCO
		soin_palliatif_SSR_HAD;
run;

proc sql;

	* On ins�re l effectif dans le Flowchart;
	INSERT INTO flowch.Flow_Chart_BPCO_EFR_SPIRO_&an_N.
		SELECT
			17,
			COUNT(DISTINCT BEN_IDT_ANO)
	FROM res.T_INDI_BPCO_EFR_SPIRO_&an_N.
	WHERE BEN_IDT_ANO IN (SELECT BEN_IDT_ANO FROM soin_palliatif);

	* On conserve les id des s�jours � supprimer;
	CREATE TABLE travail.exclus2 AS
		SELECT DISTINCT
			BEN_IDT_ANO
	FROM res.T_INDI_BPCO_EFR_SPIRO_&an_N.
	WHERE BEN_IDT_ANO IN (SELECT BEN_IDT_ANO FROM soin_palliatif);

quit;

proc datasets library = work memtype = data nolist;
	delete soin_palliatif_MCO soin_palliatif_SSR_HAD soin_palliatif ;
run; quit;

*	******************************************************************************************************************************************;
* 	Exclusion des patients hospitalis�s en SSR ou psychiatrie plus de 90 jours entre le 1er janvier de l ann�e N et le 28 f�vrier de l ann�e N+1;

* On regarde si les patients ont un s�jours > 3 mois;
proc sql;

	CREATE TABLE hospit1_3mois AS
		SELECT DISTINCT
			a.BEN_IDT_ANO
		FROM res.T_INDI_BPCO_EFR_SPIRO_&an_N. a
			INNER JOIN travail.sejours_&Annee_N._&Annee_N1. b
				ON a.BEN_IDT_ANO = b.BEN_IDT_ANO
		WHERE b.domaine IN ("RIP", "SSR") AND b.date_debut < "01JAN&Annee_N."d AND (b.date_fin - "01JAN&Annee_N."d) > 90;

	CREATE TABLE hospit2_3mois AS
		SELECT DISTINCT
			a.BEN_IDT_ANO
		FROM res.T_INDI_BPCO_EFR_SPIRO_&an_N. a
			INNER JOIN travail.sejours_&Annee_N._&Annee_N1. b
				ON a.BEN_IDT_ANO = b.BEN_IDT_ANO
		WHERE b.domaine IN ("RIP", "SSR") AND "01JAN&Annee_N."d <= b.date_debut <= "28FEB&Annee_N1"d AND 
			"01JAN&Annee_N."d <= b.date_fin <= "28FEB&Annee_N1"d AND (b.date_fin - b.date_debut) > 90;

	CREATE TABLE hospit3_3mois AS
		SELECT DISTINCT
			a.BEN_IDT_ANO
		FROM res.T_INDI_BPCO_EFR_SPIRO_&an_N. a
			INNER JOIN travail.sejours_&Annee_N._&Annee_N1. b
				ON a.BEN_IDT_ANO = b.BEN_IDT_ANO
		WHERE b.domaine IN ("RIP", "SSR") AND b.date_debut >= "01JAN&Annee_N."d AND b.date_fin > "28FEB&Annee_N1"d
			AND ("28FEB&Annee_N1"d - b.date_debut) > 90;

quit;

data hospit_3mois;
	set hospit1_3mois hospit2_3mois hospit3_3mois;
run;

proc sort data = hospit_3mois (keep = BEN_IDT_ANO) nodupkey;
	by BEN_IDT_ANO;
run;

proc sql;

	* On ins�re l effectif dans le Flowchart;
	INSERT INTO flowch.Flow_Chart_BPCO_EFR_SPIRO_&an_N.
		SELECT
			18,
			COUNT(DISTINCT BEN_IDT_ANO)
	FROM res.T_INDI_BPCO_EFR_SPIRO_&an_N.
	WHERE BEN_IDT_ANO IN (SELECT BEN_IDT_ANO FROM hospit_3mois);

	* On conserve les id des s�jours � supprimer;
	CREATE TABLE travail.exclus3 AS
		SELECT DISTINCT
			BEN_IDT_ANO
	FROM res.T_INDI_BPCO_EFR_SPIRO_&an_N.
	WHERE BEN_IDT_ANO IN (SELECT BEN_IDT_ANO FROM hospit_3mois);

quit;

proc delete data = hospit_3mois;
run; quit;
			
*	******************************************************************************************************************************************;
*	On flag les patients pour lesquels la spirom�trie ou les EFR ne sont pas r�alisables;

data exclus;
	set	travail.exclus1-travail.exclus3;
run;

proc sql;

	* On ins�re l effectif dans le Flowchart;
	INSERT INTO flowch.Flow_Chart_BPCO_EFR_SPIRO_&an_N.
		SELECT
			19,
			COUNT(DISTINCT BEN_IDT_ANO)
	FROM exclus;

	DELETE FROM res.T_INDI_BPCO_EFR_SPIRO_&an_N.
	WHERE BEN_IDT_ANO IN (SELECT BEN_IDT_ANO FROM exclus);

quit;

proc delete data = exclus;
run; quit;

proc datasets library = travail memtype = data nolist;
	delete exclus1-exclus3;
run; quit;

proc sort data = res.T_INDI_BPCO_EFR_SPIRO_&an_N. nodupkey;
	by _all_;
run;

*	******************************************************************************************************************************************;
* 	Population cible;

proc sql;

	INSERT INTO flowch.Flow_Chart_BPCO_EFR_SPIRO_&an_N.
		SELECT
			21,
			COUNT(DISTINCT BEN_IDT_ANO)
	FROM res.T_INDI_BPCO_EFR_SPIRO_&an_N.;

quit;

proc sql;

	SELECT COUNT(*), COUNT(DISTINCT BEN_IDT_ANO)
	FROM res.T_INDI_BPCO_EFR_SPIRO_&an_N.;

quit;
