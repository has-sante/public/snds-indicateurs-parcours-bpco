/*	******************************************************************************************************************************************	*/
/*																																				*/
/*		Indicateur 03 : Table finale - Ajout de formats et de labels																			*/
/*																																				*/
/*	******************************************************************************************************************************************	*/

data res.T_INDI_BPCO_EFR_SPIRO_&an_N.;
	set res.T_INDI_BPCO_EFR_SPIRO_&an_N. (keep = BEN_IDT_ANO BPCO_Probable Diag_BPCO BPCO_Diagnostique BPCO_EFR_SPIRO_CIBLE ALD_BPCO_Date
		Nb_Sej_Diag_BPCO_MCO Nb_Sej_Diag_BPCO_SSR Nb_Sej_Diag_BPCO_HAD BPCO_EFR_SPIRO_OBS Spiro Nb_Spiro Date_Spiro Lieu_Spiro EFR Nb_EFR
		Date_EFR Lieu_EFR Pneumothorax Infarctus Oxygenotherapie VNI);
	label		
		BEN_IDT_ANO = "Num�ro d'individu"
		BPCO_Probable = "Patient ayant une BPCO probable"
		Diag_BPCO = "Patient ayant eu au moins un diagnostic de BPCO cod� lors d'un s�jour hospitalier en MCO termin� en &Annee_N. ou lors d'un s�jour hospitalier en SSR, HAD en &Annee_N."
		BPCO_Diagnostique = "Patient diagnostiqu� BPCO"
		BPCO_EFR_SPIRO_CIBLE = "Patient correspondant aux crit�res d'inclusion et d'exclusion de la population cible"
		ALD_BPCO_Date = "Date de d�but de mise en ALD BPCO"
		Nb_Sej_Diag_BPCO_MCO = "Nombre de s�jours avec un diagnostic BPCO termin�s en &Annee_N. en MCO"
		Nb_Sej_Diag_BPCO_SSR = "Nombre de s�jours avec un diagnostic BPCO en &Annee_N. en SSR"
		Nb_Sej_Diag_BPCO_HAD = "Nombre de s�jours avec un diagnostic BPCO en &Annee_N. en HAD"
		BPCO_EFR_SPIRO_OBS = "Patient de la population cible ayant b�n�fici�s d'une exploration fonctionnelle respiratoire ou d'une spirom�trie entre le 1er janvier &Annee_N. et le 28 f�vrier &Annee_N1."
		Spiro = "Spirom�trie r�alis�e entre le 1er janvier &Annee_N. et le 28 f�vrier &Annee_N1."
		Nb_Spiro = "Nombre de spirom�tries r�alis�es entre le 1er janvier &Annee_N. et le 28 f�vrier &Annee_N1."
		Date_Spiro = "Date de la r�alisation de la premi�re spirom�trie entre le 1er janvier &Annee_N. et le 28 f�vrier &Annee_N1."
		Lieu_Spiro = "Lieu de r�alisation de la premi�re spirom�trie"
		EFR = "EFR r�alis� entre le 1er janvier &Annee_N. et le 28 f�vrier &Annee_N1."
		Nb_EFR = "Nombre d'EFR r�alis�s entre le 1er janvier &Annee_N. et le 28 f�vrier &Annee_N1."
		Date_EFR = "Date de la premi�re r�alisation d'EFR entre le 1er janvier &Annee_N. et le 28 f�vrier &Annee_N1."
		Lieu_EFR = "Lieu de r�alisation du premi�re EFR entre le 1er janvier &Annee_N. et le 28 f�vrier &Annee_N1."
		Pneumothorax = "Patients ayant eu un pneumothorax entre le 1er janvier &Annee_N. et le 28 f�vrier &Annee_N1."
		Infarctus = "Patients ayant un diagnostic d'infarctus entre le 1er janvier &Annee_N. et le 28 f�vrier &Annee_N1."
		Oxygenotherapie = "Patients ayant eu une d�livrance rembours�e d'un traitement par oxyg�noth�rapie entre le 1er janvier &Annee_N. et le 28 f�vrier &Annee_N1."
		VNI = "Patients ayant eu une d�livrance rembours�e d'un traitement par VNI entre le 1er janvier &Annee_N. et le 28 f�vrier &Annee_N1."
		;
	format BPCO_Probable Diag_BPCO BPCO_Diagnostique BPCO_EFR_SPIRO_CIBLE BPCO_EFR_SPIRO_OBS Spiro EFR Pneumothorax Infarctus Oxygenotherapie
		VNI f_oui_non. ALD_BPCO_Date Date_Spiro Date_EFR date9. Lieu_Spiro Lieu_EFR f_lieu_soin.;
run;
			
*	******************************************************************************************************************************************;
* 	R�alisation d EFR ou d une spirom�trie annuelle;

proc sql;

	INSERT INTO flowch.Flow_Chart_BPCO_EFR_SPIRO_&an_N.
		SELECT
			22,
			COUNT(DISTINCT BEN_IDT_ANO)
	FROM res.T_INDI_BPCO_EFR_SPIRO_&an_N.
	WHERE BPCO_EFR_SPIRO_CIBLE = 1 AND BPCO_EFR_SPIRO_OBS = 1;


	* Nombre de patients pour les num�rateur des premi�res exclusions;
	SELECT Nb_patients_ref0 into : N0 FROM travail.ref_flowcharts; 
	* Nombre de patients  de 40 ans et plus ayant une BPCO probable ou diagnostiqu�e;
	SELECT N into : N4 FROM flowch.Flow_Chart_BPCO_EFR_SPIRO_&an_N. WHERE indicateur = 4;

	* population d �tude;
	SELECT N into : N15 FROM flowch.Flow_Chart_BPCO_EFR_SPIRO_&an_N. WHERE indicateur = 15;

	* Population cible;
	SELECT N into : N21 FROM flowch.Flow_Chart_BPCO_EFR_SPIRO_&an_N. WHERE indicateur = 21;

quit;

data flowch.Flow_Chart_BPCO_EFR_SPIRO_&an_N.;
	set flowch.Flow_Chart_BPCO_EFR_SPIRO_&an_N.;
	if indicateur in (1, 2, 3, 4) then
		percent = N / &N0.;
	if indicateur in (5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15) then
		percent = N / &N4.;
	if indicateur in (16, 17, 18, 19, 20, 21) then
		percent = N / &N15.;
	if indicateur = 22 then
		percent = N / &N21.;
	format indicateur f_ind_03_flowchart. percent percent7.1;
run;

* V�rif;
proc sql;

	SELECT COUNT(*) AS nb_lignes, COUNT(DISTINCT BEN_IDT_ANO) AS nb_patients
	FROM res.T_INDI_BPCO_EFR_SPIRO_&an_N.;
	* nb_lignes = nb_patients : OK;

	SELECT COUNT(*) AS nb_lignes, COUNT(DISTINCT BEN_IDT_ANO) AS nb_patients
	FROM res.T_INDI_BPCO_EFR_SPIRO_&an_N.
	WHERE BPCO_EFR_SPIRO_CIBLE = 1;
	* nb_lignes = nb_patients : OK;

quit;

*	******************************************************************************************************************************************;
*	On ajoute le flag BPCO_EFR_SPIRO_CIBLE dans la table res.T_INDI_BPCO_&an_N.;

proc sql;

	ALTER TABLE res.T_INDI_BPCO_&an_N. DROP BPCO_EFR_SPIRO_CIBLE;

	ALTER TABLE res.T_INDI_BPCO_&an_N. ADD BPCO_EFR_SPIRO_CIBLE INT format = f_oui_non. length = 3
		label = "Patient appartenant � la population cible BPCO_EFR_SPIRO";

	UPDATE res.T_INDI_BPCO_&an_N.
		SET BPCO_EFR_SPIRO_CIBLE = CASE	WHEN BEN_IDT_ANO IN (SELECT DISTINCT BEN_IDT_ANO FROM res.T_INDI_BPCO_EFR_SPIRO_&an_N. WHERE 
											BPCO_EFR_SPIRO_CIBLE = 1) THEN 1
										ELSE 0
										END;

quit;

data res.T_INDI_BPCO_&an_N.;
	set res.T_INDI_BPCO_&an_N.;
	if BPCO_EFR_SPIRO_CIBLE = .
		then BPCO_EFR_SPIRO_CIBLE = 0;
run;
