/*	******************************************************************************************************************************************	*/
/*																																				*/
/*		Indicateur 03 : Nombre de s�jours BPCO																						 			*/
/*																																				*/
/*	******************************************************************************************************************************************	*/

proc sql;

	CREATE INDEX BEN_IDT_ANO ON res.T_INDI_BPCO_EFR_SPIRO_&an_N. (BEN_IDT_ANO);

quit;

%macro sejours_BPCO(domaine=);

	proc sql;

		CREATE TABLE sejours_BPCO_&domaine. AS
			SELECT DISTINCT
				a.BEN_IDT_ANO,
				COUNT(DISTINCT id_sejour) AS Nb_Sej_Diag_BPCO_&domaine. length = 3
			FROM res.T_INDI_BPCO_EFR_SPIRO_&an_N. a
				INNER JOIN travail.reperage_hospit_BPCO_&annee_4N._&annee_N1. b
					ON a.BEN_IDT_ANO = b.BEN_IDT_ANO
			WHERE b.domaine = "&domaine." AND annee = &Annee_N.
			GROUP BY a.BEN_IDT_ANO
			;

		CREATE INDEX BEN_IDT_ANO ON sejours_BPCO_&domaine. (BEN_IDT_ANO);

		ALTER TABLE res.T_INDI_BPCO_EFR_SPIRO_&an_N. ADD Nb_Sej_Diag_BPCO_&domaine. INT length = 3;

		UPDATE res.T_INDI_BPCO_EFR_SPIRO_&an_N. a
			SET Nb_Sej_Diag_BPCO_&domaine. =	(SELECT Nb_Sej_Diag_BPCO_&domaine.
												FROM sejours_BPCO_&domaine. b
												WHERE a.BEN_IDT_ANO = b.BEN_IDT_ANO);

	quit;

	data res.T_INDI_BPCO_EFR_SPIRO_&an_N.;
		set res.T_INDI_BPCO_EFR_SPIRO_&an_N.;
		if Nb_Sej_Diag_BPCO_&domaine. = . then
			Nb_Sej_Diag_BPCO_&domaine. = 0;
	run;

	proc delete data = sejours_BPCO_&domaine.;
	run; quit;

%mend sejours_BPCO;

%sejours_BPCO(domaine = MCO);
%sejours_BPCO(domaine = SSR);
%sejours_BPCO(domaine = HAD);
