/*	******************************************************************************************************************************************	*/
/*																																				*/
/*		Indicateur 03 : Pneumothorax, infarctus, oxyg�noth�rapie et VNI										 									*/
/*																																				*/
/*	******************************************************************************************************************************************	*/

%macro add_info_ind03(table=, in_indic=, out_indic=);

	* Patients avec le soin entre le 1er janvier &an_N. et le 28 f�vrier &an_N1.;
	proc sql;

		CREATE TABLE patients AS
			SELECT DISTINCT
				a.BEN_IDT_ANO,
				1 AS &out_indic. length = 3
			FROM res.T_INDI_BPCO_EFR_SPIRO_&an_N. a
				INNER JOIN &table. b
					ON a.BEN_IDT_ANO = b.BEN_IDT_ANO
			WHERE b.reperage IN &in_indic. AND b.date_debut <= "28FEB&Annee_N1."d AND b.date_fin >= "01JAN&Annee_N."d
			ORDER BY a.BEN_IDT_ANO;

	quit;

	data res.T_INDI_BPCO_EFR_SPIRO_&an_N.;
		merge	res.T_INDI_BPCO_EFR_SPIRO_&an_N. (in = a)
				patients (in = b);
		by BEN_IDT_ANO;
		if a;
		if not b then
			&out_indic. = 0;
	run;

	proc delete data = patients;
	run; quit;

%mend add_info_ind03;

* Pneumothorax;
%add_info_ind03(
	table = travail.pneumothorax_infarctus_&Annee_1N._&annee_N1.,
	in_indic = (11),
	out_indic = Pneumothorax
	);

* Infarctus;
%add_info_ind03(
	table = travail.pneumothorax_infarctus_&Annee_1N._&annee_N1.,
	in_indic = (12),
	out_indic = Infarctus
	);

* Oxyg�noth�rapie;
%add_info_ind03(
	table = travail.reperage_Oxygeno_&Annee_N._&annee_N1.,
	in_indic = (5),
	out_indic = Oxygenotherapie
	);

* VNI;
%add_info_ind03(
	table = travail.reperage_VNI_&Annee_N._&annee_N1.,
	in_indic = (6),
	out_indic = VNI
	);
