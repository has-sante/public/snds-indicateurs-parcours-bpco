/*	******************************************************************************************************************************************	*/
/*																																				*/
/*		Indicateur 05 : Inclusion & exclusions																					 				*/
/*																																				*/
/*	******************************************************************************************************************************************	*/

*	******************************************************************************************************************************************;
*	On r�cup�re tous les s�jours pour exacerbation de BPCO pour les patients de la table res.T_INDI_BPCO_SMED_D_EA_&an_N.;

* S�jours avec BPCO en DAS;
data BPCO_DAS;
	set travail.sejours_cible_exacerbation_&annee_N. (where = (reperage = 16 and variable = "ASS_DGN"));
	BPCO_SMED_D_EA_Sej_Index = id_sejour;
run;

proc sort data = T_INDI_BPCO_SMED_D_EA_&an_N. out = reperage_BPCO nodupkey;
	by BPCO_SMED_D_EA_Sej_Index UM_ORD_NUM;
run;

data reperage_BPCO2;
	set reperage_BPCO;
	by BPCO_SMED_D_EA_Sej_Index UM_ORD_NUM;
	if first.BPCO_SMED_D_EA_Sej_Index then
		output;
run;

proc sql undo_policy = none;

	CREATE TABLE res.T_INDI_BPCO_SMED_D_EA_&an_N. AS 
		SELECT DISTINCT
			BEN_IDT_ANO,
			date_debut,
			date_fin AS BPCO_SMED_D_EA_Date_index length = 4,
			BPCO_SMED_D_EA_Sej_Index,
			ETA_NUM AS Finess_PMSI,
			ETA_NUM_GEO AS Finess_GEO,
			DGN_PAL AS Dp,
			CASE	WHEN SUBSTR(DGN_PAL, 1, 4) IN ("J440", "J441", "J448", "J449") THEN 1
					WHEN SUBSTR(DGN_PAL, 1, 3) IN ("J12", "J13", "J14", "J15", "J16", "J17", "J18") OR SUBSTR(DGN_PAL, 1, 4) = "J181" THEN 2
					WHEN SUBSTR(DGN_PAL, 1, 4) IN ("J960") THEN 3
					WHEN SUBSTR(DGN_PAL, 1, 3) IN ("J09", "J10", "J11") THEN 4
					WHEN SUBSTR(DGN_PAL, 1, 3) IN ("I26") THEN 5
					WHEN SUBSTR(DGN_PAL, 1, 3) IN ("I50") OR SUBSTR(DGN_PAL, 1, 4) IN ("I130", "I132", "I110", "I501") THEN 6
					WHEN SUBSTR(DGN_PAL, 1, 3) IN ("J86", "J93") OR SUBSTR(DGN_PAL, 1, 4) IN ("J942") THEN 7
					END AS Dp_classe length = 3,
			CASE	WHEN BPCO_SMED_D_EA_Sej_Index IN (SELECT BPCO_SMED_D_EA_Sej_Index FROM BPCO_DAS) THEN 1
					ELSE 0
					END AS DA_BPCO length = 3,
			GRG_GHM AS GHM,
			1 AS BPCO_SMED_D_EA_CIBLE length = 3
		FROM reperage_BPCO2
		WHERE UM_ORD_NUM = 1
		ORDER BY BEN_IDT_ANO;

quit;

proc delete data = BPCO_DAS;
run; quit;

*	******************************************************************************************************************************************;
*	On exclut les s�jours dont le mode de sortie est le d�c�s. ;

proc sql;

	* On ins�re l effectif dans le Flowchart;
	INSERT INTO flowch.Flow_Chart_BPCO_SMED_D_EA_&an_N.
		SELECT
			13,
			COUNT(DISTINCT BPCO_SMED_D_EA_Sej_Index)
	FROM res.T_INDI_BPCO_SMED_D_EA_&an_N.
	WHERE BPCO_SMED_D_EA_Sej_Index IN (SELECT BPCO_SMED_D_EA_Sej_Index FROM T_INDI_BPCO_SMED_D_EA_&an_N. WHERE SOR_MOD = "9");

	* On conserve les id des s�jours � supprimer;
	CREATE TABLE travail.exclus1 AS
		SELECT DISTINCT
			BPCO_SMED_D_EA_Sej_Index
	FROM res.T_INDI_BPCO_SMED_D_EA_&an_N.
	WHERE BPCO_SMED_D_EA_Sej_Index IN (SELECT BPCO_SMED_D_EA_Sej_Index FROM T_INDI_BPCO_SMED_D_EA_&an_N. WHERE SOR_MOD = "9");

quit;

proc delete data = T_INDI_BPCO_SMED_D_EA_&an_N.;
run; quit;
			

*	******************************************************************************************************************************************;
*	Changement juillet 2021 - Nouvelle version des indicateurs : on modifie les exlcusions;
*	On exclut les s�jours index suivi d au moins une journ�e d hospitalisation en MCO, SSR, HAD ou PSY dans les 60 jours suivant la date index;

data sejours_&Annee_N._&Annee_N1.;
	set travail.sejours_&Annee_N._&Annee_N1.;
run;

proc sql;

	DELETE FROM sejours_&Annee_N._&Annee_N1.
	WHERE domaine = "MCO" AND (SUBSTR(GRG_GHM, 1, 2) = "28" OR DUREE_SEJOUR = 0);

	DELETE FROM sejours_&Annee_N._&Annee_N1.
	WHERE domaine ne "MCO" AND date_debut = date_fin;

quit;

proc sql;

	CREATE TABLE sejours_60jours AS
		SELECT DISTINCT
			a.BPCO_SMED_D_EA_Sej_Index
		FROM res.T_INDI_BPCO_SMED_D_EA_&an_N. a
			INNER JOIN sejours_&Annee_N._&Annee_N1. b
				ON a.BEN_IDT_ANO = b.BEN_IDT_ANO
		WHERE b.date_debut < (a.BPCO_SMED_D_EA_Date_index + 60) AND a.BPCO_SMED_D_EA_Date_index < b.date_fin
			AND a.BPCO_SMED_D_EA_Sej_Index NE b.id_sejour;

	* On ins�re l effectif dans le Flowchart;
	INSERT INTO flowch.Flow_Chart_BPCO_SMED_D_EA_&an_N.
		SELECT
			14,
			COUNT(DISTINCT BPCO_SMED_D_EA_Sej_Index)
	FROM res.T_INDI_BPCO_SMED_D_EA_&an_N.
	WHERE BPCO_SMED_D_EA_Sej_Index IN (SELECT BPCO_SMED_D_EA_Sej_Index FROM sejours_60jours);

	* On conserve les id des s�jours � supprimer;
	CREATE TABLE travail.exclus2 AS
		SELECT DISTINCT
			BPCO_SMED_D_EA_Sej_Index
	FROM res.T_INDI_BPCO_SMED_D_EA_&an_N.
	WHERE BPCO_SMED_D_EA_Sej_Index IN (SELECT BPCO_SMED_D_EA_Sej_Index FROM sejours_60jours);

quit;
			
*	******************************************************************************************************************************************;
*	On exclut tous les patients;

data exclus;
	set	travail.exclus1-travail.exclus2;
run;

proc sql;

	* On ins�re l effectif dans le Flowchart;
	INSERT INTO flowch.Flow_Chart_BPCO_SMED_D_EA_&an_N.
		SELECT
			15,
			COUNT(DISTINCT BPCO_SMED_D_EA_Sej_Index)
	FROM exclus;

	DELETE FROM res.T_INDI_BPCO_SMED_D_EA_&an_N.
	WHERE BPCO_SMED_D_EA_Sej_Index IN (SELECT BPCO_SMED_D_EA_Sej_Index FROM exclus);

quit;

proc sort data = res.T_INDI_BPCO_SMED_D_EA_&an_N. nodupkey;
	by _all_;
run; quit;

proc delete data = exclus sejours_&Annee_N._&Annee_N1.;
run; quit;

proc datasets library = travail memtype = data nolist;
	delete exclus1-exclus2;
run; quit;

*	******************************************************************************************************************************************;
* 	Nombre de s�jours inclus;

proc sql;

	INSERT INTO flowch.Flow_Chart_BPCO_SMED_D_EA_&an_N.
		SELECT
			17,
			COUNT(DISTINCT BPCO_SMED_D_EA_Sej_Index)
	FROM res.T_INDI_BPCO_SMED_D_EA_&an_N.;

quit;

proc sql;

	SELECT COUNT(*), COUNT(DISTINCT BPCO_SMED_D_EA_Sej_Index)
	FROM res.T_INDI_BPCO_SMED_D_EA_&an_N.;

quit;
