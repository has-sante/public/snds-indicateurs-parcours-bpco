/*	******************************************************************************************************************************************	*/
/*																																				*/
/*		Indicateur 05 : Contact avec un pneumologue																				 				*/
/*																																				*/
/*	******************************************************************************************************************************************	*/

*	******************************************************************************************************************************************;
*	Contact avec le pneumologue;
*	On joint avec la table res.T_INDI_BPCO_SMED_D_EA_&an_N. pour r�cup�rer les info dans les 60 et 180 jours suivant la date index;

%macro contact_pneumo(nb_jours=);

	proc sql;

		CREATE TABLE contact_pneumo_&nb_jours.j AS
			SELECT DISTINCT
				a.BPCO_SMED_D_EA_Sej_Index,
				1 AS BPCO_SMED_D_EA_OBS length = 3,
				COUNT(DISTINCT b.date_debut) AS Nb_Contact_pneumo_&nb_jours.j length = 3,
				MIN(b.date_debut) AS Date_Contact_pneumo_&nb_jours.j format ddmmyy10. length = 4
			FROM res.T_INDI_BPCO_SMED_D_EA_&an_N. a
				INNER JOIN travail.contact_pneumo_&Annee_N._&Annee_N1. b
					ON a.BEN_IDT_ANO = b.BEN_IDT_ANO
			WHERE a.BPCO_SMED_D_EA_Date_index <= b.date_debut <= (a.BPCO_SMED_D_EA_Date_index + &nb_jours.)
			GROUP BY BPCO_SMED_D_EA_Sej_Index
			ORDER BY BPCO_SMED_D_EA_Sej_Index;

	quit;

%mend contact_pneumo;

%contact_pneumo(nb_jours = 60);
%contact_pneumo(nb_jours = 180);

*	On ajoute les informations dans la table de r�sultats;
proc sort data = res.T_INDI_BPCO_SMED_D_EA_&an_N.;
	by BPCO_SMED_D_EA_Sej_Index;
run;

* Dans les 60 jours;
proc sort data = contact_pneumo_60j;
	by BPCO_SMED_D_EA_Sej_Index;
run;

data res.T_INDI_BPCO_SMED_D_EA_&an_N.;
	merge	res.T_INDI_BPCO_SMED_D_EA_&an_N. (in = a)
			contact_pneumo_60j (in = b keep = BPCO_SMED_D_EA_Sej_Index BPCO_SMED_D_EA_OBS Nb_Contact_pneumo_60j);
	by BPCO_SMED_D_EA_Sej_Index;
	if BPCO_SMED_D_EA_OBS ne 1 then
		BPCO_SMED_D_EA_OBS = 0;
	if Nb_Contact_pneumo_60j = . then
		Nb_Contact_pneumo_60j = 0;
	if a then
		output;
run;

* Dans les 180 jours;
proc sort data = contact_pneumo_180j;
	by BPCO_SMED_D_EA_Sej_Index;
run;

data res.T_INDI_BPCO_SMED_D_EA_&an_N.;
	merge	res.T_INDI_BPCO_SMED_D_EA_&an_N. (in = a)
			contact_pneumo_180j (in = b keep = BPCO_SMED_D_EA_Sej_Index Date_Contact_pneumo_180j);
	by BPCO_SMED_D_EA_Sej_Index;
	if a then
		output;
run;

proc datasets library = work memtype = data nolist;
	delete contact_:;
run; quit;
