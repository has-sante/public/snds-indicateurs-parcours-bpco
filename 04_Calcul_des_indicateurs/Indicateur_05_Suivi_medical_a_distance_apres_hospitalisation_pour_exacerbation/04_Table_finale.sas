/*	******************************************************************************************************************************************	*/
/*																																				*/
/*		Indicateur 05 : Table finale - Ajout de formats et de labels																			*/
/*																																				*/
/*	******************************************************************************************************************************************	*/

data res.T_INDI_BPCO_SMED_D_EA_&an_N.;
	set res.T_INDI_BPCO_SMED_D_EA_&an_N. (keep = BEN_IDT_ANO BPCO_SMED_D_EA_Date_index BPCO_SMED_D_EA_Sej_Index Finess_PMSI Finess_GEO Dp Dp_classe 
		DA_BPCO GHM BPCO_SMED_D_EA_CIBLE Nb_Sej_EA_BPCO_DP_MCO Nb_Sej_EA_BPCO_DAS_MCO BPCO_SMED_D_EA_OBS Nb_Contact_Pneumo_60j Date_Contact_Pneumo_180j);
	label		
		BEN_IDT_ANO = "Num�ro d'individu"
		BPCO_SMED_D_EA_Date_index = "Date index"
		BPCO_SMED_D_EA_Sej_Index = "Identifiant du s�jour index"
		Finess_PMSI = "Finess PMSI ayant r�alis� le s�jour index"
		Finess_GEO = "Finess g�ographique du premier RUM"
		Dp = "Diagnostic principal du s�jour index "
		Dp_classe = "Diagnostic principal du s�jour index"
		DA_BPCO = "BPCO cod� en diagnostic secondaire du s�jour index"
		GHM = "GHM du s�jour index"
		BPCO_SMED_D_EA_CIBLE = "S�jour correspondant aux crit�res d'inclusion et d'exclusion de la population cible"
		Nb_Sej_EA_BPCO_DP_MCO = "Nombre de s�jours MCO d'exacerbation de BPCO cod� en DP termin�s en &annee_N."
		Nb_Sej_EA_BPCO_DAS_MCO = "Nombre de s�jours MCO d'exacerbation de BPCO cod� en DAS termin�s en &annee_N."
		BPCO_SMED_D_EA_OBS = "S�jour de la population cible pour lequel le patient a b�n�fici� d'un suivi � distance apr�s la sortie de l'h�pital"
		Nb_Contact_Pneumo_60j = "Nombre de contacts pneumologue dans les 60 jours suivant la date index"
		Date_Contact_Pneumo_180j = "Date du premier contact pneumologue dans les 180 jours suivant la date index"
		;
	format DA_BPCO BPCO_SMED_D_EA_CIBLE BPCO_SMED_D_EA_OBS f_oui_non. Dp_Classe f_DP_classe. BPCO_SMED_D_EA_Date_index Date_Contact_Pneumo_180j date9.;
run;
			
*	******************************************************************************************************************************************;
* 	Nombre de contacts dans les 60 jours apr�s l hospitalisation;

proc sql;

	INSERT INTO flowch.Flow_Chart_BPCO_SMED_D_EA_&an_N.
		SELECT
			18,
			COUNT(DISTINCT BPCO_SMED_D_EA_Sej_Index)
	FROM res.T_INDI_BPCO_SMED_D_EA_&an_N.
	WHERE BPCO_SMED_D_EA_OBS = 1;

	* Population d �tude;
	SELECT N into : N12 FROM flowch.Flow_Chart_BPCO_SMED_D_EA_&an_N. WHERE indicateur = 12;
	* Population cible;
	SELECT N into : N17 FROM flowch.Flow_Chart_BPCO_SMED_D_EA_&an_N. WHERE indicateur = 17;

quit;

data flowch.Flow_Chart_BPCO_SMED_D_EA_&an_N.;
	set flowch.Flow_Chart_BPCO_SMED_D_EA_&an_N.;
	if indicateur in (13, 14, 15, 17) then
		percent = N / &N12.;
	if indicateur = 18 then
		percent = N / &N17.;
	format indicateur f_ind_05_flowchart. percent percent7.1;		
run;

* V�rif;
proc sql;

	SELECT COUNT(*) AS nb_lignes, COUNT(DISTINCT BPCO_SMED_D_EA_Sej_Index) AS nb_sejours
	FROM res.T_INDI_BPCO_SMED_D_EA_&an_N.;
	* nb_lignes = nb_sejours : OK;

	SELECT COUNT(*) AS nb_lignes, COUNT(DISTINCT BPCO_SMED_D_EA_Sej_Index) AS nb_sejours
	FROM res.T_INDI_BPCO_SMED_D_EA_&an_N.
	WHERE BPCO_SMED_D_EA_OBS = 1;
	* nb_lignes = nb_sejours : OK;

quit;

*	******************************************************************************************************************************************;
*	On ajoute le flag BPCO_SMED_D_EA_CIBLE dans la table res.T_INDI_BPCO_&an_N.;

proc sql;

	ALTER TABLE res.T_INDI_BPCO_&an_N. DROP BPCO_SMED_D_EA_CIBLE;

	ALTER TABLE res.T_INDI_BPCO_&an_N. ADD BPCO_SMED_D_EA_CIBLE INT format = f_oui_non. length = 3
		label = "Patient appartenant � la population cible BPCO_SMED_D_EA";

	UPDATE res.T_INDI_BPCO_&an_N.
		SET BPCO_SMED_D_EA_CIBLE =	1
		WHERE BEN_IDT_ANO IN (SELECT DISTINCT BEN_IDT_ANO FROM res.T_INDI_BPCO_SMED_D_EA_&an_N.);

quit;

data res.T_INDI_BPCO_&an_N.;
	set res.T_INDI_BPCO_&an_N.;
	if BPCO_SMED_D_EA_CIBLE = .
		then BPCO_SMED_D_EA_CIBLE = 0;
run;
