/*	******************************************************************************************************************************************	*/
/*																																				*/
/*		Table de population - Patients avec indicateur_05 = 1																			 		*/
/*																																				*/
/*	******************************************************************************************************************************************	*/

data T_INDI_BPCO_SMED_D_EA_&an_N. (keep = BEN_IDT_ANO);
	set pop.indicateurs_&an_N. (where = (indicateur_05 = 1));
run;

*	******************************************************************************************************************************************;
*	Effectifs pour le flowchart;

* S�jours pour exacerbation : individus de 40 ans et plus ayant b�n�fici� de soins rembours�s au moins une fois l ann�e N;

proc sort data = T_INDI_BPCO_SMED_D_EA_&an_N. nodupkey;
	by BEN_IDT_ANO;
run;

proc sort data = travail.sejours_cible_exacerbation_&annee_N.;
	by BEN_IDT_ANO;
run;

data T_INDI_BPCO_SMED_D_EA_&an_N.;
	merge	T_INDI_BPCO_SMED_D_EA_&an_N. (in = a)
			travail.sejours_cible_exacerbation_&annee_N. (in = b);
	by BEN_IDT_ANO;
	if a and b;
	BPCO_SMED_D_EA_Sej_Index = id_sejour;
run;

proc sql;

	CREATE TABLE flowch.Flow_Chart_BPCO_SMED_D_EA_&an_N.  AS
		SELECT *
		FROM (
			SELECT 1 AS indicateur length = 3, COUNT(DISTINCT BPCO_SMED_D_EA_Sej_Index) AS N length = 5 FROM T_INDI_BPCO_SMED_D_EA_&an_N. WHERE exacerbation_BPCO = 1
			UNION ALL
			SELECT 2, COUNT(DISTINCT BPCO_SMED_D_EA_Sej_Index) FROM T_INDI_BPCO_SMED_D_EA_&an_N. WHERE exacerbation_BPCO = 2
			UNION ALL
			SELECT 3, COUNT(DISTINCT BPCO_SMED_D_EA_Sej_Index) FROM T_INDI_BPCO_SMED_D_EA_&an_N. WHERE exacerbation_BPCO = 3
			UNION ALL
			SELECT 4, COUNT(DISTINCT BPCO_SMED_D_EA_Sej_Index) FROM T_INDI_BPCO_SMED_D_EA_&an_N. WHERE exacerbation_BPCO = 4
			UNION ALL
			SELECT 5, COUNT(DISTINCT BPCO_SMED_D_EA_Sej_Index) FROM T_INDI_BPCO_SMED_D_EA_&an_N. WHERE exacerbation_BPCO = 5
			UNION ALL
			SELECT 6, COUNT(DISTINCT BPCO_SMED_D_EA_Sej_Index) FROM T_INDI_BPCO_SMED_D_EA_&an_N. WHERE exacerbation_BPCO = 6
			UNION ALL
			SELECT 7, COUNT(DISTINCT BPCO_SMED_D_EA_Sej_Index) FROM T_INDI_BPCO_SMED_D_EA_&an_N. WHERE exacerbation_BPCO = 7
			UNION ALL
			SELECT 8, COUNT(DISTINCT BPCO_SMED_D_EA_Sej_Index) FROM T_INDI_BPCO_SMED_D_EA_&an_N. WHERE exacerbation_BPCO = 8
			UNION ALL
			SELECT 9, COUNT(DISTINCT BPCO_SMED_D_EA_Sej_Index) FROM T_INDI_BPCO_SMED_D_EA_&an_N. WHERE exacerbation_BPCO = 9
			UNION ALL
			SELECT 10, COUNT(DISTINCT BPCO_SMED_D_EA_Sej_Index) FROM T_INDI_BPCO_SMED_D_EA_&an_N. WHERE exacerbation_BPCO = 10
			UNION ALL
			SELECT 11, COUNT(DISTINCT BPCO_SMED_D_EA_Sej_Index) FROM T_INDI_BPCO_SMED_D_EA_&an_N. WHERE exacerbation_BPCO = 11
			UNION ALL
			SELECT 12, COUNT(DISTINCT BPCO_SMED_D_EA_Sej_Index) FROM T_INDI_BPCO_SMED_D_EA_&an_N.
		) ;

quit;
