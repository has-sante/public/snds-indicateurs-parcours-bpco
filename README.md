# Indicateurs de parcours BPCO à partir du SNDS

Programme SAS utilisés pour calculer les indicateurs de parcours BPCO à partir du SNDS.

## Contexte

Pour une présentation rapide de ce travail, cf le [communiqué de presse](https://www.has-sante.fr/jcms/p_3329428/fr/bpco-des-indicateurs-de-qualite-pour-evaluer-le-parcours-de-soins-des-patients) de sa publication.

L'ensemble des documents concernant ces indicateurs est accessible dans la [section dédiée de cette page](https://www.has-sante.fr/jcms/p_3306659/fr/indicateurs-de-qualite-des-parcours-de-soins#toc_1_3_1).

## Eléments techniques

Les éléments techniques sont rassemblés sur cette [page dédiée](https://www.has-sante.fr/jcms/p_3326301/fr/outils-pour-calculer-les-indicateurs-de-parcours-bpco-a-partir-du-snds-dcir/pmsi).

Voir en particulier le [guide d'utilisation des programmes SAS](https://www.has-sante.fr/upload/docs/application/pdf/2022-04/guide_utilisation_programme_sas.pdf). 

Les programmes, disponibles sur le site de la HAS,  sont copiés dans ce dépôt pour en faciliter le partage et la référence. 

## Licence

Ces programmes ont été développés par le service Évaluation et Outils pour la Qualité et la Sécurité des Soins (EvOQSS) de la Haute Autorité de Santé (HAS).

Ils sont partagés sous [licence EUPL](LICENCE).