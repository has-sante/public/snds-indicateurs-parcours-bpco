/*	******************************************************************************************************************************************	*/
/*																																				*/
/*		S�lection des patients - Exclusion des jumeaux																							*/
/*																																				*/
/*	******************************************************************************************************************************************	*/

*	******************************************************************************************************************************************;
* On r�cup�re les jumeaux de m�me sexes;
proc sql;

	CREATE TABLE jumeaux AS
		SELECT *
		FROM pop.T_INDI_BPCO_&an_N.
		GROUP BY BEN_NIR_PSA
		HAVING COUNT(DISTINCT BEN_IDT_ANO) > 1;

quit;

*	******************************************************************************************************************************************;
* On les exclut de notre r�f�rentiel;
proc sql;

	CREATE INDEX BEN_IDT_ANO ON pop.T_INDI_BPCO_&an_N. (BEN_IDT_ANO);

	CREATE INDEX BEN_IDT_ANO ON jumeaux (BEN_IDT_ANO);

	ALTER TABLE pop.T_INDI_BPCO_&an_N. ADD exclus_jumeaux VARCHAR(1);

	UPDATE pop.T_INDI_BPCO_&an_N. SET
		exclus_jumeaux = CASE	WHEN BEN_IDT_ANO IN (SELECT BEN_IDT_ANO FROM jumeaux) THEN "1"
								ELSE "0"
								END;

quit;

*	******************************************************************************************************************************************;
* On supprime la table temporaire;

proc delete data = jumeaux;
run; quit;

*	******************************************************************************************************************************************;
*	On cr�e une table de correspondance;

%suppr_table(
	lib = orauser, 
	table = corresp_id_patient
	);

proc sort data = pop.T_INDI_BPCO_&an_N. out = orauser.corresp_id_patient (keep = BEN_IDT_ANO BEN_NIR_PSA) nodupkey;
	by BEN_IDT_ANO BEN_NIR_PSA;
run;

*	******************************************************************************************************************************************;
*	V�rifications;

*	Variable Exclus_Jumeaux ;
%proc_freq(
	in_tbl = pop.T_INDI_BPCO_&an_N., 
	out_tbl = _pop_Exclus_Jumeaux, 
	list_var_in = Exclus_Jumeaux, 
	list_var_out = Exclus_Jumeaux Frequency
	);
