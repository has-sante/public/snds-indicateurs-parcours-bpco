/*	******************************************************************************************************************************************	*/
/*																																				*/
/*		S�lection des patients - Table finale : Pr�-s�lection des populations de chaque indicateur												*/
/*																																				*/
/*	******************************************************************************************************************************************	*/

proc sql;

	ALTER TABLE pop.T_INDI_BPCO_&an_N. ADD indicateur_01 INT length = 3, indicateur_02a INT length = 3, indicateur_02b INT length = 3, 
		indicateur_03 INT length = 3, indicateur_04 INT length = 3, indicateur_05 INT length = 3, indicateur_06 INT length = 3, 
		indicateur_07 INT length = 3, indicateur_09 INT length = 3;

	* 01 : Patients suspects de BPCO (1 BDLA, 1 ATB, 1 TTT arr�t tabac), de 40 ans et plus;
	UPDATE pop.T_INDI_BPCO_&an_N. a
		SET indicateur_01 =	CASE	WHEN (Nb_Broncho_LDA >= 1 OR Nb_ATB >= 1 OR nb_Tabac >= 1) AND Age >= 40 THEN 1
									ELSE 0
									END;

	* 02a : Patients BPCO probable (3 BDLA), de 40 ans et plus;
	UPDATE pop.T_INDI_BPCO_&an_N. a
		SET indicateur_02a =	CASE	WHEN Nb_Broncho_LDA >= 3 AND Age >= 40 THEN 1
										ELSE 0
										END;

	* 02b : Patients BPCO diagnostiqu�s
				ALD active au 1er septembre de l ann�e N, de 40 ans et plus
				hospitalis�s pour BPCO en MCO, SSR ou HAD, de 40 ans et plus;
	UPDATE pop.T_INDI_BPCO_&an_N. a
		SET indicateur_02b =	CASE	WHEN (ALD_BPCO_septembre = 1 AND Age >= 40) OR (Nb_hospit_BPCO_Ind02 >= 1 AND Age >= 40) THEN 1
										ELSE 0
										END;

	* 03 : Patients BPCO probable (3 BDLA) ou diagnostiqu�s BPCO (ALD active au 31 d�cembre de l ann�e N ou hospitalis�s pour BPCO en MCO, SSR ou HAD),
		de 40 ans et plus;
	UPDATE pop.T_INDI_BPCO_&an_N. a
		SET indicateur_03 =	CASE	WHEN (Nb_Broncho_LDA >= 3 OR ALD_BPCO_decembre = 1 OR Nb_hospit_BPCO >= 1) AND Age >= 40 THEN 1
									ELSE 0
									END;

	* 04 : Patients avec au moins un s�jour pour exacerbation de BPCO, de 40 ans et plus;
	UPDATE pop.T_INDI_BPCO_&an_N. a
		SET indicateur_04 =	CASE	WHEN Nb_Sej_Exacerbation >= 1 AND Age >= 40 THEN 1
									ELSE 0
									END;

	* 05 : Patients avec au moins un s�jour pour exacerbation de BPCO, de 40 ans et plus;
	UPDATE pop.T_INDI_BPCO_&an_N. a
		SET indicateur_05 =	CASE	WHEN Nb_Sej_Exacerbation >= 1 AND Age >= 40 THEN 1
										ELSE 0
										END;

	* 06 : Patients avec au moins un s�jour pour exacerbation de BPCO, de 40 ans et plus;
	UPDATE pop.T_INDI_BPCO_&an_N. a
		SET indicateur_06 =	CASE	WHEN Nb_Sej_Exacerbation >= 1 AND Age >= 40 THEN 1
										ELSE 0
										END;

	* 07 : Patients avec au moins un s�jour pour exacerbation de BPCO, de 40 ans et plus;
	UPDATE pop.T_INDI_BPCO_&an_N. a
		SET indicateur_07 =	CASE	WHEN Nb_Sej_Exacerbation >= 1 AND Age >= 40 THEN 1
										ELSE 0
										END;

	* 09 : Patients avec au moins un s�jour pour exacerbation de BPCO, de 40 ans et plus;
	UPDATE pop.T_INDI_BPCO_&an_N. a
		SET indicateur_09 =	CASE	WHEN Nb_Sej_Exacerbation >= 1 AND Age >= 40 THEN 1
									ELSE 0
									END;

quit;

*	******************************************************************************************************************************************;
*	V�rifications;

data verif;
	set pop.T_INDI_BPCO_&an_N.;
	length age_40 3.;
	format age_40 f_age_40_.;
	* Plus ou moins de 40 ans;
	if age < 40 then
		age_40 = 0;
	else
		age_40 = 1;
run;

* Indicateur 01;
%proc_freq(
	in_tbl = verif, 
	out_tbl = _ind01, 
	list_var_in = Indicateur_01*Age_40*NB_BRONCHO_LDA*NB_ATB*NB_TABAC, 
	list_var_out = Indicateur_01 Age_40 NB_BRONCHO_LDA NB_ATB NB_TABAC Frequency
	);

* Indicateur 02a;
%proc_freq(
	in_tbl = verif, 
	out_tbl = _ind02a, 
	list_var_in = Indicateur_02a*age_40*NB_BRONCHO_LDA, 
	list_var_out = Indicateur_02a age_40 NB_BRONCHO_LDA Frequency
	);

* Indicateur 02b;
%proc_freq(
	in_tbl = verif, 
	out_tbl = _ind02b, 
	list_var_in = Indicateur_02b*age_40*ALD_BPCO_SEPTEMBRE*Nb_hospit_BPCO_Ind02, 
	list_var_out = Indicateur_02b age_40 ALD_BPCO_SEPTEMBRE Nb_hospit_BPCO_Ind02 Frequency
	);

* Indicateur 03;
%proc_freq(
	in_tbl = verif, 
	out_tbl = _ind03, 
	list_var_in = Indicateur_03*Age_40*NB_BRONCHO_LDA*ALD_BPCO_DECEMBRE*NB_HOSPIT_BPCO, 
	list_var_out = Indicateur_03 Age_40 NB_BRONCHO_LDA ALD_BPCO_DECEMBRE NB_HOSPIT_BPCO Frequency
	);

* Indicateur 04;
%proc_freq(
	in_tbl = verif, 
	out_tbl = _ind04, 
	list_var_in = Indicateur_04*Age_40*Nb_Sej_Exacerbation, 
	list_var_out = Indicateur_04 Age_40 Nb_Sej_Exacerbation Frequency
	);

* Indicateur 05;
%proc_freq(
	in_tbl = verif, 
	out_tbl = _ind05, 
	list_var_in = Indicateur_05*Age_40*Nb_Sej_Exacerbation, 
	list_var_out = Indicateur_05 Age_40 Nb_Sej_Exacerbation Frequency
	);

* Indicateur 06;
%proc_freq(
	in_tbl = verif, 
	out_tbl = _ind06, 
	list_var_in = Indicateur_06*Age_40*Nb_Sej_Exacerbation, 
	list_var_out = Indicateur_06 Age_40 Nb_Sej_Exacerbation Frequency
	);

* Indicateur 07;
%proc_freq(
	in_tbl = verif, 
	out_tbl = _ind07, 
	list_var_in = Indicateur_07*Age_40*Nb_Sej_Exacerbation, 
	list_var_out = Indicateur_07 Age_40 Nb_Sej_Exacerbation Frequency
	);

* Indicateur 09;
%proc_freq(
	in_tbl = verif, 
	out_tbl = _ind09, 
	list_var_in = Indicateur_09*Age_40*Nb_Sej_Exacerbation, 
	list_var_out = Indicateur_09 Age_40 Nb_Sej_Exacerbation Frequency
	);

proc delete data = verif;
run; quit;

*	******************************************************************************************************************************************;
*	On cr�e la table de r�sultat et une table de correspondance pour les patients qu on a conserv�s pour un des indicateurs;

data res.T_INDI_BPCO_&an_N.;
	set pop.T_INDI_BPCO_&an_N. (keep = Ben_Idt_Ano BEN_NIR_PSA Age Ben_Sex_Cod Ben_Dcd_Dte BEN_RES_DPT BEN_RES_COM RGM_GRG_COD Nb_hospit_BPCO
		Nb_hospit_BPCO_Ind02 Nb_Sej_Exacerbation ALD_BPCO_&an_N. ALD_BPCO_septembre ALD_BPCO_decembre Nb_Broncho_LDA Nb_ATB Nb_Tabac 
		exclus_pas_40ans exclus_jumeaux exclus_pas_remb exclus_NIR_fictif Indicateur_01 Indicateur_02a Indicateur_02b Indicateur_03 
		Indicateur_04 Indicateur_05 Indicateur_06 Indicateur_07 Indicateur_09 where = (exclus_pas_40ans = "0" and exclus_jumeaux = "0" 
		and exclus_pas_remb = "0" and exclus_NIR_fictif = "0" and (Indicateur_01 = 1 or Indicateur_02a = 1 or Indicateur_02b = 1 or 
		Indicateur_03 = 1 or Indicateur_04 = 1 or Indicateur_05 = 1 or Indicateur_06 = 1 or Indicateur_07 = 1 or Indicateur_09 = 1)));
run;

* On supprime la table de correspondance si elle existe;
%suppr_table(
	lib = orauser, 
	table = corresp_id_patient
	);

proc sort data = res.T_INDI_BPCO_&an_N. (keep = BEN_IDT_ANO BEN_NIR_PSA) out = orauser.corresp_id_patient nodupkey;
	by BEN_IDT_ANO BEN_NIR_PSA;
run;

proc sort data = res.T_INDI_BPCO_&an_N. (drop = BEN_NIR_PSA) nodupkey;
	by BEN_IDT_ANO;
run;

proc sql;

	CREATE INDEX BEN_IDT_ANO ON res.T_INDI_BPCO_&an_N. (BEN_IDT_ANO);

quit;

* V�rif;
proc sql;

	SELECT COUNT(*) AS nb_lignes, COUNT(DISTINCT BEN_IDT_ANO) AS nb_patients
	FROM res.T_INDI_BPCO_&an_N.;

quit;

*	******************************************************************************************************************************************;
*	On cr�e une table contenant les indicateurs;

data pop.indicateurs_&an_N.;
	set pop.T_INDI_BPCO_&an_N. (keep = Ben_Idt_Ano exclus_pas_40ans exclus_jumeaux exclus_pas_remb exclus_NIR_fictif Indicateur_01 
			Indicateur_02a Indicateur_02b Indicateur_03 Indicateur_04 Indicateur_05 Indicateur_06 Indicateur_07 Indicateur_09 
			where = (exclus_pas_40ans = "0" and exclus_jumeaux = "0" and exclus_pas_remb = "0" and exclus_NIR_fictif = "0" and (Indicateur_01 = 1 
			or Indicateur_02a = 1 or Indicateur_02b = 1 or Indicateur_03 = 1 or Indicateur_04 = 1 or Indicateur_05 = 1 or Indicateur_06 = 1
			or Indicateur_07 = 1 or Indicateur_09 = 1)));
	drop exclus_pas_40ans exclus_jumeaux exclus_pas_remb exclus_NIR_fictif;
run;

*	******************************************************************************************************************************************;
*	On conserve des nombre de patients de 40 ans et plus ayant b�n�fici� de soins rembours�s au moins une fois l ann�e N (base pour calculer 
*	les % des flowcharts);

proc sql;

	CREATE TABLE travail.ref_flowcharts AS
		SELECT 
			COUNT(DISTINCT BEN_IDT_ANO) AS Nb_patients_ref0 length = 3
		FROM pop.T_indi_BPCO_&an_N.
		WHERE Age >= 40 AND exclus_jumeaux = "0" AND exclus_pas_remb = "0" AND exclus_NIR_fictif = "0";

quit;

*	******************************************************************************************************************************************;
*	On supprime la table pop.T_indi_BPCO_&an_N.;

proc delete data = pop.T_indi_BPCO_&an_N.;
run; quit;
