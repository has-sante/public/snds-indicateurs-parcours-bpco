/*	******************************************************************************************************************************************	*/
/*																																				*/
/*		S�lection des patients - Nombre de s�jours pour exacerbation dans l ann�e																*/
/*																																				*/
/*		S�jours pour exacerbation de BPCO = S�jour hospitalier MCO termin� dans l ann�e pour lequel : 											*/
/*			- Un code diagnostic de BPCO a �t� cod� en DP du s�jour																				*/
/*			- OU un code de pneumopathie lobaire a �t� cod� en DP du s�jour avec un diagnostic de BPCO cod� en DAS								*/
/*			- OU un code d insuffisance respiratoire aig�e a �t� cod� en DP du s�jour avec un diagnostic de BPCO cod� en DAS 					*/
/*			- OU un code de grippe a �t� cod� en DP du s�jour avec un diagnostic de BPCO cod� en DAS 											*/
/*			- OU un code d infection des voies a�riennes a �t� cod� en DP du s�jour avec un diagnostic de BPCO cod� en DAS 						*/
/*			- OU un code de bronchopneumopathie a �t� cod� en DP du s�jour avec un diagnostic de BPCO cod� en DAS								*/
/*			- OU un code de pneumonie a �t� cod� en DP du s�jour avec un diagnostic de BPCO cod� en DAS 										*/
/*			- OU un code d embolie pulmonaire a �t� cod� en DP du s�jour avec un diagnostic de BPCO cod� en DAS 								*/
/*			- OU un code d insuffisance cardiaque aigue a �t� cod� en DP du s�jour avec un diagnostic de BPCO cod� en DAS 						*/
/*			- OU un code d oed�me aigu du poumon a �t� cod� en DP du s�jour avec un diagnostic de BPCO cod� en DAS 								*/
/*			- OU un code de pneumothorax a �t� cod� en DP du s�jour avec un diagnostic de BPCO cod� en DAS 										*/
/*																																				*/
/*	******************************************************************************************************************************************	*/

*	******************************************************************************************************************************************;
*	On r�cup�re les s�jours pour exacerbation de BPCO;

%suppr_table(
	lib = orauser, 
	table = diag_codes_CIM_exa_BPCO
	);

data orauser.diag_codes_CIM_exa_BPCO;
	set orauser.diag_codes_CIM_BPCO (where = (reperage in (16, 18, 17, 19, 23, 25, 24, 20, 21, 22) or (reperage = 11 and flag_pneumo = 1)));
run;

* On appelle la macro pour le rep�rage dans le PMSI MCO;
%suppr_table(
	lib = travail, 
	table = reperage_CIM10_exa_BPCO
	);

%extract_CIM10_PMSI(
	annee_deb = &annee_N.,
	annee_fin = &annee_N.,
	HAD_DP = 0,
	HAD_DAS = 0, 
	HAD_MPP = 0, 
	HAD_MPA = 0, 
	MCO_DP = 1, 
	MCO_DR = 1, 
	MCO_DAS = 1, 
	MCO_DP_UM = 1, 
	MCO_DR_UM = 1,
	SSR_FP = 0, 
	SSR_MPP = 0, 
	SSR_AE = 0, 
	SSR_DAS = 0, 
	tbl_out = travail.reperage_CIM10_exa_BPCO, 
	tbl_codes = diag_codes_CIM_exa_BPCO,
	tbl_patients = corresp_id_patient
	);

proc sql;

	DELETE FROM travail.reperage_CIM10_exa_BPCO
	WHERE SUBSTR(GRG_GHM, 1, 2) = "28" OR SEJ_NBJ = 0;

quit;

proc sort data = travail.reperage_CIM10_exa_BPCO;
	by ETA_NUM RSA_NUM annee;
run;

*	******************************************************************************************************************************************;
*	On r�cup�re les s�jours avec un code diagnostic de BPCO a �t� cod� en DP du s�jour;

data sejours_exacerbation1;
	set travail.reperage_CIM10_exa_BPCO (where = (reperage = 16 and table = "B" and variable = "DGN_PAL"));
	length exacerbation_BPCO 3.;
	exacerbation_BPCO = 1;
run;

*	******************************************************************************************************************************************;
*	On r�cup�re les s�jours avec un code diagnostic de BPCO a �t� cod� en DAS du s�jour;

proc sort data = travail.reperage_CIM10_exa_BPCO (where = (reperage = 16 and variable = "ASS_DGN")) out = reperage_CIM10_exa_BPCO
		nodupkey;
	by ETA_NUM RSA_NUM annee;
run;

*	******************************************************************************************************************************************;
*	On r�cup�re les s�jours avec un code de pneumopathie lobaire en DP et un diagnostic de BPCO en DAS;

data sejours_exacerbation2;
	merge	travail.reperage_CIM10_exa_BPCO (in = a where = (reperage = 18 and table = "B" and variable = "DGN_PAL"))
			reperage_CIM10_exa_BPCO (in = b);
	by ETA_NUM RSA_NUM annee;
	if a and b;
	length exacerbation_BPCO 3.;
	exacerbation_BPCO = 2;
run;

*	******************************************************************************************************************************************;
*	On r�cup�re les s�jours avec un code d insuffisance respiratoire aig�e en DP avec un diagnostic de BPCO en DAS;

data sejours_exacerbation3;
	merge	travail.reperage_CIM10_exa_BPCO (in = a where = (reperage = 17 and table = "B" and variable = "DGN_PAL"))
			reperage_CIM10_exa_BPCO (in = b);
	by ETA_NUM RSA_NUM annee;
	if a and b;
	length exacerbation_BPCO 3.;
	exacerbation_BPCO = 3;
run;

*	******************************************************************************************************************************************;
*	On r�cup�re les s�jours avec un code de grippe en DP avec un diagnostic de BPCO en DAS;

data sejours_exacerbation4;
	merge	travail.reperage_CIM10_exa_BPCO (in = a where = (reperage = 19 and table = "B" and variable = "DGN_PAL"))
			reperage_CIM10_exa_BPCO (in = b);
	by ETA_NUM RSA_NUM annee;
	if a and b;
	length exacerbation_BPCO 3.;
	exacerbation_BPCO = 4;
run;

*	******************************************************************************************************************************************;
*	On r�cup�re les s�jours avec un code d infection des voies a�riennes en DP avec un diagnostic de BPCO en DAS;

data sejours_exacerbation5;
	merge	travail.reperage_CIM10_exa_BPCO (in = a where = (reperage = 23 and table = "B" and variable = "DGN_PAL"))
			reperage_CIM10_exa_BPCO (in = b);
	by ETA_NUM RSA_NUM annee;
	if a and b;
	length exacerbation_BPCO 3.;
	exacerbation_BPCO = 5;
run;

*	******************************************************************************************************************************************;
*	On r�cup�re les s�jours avec un code de bronchopneumopathie en DP avec un diagnostic de BPCO en DAS;

data sejours_exacerbation6;
	merge	travail.reperage_CIM10_exa_BPCO (in = a where = (reperage = 25 and table = "B" and variable = "DGN_PAL"))
			reperage_CIM10_exa_BPCO (in = b);
	by ETA_NUM RSA_NUM annee;
	if a and b;
	length exacerbation_BPCO 3.;
	exacerbation_BPCO = 6;
run;

*	******************************************************************************************************************************************;
*	On r�cup�re les s�jours avec un code de pneumonie en DP avec un diagnostic de BPCO en DAS;

data sejours_exacerbation7;
	merge	travail.reperage_CIM10_exa_BPCO (in = a where = (reperage = 24 and table = "B" and variable = "DGN_PAL"))
			reperage_CIM10_exa_BPCO (in = b);
	by ETA_NUM RSA_NUM annee;
	if a and b;
	length exacerbation_BPCO 3.;
	exacerbation_BPCO = 7;
run;

*	******************************************************************************************************************************************;
*	On r�cup�re les s�jours avec un code d embolie pulmonaire en DP avec un diagnostic de BPCO en DAS;

data sejours_exacerbation8;
	merge	travail.reperage_CIM10_exa_BPCO (in = a where = (reperage = 20 and table = "B" and variable = "DGN_PAL"))
			reperage_CIM10_exa_BPCO (in = b);
	by ETA_NUM RSA_NUM annee;
	if a and b;
	length exacerbation_BPCO 3.;
	exacerbation_BPCO = 8;
run;

*	******************************************************************************************************************************************;
*	On r�cup�re les s�jours avec un code d insuffisance cardiaque aigue en DP avec un diagnostic de BPCO en DAS;

data sejours_exacerbation9;
	merge	travail.reperage_CIM10_exa_BPCO (in = a where = (reperage = 21 and table = "B" and variable = "DGN_PAL"))
			reperage_CIM10_exa_BPCO (in = b);
	by ETA_NUM RSA_NUM annee;
	if a and b;
	length exacerbation_BPCO 3.;
	exacerbation_BPCO = 9;
run;

*	******************************************************************************************************************************************;
*	On r�cup�re les s�jours avec un code d �d�me aigu du poumon en DP avec un diagnostic de BPCO en DAS;

data sejours_exacerbation10;
	merge	travail.reperage_CIM10_exa_BPCO (in = a where = (reperage = 22 and table = "B" and variable = "DGN_PAL"))
			reperage_CIM10_exa_BPCO (in = b);
	by ETA_NUM RSA_NUM annee;
	if a and b;
	length exacerbation_BPCO 3.;
	exacerbation_BPCO = 10;
run;

*	******************************************************************************************************************************************;
*	On r�cup�re les s�jours avec un code de pneumothorax en DP avec un diagnostic de BPCO en DAS;

data sejours_exacerbation11;
	merge	travail.reperage_CIM10_exa_BPCO (in = a where = (reperage = 11 and table = "B" and variable = "DGN_PAL"))
			reperage_CIM10_exa_BPCO (in = b);
	by ETA_NUM RSA_NUM annee;
	if a and b;
	length exacerbation_BPCO 3.;
	exacerbation_BPCO = 11;
run;

*	******************************************************************************************************************************************;
*	On concat�ne toutes les tables et on compte le nombre de s�jours par patient;

data travail.sejours_exacerbation_&Annee_N.;
	set sejours_exacerbation1-sejours_exacerbation11;
	length id_sejour $ 30;
	id_sejour = ETA_NUM||RSA_NUM||put(Annee, 4.);
run;

*	******************************************************************************************************************************************;
*	V�rifications;

%let tmp_num_tab = 15;

%proc_freq(
	in_tbl = travail.sejours_exacerbation_&Annee_N., 
	out_tbl = _exa_BPCO, 
	list_var_in = Annee*Domaine*Table*Variable*DGN_PAL*CODE_CIM, 
	list_var_out = Annee Domaine Table2 Variable DGN_PAL CODE_CIM Frequency
	);

proc sql undo_policy = none;

	CREATE TABLE nb_sejours_exacerbation AS
		SELECT
			BEN_IDT_ANO,
			COUNT(DISTINCT id_sejour) AS Nb_Sej_Exacerbation length = 3
		FROM travail.sejours_exacerbation_&Annee_N.
		GROUP BY BEN_IDT_ANO;

quit;

*	******************************************************************************************************************************************;
*	On ajoute l information du nombre de s�jours pour exacerbation dans l ann�e dans la table de population;

data pop.T_INDI_BPCO_&an_N.;
	merge	pop.T_INDI_BPCO_&an_N. (in = a)
			nb_sejours_exacerbation (in = b);
	by BEN_IDT_ANO;
	if a;
	if not b then
		Nb_Sej_Exacerbation = 0;
run;

*	******************************************************************************************************************************************;
*	V�rifications;

%proc_freq(
	in_tbl = pop.T_INDI_BPCO_&an_N., 
	out_tbl = _exa_BPCO_Nb_Sej, 
	list_var_in = Nb_Sej_Exacerbation, 
	list_var_out = Nb_Sej_Exacerbation Frequency
	);

*	******************************************************************************************************************************************;
*	On supprime les tables temporaires;

proc datasets library = work memtype = data nolist;
	delete sejours_exacerbation: nb_sejours_exacerbation reperage_CIM10_exa_BPCO;
run; quit;

proc delete data = orauser.diag_codes_CIM_exa_BPCO;
run; quit;

proc delete data = travail.reperage_CIM10_exa_BPCO travail.sejours_exacerbation_&Annee_N.;
run; quit;
