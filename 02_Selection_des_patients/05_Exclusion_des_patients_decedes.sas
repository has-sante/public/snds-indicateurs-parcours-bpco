/*	******************************************************************************************************************************************	*/
/*																																				*/
/*		S�lection des patients - Exclusion de patients non vivant au 1er janvier de l'ann�e														*/
/*																																				*/
/*	******************************************************************************************************************************************	*/

*	******************************************************************************************************************************************;
*	On r�cup�re l information du d�c�s dans IR_BEN_R;

data deces_IR_BEN_R;
	set pop.T_INDI_BPCO_&an_N. (keep = BEN_IDT_ANO BEN_DCD_DTE where = (BEN_DCD_DTE ne .));
run;

*	******************************************************************************************************************************************;
*	On r�cup�re l information du d�c�s dans le PMSI;

%macro deces_PMSI(annee_deb=, annee_fin=);

	%let an_deb_pmsi = %sysevalf(&annee_deb. - 2000);
	%let an_fin_pmsi = %sysevalf(&annee_fin. - 2000);

	%do an = &an_deb_pmsi. %to &an_fin_pmsi.;

		* Dans le PMSI - HAD;		
		proc sql;

			%connectora;

				CREATE TABLE deces_PMSI_HAD_&an. AS SELECT * FROM CONNECTION TO ORACLE (
						SELECT DISTINCT
							a.BEN_IDT_ANO,
							c.EXE_SOI_DTD,
							c.EXE_SOI_DTF AS BEN_DCD_DTE
						FROM corresp_id_patient a
							INNER JOIN T_HAD&an.C c
								ON a.BEN_NIR_PSA = c.NIR_ANO_17
							INNER JOIN T_HAD&an.B b
								ON	c.ETA_NUM_EPMSI = b.ETA_NUM_EPMSI
								AND	c.RHAD_NUM = b.RHAD_NUM
						WHERE b.SOR_MOD = '9' AND c.NIR_ANO_17 NOT IN ('&cle_inc1.', '&cle_inc2.')						
							AND NIR_RET = '0' AND NAI_RET = '0' AND SEX_RET = '0' AND SEJ_RET = '0' AND FHO_RET = '0' 
							AND PMS_RET = '0' AND DAT_RET = '0' %if &an. > 12 %then %do; AND COH_NAI_RET = '0' 
							AND COH_SEX_RET = '0' %end;
						);

			DISCONNECT FROM ORACLE;

		quit;

		data deces_PMSI_HAD_&an. (rename = (EXE_SOI_DTD2 = EXE_SOI_DTD BEN_DCD_DTE2 = BEN_DCD_DTE));
			set deces_PMSI_HAD_&an.;
			length EXE_SOI_DTD2 BEN_DCD_DTE2 4.;
			EXE_SOI_DTD2 = datepart(EXE_SOI_DTD);
			BEN_DCD_DTE2 = datepart(BEN_DCD_DTE);
			format EXE_SOI_DTD2 BEN_DCD_DTE2 ddmmyy10.;
			drop EXE_SOI_DTD BEN_DCD_DTE;
		run;

		* Dans le PMSI - MCO;		
		proc sql;

			%connectora;

				CREATE TABLE deces_PMSI_MCO_&an. AS SELECT * FROM CONNECTION TO ORACLE (
						SELECT DISTINCT
							a.BEN_IDT_ANO,
							c.EXE_SOI_DTD,
							c.EXE_SOI_DTF AS BEN_DCD_DTE
						FROM corresp_id_patient a
							INNER JOIN T_MCO&an.C c
								ON a.BEN_NIR_PSA = c.NIR_ANO_17
							INNER JOIN T_MCO&an.B b
								ON	c.ETA_NUM = b.ETA_NUM
								AND	c.RSA_NUM = b.RSA_NUM
						WHERE b.SOR_MOD = '9' AND c.NIR_ANO_17 NOT IN ('&cle_inc1.', '&cle_inc2.')
							AND NIR_RET = '0' AND NAI_RET = '0' AND SEX_RET = '0' AND SEJ_RET = '0' AND FHO_RET = '0' 
							AND PMS_RET = '0' AND DAT_RET = '0' %if &an. > 12 %then %do; AND COH_NAI_RET = '0' 
							AND COH_SEX_RET = '0' %end;
							AND c.ETA_NUM NOT IN ('130780521', '130783236', '130783293', '130784234', '130804297', '600100101', 
								'750041543', '750100018', '750100042', '750100075', '750100083', '750100091', '750100109', 
								'750100125', '750100166', '750100208', '750100216', '750100232', '750100273', '750100299', 
								'750801441', '750803447', '750803454', '910100015', '910100023', '920100013', '920100021', 
								'920100039', '920100047', '920100054', '920100062', '930100011', '930100037', '930100045', 
								'940100027', '940100035', '940100043', '940100050', '940100068', '950100016', '690783154', 
								'690784137', '690784152', '690784178', '690787478', '830100558')
							AND GRG_GHM NOT IN ('90Z00Z') AND GRG_RET NOT IN ('024') AND GRG_GHM NOT IN ('90H01Z', '90Z00Z', '90Z01Z', 
								'90Z02Z', '90Z03Z') 
							AND ((SEJ_TYP = 'A' OR SEJ_TYP IS NULL) OR (SEJ_TYP = 'B' AND GRG_GHM NOT IN ('28Z14Z','28Z15Z','28Z16Z')))
						);

			DISCONNECT FROM ORACLE;

		quit;

		data deces_PMSI_MCO_&an. (rename = (EXE_SOI_DTD2 = EXE_SOI_DTD BEN_DCD_DTE2 = BEN_DCD_DTE));
			set deces_PMSI_MCO_&an.;
			length EXE_SOI_DTD2 BEN_DCD_DTE2 4.;
			EXE_SOI_DTD2 = datepart(EXE_SOI_DTD);
			BEN_DCD_DTE2 = datepart(BEN_DCD_DTE);
			format EXE_SOI_DTD2 BEN_DCD_DTE2 ddmmyy10.;
			drop EXE_SOI_DTD BEN_DCD_DTE;
		run;

		* Dans le PMSI - RIP;		
		proc sql;

			%connectora;

				CREATE TABLE deces_PMSI_RIP_&an. AS SELECT * FROM CONNECTION TO ORACLE (
						SELECT DISTINCT
							a.BEN_IDT_ANO,
							c.EXE_SOI_DTD,
							c.EXE_SOI_DTF AS BEN_DCD_DTE
						FROM corresp_id_patient a
							INNER JOIN T_RIP&an.C c
								ON a.BEN_NIR_PSA = c.NIR_ANO_17
							INNER JOIN T_RIP&an.RSA rsa
								ON	c.ETA_NUM_EPMSI = rsa.ETA_NUM_EPMSI
								AND	c.RIP_NUM = rsa.RIP_NUM
						WHERE rsa.SOR_MOD = '9' AND c.NIR_ANO_17 NOT IN ('&cle_inc1.', '&cle_inc2.')
							AND NIR_RET = '0' AND NAI_RET = '0' AND SEX_RET = '0' AND SEJ_RET = '0' AND FHO_RET = '0' 
							AND PMS_RET = '0' AND DAT_RET = '0' %if &an. > 12 %then %do; AND COH_NAI_RET = '0' 
							AND COH_SEX_RET = '0' %end;
						);

			DISCONNECT FROM ORACLE;

		quit;

		data deces_PMSI_RIP_&an. (rename = (EXE_SOI_DTD2 = EXE_SOI_DTD BEN_DCD_DTE2 = BEN_DCD_DTE));
			set deces_PMSI_RIP_&an.;
			length EXE_SOI_DTD2 BEN_DCD_DTE2 4.;
			EXE_SOI_DTD2 = datepart(EXE_SOI_DTD);
			BEN_DCD_DTE2 = datepart(BEN_DCD_DTE);
			format EXE_SOI_DTD2 BEN_DCD_DTE2 ddmmyy10.;
			drop EXE_SOI_DTD BEN_DCD_DTE;
		run;

		* Dans le PMSI - SSR;		
		proc sql;

			%connectora;

				CREATE TABLE deces_PMSI_SSR_&an. AS SELECT * FROM CONNECTION TO ORACLE (
						SELECT DISTINCT
							a.BEN_IDT_ANO,
							c.EXE_SOI_DTD,
							c.EXE_SOI_DTF AS BEN_DCD_DTE
						FROM corresp_id_patient a
							INNER JOIN T_SSR&an.C c
								ON a.BEN_NIR_PSA = c.NIR_ANO_17
							INNER JOIN T_SSR&an.B b
								ON	c.ETA_NUM = b.ETA_NUM
								AND	c.RHA_NUM = b.RHA_NUM
						WHERE b.SOR_MOD = '9' AND c.NIR_ANO_17 NOT IN ('&cle_inc1.', '&cle_inc2.')
							AND NIR_RET = '0' AND NAI_RET = '0' AND SEX_RET = '0' AND SEJ_RET = '0' AND FHO_RET = '0' 
							AND PMS_RET = '0' AND DAT_RET = '0' %if &an. > 12 %then %do; AND COH_NAI_RET = '0' 
							AND COH_SEX_RET = '0' %end;
						);

			DISCONNECT FROM ORACLE;

		quit;

		data deces_PMSI_SSR_&an. (rename = (EXE_SOI_DTD2 = EXE_SOI_DTD BEN_DCD_DTE2 = BEN_DCD_DTE));
			set deces_PMSI_SSR_&an.;
			length EXE_SOI_DTD2 BEN_DCD_DTE2 4.;
			EXE_SOI_DTD2 = datepart(EXE_SOI_DTD);
			BEN_DCD_DTE2 = datepart(BEN_DCD_DTE);
			format EXE_SOI_DTD2 BEN_DCD_DTE2 ddmmyy10.;
			drop EXE_SOI_DTD BEN_DCD_DTE;
		run;

	%end;

	* On concat�ne toutes les tables;
	data deces_PMSI;
		set deces_PMSI_:;
	run;

	* On supprime les tables temporaires;
	proc datasets library = work memtype = data nolist;
		delete deces_PMSI_:;
	run; quit;

%mend deces_PMSI;

%deces_PMSI(
	annee_deb = &annee_2N.,
	annee_fin = &annee_N1.
	);

*	******************************************************************************************************************************************;
*	On r�cup�re l information du d�c�s dans le DCIR;

%macro deces_DCIR(annee_deb=, annee_fin=);

	%let annee_fin_flx = %sysevalf(&annee_fin. + 1);

	* On boucle sur les ann�es de rep�rage (en flux);
	%do annee = &annee_deb. %to &annee_fin_flx.;

		* On boucle sur les 12 mois de l ann�e;
		%do i = 1 %to 12;

			%if &i. < 10 %then %let Mois = 0&i.;
			%else %let Mois = &i.;

			%put Donn�es de &Mois./&Annee.;

			proc sql;

				%connectora;

					CREATE TABLE deces_DCIR_&annee._&mois. AS SELECT * FROM CONNECTION TO ORACLE (
							SELECT DISTINCT
								a.BEN_IDT_ANO,
								prs.EXE_SOI_DTD,
								prs.BEN_DCD_DTE
							FROM corresp_id_patient a
								INNER JOIN ER_PRS_F prs
								ON a.BEN_NIR_PSA = prs.BEN_NIR_PSA
							WHERE BEN_DCD_DTE > TO_DATE('16000101', 'YYYYMMDD')
								AND prs.FLX_DIS_DTD = TO_DATE(%str(%'&Annee.&Mois.01%'), 'YYYYMMDD')
							);

				disconnect from oracle;

			quit;

			data deces_DCIR_&annee._&mois. (rename = (EXE_SOI_DTD2 = EXE_SOI_DTD BEN_DCD_DTE2 = BEN_DCD_DTE));
				set deces_DCIR_&annee._&mois.;
				length EXE_SOI_DTD2 BEN_DCD_DTE2 4.;
				EXE_SOI_DTD2 = datepart(EXE_SOI_DTD);
				BEN_DCD_DTE2 = datepart(BEN_DCD_DTE);
				format EXE_SOI_DTD2 BEN_DCD_DTE2 ddmmyy10.;
				drop EXE_SOI_DTD BEN_DCD_DTE;
			run;

			%arret_erreur;

			* On empile toutes les tables mensuelles;
			%if %sysfunc(exist(deces_DCIR)) = 1 %then 
				%do;

					proc append base = deces_DCIR data = deces_DCIR_&annee._&mois.;
					run;

					%arret_erreur;

					proc delete data = deces_DCIR_&annee._&mois.;
					run; quit;

				%end;

			%if %sysfunc(exist(deces_DCIR)) = 0 %then 
				%do;

					data deces_DCIR;
						set deces_DCIR_&annee._&mois.;
					run;

					%arret_erreur;

					proc delete data = deces_DCIR_&annee._&mois.;
					run; quit;

				%end;

		%end;
		* Fin de la boucle sur les 12 mois;

	%end;
	* Fin de la boucle par ann�e;

	%arret_erreur;

%mend deces_DCIR;

%deces_DCIR(
	annee_deb = &annee_2N.,
	annee_fin = &annee_N.
	);

*	******************************************************************************************************************************************;
*	On concat�ne toutes les donn�es et on conserve l information de mani�re unique;

data travail.histo_deces;
	set deces_IR_BEN_R deces_PMSI deces_DCIR;
run;

proc delete data = deces_IR_BEN_R deces_PMSI deces_DCIR;
run; quit;

proc sort data = travail.histo_deces nodupkey;
	by _all_;
run;

*	******************************************************************************************************************************************;
*	On r�cup�re la date de d�c�s minimale;

proc sql;

		CREATE TABLE date_deces AS
			SELECT
				BEN_IDT_ANO,
				MIN(BEN_DCD_DTE) AS BEN_DCD_DTE length = 4 format date9.
			FROM travail.histo_deces
			GROUP BY BEN_IDT_ANO;

quit;

*	******************************************************************************************************************************************;
*	On ajoute l info du d�c�s dans la table de patients;

proc sort data = pop.T_INDI_BPCO_&an_N. force;
	by BEN_IDT_ANO;
run;

proc sort data = date_deces force;
	by BEN_IDT_ANO;
run;

data pop.T_INDI_BPCO_&an_N.;
	merge	pop.T_INDI_BPCO_&an_N. (in = a drop = BEN_DCD_DTE)
			date_deces (in = b);
	by BEN_IDT_ANO;
	if a;
	format BEN_DCD_DTE date9.;
run;

*	******************************************************************************************************************************************;
*	On exclut les patients qui sont d�c�d�s avant l ann�e;

proc sql;

	ALTER TABLE pop.T_INDI_BPCO_&an_N. ADD exclus_deces VARCHAR(1);

	UPDATE pop.T_INDI_BPCO_&an_N. SET
		exclus_deces = CASE	WHEN YEAR(BEN_DCD_DTE) < &annee_N. AND BEN_DCD_DTE IS NOT NULL THEN "1"
							ELSE "0"
							END;

quit;

*	******************************************************************************************************************************************;
*	On cr�e une table de correspondance pour les patients qu on a conserv�s;

* On supprime la table si elle existe;
%suppr_table(
	lib = orauser, 
	table = corresp_id_patient
	);

data corresp_id_patient;
	set pop.T_INDI_BPCO_&an_N. (where = (exclus_jumeaux = "0" and exclus_deces = "0" and exclus_pas_40ans = "0" and exclus_pas_remb = "0"
		and exclus_NIR_fictif = "0"));
run;

proc sort data = corresp_id_patient out = orauser.corresp_id_patient (keep = BEN_IDT_ANO BEN_NIR_PSA) nodupkey;
	by BEN_IDT_ANO BEN_NIR_PSA;
run;

*	******************************************************************************************************************************************;
*	On supprime les tables temporaires;

proc datasets library = work memtype = data nolist;
	delete date_deces corresp_id_patient;
run; quit;

*	******************************************************************************************************************************************;
*	V�rifications;

*	Variable exclus_pas_remb ;
%proc_freq(
	in_tbl = pop.T_INDI_BPCO_&an_N., 
	out_tbl = _pop_exclus_DC, 
	list_var_in = exclus_deces, 
	list_var_out = exclus_deces Frequency
	);

*	Variable ann�e de BEN_DCD_DTE ;

data annee_deces;
	set pop.T_INDI_BPCO_&an_N. (keep = BEN_IDT_ANO BEN_DCD_DTE where = (BEN_DCD_DTE ne .));
	annee_deces = year(BEN_DCD_DTE);
run;

%proc_freq(
	in_tbl = annee_deces, 
	out_tbl = _pop_annee_DC, 
	list_var_in = annee_deces, 
	list_var_out = annee_deces Frequency
	);

proc delete data = annee_deces;
run; quit;
