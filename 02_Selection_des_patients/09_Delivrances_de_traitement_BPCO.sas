/*	******************************************************************************************************************************************	*/
/*																																				*/
/*		S�lection des patients - Nombre de d�livrances rembours�es de traitements BPCO : BDLA, ATB et arr�t du tabac							*/
/*																																				*/
/*	******************************************************************************************************************************************	*/

*	******************************************************************************************************************************************;
*	BDLA;
*	******************************************************************************************************************************************;

*	On r�cup�re les d�livrances rembours�es de bronchodilatateur anticholinergique ou b�ta-2 longue dur�e d action;

%suppr_table(
	lib = orauser, 
	table = codes_ATC_BDLA
	);

data orauser.codes_ATC_BDLA;
	set orauser.codes_ATC_BPCO (where = (reperage in (45, 46, 47, 48, 49)));
run;

* On appelle la macro pour le rep�rage dans le DCIR - CIP;
%suppr_table(
	lib = travail, 
	table = reperage_BDLA_&Annee_N.
	);

* Dans le DCIR - Codes CIP;
%extract_CIP(
	annee_deb = &Annee_N., 
	annee_fin = &Annee_N., 
	tbl_out = travail.reperage_BDLA_&Annee_N., 
	tbl_codes = codes_ATC_BDLA, 
	tbl_patients = corresp_id_patient
	);

*	******************************************************************************************************************************************;
*	V�rifications;

data verif;
	set travail.reperage_BDLA_&Annee_N.;
	annee_soin = year(date_debut);
run;

%proc_freq(
	in_tbl = verif, 
	out_tbl = _trt_BPCO_BDLA, 
	list_var_in = annee_soin*PHA_ATC_C07*PHA_CND_TOP, 
	list_var_out = annee_soin PHA_ATC_C07 PHA_CND_TOP Frequency
	);

proc delete data = verif;
run; quit;

*	******************************************************************************************************************************************;
*	On compte le nombre de d�livrances diff�rentes par patient;

proc sql undo_policy = none;

	CREATE TABLE travail.reperage_BDLA_&Annee_N. AS
		SELECT
			BEN_IDT_ANO,
			COUNT(DISTINCT date_debut) AS Nb_Broncho_LDA length = 3
		FROM travail.reperage_BDLA_&Annee_N.
		WHERE YEAR(date_debut) = &annee_N.
		GROUP BY BEN_IDT_ANO;

quit;

*	******************************************************************************************************************************************;
*	On ajoute l information du nombre de d�livrances rembours�es de bronchodilatateur anticholinergique ou b�ta-2 longue dur�e d action dans 
	l ann�e dans la table de population;

proc sort data = pop.T_INDI_BPCO_&an_N.;
	by BEN_IDT_ANO;
run;

data pop.T_INDI_BPCO_&an_N.;
	merge	pop.T_INDI_BPCO_&an_N. (in = a )
			travail.reperage_BDLA_&Annee_N. (in = b);
	by BEN_IDT_ANO;
	if a;
	if not b then
		Nb_Broncho_LDA = 0;
run;

proc delete data = travail.reperage_BDLA_&Annee_N.;
run; quit;

proc delete data = orauser.codes_ATC_BDLA;
run; quit;

*	******************************************************************************************************************************************;
*	ATB;
*	******************************************************************************************************************************************;

*	On r�cup�re les d�livrances rembours�es d antibiotiques;

%suppr_table(
	lib = orauser, 
	table = codes_ATC_ATB
	);

data orauser.codes_ATC_ATB;
	set orauser.codes_ATC_BPCO (where = (reperage = 36));
run;

* On appelle la macro pour le rep�rage dans le DCIR - CIP;
%suppr_table(
	lib = travail, 
	table = reperage_ATC_ATB
	);

%extract_CIP(
	annee_deb = &Annee_N.,
	annee_fin = &Annee_N., 
	tbl_out = travail.reperage_ATC_ATB, 
	tbl_codes = codes_ATC_ATB,
	tbl_patients = corresp_id_patient
	);

*	******************************************************************************************************************************************;
*	V�rifications;

data verif;
	set travail.reperage_ATC_ATB;
	annee_soin = year(date_debut);
run;

%proc_freq(
	in_tbl = verif, 
	out_tbl = _trt_BPCO_ATB, 
	list_var_in = annee_soin*PHA_ATC_C07*PHA_CND_TOP, 
	list_var_out = annee_soin PHA_ATC_C07 PHA_CND_TOP Frequency
	);

proc delete data = verif;
run; quit;

*	******************************************************************************************************************************************;
*	On compte le nombre de d�livrances diff�rentes par patient;

proc sql undo_policy = none;

	CREATE TABLE travail.reperage_ATC_ATB AS
		SELECT
			BEN_IDT_ANO,
			COUNT(DISTINCT date_debut) AS Nb_ATB length = 3
		FROM travail.reperage_ATC_ATB
		WHERE YEAR(date_debut) = &annee_N.
		GROUP BY BEN_IDT_ANO;

quit;

*	******************************************************************************************************************************************;
*	On ajoute l information du nombre de d�livrances rembours�es d antibiotiques dans l ann�e dans la table de population;

data pop.T_INDI_BPCO_&an_N.;
	merge	pop.T_INDI_BPCO_&an_N. (in = a)
			travail.reperage_ATC_ATB (in = b);
	by BEN_IDT_ANO;
	if a;
	if not b then
		Nb_ATB = 0;
run;

proc delete data = travail.reperage_ATC_ATB;
run; quit;

proc delete data = orauser.codes_ATC_ATB;
run; quit;

*	******************************************************************************************************************************************;
*	Tabac;
*	******************************************************************************************************************************************;

*	On r�cup�re les d�livrances rembours�es de m�dicaments utilis�s dans la d�pendance tabagique;

%suppr_table(
	lib = orauser, 
	table = codes_ATC_Tabac
	);

data orauser.codes_ATC_Tabac;
	set orauser.codes_ATC_BPCO (where = (reperage = 35));
run;

* On appelle la macro pour le rep�rage dans le DCIR - CIP;
%suppr_table(
	lib = travail, 
	table = reperage_CIP_sdv_Tabac
	);

%extract_CIP(
	annee_deb = &Annee_N.,
	annee_fin = &Annee_N., 
	tbl_out = travail.reperage_ATC_Tabac, 
	tbl_codes = codes_ATC_Tabac,
	tbl_patients = corresp_id_patient
	);

*	******************************************************************************************************************************************;
*	V�rifications;

data verif;
	set travail.reperage_ATC_Tabac;
	annee_soin = year(date_debut);
run;

%proc_freq(
	in_tbl = verif, 
	out_tbl = _trt_BPCO_Tabac, 
	list_var_in = annee_soin*PHA_ATC_C07*PHA_CND_TOP, 
	list_var_out = annee_soin PHA_ATC_C07 PHA_CND_TOP Frequency
	);

proc delete data = verif;
run; quit;

*	******************************************************************************************************************************************;
*	On compte le nombre de d�livrances diff�rentes par patient;

proc sql undo_policy = none;

	CREATE TABLE travail.reperage_ATC_Tabac AS
		SELECT
			BEN_IDT_ANO,
			COUNT(DISTINCT date_debut) AS Nb_Tabac length = 3
		FROM travail.reperage_ATC_Tabac
		WHERE YEAR(date_debut) = &annee_N.
		GROUP BY BEN_IDT_ANO;

quit;

*	******************************************************************************************************************************************;
*	On ajoute l information du nombre de d�livrances rembours�es de m�dicaments utilis�s dans la d�pendance tabagique dans l ann�e dans la 
	table de population;

data pop.T_INDI_BPCO_&an_N.;
	merge	pop.T_INDI_BPCO_&an_N. (in = a)
			travail.reperage_ATC_Tabac (in = b);
	by BEN_IDT_ANO;
	if a;
	if not b then
		Nb_Tabac = 0;
run;

proc delete data = travail.reperage_ATC_Tabac;
run; quit;

proc delete data = orauser.codes_ATC_Tabac;
run; quit;

*	******************************************************************************************************************************************;
*	V�rifications;

%proc_freq(
	in_tbl = pop.T_INDI_BPCO_&an_N., 
	out_tbl = _Nb_BRONCHO_LDA, 
	list_var_in = Nb_BRONCHO_LDA, 
	list_var_out = Nb_BRONCHO_LDA Frequency
	);

%proc_freq(
	in_tbl = pop.T_INDI_BPCO_&an_N., 
	out_tbl = _NB_ATB, 
	list_var_in = NB_ATB, 
	list_var_out = NB_ATB Frequency
	);

%proc_freq(
	in_tbl = pop.T_INDI_BPCO_&an_N., 
	out_tbl = _NB_TABAC, 
	list_var_in = NB_TABAC, 
	list_var_out = NB_TABAC Frequency
	);
