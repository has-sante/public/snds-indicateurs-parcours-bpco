/*	******************************************************************************************************************************************	*/
/*																																				*/
/*		S�lection des patients - ALD BPCO active dans l ann�e																					*/
/*																																				*/
/*	******************************************************************************************************************************************	*/

*	******************************************************************************************************************************************;
*	On r�cup�re les patients avec une ALD BPCO (J44*) active dans l ann�e;

proc sql undo_policy = none;

	%connectora;

		CREATE TABLE ALD_BPCO_&annee_N. AS
			SELECT DISTINCT * FROM CONNECTION TO ORACLE (
				SELECT DISTINCT
					a.BEN_IDT_ANO,
					b.IMB_ETM_NAT,
					b.IMB_ALD_DTD,
					b.IMB_ALD_DTF,
					b.INS_DTE,
					b.UPD_DTE,
					b.IMB_ALD_NUM,
					b.MED_MTF_COD,
					1 AS ALD_BPCO
				FROM corresp_id_patient a
					INNER JOIN IR_IMB_R b
						ON a.BEN_NIR_PSA = b.BEN_NIR_PSA
					INNER JOIN IR_CIM_V c
						ON SUBSTR(b.MED_MTF_COD, 1, 3) = SUBSTR(c.CIM_COD, 1, 3)
				WHERE IMB_ETM_NAT IN (41, 43, 45) AND SUBSTR(TRIM(b.MED_MTF_COD), 1, 3) = 'J44' 
					AND b.INS_DTE < TO_DATE(%str(%'&Annee_N1.0501%'), 'YYYYMMDD')
				);

	disconnect from oracle;

quit;

proc sort data = ALD_BPCO_&annee_N.;
		by BEN_IDT_ANO IMB_ETM_NAT IMB_ALD_NUM MED_MTF_COD INS_DTE UPD_DTE IMB_ALD_DTF descending IMB_ALD_DTD;
run;

data ALD_BPCO_&annee_N.;
	set ALD_BPCO_&annee_N.;
	by BEN_IDT_ANO IMB_ETM_NAT IMB_ALD_NUM MED_MTF_COD INS_DTE UPD_DTE IMB_ALD_DTF descending IMB_ALD_DTD;
	if last.MED_MTF_COD then
		output;
run;

data ALD_BPCO_&annee_N. (rename = (IMB_ALD_DTD2 = IMB_ALD_DTD IMB_ALD_DTF2 = IMB_ALD_DTF));
	set ALD_BPCO_&annee_N.;
	length IMB_ALD_DTD2 IMB_ALD_DTF2 4. ALD_BPCO_&an_N. 3.;
	IMB_ALD_DTD2 = datepart(IMB_ALD_DTD);
	IMB_ALD_DTF2 = datepart(IMB_ALD_DTF);
	ALD_BPCO_&an_N. = ALD_BPCO;
	format IMB_ALD_DTD2 IMB_ALD_DTF2 ddmmyy10.;
	drop IMB_ALD_DTD IMB_ALD_DTF ALD_BPCO;
	if IMB_ALD_DTD2 <= "31DEC&Annee_N."d and (IMB_ALD_DTF2 = "01JAN1600"d or IMB_ALD_DTF2 >= "01JAN&Annee_N."d)
		then output;
run;

*	******************************************************************************************************************************************;
*	On ajoute l information de l ALD BPCO dans la table de population;

proc sql;

	ALTER TABLE pop.T_INDI_BPCO_&an_N. ADD ALD_BPCO_&an_N. INT length = 3;

	UPDATE pop.T_INDI_BPCO_&an_N. a
		SET ALD_BPCO_&an_N. = CASE	WHEN BEN_IDT_ANO IN (SELECT BEN_IDT_ANO FROM ALD_BPCO_&annee_N.) THEN 1
									ELSE 0
									END;

quit;

*	******************************************************************************************************************************************;
*	Patients avec une ALD BPCO (J44*) active au 1er septembre;

data ALD_BPCO_septembre;
	set ALD_BPCO_&annee_N.;
	where IMB_ALD_DTD <= "01SEP&annee_N."d <= IMB_ALD_DTF or IMB_ALD_DTD <= "01SEP&annee_N."d and IMB_ALD_DTF = "01JAN1600"d;
run;


proc sql;

	ALTER TABLE pop.T_INDI_BPCO_&an_N. ADD ALD_BPCO_septembre INT length = 3;

	UPDATE pop.T_INDI_BPCO_&an_N. a
		SET ALD_BPCO_septembre = CASE	WHEN BEN_IDT_ANO IN (SELECT BEN_IDT_ANO FROM ALD_BPCO_septembre) THEN 1
										ELSE 0
										END;

quit;

*	******************************************************************************************************************************************;
*	Patients avec une ALD BPCO (J44*) active au 31 d�cembre;

data ALD_BPCO_decembre;
	set ALD_BPCO_&annee_N.;
	where (IMB_ALD_DTD <= "31DEC&annee_N."d <= IMB_ALD_DTF) or (IMB_ALD_DTD <= "31DEC&annee_N."d and IMB_ALD_DTF = "01JAN1600"d);
run;


proc sql;

	ALTER TABLE pop.T_INDI_BPCO_&an_N. ADD ALD_BPCO_decembre INT length = 3;

	UPDATE pop.T_INDI_BPCO_&an_N. a
		SET ALD_BPCO_decembre = CASE	WHEN BEN_IDT_ANO IN (SELECT BEN_IDT_ANO FROM ALD_BPCO_decembre) THEN 1
										ELSE 0
										END;

quit;

*	******************************************************************************************************************************************;
*	V�rifications;

%proc_freq(
	in_tbl = pop.T_INDI_BPCO_&an_N., 
	out_tbl = _ALD_BPCO, 
	list_var_in = ALD_BPCO_&an_N.*ALD_BPCO_septembre*ALD_BPCO_decembre, 
	list_var_out = ALD_BPCO_&an_N. ALD_BPCO_septembre ALD_BPCO_decembre Frequency
	);

*	******************************************************************************************************************************************;
*	On supprime les tables temporaires;

proc datasets library = work memtype = data nolist;
	delete ALD_BPCO_&annee_N. ALD_BPCO_septembre ALD_BPCO_decembre;
run; quit;
