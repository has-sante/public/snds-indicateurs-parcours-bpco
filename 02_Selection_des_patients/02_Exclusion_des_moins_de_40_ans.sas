/*	******************************************************************************************************************************************	*/
/*																																				*/
/*		S�lection des patients - Exclusion de patients de moins de 40 ans																		*/
/*																																				*/
/*	******************************************************************************************************************************************	*/

*	******************************************************************************************************************************************;
*	On ajout l �ge l ann�e N :
			si ann�e et mois inconnus, ou ann�e de naissance avant 1900 ou apr�s l ann�e de traitement : �ge manquant
			si mois inconnu, on force au milieu d ann�e : juin
			si ann�e et mois connus, on force le jour au milieu du mois = 15;

data pop.T_INDI_BPCO_&an_N.;
	set pop.T_INDI_BPCO_&an_N.;
	length age 3.;
	* �ge;
	if 1900 <= input(BEN_NAI_ANN, 4.) <= year(today()) then
		do;
			if 1 <= input(BEN_NAI_MOI, 2.) <= 12 then
				Age = int(yrdif(mdy(input(BEN_NAI_MOI, 2.), 15, input(BEN_NAI_ANN, 4.)), "01JAN&annee_N."d, 'ACTUAL'));
			else
				Age = int(yrdif(mdy(06, 15, input(BEN_NAI_ANN, 4.)), "01JAN&annee_N."d, 'ACTUAL'));
		end;
	else Age = .;
run;

*	******************************************************************************************************************************************;
*	On exclut les patients qui n ont pas 40 ans;

proc sql;

	ALTER TABLE pop.T_INDI_BPCO_&an_N. ADD exclus_pas_40ans VARCHAR(1);

	UPDATE pop.T_INDI_BPCO_&an_N. SET
		exclus_pas_40ans = CASE	WHEN Age < 40 AND Age NE . THEN "1"
								ELSE "0"
								END;

quit;

*	******************************************************************************************************************************************;
*	V�rifications;

*	Variables AGE * EXCLUS_PAS_40ans ;
%proc_freq(
	in_tbl = pop.T_INDI_BPCO_&an_N., 
	out_tbl = _pop_AGE_EXCLUS, 
	list_var_in = AGE*EXCLUS_PAS_40ans, 
	list_var_out = AGE EXCLUS_PAS_40ans Frequency
	);
