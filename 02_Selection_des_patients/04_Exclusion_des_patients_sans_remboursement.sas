/*	******************************************************************************************************************************************	*/
/*																																				*/
/*		S�lection des patients - On supprime les patients n'ayant pas b�n�fici� de soin au cours de l'ann�e (table ER_PRS_F)					*/
/*																																				*/
/*	******************************************************************************************************************************************	*/

*	******************************************************************************************************************************************;
*	On r�cup�re les patients avec au moins une ligne dans ER_PRS_F au cours de l ann�e;

%macro soins_annee(annee_deb=, annee_fin=);

	%let annee_fin_flx = %sysevalf(&annee_fin. + 1);

	* On boucle sur les ann�es de rep�rage (en flux);
	%do Annee = &annee_deb. %to &annee_fin_flx.;

		* On boucle sur les 12 mois de l ann�e;
		%do i = 1 %to 12;

			%if &i. < 10 %then %let Mois = 0&i.;
			%else %let Mois = &i.;

			%put Donn�es de &Mois./&Annee.;

			proc sql;

				%connectora;

					CREATE TABLE soins_&annee._&mois. AS SELECT * FROM CONNECTION TO ORACLE (
							SELECT DISTINCT
								a.BEN_IDT_ANO,
								prs.EXE_SOI_DTD,
								prs.FLX_DIS_DTD
							FROM corresp_id_patient a
								INNER JOIN ER_PRS_F prs
									ON a.BEN_NIR_PSA = prs.BEN_NIR_PSA
							WHERE prs.FLX_DIS_DTD = TO_DATE(%str(%'&Annee.&Mois.01%'), 'YYYYMMDD')
								AND prs.EXE_SOI_DTD BETWEEN TO_DATE(%str(%'&Annee_deb.0101%'), 'YYYYMMDD') AND TO_DATE(%str(%'&Annee_fin.1231%'), 'YYYYMMDD')
							);

				disconnect from oracle;

			quit;

			%arret_erreur;

			* On empile toutes les tables mensuelles;
			%if %sysfunc(exist(patients_soins_&annee_N.)) = 1 %then 
				%do;

					proc append base = patients_soins_&annee_N. data = soins_&annee._&mois.;
					run;

					%arret_erreur;

					proc delete data = soins_&annee._&mois.;
					run; quit;

				%end;

			%if %sysfunc(exist(patients_soins_&annee_N.)) = 0 %then 
				%do;

					data patients_soins_&annee_N.;
						set soins_&annee._&mois.;
					run;

					%arret_erreur;

					proc delete data = soins_&annee._&mois.;
					run; quit;

				%end;

		%end;
		* Fin de la boucle sur les 12 mois;

	%end;
	* Fin de la boucle par ann�e;

	%arret_erreur;

%mend soins_annee;

%soins_annee(
	annee_deb = &annee_N., 
	annee_fin = &annee_N.
	);

*	******************************************************************************************************************************************;
*	V�rifications;

*	Variable Ann�e de la Date de soins;

data verif_date_soins;
	set patients_soins_&annee_N.;
	annee_soins = year(datepart(EXE_SOI_DTD));
	annee_flux = year(datepart(FLX_DIS_DTD));
run;

%proc_freq(
	in_tbl = verif_date_soins, 
	out_tbl = _annee_soins, 
	list_var_in = annee_soins, 
	list_var_out = annee_soins Frequency
	);

%proc_freq(
	in_tbl = verif_date_soins, 
	out_tbl = _annee_flux, 
	list_var_in = annee_flux, 
	list_var_out = annee_flux Frequency
	);

proc delete data = verif_date_soins;
run; quit;

*	******************************************************************************************************************************************;
*	On exclut les patients qui n ont pas de soins dans l ann�e;

* On les exclut de notre r�f�rentiel;

proc sort data = patients_soins_&annee_N. nodupkey;
  by BEN_IDT_ANO;
run;

proc sql;

	CREATE INDEX BEN_IDT_ANO ON patients_soins_&annee_N. (BEN_IDT_ANO);

	ALTER TABLE pop.T_INDI_BPCO_&an_N. ADD exclus_pas_remb VARCHAR(1);

	UPDATE pop.T_INDI_BPCO_&an_N. SET
		exclus_pas_remb = CASE	WHEN BEN_IDT_ANO NOT IN (SELECT BEN_IDT_ANO FROM patients_soins_&annee_N.) THEN "1"
								ELSE "0"
								END;

quit;

*	******************************************************************************************************************************************;
*	On supprime les tables temporaires;

proc delete data = patients_soins_&annee_N.;
run; quit;

*	******************************************************************************************************************************************;
*	V�rifications;

*	Variable exclus_pas_remb ;
%proc_freq(
	in_tbl = pop.T_INDI_BPCO_&an_N., 
	out_tbl = _pop_exclus_pas_remb, 
	list_var_in = exclus_pas_remb, 
	list_var_out = exclus_pas_remb Frequency
	);
