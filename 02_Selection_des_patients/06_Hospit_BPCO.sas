/*	******************************************************************************************************************************************	*/
/*																																				*/
/*		S�lection des patients - Hospitalisation pour BPCO en MCO, SSR ou HAD dans l'ann�e														*/
/*																																				*/
/*	******************************************************************************************************************************************	*/

*	******************************************************************************************************************************************;
*	On r�cup�re les s�jours pour BPCO en MCO, SSR et HAD;

%suppr_table(
	lib = orauser, 
	table = hospit_CIM_BPCO
	);

data orauser.hospit_CIM_BPCO;
	set orauser.diag_codes_CIM_BPCO (where = (reperage = 16));
run;

* On appelle la macro pour le rep�rage dans le PMSI MCO, HAD et SSR;
%suppr_table(
	lib = travail, 
	table = reperage_hospit_BPCO_&annee_4N._&annee_N1.
	);

%extract_CIM10_PMSI(
	annee_deb = &annee_4N.,
	annee_fin = &annee_N1.,
	HAD_DP = 1,
	HAD_DAS = 1, 
	HAD_MPP = 1, 
	HAD_MPA = 1, 
	MCO_DP = 1, 
	MCO_DR = 1, 
	MCO_DAS = 1, 
	MCO_DP_UM = 1, 
	MCO_DR_UM = 1,
	SSR_FP = 1, 
	SSR_MPP = 1, 
	SSR_AE = 1, 
	SSR_DAS = 1, 
	tbl_out = travail.reperage_hospit_BPCO_&annee_4N._&annee_N1., 
	tbl_codes = hospit_CIM_BPCO,
	tbl_patients = corresp_id_patient
	);

data travail.reperage_hospit_BPCO_&annee_4N._&annee_N1.;
	set travail.reperage_hospit_BPCO_&annee_4N._&annee_N1.;
	format id_sejour $30.;
	if domaine = "HAD" then
		id_sejour = ETA_NUM_EPMSI||"_"||RHAD_NUM||"_"||put(annee, 4.);
	if domaine = "MCO" then
		id_sejour = ETA_NUM||"_"||RSA_NUM||"_"||put(annee, 4.);
	if domaine = "SSR" then
		id_sejour = ETA_NUM||"_"||RHA_NUM||"_"||put(annee, 4.);
	if domaine ne "MCO" and date_fin = . then
		date_fin = mdy(12, 31, annee);
run;

*	******************************************************************************************************************************************;
*	V�rifications;

data verifs;
	set travail.reperage_hospit_BPCO_&annee_4N._&annee_N1. (keep = id_sejour annee date_debut date_fin CODE_CIM domaine table variable type);
	annee_debut = year(date_debut);
	annee_fin = year(date_fin);
run;

%proc_freq(
	in_tbl = verifs, 
	out_tbl = _hospit_BPCO_debut, 
	list_var_in = annee_debut, 
	list_var_out = annee_debut Frequency
	);

%proc_freq(
	in_tbl = verifs, 
	out_tbl = _hospit_BPCO_fin, 
	list_var_in = annee_fin, 
	list_var_out = annee_fin Frequency
	);

%proc_freq(
	in_tbl = verifs, 
	out_tbl = _hospit_BPCO_CIM, 
	list_var_in = CODE_CIM, 
	list_var_out = CODE_CIM Frequency
	);

%proc_freq(
	in_tbl = verifs, 
	out_tbl = _hospit_BPCO, 
	list_var_in = Annee*Domaine*Table*Variable*Type, 
	list_var_out = Annee Domaine Table2 Variable Type Frequency
	);

proc delete data = verifs;
run; quit;

*	******************************************************************************************************************************************;
*	On compte le nombre de s�jours dans l ann�e;

proc sql undo_policy = none;

	CREATE TABLE nb_hospit_BPCO_&annee_N. AS
		SELECT
			BEN_IDT_ANO,
			COUNT(DISTINCT id_sejour) AS Nb_hospit_BPCO length = 3
		FROM travail.reperage_hospit_BPCO_&annee_4N._&annee_N1.
		WHERE annee = &annee_N.
		GROUP BY BEN_IDT_ANO;

quit;

*	******************************************************************************************************************************************;
*	On ajoute l information du nombre d hospitalisations dans l ann�e dans la table de population;

data pop.T_INDI_BPCO_&an_N.;
	merge	pop.T_INDI_BPCO_&an_N. (in = a)
			nb_hospit_BPCO_&annee_N. (in = b);
	by BEN_IDT_ANO;
	if a;
	if not b then
		Nb_hospit_BPCO = 0;
run;

*	******************************************************************************************************************************************;
*	On supprime les tables temporaires;

proc delete data = nb_hospit_BPCO_&annee_N.;
run; quit;

proc delete data = orauser.hospit_CIM_BPCO;
run; quit;

*	******************************************************************************************************************************************;
*	On r�cup�re les patients hospitalis�s pour BPCO en MCO, SSR et HAD entre le 1er janvier de l ann�e N-1 et le 31 ao�t de l ann�e N;

*	On compte le nombre de s�jours entre l ann�e N-1 et l ann�e N;
proc sql undo_policy = none;

	CREATE TABLE nb_hospit_BPCO_ind02 AS
		SELECT
			BEN_IDT_ANO,
			COUNT(DISTINCT id_sejour) AS Nb_hospit_BPCO_Ind02 length = 3
		FROM travail.reperage_hospit_BPCO_&annee_4N._&annee_N1.
		WHERE (domaine = "MCO" AND "01JAN&Annee_1N."d <= date_fin <= "31AUG&Annee_N."d) OR (domaine IN ("HAD", "SSR") AND 
			(date_debut <= "31AUG&Annee_N."d AND "01JAN&Annee_1N."d <= date_fin))
		GROUP BY BEN_IDT_ANO;

quit;

*	On ajoute l information du nombre d hospitalisations dans l ann�e dans la table de population;
data pop.T_INDI_BPCO_&an_N.;
	merge	pop.T_INDI_BPCO_&an_N. (in = a)
			nb_hospit_BPCO_ind02 (in = b);
	by BEN_IDT_ANO;
	if a;
	if not b then
		Nb_hospit_BPCO_Ind02 = 0;
run;

*	******************************************************************************************************************************************;
*	On supprime les tables temporaires;

proc delete data = nb_hospit_BPCO_ind02;
run; quit;
