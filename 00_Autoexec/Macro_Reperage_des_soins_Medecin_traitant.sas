/*	******************************************************************************************************************************************	*/
/*																																				*/
/*		Macro pour rep�rer les remboursement li�s � une consultation chez le m�decin traitant : extract_MT										*/
/*																																				*/
/*	******************************************************************************************************************************************	*/
/*																																				*/
/*		Arguments en entr�e :																													*/
/*																																				*/
/*			- annee_deb = la premi�re ann�e de soins (ie. filtre sur EXE_SOI_DTD) pour laquelle on souhaite rep�rer les codes					*/
/*																																				*/
/*			- annee_fin = la derni�re ann�e de soins (ie. filtre sur EXE_SOI_DTD) pour laquelle on souhaite rep�rer les codes					*/
/*																																				*/
/*			- tbl_out = le nom de la table en sortie (peut contenir un nom de librairie)														*/
/*																																				*/
/*			- tbl_patients = le nom de la table contenant la correspondance BEN_IDT_ANO <-> BEN_NR_PSA (au format Oracle)						*/
/*																																				*/
/*	******************************************************************************************************************************************	*/
/*																																				*/
/*		Tables en sortie : &tbl_out. (renseign�e en param�tres de la macro)																		*/
/*		filtr�e sur les soins de ville uniquement, les NIR  normaux  + suppression des lignes pour information									*/
/*																																				*/
/*	******************************************************************************************************************************************	*/

%macro extract_MT(annee_deb=, annee_fin=, tbl_out=, tbl_patients=);

	%let annee_fin_flx = %sysevalf(&annee_fin. + 1);

	* On boucle sur les ann�es de rep�rage (en flux);
	%do Annee = &annee_deb. %to &annee_fin_flx.;

		* On boucle sur les 12 mois de l ann�e;
		%do i = 1 %to 12;

			%if &i. < 10 %then %let Mois = 0&i.;
			%else %let Mois = &i.;

			%put Donn�es de &Mois./&Annee.;

			proc sql;

				%connectora;

					CREATE TABLE tmp_extract_MT_&annee._&mois. AS SELECT * FROM CONNECTION TO ORACLE (
							SELECT
								pop.BEN_IDT_ANO,
								prs.EXE_SOI_DTD,
								prs.EXE_SOI_DTF,
								prs.BSE_PRS_NAT,
								prs.PRS_ACT_QTE,
								prs.PSE_SPE_COD,
								prs.BSE_REM_MNT,
								prs.PFS_EXE_NUM,
								prs.PRS_MTT_NUM,
								ete.ETB_EXE_FIN,
								ete.ETE_MCO_DDP,
								ete.MDT_COD,
								ete.ETE_IND_TAA,
								CASE	WHEN prs.PFS_EXE_NUM = prs.PRS_MTT_NUM THEN 1
										ELSE 0
										END AS contact_MT
							FROM &tbl_patients. pop
								INNER JOIN ER_PRS_F prs
									ON	pop.BEN_NIR_PSA = prs.BEN_NIR_PSA
								LEFT JOIN ER_ETE_F ete
									ON	ete.FLX_DIS_DTD = prs.FLX_DIS_DTD 
									AND	ete.FLX_TRT_DTD = prs.FLX_TRT_DTD 
									AND	ete.FLX_EMT_TYP = prs.FLX_EMT_TYP 
									AND	ete.FLX_EMT_NUM = prs.FLX_EMT_NUM 
									AND	ete.FLX_EMT_ORD = prs.FLX_EMT_ORD 
									AND	ete.ORG_CLE_NUM = prs.ORG_CLE_NUM 
									AND	ete.DCT_ORD_NUM = prs.DCT_ORD_NUM 
									AND	ete.PRS_ORD_NUM = prs.PRS_ORD_NUM
									AND	ete.REM_TYP_AFF = prs.REM_TYP_AFF
							WHERE  prs.BEN_CDI_NIR = '00' AND prs.CPL_MAJ_TOP IN (0, 1)
								AND (ete.ETE_IND_TAA NOT IN (1, 2) OR ete.ETE_IND_TAA IS NULL)
								AND prs.DPN_QLF NOT IN (71, 72) AND NOT(prs.DPN_QLF = 0 AND prs.PRS_DPN_QLP = 71)
								AND prs.FLX_DIS_DTD = TO_DATE(%str(%'&Annee.&Mois.01%'), 'YYYYMMDD')
								AND prs.EXE_SOI_DTD BETWEEN TO_DATE(%str(%'&Annee_deb.0101%'), 'YYYYMMDD') AND TO_DATE(%str(%'&Annee_fin.1231%'), 'YYYYMMDD')
								AND ((prs.PFS_EXE_NUM = prs.PRS_MTT_NUM AND PRS_MTT_NUM NOT IN ('A01234567', 'A', ' ', '00000000', '99999999')
								OR (PSE_SPE_COD IN (1, 22, 23))) AND prs.PRS_NAT_REF IN (SELECT code_presta FROM codes_presta_BPCO WHERE 
								reperage = 33))
							);

				disconnect from oracle;

			quit;

			%arret_erreur;

			* On empile toutes les tables mensuelles;
			%if %sysfunc(exist(&tbl_out.)) = 1 %then 
				%do;

					proc append base = &tbl_out. data = tmp_extract_MT_&annee._&mois.;
					run;

					%arret_erreur;

					proc delete data = tmp_extract_MT_&annee._&mois.;
					run; quit;

				%end;

			%if %sysfunc(exist(&tbl_out.)) = 0 %then 
				%do;

					data &tbl_out.;
						set tmp_extract_MT_&annee._&mois.;
					run;

					%arret_erreur;

					proc delete data = tmp_extract_MT_&annee._&mois.;
					run; quit;

				%end;

		%end;
		* Fin de la boucle sur les 12 mois;

	%end;
	* Fin de la boucle par ann�e;

	data &tbl_out.;
		set &tbl_out. (where = (ETE_IND_TAA not in (1, 2) and MDT_COD in (., 0, 7)));
	run;

	* On applique le programme de r�gularisations pour c�er la table finale;
	proc sql undo_policy = none;

		CREATE TABLE &tbl_out. AS
			SELECT
				BEN_IDT_ANO,
				datepart(EXE_SOI_DTD) AS date_debut length = 4 format ddmmyy10.,
				CASE	WHEN datepart(EXE_SOI_DTF) IS NULL THEN datepart(EXE_SOI_DTD)
						ELSE datepart(EXE_SOI_DTF)
						END AS date_fin length = 4 format ddmmyy10.,
				"DCIR" AS type,
				BSE_PRS_NAT,
				PSE_SPE_COD,
				ETB_EXE_FIN,
				ETE_MCO_DDP,
				contact_MT,
				SUM(BSE_REM_MNT) AS montant,
				SUM(PRS_ACT_QTE) AS quantite
			FROM &tbl_out.
			GROUP BY 1, 2, 3, 4, 5, 6, 7, 8, 9;	

			DELETE FROM &tbl_out. WHERE montant <= 0 OR quantite < 0;

	quit;

	%arret_erreur;

%mend extract_MT;
