/*	******************************************************************************************************************************************	*/
/*																																				*/
/*		Macro pour rep�rer les contacts avec un pneumologue lors d'une hospitalisation en SSR : extract_SSR_pneumo								*/
/*																																				*/
/*	******************************************************************************************************************************************	*/
/*																																				*/
/*		Arguments en entr�e :																													*/
/*																																				*/
/*			- annee_deb = la premi�re ann�e de soins (ie. ann�e des tables PMSI) pour laquelle on souhaite rep�rer les codes					*/
/*																																				*/
/*			- annee_fin = la derni�re ann�e de soins (ie. ann�e des tables PMSI) pour laquelle on souhaite rep�rer les codes					*/
/*																																				*/
/*			- tbl_out = le nom de la table en sortie (peut contenir un nom de librairie)														*/
/*																																				*/
/*			- tbl_patients = le nom de la table contenant la correspondance BEN_IDT_ANO <-> BEN_NR_PSA (au format Oracle)						*/
/*																																				*/
/*	******************************************************************************************************************************************	*/
/*																																				*/
/*		Tables en sortie : &tbl_out. (renseign�e en param�tres de la macro)																		*/
/*																																				*/
/*	******************************************************************************************************************************************	*/

*	******************************************************************************************************************************************	;
*	Macro pour rep�rer les contacts avec un pneumologue lors d'une hospitalisation en SSR														;
*	******************************************************************************************************************************************	;

%macro extract_SSR_pneumo(annee_deb=, annee_fin=, an_crea_t = 14 , an_fin_t = %eval(%sysfunc(YEAR(%sysfunc(TODAY()))) - 2000), tbl_out=, 
	tbl_patients=);

	%let an_deb_pmsi = %sysevalf(&annee_deb. - 2000);
	%let an_fin_pmsi = %sysevalf(&annee_fin. - 2000);

	%do i = %sysfunc(max(&an_deb_pmsi., &an_crea_t. )) %to %sysfunc(min(&an_fin_pmsi., &an_fin_t.));

		%if &i. < 10 %then %let an = 0&i.;
		%else %let an = &i.;

		* On d�finit les variables de jointure pour chaque domaine du PMSI;
		%let var_join1 = ETA_NUM;
		%let var_join2 = RHA_NUM;

		
		proc sql;

			%connectora;

				CREATE TABLE ext_pneumo_SSR_&an. AS 
				 	SELECT *
					FROM CONNECTION TO ORACLE (
						SELECT DISTINCT
							c.&var_join1.,
							c.&var_join2.,
							pop.BEN_IDT_ANO,
							c.EXE_SOI_DTD,
							c.EXE_SOI_DTF,
							SUBSTR(b.AUT_TYP_UM, 1, 2) AS type_UM,
							20&an. AS annee,
							'SSR' AS domaine
						FROM &tbl_patients. pop
							INNER JOIN T_SSR&an.C c
								ON	pop.BEN_NIR_PSA = c.NIR_ANO_17
							INNER JOIN T_SSR&an.B b
								ON	c.&var_join1. = b.&var_join1.
								AND	c.&var_join2. = b.&var_join2.
							INNER JOIN T_SSR&an.E e
								ON	c.&var_join1. = e.ETA_NUM
						WHERE c.NIR_ANO_17 NOT IN ('&cle_inc1.', '&cle_inc2.') AND SUBSTR(b.AUT_TYP_UM, 1, 2) = '54' 
							AND SUBSTR(TRIM(b.GRG_GME), 1, 2) = '04'
							AND c.NIR_RET = '0' AND c.NAI_RET = '0' AND c.SEX_RET = '0' AND c.SEJ_RET = '0' AND c.FHO_RET = '0' 
							AND c.PMS_RET = '0' AND c.DAT_RET = '0' AND c.COH_NAI_RET = '0' AND c.COH_SEX_RET = '0'
						);

			DISCONNECT FROM ORACLE;

		quit;

		%arret_erreur;

	%end;

	* Empilage de la table temporaire � la table R�sultat;
	data &tbl_out.;
		set ext_pneumo_SSR_:;
		length date_debut date_fin 4.;
		type = "PMSI s�jours";
		date_debut = datepart(EXE_SOI_DTD);
		date_fin = datepart(EXE_SOI_DTF);
		format date_debut date_fin ddmmyy10.;
		drop EXE_SOI_DTD EXE_SOI_DTF;
	run;

	%arret_erreur;

	* Suppression de la table temporaire;
	proc datasets library = work memtype = data nolist;
		delete ext_pneumo_SSR_:;
	run;

%mend extract_SSR_pneumo;
