/*	******************************************************************************************************************************************	*/
/*																																				*/
/*		Macro pour rep�rer les remboursement des codes diagnostics souhait�s, dans le PMSI : extract_ACT_COD_PMSI								*/
/*																																				*/
/*	******************************************************************************************************************************************	*/
/*																																				*/
/*		Arguments en entr�e :																													*/
/*																																				*/
/*			- annee_deb = la premi�re ann�e de soins (ie. ann�e des tables PMSI) pour laquelle on souhaite rep�rer les codes					*/
/*																																				*/
/*			- annee_fin = la derni�re ann�e de soins (ie. ann�e des tables PMSI) pour laquelle on souhaite rep�rer les codes					*/
/*																																				*/
/*			- tbl_out = le nom de la table en sortie (peut contenir un nom de librairie)														*/
/*																																				*/
/*			- tbl_codes = la table contenant les codes actes � rep�rer (au format Oracle)														*/
/*																																				*/
/*			- tbl_patients = le nom de la table contenant la correspondance BEN_IDT_ANO <-> BEN_NR_PSA (au format Oracle)						*/
/*																																				*/
/*			- MCO = flag (0/1) : si 1, on recherche les codes acte dans le PMSI-MCO (Par d�faut : 1)											*/
/*																																				*/
/*			- SSR = flag (0/1) : si 1, on recherche les codes acte dans le PMSI-SSR (Par d�faut : 1)											*/
/*																																				*/
/*	******************************************************************************************************************************************	*/
/*																																				*/
/*		Tables en sortie : &tbl_out. (renseign�e en param�tres de la macro)																		*/
/*		avec s�lection des codes disponibles dans le r�f�rentiel &tbl_codes., hors s�jours en erreur											*/
/*																																				*/
/*	******************************************************************************************************************************************	*/

*	******************************************************************************************************************************************	;
*	Macro pour rep�rer les codes actes dans le PMSI - s�jours																					;
*	******************************************************************************************************************************************	;

%macro ACT_COD_sejours(an_deb_pmsi=, an_fin_pmsi=, an_crea_t=, an_fin_t=, tbl_codes=, table=, domaine=, tbl_out=, tbl_patients=);

	%do i = %sysfunc(max(&an_deb_pmsi., &an_crea_t. )) %to %sysfunc(min(&an_fin_pmsi., &an_fin_t.));

		%if &i. < 10 %then %let an = 0&i.;
		%else %let an = &i.;

		* On d�finit les variables de jointure pour chaque domaine du PMSI;
		%if &domaine. = MCO %then
			%do;
				%let var_join1 = ETA_NUM;
				%let var_join2 = RSA_NUM;
			%end;
		%if &domaine. = SSR %then
			%do;
				%let var_join1 = ETA_NUM;
				%let var_join2 = RHA_NUM;
			%end;

		* On joint la table de factures avec la table des s�jours en filtrant sur les codes actes s�lectionn�s;
		%put R�cup�ration des s�jours du PMSI-&domaine. (table &table.) contenant des codes actes pr�-d�finis - Ann�e &an. ...;
		
		proc sql;

			%connectora;

				CREATE TABLE ACT_COD_&domaine._&table._&an. AS 
				 	SELECT *
					FROM CONNECTION TO ORACLE (
						SELECT DISTINCT
							c.&var_join1.,
							c.&var_join2.,
							pop.BEN_IDT_ANO,
							c.EXE_SOI_DTD,
							c.EXE_SOI_DTF,
							20&an. AS annee,
							act.ACT_COD,
							act.ACT_COE,
							%if &table. = FC and &an. >= 17 %then
								%do;
									EXE_SPE,
								%end;
							cod.*
						FROM &tbl_patients. pop
							INNER JOIN T_&domaine.&an.C c
								ON	pop.BEN_NIR_PSA = c.NIR_ANO_17
							INNER JOIN T_&domaine.&an.B b
								ON	c.&var_join1. = b.&var_join1.
								AND	c.&var_join2. = b.&var_join2.
							INNER JOIN T_&domaine.&an.E e
								ON	c.&var_join1. = e.ETA_NUM
							INNER JOIN T_&domaine.&an.&table. act
								ON	c.&var_join1. = act.&var_join1.
								AND	c.&var_join2. = act.&var_join2.
							, &tbl_codes. cod
						WHERE SUBSTR(act.ACT_COD, 1, taille) = TRIM(cod.code_acte)
							AND c.NIR_ANO_17 NOT IN ('&cle_inc1.', '&cle_inc2.')
							%if &domaine. = MCO %then
								%do;
									AND NIR_RET = '0' AND NAI_RET = '0' AND SEX_RET = '0' AND SEJ_RET = '0' AND FHO_RET = '0' 
									AND PMS_RET = '0' AND DAT_RET = '0' %if &an. > 12 %then %do; AND COH_NAI_RET = '0' 
									AND COH_SEX_RET = '0' %end;
									AND c.ETA_NUM NOT IN ('130780521', '130783236', '130783293', '130784234', '130804297', '600100101', 
										'750041543', '750100018', '750100042', '750100075', '750100083', '750100091', '750100109', 
										'750100125', '750100166', '750100208', '750100216', '750100232', '750100273', '750100299', 
										'750801441', '750803447', '750803454', '910100015', '910100023', '920100013', '920100021', 
										'920100039', '920100047', '920100054', '920100062', '930100011', '930100037', '930100045', 
										'940100027', '940100035', '940100043', '940100050', '940100068', '950100016', '690783154', 
										'690784137', '690784152', '690784178', '690787478', '830100558')
									AND GRG_GHM NOT IN ('90Z00Z') AND GRG_RET NOT IN ('024') AND GRG_GHM NOT IN ('90H01Z', '90Z00Z', 
										'90Z01Z', '90Z02Z', '90Z03Z') 
									AND ((SEJ_TYP = 'A' OR SEJ_TYP IS NULL) OR (SEJ_TYP = 'B' AND GRG_GHM NOT IN ('28Z14Z','28Z15Z','28Z16Z')))
								%end;
							%if &domaine. = SSR %then
								%do;
									AND NIR_RET = '0' AND NAI_RET = '0' AND SEX_RET = '0' AND SEJ_RET = '0' AND FHO_RET = '0' 
									AND PMS_RET = '0' AND DAT_RET = '0' %if &an. > 12 %then %do; AND COH_NAI_RET = '0' 
									AND COH_SEX_RET = '0' %end;
								%end;
						);

			DISCONNECT FROM ORACLE;

		quit;

		data ACT_COD_&domaine._&table._&an.;
			set ACT_COD_&domaine._&table._&an.;
			length date_debut date_fin 4.;
			table = "&table.";
			domaine = "&domaine.";
			type = "PMSI s�jours";
			date_debut = datepart(EXE_SOI_DTD);
			date_fin = datepart(EXE_SOI_DTF);			
			drop EXE_SOI_DTD EXE_SOI_DTF;
			format date_debut date_fin ddmmyy10.;
		run;

		%arret_erreur;

	%end;

%mend ACT_COD_sejours;

*	******************************************************************************************************************************************	;
*	Macro pour rep�rer les codes actes dans le PMSI - ACE																						;
*	******************************************************************************************************************************************	;

%macro ACT_COD_ACE(an_deb_pmsi=, an_fin_pmsi=, an_crea_t=, an_fin_t=, tbl_codes=, table=, domaine=, tbl_out=, tbl_patients=);

	%do i = %sysfunc(max(&an_deb_pmsi., &an_crea_t. )) %to %sysfunc(min(&an_fin_pmsi., &an_fin_t.));

		%if &i. < 10 %then %let an = 0&i.;
		%else %let an = &i.;

		* On d�finit les variables de jointure pour chaque domaine du PMSI;
		%let var_join1 = ETA_NUM;
		%let var_join2 = SEQ_NUM;

		* On joint la table de factures avec la table des ACE en filtrant sur les codes actes s�lectionn�s;
		%put R�cup�ration des ACE du PMSI-&domaine. (table &table.) contenant des codes actes pr�-d�finis - Ann�e &an. ...;

		proc sql;

			%connectora;

				CREATE TABLE ACT_COD_&domaine._&table._&an. AS 
				 	SELECT *
					FROM CONNECTION TO ORACLE (
						SELECT DISTINCT
							c.&var_join1.,
							c.&var_join2.,
							pop.BEN_IDT_ANO,
							c.EXE_SOI_DTD,
							c.EXE_SOI_DTF,
							20&an. AS annee,
							act.ACT_COD,
							act.ACT_COE,
							EXE_SPE,
							cod.*
						FROM &tbl_patients. pop
								INNER JOIN T_&domaine.&an.CSTC c
									ON	pop.BEN_NIR_PSA = c.NIR_ANO_17
							INNER JOIN T_&domaine.&an.&table. act
								ON	c.&var_join1. = act.&var_join1.
								AND	c.&var_join2. = act.&var_join2.
							, &tbl_codes. cod
						WHERE SUBSTR(act.ACT_COD, 1, taille) = TRIM(cod.code_acte)
							AND c.NIR_ANO_17 NOT IN ('&cle_inc1.', '&cle_inc2.')
							AND c.EXE_SOI_DTD IS NOT NULL AND c.EXE_SOI_DTF IS NOT NULL  AND PSH_MDT IN ('00', '07')
							%if &domaine. = MCO %then
								%do;
									AND NIR_RET = '0' AND NAI_RET = '0' AND SEX_RET = '0' AND IAS_RET = '0' AND ENT_DAT_RET = '0' 
									AND c.ETA_NUM NOT IN ('130780521', '130783236', '130783293', '130784234', '130804297', '600100101', 
										'750041543', '750100018', '750100042', '750100075', '750100083', '750100091', '750100109', 
										'750100125', '750100166', '750100208', '750100216', '750100232', '750100273', '750100299', 
										'750801441', '750803447', '750803454', '910100015', '910100023', '920100013', '920100021', 
										'920100039', '920100047', '920100054', '920100062', '930100011', '930100037', '930100045', 
										'940100027', '940100035', '940100043', '940100050', '940100068', '950100016', '690783154', 
										'690784137', '690784152', '690784178', '690787478', '830100558')
								%end;
							%if &tbl_codes. = actes_MG or &tbl_codes. = actes_pneumo %then
								%do; 
									AND act.PSH_MDT IN ('00', '07')
								%end;
						);

			DISCONNECT FROM ORACLE;

		quit;

		data ACT_COD_&domaine._&table._&an.;
			set ACT_COD_&domaine._&table._&an.;
			length date_debut date_fin 4.;
			table = "&table.";
			domaine = "&domaine.";
			type = "PMSI ACE";
			date_debut = datepart(EXE_SOI_DTD);
			date_fin = datepart(EXE_SOI_DTF);
			format date_debut date_fin ddmmyy10.;
			drop EXE_SOI_DTD EXE_SOI_DTF;
			if (year(date_fin) = 20&an. AND 0 <= yrdif(date_debut, date_fin, 'act/act') <= 1) then output;
		run;

		%arret_erreur;

	%end;

%mend ACT_COD_ACE;

*	******************************************************************************************************************************************	;
*	Macro finale pour appeler les pr�c�dentes, pour chaque table du PMSI																		;
*	******************************************************************************************************************************************	;

%macro extract_ACT_COD_PMSI(annee_deb=, annee_fin=, MCO = 1, SSR = 1, tbl_out=, tbl_codes=, tbl_patients=);

	%let an_deb_pmsi = %sysevalf(&annee_deb. - 2000);
	%let an_fin_pmsi = %sysevalf(&annee_fin. - 2000);

	* Pour le PMSI-MCO;
	%if &MCO. = 1 %then
		%do;

			%let domaine = MCO;

			* Dans les tables FB;
			%ACT_COD_sejours(
				an_deb_pmsi = &an_deb_pmsi.,
				an_fin_pmsi = &an_fin_pmsi.,
				an_crea_t = 06,
				an_fin_t = %eval(%sysfunc(YEAR(%sysfunc(TODAY()))) - 2000),
				tbl_codes = &tbl_codes.,
				table = FB,
				domaine = &domaine.,
				tbl_out = &tbl_out.,
				tbl_patients = &tbl_patients.
				);

			* Dans les tables FC;
			%ACT_COD_sejours(
				an_deb_pmsi = &an_deb_pmsi.,
				an_fin_pmsi = &an_fin_pmsi.,
				an_crea_t = 06,
				an_fin_t = %eval(%sysfunc(YEAR(%sysfunc(TODAY()))) - 2000),
				tbl_codes = &tbl_codes.,
				table = FC,
				domaine = &domaine.,
				tbl_out = &tbl_out.,
				tbl_patients = &tbl_patients.
				);

			* Dans les tables FBSTC;
			%ACT_COD_ACE(
				an_deb_pmsi = &an_deb_pmsi.,
				an_fin_pmsi = &an_fin_pmsi.,
				an_crea_t = 08,
				an_fin_t = %eval(%sysfunc(YEAR(%sysfunc(TODAY()))) - 2000),
				tbl_codes = &tbl_codes.,
				table = FBSTC,
				domaine = &domaine.,
				tbl_out = &tbl_out.,
				tbl_patients = &tbl_patients.
				);

			* Dans les tables FCSTC;
			%ACT_COD_ACE(
				an_deb_pmsi = &an_deb_pmsi.,
				an_fin_pmsi = &an_fin_pmsi.,
				an_crea_t = 08,
				an_fin_t = %eval(%sysfunc(YEAR(%sysfunc(TODAY()))) - 2000),
				tbl_codes = &tbl_codes.,
				table = FCSTC,
				domaine = &domaine.,
				tbl_out = &tbl_out.,
				tbl_patients = &tbl_patients.
				);


		%end;
		* Fin du PMSI-MCO;

	* Pour le PMSI-SSR;
	%if &SSR. = 1 %then
		%do;

			%let domaine = SSR;

			* Dans les tables FB;
			%ACT_COD_sejours(
				an_deb_pmsi = &an_deb_pmsi.,
				an_fin_pmsi = &an_fin_pmsi.,
				an_crea_t = 06,
				an_fin_t = %eval(%sysfunc(YEAR(%sysfunc(TODAY()))) - 2000),
				tbl_codes = &tbl_codes.,
				table = FB,
				domaine = &domaine.,
				tbl_out = &tbl_out.,
				tbl_patients = &tbl_patients.
				);

			* Dans les tables FC;
			%ACT_COD_sejours(
				an_deb_pmsi = &an_deb_pmsi.,
				an_fin_pmsi = &an_fin_pmsi.,
				an_crea_t = 06,
				an_fin_t = %eval(%sysfunc(YEAR(%sysfunc(TODAY()))) - 2000),
				tbl_codes = &tbl_codes.,
				table = FC,
				domaine = &domaine.,
				tbl_out = &tbl_out.,
				tbl_patients = &tbl_patients.
				);

			* Dans les tables FBSTC;
			%ACT_COD_ACE(
				an_deb_pmsi = &an_deb_pmsi.,
				an_fin_pmsi = &an_fin_pmsi.,
				an_crea_t = 13,
				an_fin_t = %eval(%sysfunc(YEAR(%sysfunc(TODAY()))) - 2000),
				tbl_codes = &tbl_codes.,
				table = FBSTC,
				domaine = &domaine.,
				tbl_out = &tbl_out.,
				tbl_patients = &tbl_patients.
				);

			* Dans les tables FCSTC;
			%ACT_COD_ACE(
				an_deb_pmsi = &an_deb_pmsi.,
				an_fin_pmsi = &an_fin_pmsi.,
				an_crea_t = 13,
				an_fin_t = %eval(%sysfunc(YEAR(%sysfunc(TODAY()))) - 2000),
				tbl_codes = &tbl_codes.,
				table = FCSTC,
				domaine = &domaine.,
				tbl_out = &tbl_out.,
				tbl_patients = &tbl_patients.
				);

		%end;
		* Fin du PMSI-SSR;

	* Empilage de la table temporaire � la table R�sultat;
	data &tbl_out.;
		length type $12. table $15.;
		set ACT_COD_:;
	run;

	%arret_erreur;

	* Suppression de la table temporaire;
	proc datasets library = work memtype = data nolist;
		delete ACT_COD_:;
	run;

%mend extract_ACT_COD_PMSI;
