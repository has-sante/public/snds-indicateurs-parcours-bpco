/*	******************************************************************************************************************************************	*/
/*																																				*/
/*		Macro pour v�rifier si une variable existe : var_exist																					*/
/*																																				*/
/*	******************************************************************************************************************************************	*/
/*																																				*/
/*		Arguments en entr�e :																													*/
/*																																				*/
/*			- lib = le nom de la librairie dans laquelle est stock�e la table contenant la variable � v�rifier									*/
/*			  Par d�faut : work																													*/
/*																																				*/
/*			- table = le nom de la table contenant la variable � v�rifier																		*/
/*																																				*/
/*			- variable = le nom de la variable � v�rifier																						*/
/*																																				*/
/*	******************************************************************************************************************************************	*/

%macro varexist(lib = work, table=, variable=) ;

    %local dsid rc ;

    %let dsid = %sysfunc(open(&lib..&table.)) ;

    %if (&dsid.) %then %do ;

        %if %sysfunc(varnum(&dsid., &variable.)) %then 1 ;
        %else 0 ;
        %let rc = %sysfunc(close(&dsid.)) ;

    %end ;

    %else 0 ;

%mend varexist ;

/* Exemple d appel : Au sein d'une macro */
/*
%macro test;

    %if %varesxist(lib = databrut, table = ER_PRS_F_2010, variable = DPN_QLF) %then
		%put La variable DPN_QLF existe dans la table databrut.ER_PRS_F_2010;

    %else
		%put La variable DPN_QLF n existe pas dans la table databrut.ER_PRS_F_2010;

    %if %varesxist(lib = databrut, table = ER_PRS_F_2010, variable = DPN_QLM) %then
		%put La variable DPN_QLM existe dans la table databrut.ER_PRS_F_2010;
    %else
		%put La variable DPN_QLM n existe pas dans la table databrut.ER_PRS_F_2010;

%mend test;
%test;
*/
