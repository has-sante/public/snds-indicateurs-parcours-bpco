/*	******************************************************************************************************************************************	*/
/*																																				*/
/*		Macro pour supprimer une table si elle existe : suppr_table																				*/
/*																																				*/
/*	******************************************************************************************************************************************	*/
/*																																				*/
/*		Arguments en entr�e :																													*/
/*																																				*/
/*			- lib = le nom de la librairie dans laquelle est stock�e la table																	*/
/*			  Par d�faut : work																													*/
/*																																				*/
/*			- table = le nom de la table � supprimer																							*/
/*																																				*/
/*	******************************************************************************************************************************************	*/

%macro suppr_table(lib = work, table=);

	%if %sysfunc(exist(&lib..&table.)) = 1 %then 
		%do;

			proc delete data = &lib..&table.;
			run;

		%end;

	%else
		%put La table &lib..&table. n existe pas !;

%mend suppr_table;

/* Exemple d appel */
/*
%suppr_table(
	lib = work, 
	table = test
	);
*/
