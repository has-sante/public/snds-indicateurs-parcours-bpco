/*	******************************************************************************************************************************************	*/
/*																																				*/
/*		Macro pour rep�rer les remboursement via des codes GHM souhait�s, dans le PMSI : extract_GHM_PMSI										*/
/*																																				*/
/*	******************************************************************************************************************************************	*/
/*																																				*/
/*		Arguments en entr�e :																													*/
/*																																				*/
/*			- annee_deb = la premi�re ann�e de soins (ie. ann�e des tables PMSI) pour laquelle on souhaite rep�rer les codes					*/
/*																																				*/
/*			- annee_fin = la derni�re ann�e de soins (ie. ann�e des tables PMSI) pour laquelle on souhaite rep�rer les codes					*/
/*																																				*/
/*			- tbl_out = le nom de la table en sortie (peut contenir un nom de librairie)														*/
/*																																				*/
/*			- tbl_codes = la table contenant les codes GHM � rep�rer (au format Oracle)															*/
/*																																				*/
/*			- tbl_patients = le nom de la table contenant la correspondance BEN_IDT_ANO <-> BEN_NR_PSA (au format Oracle)						*/
/*																																				*/
/*	******************************************************************************************************************************************	*/
/*																																				*/
/*		Tables en sortie : &tbl_out. (renseign�e en param�tres de la macro)																		*/
/*		avec s�lection des codes disponibles dans le r�f�rentiel &tbl_codes., hors s�jours en erreur											*/
/*																																				*/
/*	******************************************************************************************************************************************	*/

*	******************************************************************************************************************************************	;
*	Macro pour rep�rer les codes GHM dans le PMSI - s�jours																						;
*	******************************************************************************************************************************************	;

%macro extract_GHM_PMSI(annee_deb=, annee_fin=, an_crea_t = 06, an_fin_t = %eval(%sysfunc(YEAR(%sysfunc(TODAY()))) - 2000), tbl_codes=, 
	tbl_out=, tbl_patients=);

	%let an_deb_pmsi = %sysevalf(&annee_deb. - 2000);
	%let an_fin_pmsi = %sysevalf(&annee_fin. - 2000);

	%do i = %sysfunc(max(&an_deb_pmsi., &an_crea_t. )) %to %sysfunc(min(&an_fin_pmsi., &an_fin_t.));

		%if &i. < 10 %then %let an = 0&i.;
		%else %let an = &i.;

		* On d�finit les variables de jointure pour chaque domaine du PMSI;
		%let var_join1 = ETA_NUM;
		%let var_join2 = RSA_NUM;

		* On r�cup�re les s�jours en filtrant sur les codes GHM s�lectionn�s;
		%put R�cup�ration des s�jours du PMSI-MCO contenant des GHM pr�-d�finis - Ann�e &an. ...;
		
		proc sql;

			%connectora;

				CREATE TABLE ext_GHM_PMSI_&an. AS 
				 	SELECT *
					FROM CONNECTION TO ORACLE (
						SELECT DISTINCT
							c.&var_join1.,
							c.&var_join2.,
							pop.BEN_IDT_ANO,
							c.EXE_SOI_DTD,
							c.EXE_SOI_DTF,
							20&an. AS annee,
							b.GRG_GHM,
							b.SEJ_NBJ,
							cod.*
						FROM &tbl_patients. pop
							INNER JOIN T_MCO&an.C c
								ON	pop.BEN_NIR_PSA = c.NIR_ANO_17
							INNER JOIN T_MCO&an.B b
								ON	c.&var_join1. = b.&var_join1.
								AND	c.&var_join2. = b.&var_join2.
							, &tbl_codes. cod
						WHERE SUBSTR(TRIM(b.GRG_GHM), 1, taille) = TRIM(cod.code_GHM) 
							AND c.NIR_ANO_17 NOT IN ('&cle_inc1.', '&cle_inc2.')
							AND NIR_RET = '0' AND NAI_RET = '0' AND SEX_RET = '0' AND SEJ_RET = '0' AND FHO_RET = '0' 
							AND PMS_RET = '0' AND DAT_RET = '0' %if &an. > 12 %then %do; AND COH_NAI_RET = '0' AND COH_SEX_RET = '0' %end;
							AND c.ETA_NUM NOT IN ('130780521', '130783236', '130783293', '130784234', '130804297', '600100101', '750041543', 
								'750100018', '750100042', '750100075', '750100083', '750100091', '750100109', '750100125', '750100166', 
								'750100208', '750100216', '750100232', '750100273', '750100299', '750801441', '750803447', '750803454',
								'910100015', '910100023', '920100013', '920100021', '920100039', '920100047', '920100054', '920100062', 
								'930100011', '930100037', '930100045', '940100027', '940100035', '940100043', '940100050', '940100068', 
								'950100016', '690783154', '690784137', '690784152', '690784178', '690787478', '830100558')
							AND GRG_GHM NOT IN ('90Z00Z') AND GRG_RET NOT IN ('024') AND GRG_GHM NOT IN ('90H01Z', '90Z00Z', '90Z01Z', 
								'90Z02Z', '90Z03Z') 
							AND ((SEJ_TYP = 'A' OR SEJ_TYP IS NULL) OR (SEJ_TYP = 'B' AND GRG_GHM NOT IN ('28Z14Z','28Z15Z','28Z16Z')))
						);

			DISCONNECT FROM ORACLE;

		quit;

		%arret_erreur;

	%end;

	* Empilage de la table temporaire � la table R�sultat;
	data &tbl_out.;
		set ext_GHM_PMSI_:;
		length date_debut date_fin 4.;
		type = "PMSI s�jours";
		date_debut = datepart(EXE_SOI_DTD);
		date_fin = datepart(EXE_SOI_DTF);
		format date_debut date_fin ddmmyy10.;
		drop EXE_SOI_DTD EXE_SOI_DTF;
	run;

	%arret_erreur;

	* Suppression de la table temporaire;
	proc datasets library = work memtype = data nolist;
		delete ext_GHM_PMSI_:;
	run;

%mend extract_GHM_PMSI;
