/*	******************************************************************************************************************************************	*/
/*																																				*/
/*		Macro pour rep�rer les remboursement des codes diagnostics souhait�s, dans le PMSI : extract_CIM10_PMSI									*/
/*																																				*/
/*	******************************************************************************************************************************************	*/
/*																																				*/
/*		Arguments en entr�e :																													*/
/*																																				*/
/*			- annee_deb = la premi�re ann�e de soins (ie. ann�e des tables PMSI) pour laquelle on souhaite rep�rer les codes					*/
/*																																				*/
/*			- annee_fin = la derni�re ann�e de soins (ie. ann�e des tables PMSI) pour laquelle on souhaite rep�rer les codes					*/
/*																																				*/
/*			- tbl_out = le nom de la table en sortie (peut contenir un nom de librairie)														*/
/*																																				*/
/*			- tbl_codes = la table contenant les codes CIM10 � rep�rer (au format Oracle)														*/
/*																																				*/
/*			- tbl_patients = le nom de la table contenant la correspondance BEN_IDT_ANO <-> BEN_NR_PSA (au format Oracle)						*/
/*																																				*/
/*			- HAD_DP = flag (0/1) : si 1, on recherche les codes CIM10 dans les DP des s�jours du PMSI-HAD (Par d�faut : 1)						*/
/*																																				*/
/*			- HAD_DAS = flag (0/1) : si 1, on recherche les codes CIM10 dans les DSA des s�jours du PMSI-HAD (Par d�faut : 1)					*/
/*																																				*/
/*			- HAD_MPP = flag (0/1) : si 1, on recherche les codes CIM10 dans les MPP des sous-s�quences du PMSI-HAD (Par d�faut : 1)			*/
/*																																				*/
/*			- HAD_MPA = flag (0/1) : si 1, on recherche les codes CIM10 dans les MPA des sous-s�quences du PMSI-HAD (Par d�faut : 1)			*/
/*																																				*/
/*			- MCO_DP = flag (0/1) : si 1, on recherche les codes CIM10 dans les DP des s�jours du PMSI-MCO (Par d�faut : 1)						*/
/*																																				*/
/*			- MCO_DR = flag (0/1) : si 1, on recherche les codes CIM10 dans les DR des s�jours du PMSI-MCO (Par d�faut : 1)						*/
/*																																				*/
/*			- MCO_DAS = flag (0/1) : si 1, on recherche les codes CIM10 dans les DSA des s�jours du PMSI-MCO (Par d�faut : 1)					*/
/*																																				*/
/*			- MCO_DP_UM = flag (0/1) : si 1, on recherche les codes CIM10 dans les DP des UM du PMSI-MCO (Par d�faut : 1)						*/
/*																																				*/
/*			- MCO_DR_UM = flag (0/1) : si 1, on recherche les codes CIM10 dans les DR des UM du PMSI-MCO (Par d�faut : 1)						*/
/*																																				*/
/*			- SSR_FP = flag (0/1) : si 1, on recherche les codes CIM10 dans les FP des RHS du PMSI-SSR (Par d�faut : 1)							*/
/*																																				*/
/*			- SSR_MPP = flag (0/1) : si 1, on recherche les codes CIM10 dans les MPP des RHS du PMSI-SSR (Par d�faut : 1)						*/
/*																																				*/
/*			- SSR_AE = flag (0/1) : si 1, on recherche les codes CIM10 dans les AE des RHS du PMSI-SSR (Par d�faut : 1)							*/
/*																																				*/
/*			- SSR_DAS = flag (0/1) : si 1, on recherche les codes CIM10 dans les DAS du PMSI-SSR (Par d�faut : 1)								*/
/*																																				*/
/*	******************************************************************************************************************************************	*/
/*																																				*/
/*		Tables en sortie : &tbl_out. (renseign�e en param�tres de la macro)																		*/
/*		avec s�lection des codes disponibles dans le r�f�rentiel &tbl_codes., hors s�jours en erreur											*/
/*																																				*/
/*	******************************************************************************************************************************************	*/

*	******************************************************************************************************************************************	;
*	Macro pour rep�rer les codes CIM10 dans le PMSI - s�jours																					;
*	******************************************************************************************************************************************	;

%macro CIM10_sejours(an_deb_pmsi=, an_fin_pmsi=, an_crea_t=, an_fin_t=, tbl_codes=, table=, domaine=, var_CIM10=, tbl_out=, tbl_patients=);

	%do i = %sysfunc(max(&an_deb_pmsi., &an_crea_t. )) %to %sysfunc(min(&an_fin_pmsi., &an_fin_t.));

		%if &i. < 10 %then %let an = 0&i.;
		%else %let an = &i.;

		* On d�finit les variables de jointure pour chaque domaine du PMSI;
		%if &domaine. = HAD %then
			%do;
				%let var_join1 = ETA_NUM_EPMSI;
				%let var_join2 = RHAD_NUM;
			%end;
		%if &domaine. = MCO %then
			%do;
				%let var_join1 = ETA_NUM;
				%let var_join2 = RSA_NUM;
			%end;
		%if &domaine. = RIP %then
			%do;
				%let var_join1 = ETA_NUM_EPMSI;
				%let var_join2 = RIP_NUM;
			%end;
		%if &domaine. = SSR %then
			%do;
				%let var_join1 = ETA_NUM;
				%let var_join2 = RHA_NUM;
			%end;

		* On joint la table CIM10 avec la table des s�jours en filtrant sur les codes CIM10 s�lectionn�s;
		%put R�cup�ration des s�jours du PMSI-&domaine. (table &table.) contenant des actes CIM10 pr�-d�finis - Ann�e &an. ...;
		
		proc sql;

			%connectora;

				CREATE TABLE CIM10_&domaine._&table._&var_CIM10._&an. AS 
				 	SELECT *
					FROM CONNECTION TO ORACLE (
						SELECT DISTINCT
							c.&var_join1.,
							c.&var_join2.,
							pop.BEN_IDT_ANO,
							c.EXE_SOI_DTD,
							c.EXE_SOI_DTF,
							%if &domaine. = MCO %then
								%do;
									%if &an. < 12 %then %do; '' AS ETA_NUM_GEO, %end;
									%if &an. = 12 %then %do; um.ETA_NUM_GEO1 AS ETA_NUM_GEO, %end;
									%if &an.> 12 %then %do; um.ETA_NUM_GEO, %end;
									um.UM_ORD_NUM,
									b.SOR_MOD,
									b.DGN_PAL,
									b.DGN_REL,
									b.GRG_GHM,
									b.SEJ_NBJ,
								%end;
							%if &domaine. = SSR %then
								%do;
									b.HOS_TYP_UM,
									s.SEJ_NBJ,
								%end;
							%if &domaine. = RIP %then
								%do;
									rsa.FOR_ACT,
									b.SEJ_NBJ,
								%end;
							%if &domaine. = HAD %then
								%do;
									b.SEJ_NBJ,
								%end;
							20&an. AS annee,
							cod.*
						FROM &tbl_patients. pop
							INNER JOIN T_&domaine.&an.C c
								ON	pop.BEN_NIR_PSA = c.NIR_ANO_17
							%if &domaine. = MCO or &domaine. = HAD or &domaine. = SSR %then
								%do;
									INNER JOIN T_&domaine.&an.B b
										ON	c.&var_join1. = b.&var_join1.
										AND	c.&var_join2. = b.&var_join2.
								%end;
							%if &domaine. = MCO %then
								%do;
									INNER JOIN T_&domaine.&an.UM um
										ON	c.&var_join1. = um.&var_join1.
										AND	c.&var_join2. = um.&var_join2.
								%end;
							%if &domaine. = SSR %then
								%do;
									INNER JOIN T_&domaine.&an.S s
										ON	c.&var_join1. = s.&var_join1.
										AND	c.&var_join2. = s.&var_join2.
								%end;
							%if &domaine. = RIP %then
								%do;
									INNER JOIN T_&domaine.&an.RSA rsa
										ON	c.&var_join1. = rsa.&var_join1.
										AND	c.&var_join2. = rsa.&var_join2.
								%end;
							%if &table. = D or &table. = DMPA or &table. = DMPP %then
								%do;
									INNER JOIN T_&domaine.&an.&table. cim10
										ON	c.&var_join1. = cim10.&var_join1.
										AND	c.&var_join2. = cim10.&var_join2.
								%end;
							, &tbl_codes. cod
								
						WHERE 
								%if &table. = D or &table. = DMPA or &table. = DMPP %then
									%do;
										SUBSTR(TRIM(cim10.&var_CIM10.), 1, taille) = TRIM(cod.code_CIM)
									%end;
								%if &table. = UM %then
									%do;
										SUBSTR(TRIM(um.&var_CIM10.), 1, taille) = TRIM(cod.code_CIM)
									%end;
								%if &table. = B %then
									%do;
										SUBSTR(TRIM(b.&var_CIM10.), 1, taille) = TRIM(cod.code_CIM)
									%end;
							AND c.NIR_ANO_17 NOT IN ('&cle_inc1.', '&cle_inc2.')
							%if &domaine. = HAD %then
								%do;
									AND NIR_RET = '0' AND NAI_RET = '0' AND SEX_RET = '0' AND SEJ_RET = '0' AND FHO_RET = '0' 
									AND PMS_RET = '0' AND DAT_RET = '0' %if &an. > 12 %then %do; AND COH_NAI_RET = '0' 
									AND COH_SEX_RET = '0' %end;
								%end;
							%if &domaine. = MCO %then
								%do;
									AND NIR_RET = '0' AND NAI_RET = '0' AND SEX_RET = '0' AND SEJ_RET = '0' AND FHO_RET = '0' 
									AND PMS_RET = '0' AND DAT_RET = '0' %if &an. > 12 %then %do; AND COH_NAI_RET = '0' 
									AND COH_SEX_RET = '0' %end;
									AND c.ETA_NUM NOT IN ('130780521', '130783236', '130783293', '130784234', '130804297', '600100101', 
										'750041543', '750100018', '750100042', '750100075', '750100083', '750100091', '750100109', 
										'750100125', '750100166', '750100208', '750100216', '750100232', '750100273', '750100299', 
										'750801441', '750803447', '750803454', '910100015', '910100023', '920100013', '920100021', 
										'920100039', '920100047', '920100054', '920100062', '930100011', '930100037', '930100045', 
										'940100027', '940100035', '940100043', '940100050', '940100068', '950100016', '690783154', 
										'690784137', '690784152', '690784178', '690787478', '830100558')
									AND GRG_GHM NOT IN ('90Z00Z') AND GRG_RET NOT IN ('024') AND GRG_GHM NOT IN ('90H01Z', '90Z00Z', '90Z01Z', 
										'90Z02Z', '90Z03Z') 
									AND ((SEJ_TYP = 'A' OR SEJ_TYP IS NULL) OR (SEJ_TYP = 'B' AND GRG_GHM NOT IN ('28Z14Z','28Z15Z','28Z16Z')))
								%end;
							%if &domaine. = RIP %then
								%do;
									AND NIR_RET = '0' AND NAI_RET = '0' AND SEX_RET = '0' AND SEJ_RET = '0' AND FHO_RET = '0' 
									AND PMS_RET = '0' AND DAT_RET = '0' %if &an. > 12 %then %do; AND COH_NAI_RET = '0' 
									AND COH_SEX_RET = '0' %end;
								%end;
							%if &domaine. = SSR %then
								%do;
									AND NIR_RET = '0' AND NAI_RET = '0' AND SEX_RET = '0' AND SEJ_RET = '0' AND FHO_RET = '0' 
									AND PMS_RET = '0' AND DAT_RET = '0' %if &an. > 12 %then %do; AND COH_NAI_RET = '0' 
									AND COH_SEX_RET = '0' %end;
								%end;
						);

			DISCONNECT FROM ORACLE;

		quit;

		data CIM10_&domaine._&table._&var_CIM10._&an. (rename = (ETA_NUM_GEO2 = ETA_NUM_GEO));
			set CIM10_&domaine._&table._&var_CIM10._&an.;
			length table $5. date_debut date_fin 4. ETA_NUM_GEO2 $9. variable $11.;
			table = "&table.";
			domaine = "&domaine.";
			variable = "&var_CIM10.";
			type = "PMSI s�jours";
			date_debut = datepart(EXE_SOI_DTD);
			date_fin = datepart(EXE_SOI_DTF);
			ETA_NUM_GEO2 = ETA_NUM_GEO;
			format date_debut date_fin ddmmyy10.;
			drop EXE_SOI_DTD EXE_SOI_DTF ETA_NUM_GEO;
		run;

		%arret_erreur;

	%end;

%mend CIM10_sejours;

*	******************************************************************************************************************************************	;
*	Macro finale pour appeler les pr�c�dentes, pour chaque table du PMSI																		;
*	******************************************************************************************************************************************	;

%macro extract_CIM10_PMSI(annee_deb=, annee_fin=, HAD_DP = 1, HAD_DAS = 1, HAD_MPP = 1, HAD_MPA = 1, MCO_DP = 1, MCO_DR = 1, MCO_DAS = 1,
	MCO_DP_UM = 1, MCO_DR_UM = 1, SSR_FP = 1, SSR_MPP = 1, SSR_AE = 1, SSR_DAS = 1, tbl_out=, tbl_codes=, tbl_patients=);

	%let an_deb_pmsi = %sysevalf(&annee_deb. - 2000);
	%let an_fin_pmsi = %sysevalf(&annee_fin. - 2000);

	* Pour le PMSI-HAD;
	%if &HAD_MPP. = 1 or &HAD_MPA. = 1 %then
		%do;

			%let domaine = HAD;

			%if &HAD_DP. = 1 %then
				%do;

					* Dans les tables B;
					%CIM10_sejours(
						an_deb_pmsi = &an_deb_pmsi.,
						an_fin_pmsi = &an_fin_pmsi.,
						an_crea_t = 06,
						an_fin_t = 11,
						tbl_codes = &tbl_codes.,
						table = B,
						domaine = &domaine.,
						var_CIM10 = DGN_PAL,
						tbl_out = &tbl_out.,
						tbl_patients = &tbl_patients.
						);

					%CIM10_sejours(
						an_deb_pmsi = &an_deb_pmsi.,
						an_fin_pmsi = &an_fin_pmsi.,
						an_crea_t = 14,
						an_fin_t = %eval(%sysfunc(YEAR(%sysfunc(TODAY()))) - 2000),
						tbl_codes = &tbl_codes.,
						table = B,
						domaine = &domaine.,
						var_CIM10 = DGN_PAL,
						tbl_out = &tbl_out.,
						tbl_patients = &tbl_patients.
						);

				%end;

			%if &HAD_DAS. = 1 %then
				%do;

					* Dans les tables D;
					%CIM10_sejours(
						an_deb_pmsi = &an_deb_pmsi.,
						an_fin_pmsi = &an_fin_pmsi.,
						an_crea_t = 10,
						an_fin_t = %eval(%sysfunc(YEAR(%sysfunc(TODAY()))) - 2000),
						tbl_codes = &tbl_codes.,
						table = D,
						domaine = &domaine.,
						var_CIM10 = DGN_ASS,
						tbl_out = &tbl_out.,
						tbl_patients = &tbl_patients.
						);

				%end;

			%if &HAD_MPA. = 1 %then
				%do;

					* Dans les tables DMPA;
					%CIM10_sejours(
						an_deb_pmsi = &an_deb_pmsi.,
						an_fin_pmsi = &an_fin_pmsi.,
						an_crea_t = 12,
						an_fin_t = %eval(%sysfunc(YEAR(%sysfunc(TODAY()))) - 2000),
						tbl_codes = &tbl_codes.,
						table = DMPA,
						domaine = &domaine.,
						var_CIM10 = DGN_ASS_MPA,
						tbl_out = &tbl_out.,
						tbl_patients = &tbl_patients.
						);

				%end;

			%if &HAD_MPP. = 1 %then
				%do;

					* Dans les tables DMPP;
					%CIM10_sejours(
						an_deb_pmsi = &an_deb_pmsi.,
						an_fin_pmsi = &an_fin_pmsi.,
						an_crea_t = 12,
						an_fin_t = %eval(%sysfunc(YEAR(%sysfunc(TODAY()))) - 2000),
						tbl_codes = &tbl_codes.,
						table = DMPP,
						domaine = &domaine.,
						var_CIM10 = DGN_ASS_MPP,
						tbl_out = &tbl_out.,
						tbl_patients = &tbl_patients.
						);

				%end;

		%end;
		* Fin du PMSI-HAD;

	* Pour le PMSI-MCO;
	%if &MCO_DP. = 1 or &MCO_DR. = 1 or &MCO_DAS. = 1 or &MCO_DP_UM. = 1 or &MCO_DR_UM. = 1 %then
		%do;

			%let domaine = MCO;

			%if &MCO_DP. = 1 %then
				%do;

					* Dans les tables B;
					%CIM10_sejours(
						an_deb_pmsi = &an_deb_pmsi.,
						an_fin_pmsi = &an_fin_pmsi.,
						an_crea_t = 06,
						an_fin_t = %eval(%sysfunc(YEAR(%sysfunc(TODAY()))) - 2000),
						tbl_codes = &tbl_codes.,
						table = B,
						domaine = &domaine.,
						var_CIM10 = DGN_PAL,
						tbl_out = &tbl_out.,
						tbl_patients = &tbl_patients.
						);

					%end;

			%if &MCO_DR. = 1 %then
				%do;

					* Dans les tables B;
					%CIM10_sejours(
						an_deb_pmsi = &an_deb_pmsi.,
						an_fin_pmsi = &an_fin_pmsi.,
						an_crea_t = 06,
						an_fin_t = %eval(%sysfunc(YEAR(%sysfunc(TODAY()))) - 2000),
						tbl_codes = &tbl_codes.,
						table = B,
						domaine = &domaine.,
						var_CIM10 = DGN_REL,
						tbl_out = &tbl_out.,
						tbl_patients = &tbl_patients.
						);

					%end;

			%if &MCO_DAS. = 1 %then
				%do;

					* Dans les tables D;
					%CIM10_sejours(
						an_deb_pmsi = &an_deb_pmsi.,
						an_fin_pmsi = &an_fin_pmsi.,
						an_crea_t = 06,
						an_fin_t = %eval(%sysfunc(YEAR(%sysfunc(TODAY()))) - 2000),
						tbl_codes = &tbl_codes.,
						table = D,
						domaine = &domaine.,
						var_CIM10 = ASS_DGN,
						tbl_out = &tbl_out.,
						tbl_patients = &tbl_patients.
						);

					%end;

			%if &MCO_DP_UM. = 1 %then
				%do;

					* Dans les tables UM;
					%CIM10_sejours(
						an_deb_pmsi = &an_deb_pmsi.,
						an_fin_pmsi = &an_fin_pmsi.,
						an_crea_t = 08,
						an_fin_t = %eval(%sysfunc(YEAR(%sysfunc(TODAY()))) - 2000),
						tbl_codes = &tbl_codes.,
						table = UM,
						domaine = &domaine.,
						var_CIM10 = DGN_PAL,
						tbl_out = &tbl_out.,
						tbl_patients = &tbl_patients.
						);

					%end;

			%if &MCO_DR_UM. = 1 %then
				%do;

					* Dans les tables UM;
					%CIM10_sejours(
						an_deb_pmsi = &an_deb_pmsi.,
						an_fin_pmsi = &an_fin_pmsi.,
						an_crea_t = 08,
						an_fin_t = %eval(%sysfunc(YEAR(%sysfunc(TODAY()))) - 2000),
						tbl_codes = &tbl_codes.,
						table = UM,
						domaine = &domaine.,
						var_CIM10 = DGN_REL,
						tbl_out = &tbl_out.,
						tbl_patients = &tbl_patients.
						);

					%end;

		%end;
		* Fin du PMSI-MCO;

	* Pour le PMSI-SSR;
	%if &SSR_FP. = 1 or &SSR_MPP. = 1 or &SSR_AE. = 1 or &SSR_DAS. = 1 %then
		%do;

			%let domaine = SSR;

			%if &SSR_FP. = 1 %then
				%do;

					* Dans les tables B;
					%CIM10_sejours(
						an_deb_pmsi = &an_deb_pmsi.,
						an_fin_pmsi = &an_fin_pmsi.,
						an_crea_t = 06,
						an_fin_t = %eval(%sysfunc(YEAR(%sysfunc(TODAY()))) - 2000),
						tbl_codes = &tbl_codes.,
						table = B,
						domaine = &domaine.,
						var_CIM10 = FP_PEC,
						tbl_out = &tbl_out.,
						tbl_patients = &tbl_patients.
						);

				%end;

			%if &SSR_MPP. = 1 %then
				%do;

					* Dans les tables B;
					%CIM10_sejours(
						an_deb_pmsi = &an_deb_pmsi.,
						an_fin_pmsi = &an_fin_pmsi.,
						an_crea_t = 06,
						an_fin_t = %eval(%sysfunc(YEAR(%sysfunc(TODAY()))) - 2000),
						tbl_codes = &tbl_codes.,
						table = B,
						domaine = &domaine.,
						var_CIM10 = MOR_PRP,
						tbl_out = &tbl_out.,
						tbl_patients = &tbl_patients.
						);

				%end;

			%if &SSR_AE. = 1 %then
				%do;

					* Dans les tables B;
					%CIM10_sejours(
						an_deb_pmsi = &an_deb_pmsi.,
						an_fin_pmsi = &an_fin_pmsi.,
						an_crea_t = 06,
						an_fin_t = %eval(%sysfunc(YEAR(%sysfunc(TODAY()))) - 2000),
						tbl_codes = &tbl_codes.,
						table = B,
						domaine = &domaine.,
						var_CIM10 = ETL_AFF,
						tbl_out = &tbl_out.,
						tbl_patients = &tbl_patients.
						);

				%end;

			%if &SSR_DAS. = 1 %then
				%do;

					* Dans les tables D;
					%CIM10_sejours(
						an_deb_pmsi = &an_deb_pmsi.,
						an_fin_pmsi = &an_fin_pmsi.,
						an_crea_t = 09,
						an_fin_t = %eval(%sysfunc(YEAR(%sysfunc(TODAY()))) - 2000),
						tbl_codes = &tbl_codes.,
						table = D,
						domaine = &domaine.,
						var_CIM10 = DGN_COD,
						tbl_out = &tbl_out.,
						tbl_patients = &tbl_patients.
						);

				%end;

		%end;
		* Fin du PMSI-SSR;

	* Empilage de la table temporaire � la table R�sultat;
	data &tbl_out.;
		set CIM10_:;
	run;

	%arret_erreur;

	* Suppression de la table temporaire;
	proc datasets library = work memtype = data nolist;
		delete CIM10_:;
	run;

%mend extract_CIM10_PMSI;
