/*	******************************************************************************************************************************************	*/
/*																																				*/
/*		Macro pour effectuer des v�rifications (proc freq) au cours du projet																	*/
/*																																				*/
/*	******************************************************************************************************************************************	*/
/*																																				*/
/*		Arguments en entr�e :																													*/
/*																																				*/
/*			- in_tbl = le nom de la table en entr�e (peut contenir une librairie)																*/
/*																																				*/
/*			- out_tbl = le nom de la table en sortie (stock�e dans la librairie "verif", commen�ant par _ + num�ro de la v�rif 					*/
/*																																				*/
/*			- list_var_in = la ou les variable(s) � analyser (si plusieurs variables, les s�parer par des *)									*/
/*																																				*/
/*			- list_var_out = la liste de variables � conserver en sortie (inutile sur une seule variable dans list_var_in)						*/
/*																																				*/
/*	******************************************************************************************************************************************	*/

%global tmp_num_tab;

%let tmp_num_tab = 1;

%macro proc_freq(in_tbl=, out_tbl=, list_var_in=, list_var_out=);

	%if &tmp_num_tab. < 10 %then
		%let num_tab = 00&tmp_num_tab.;

	%else %if &tmp_num_tab. < 100 %then
		%let num_tab = 0&tmp_num_tab.;

	%else
		%let num_tab = &tmp_num_tab.;

	%if %sysfunc(index(&list_var_in., *)) >= 1 %then
		%do;

			ods exclude all;
			ods output Crosstabfreqs = verif._&num_tab.&out_tbl.;
			proc freq data = &in_tbl.;
			   table &list_var_in. / missing;
			run;

			data verif._&num_tab.&out_tbl. (keep = &list_var_out.);
				set verif._&num_tab.&out_tbl. (where = (index(_TYPE_, "0") = 0 and Frequency > 0));
			run;

			proc sql noprint; SELECT SUM(Frequency) INTO: nb_tot FROM verif._&num_tab.&out_tbl.; quit;

			data verif._&num_tab.&out_tbl.;
				set verif._&num_tab.&out_tbl.;
				length percent 4.;
				format percent nlpct7.1 Frequency commafr_0ch.;
				percent = Frequency/&nb_tot.;
			run;
			ods exclude none;

		%end;

	%else
		%do;

			ods exclude all;
			ods output Onewayfreqs = verif._&num_tab.&out_tbl.;
			proc freq data = &in_tbl.;
			   table &list_var_in.;
			run;

			proc sql noprint; SELECT SUM(Frequency) INTO: nb_tot FROM verif._&num_tab.&out_tbl.; quit;

			data verif._&num_tab.&out_tbl. (keep = F_&list_var_in. Frequency);
				set verif._&num_tab.&out_tbl. (where = (Frequency > 0));
			run;

			data verif._&num_tab.&out_tbl.;
				set verif._&num_tab.&out_tbl.;
				length percent 4.;
				format percent nlpct7.1 Frequency commafr_0ch.;
				percent = Frequency/&nb_tot.;
			run;
			ods exclude none;

		%end;

	%let tmp_num_tab = %sysevalf(&tmp_num_tab. + 1);

%mend proc_freq;
