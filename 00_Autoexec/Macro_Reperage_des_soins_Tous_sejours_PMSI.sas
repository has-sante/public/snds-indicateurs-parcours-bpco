/*	******************************************************************************************************************************************	*/
/*																																				*/
/*		Macro pour rep�rer les s�jours autour d'une date, dans le PMSI : extract_sej_PMSI														*/
/*																																				*/
/*	******************************************************************************************************************************************	*/
/*																																				*/
/*		Arguments en entr�e :																													*/
/*																																				*/
/*			- annee_deb = la premi�re ann�e de soins (ie. ann�e des tables PMSI) pour laquelle on souhaite rep�rer les codes					*/
/*																																				*/
/*			- annee_fin = la derni�re ann�e de soins (ie. ann�e des tables PMSI) pour laquelle on souhaite rep�rer les codes					*/
/*																																				*/
/*			- tbl_patients = le nom de la table en entr�e, qui doit contenir la correspondance BEN_IDT_ANO <-> BEN_NIR_PSA						*/
/*																																				*/
/*			- tbl_out = le nom de la table en sortie (peut contenir un nom de librairie)														*/
/*																																				*/
/*			- HAD = flag (0/1) : si 1, on recherche les s�jours du PMSI-HAD (Par d�faut : 1)													*/
/*																																				*/
/*			- MCO = flag (0/1) : si 1, on recherche les s�jours du PMSI-MCO (Par d�faut : 1)													*/
/*																																				*/
/*			- RIP = flag (0/1) : si 1, on recherche les s�jours du PMSI-RIP (Par d�faut : 1)													*/
/*																																				*/
/*			- SSR = flag (0/1) : si 1, on recherche les s�jours du PMSI-SSR (Par d�faut : 1)													*/
/*																																				*/
/*	******************************************************************************************************************************************	*/
/*																																				*/
/*		Tables en sortie : &tbl_out. (renseign�e en param�tres de la macro)																		*/
/*		hors s�jours en erreur																													*/
/*																																				*/
/*	******************************************************************************************************************************************	*/

*	******************************************************************************************************************************************	;
*	Macro pour rep�rer tous les s�jours du PMSI																									;
*	******************************************************************************************************************************************	;

%macro sejours_PMSI(an_deb_pmsi=, an_fin_pmsi=, an_crea_t=, an_fin_t=, domaine=, tbl_out=, tbl_patients=);

	%do i = %sysfunc(max(&an_deb_pmsi., &an_crea_t. )) %to %sysfunc(min(&an_fin_pmsi., &an_fin_t.));

		%if &i. < 10 %then %let an = 0&i.;
		%else %let an = &i.;

		* On d�finit les variables de jointure pour chaque domaine du PMSI;
		%if &domaine. = HAD %then
			%do;
				%let var_join1 = ETA_NUM_EPMSI;
				%let var_join2 = RHAD_NUM;
			%end;
		%if &domaine. = MCO %then
			%do;
				%let var_join1 = ETA_NUM;
				%let var_join2 = RSA_NUM;
			%end;
		%if &domaine. = RIP %then
			%do;
				%let var_join1 = ETA_NUM_EPMSI;
				%let var_join2 = RIP_NUM;
			%end;
		%if &domaine. = SSR %then
			%do;
				%let var_join1 = ETA_NUM;
				%let var_join2 = RHA_NUM;
			%end;

	
		proc sql;

			%connectora;

				CREATE TABLE sejours_&domaine._&an. AS 
				 	SELECT *
					FROM CONNECTION TO ORACLE (
						SELECT DISTINCT
							pop.BEN_IDT_ANO,
							c.&var_join1.,
							c.&var_join2.,
							c.EXE_SOI_DTD,
							c.EXE_SOI_DTF,
							%if &domaine. = MCO %then %do;
								b.GRG_GHM,
							%end;
							%if &domaine. = MCO or &domaine. = HAD %then %do;
								b.SEJ_NBJ AS duree_sejour,
							%end;
							%if &domaine. = RIP %then %do;
								s.SEJ_DUREE AS duree_sejour,
							%end;
							%if &domaine. = SSR %then %do;
								s.SEJ_NBJ AS duree_sejour,
								SUBSTR(b.AUT_TYP_UM, 1, 2) AS type_UM,
							%end;
							20&an. AS annee
						FROM &tbl_patients. pop
							INNER JOIN T_&domaine.&an.C c
								ON	pop.BEN_NIR_PSA = c.NIR_ANO_17
							%if &domaine. = MCO or &domaine. = HAD or &domaine. = SSR %then
								%do;
									INNER JOIN T_&domaine.&an.B b
										ON	c.&var_join1. = b.&var_join1.
										AND	c.&var_join2. = b.&var_join2.
								%end;
							%if &domaine. = RIP or &domaine. = SSR %then
								%do;
									LEFT JOIN T_&domaine.&an.S s
										ON	c.&var_join1. = s.&var_join1.
										AND	c.&var_join2. = s.&var_join2.
								%end;
								
						WHERE c.NIR_ANO_17 NOT IN ('&cle_inc1.', '&cle_inc2.')
							%if &domaine. = HAD %then
								%do;
									AND NIR_RET = '0' AND NAI_RET = '0' AND SEX_RET = '0' AND SEJ_RET = '0' AND FHO_RET = '0' 
									AND PMS_RET = '0' AND DAT_RET = '0' %if &an. > 12 %then %do; AND COH_NAI_RET = '0' 
									AND COH_SEX_RET = '0' %end;
								%end;
							%if &domaine. = MCO %then
								%do;
									AND NIR_RET = '0' AND NAI_RET = '0' AND SEX_RET = '0' AND SEJ_RET = '0' AND FHO_RET = '0' 
									AND PMS_RET = '0' AND DAT_RET = '0' %if &an. > 12 %then %do; AND COH_NAI_RET = '0' 
									AND COH_SEX_RET = '0' %end;
									AND c.ETA_NUM NOT IN ('130780521', '130783236', '130783293', '130784234', '130804297', '600100101', 
										'750041543', '750100018', '750100042', '750100075', '750100083', '750100091', '750100109', 
										'750100125', '750100166', '750100208', '750100216', '750100232', '750100273', '750100299', 
										'750801441', '750803447', '750803454', '910100015', '910100023', '920100013', '920100021', 
										'920100039', '920100047', '920100054', '920100062', '930100011', '930100037', '930100045', 
										'940100027', '940100035', '940100043', '940100050', '940100068', '950100016', '690783154', 
										'690784137', '690784152', '690784178', '690787478', '830100558')
									AND GRG_GHM NOT IN ('90Z00Z') AND GRG_RET NOT IN ('024') AND GRG_GHM NOT IN ('90H01Z', '90Z00Z', '90Z01Z', 
										'90Z02Z', '90Z03Z') 
									AND ((SEJ_TYP = 'A' OR SEJ_TYP IS NULL) OR (SEJ_TYP = 'B' AND GRG_GHM NOT IN ('28Z14Z','28Z15Z','28Z16Z')))
								%end;
							%if &domaine. = RIP %then
								%do;
									AND NIR_RET = '0' AND NAI_RET = '0' AND SEX_RET = '0' AND SEJ_RET = '0' AND FHO_RET = '0' 
									AND PMS_RET = '0' AND DAT_RET = '0' %if &an. > 12 %then %do; AND COH_NAI_RET = '0' 
									AND COH_SEX_RET = '0' %end;
								%end;
							%if &domaine. = SSR %then
								%do;
									AND NIR_RET = '0' AND NAI_RET = '0' AND SEX_RET = '0' AND SEJ_RET = '0' AND FHO_RET = '0' 
									AND PMS_RET = '0' AND DAT_RET = '0' %if &an. > 12 %then %do; AND COH_NAI_RET = '0' 
									AND COH_SEX_RET = '0' %end;
								%end;
						);

			DISCONNECT FROM ORACLE;

		quit;

		data sejours_&domaine._&an.;
			set sejours_&domaine._&an.;
			length date_debut date_fin 4.;
			domaine = "&domaine.";
			type = "PMSI s�jours";
			date_debut = datepart(EXE_SOI_DTD);
			date_fin = datepart(EXE_SOI_DTF);
			format date_debut date_fin ddmmyy10.;
			drop EXE_SOI_DTD EXE_SOI_DTF;
			id_sejour = &var_join1.||"_"||&var_join2.||"_"||put(annee, 4.);
		run;

		%arret_erreur;

	%end;

%mend sejours_PMSI;

*	******************************************************************************************************************************************	;
*	Macro finale pour appeler les pr�c�dentes, pour chaque table du PMSI																		;
*	******************************************************************************************************************************************	;

%macro extract_sej_PMSI(annee_deb=, annee_fin=, HAD = 1, MCO = 1, SSR = 1, RIP = 1, tbl_out=, tbl_patients=);

	%let an_deb_pmsi = %sysevalf(&annee_deb. - 2000);
	%let an_fin_pmsi = %sysevalf(&annee_fin. - 2000);

	* Pour le PMSI-HAD;
	%if &HAD. = 1 %then
		%do;

			%let domaine = HAD;

			%sejours_PMSI(
				an_deb_pmsi = &an_deb_pmsi.,
				an_fin_pmsi = &an_fin_pmsi.,
				an_crea_t = 06,
				an_fin_t = %eval(%sysfunc(YEAR(%sysfunc(TODAY()))) - 2000),
				domaine = &domaine.,
				tbl_out = &tbl_out.,
				tbl_patients = &tbl_patients.
				);

		%end;
		* Fin du PMSI-HAD;

	* Pour le PMSI-MCO;
	%if &MCO. = 1 %then
		%do;

			%let domaine = MCO;

			%sejours_PMSI(
				an_deb_pmsi = &an_deb_pmsi.,
				an_fin_pmsi = &an_fin_pmsi.,
				an_crea_t = 06,
				an_fin_t = %eval(%sysfunc(YEAR(%sysfunc(TODAY()))) - 2000),
				domaine = &domaine.,
				tbl_out = &tbl_out.,
				tbl_patients = &tbl_patients.
				);

			%end;
		* Fin du PMSI-MCO;

	* Pour le PMSI-RIP;
	%if &RIP. = 1 %then
		%do;

			%let domaine = RIP;

			%sejours_PMSI(
				an_deb_pmsi = &an_deb_pmsi.,
				an_fin_pmsi = &an_fin_pmsi.,
				an_crea_t = 06,
				an_fin_t = %eval(%sysfunc(YEAR(%sysfunc(TODAY()))) - 2000),
				domaine = &domaine.,
				tbl_out = &tbl_out.,
				tbl_patients = &tbl_patients.
				);

			%end;
		* Fin du PMSI-MCO;

	* Pour le PMSI-SSR;
	%if &SSR. = 1 %then
		%do;

			%let domaine = SSR;

			%sejours_PMSI(
				an_deb_pmsi = &an_deb_pmsi.,
				an_fin_pmsi = &an_fin_pmsi.,
				an_crea_t = 06,
				an_fin_t = %eval(%sysfunc(YEAR(%sysfunc(TODAY()))) - 2000),
				domaine = &domaine.,
				tbl_out = &tbl_out.,
				tbl_patients = &tbl_patients.
				);

		%end;
		* Fin du PMSI-SSR;

	* Empilage de la table temporaire � la table R�sultat;
	data &tbl_out.;
		length id_sejour $50.;
		set sejours_:;
	run;

	%arret_erreur;

	* Suppression de la table temporaire;
	proc datasets library = work memtype = data nolist;
		delete sejours_:;
	run;

%mend extract_sej_PMSI;
