/*	******************************************************************************************************************************************	*/
/*																																				*/
/*		Options de la log et param�tres du projet																								*/
/*																																				*/
/*	******************************************************************************************************************************************	*/

*	******************************************************************************************************************************************	;
*	Options de la log																															;
*	******************************************************************************************************************************************	;

option nodate nonumber notes nosymbolgen ;
ods graphics on ;

option fmtsearch = (work library formats) nofmterr ;

*	******************************************************************************************************************************************	;
*	D�finition des param�tres 																													;
*	******************************************************************************************************************************************	;

* Ann�e pour laquelle l indicateur est calcul�;
%let annee_N = 2017;
%let an_N = %sysevalf(&annee_N. - 2000);

* D�finitions des ann�es N-4, N-2, N-1 et N+1 pour les recerches dans le DCIR et le PMSI;
%let annee_4N = %sysevalf(&annee_N. - 4);
%let annee_2N = %sysevalf(&annee_N. - 2);
%let annee_1N = %sysevalf(&annee_N. - 1);
%let annee_N1 = %sysevalf(&annee_N. + 1);

* Derni�re version de la carto disponible;
%let version_carto = G6;

* Exclusion des cl�s de chainage incorrects;
* Peuvent �tre modifi�es � chaque nouvelle pseudonymisation;
%let cle_inc1 = xxxxxxxxxxxxxxxxx;
%let cle_inc2 = BXXXXXXXXXXXXXXXX;

*	******************************************************************************************************************************************	;
*	D�finition des librairies 																													;
*	******************************************************************************************************************************************	;

* Librairie contenant les tables de travail;
libname travail "/sasdata/prd/repbpcoh/data/Bases/travail";

* Librairie contenant les tables de populations;
libname pop "/sasdata/prd/repbpcoh/data/Bases/population";

* Librairie contenant les tables de r�sultats;
libname res "/sasdata/prd/repbpcoh/data/Bases/resultats";

* Librairie contenant les tables pour r�aliser le flowchart;
libname flowch "/sasdata/prd/repbpcoh/data/Bases/flowchart";

* Librairie contenant les formats;
libname formats "/sasdata/prd/repbpcoh/data/Bases/formats";

* Librairie contenant les tables de v�rifications;
libname verif "/sasdata/prd/repbpcoh/data/Bases/verifications";
