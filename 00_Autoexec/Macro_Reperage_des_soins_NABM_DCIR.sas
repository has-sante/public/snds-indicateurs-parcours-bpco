/*	******************************************************************************************************************************************	*/
/*																																				*/
/*		Macro pour rep�rer les remboursement des codes NABM souhait�s : extract_NABM_DCIR														*/
/*																																				*/
/*	******************************************************************************************************************************************	*/
/*																																				*/
/*		Arguments en entr�e :																													*/
/*																																				*/
/*			- annee_deb = la premi�re ann�e de soins (ie. filtre sur EXE_SOI_DTD) pour laquelle on souhaite rep�rer les codes					*/
/*																																				*/
/*			- annee_fin = la derni�re ann�e de soins (ie. filtre sur EXE_SOI_DTD) pour laquelle on souhaite rep�rer les codes					*/
/*																																				*/
/*			- tbl_out = le nom de la table en sortie (peut contenir un nom de librairie)														*/
/*																																				*/
/*			- tbl_codes = la table contenant les codes NABM � rep�rer (au format Oracle)														*/
/*																																				*/
/*			- tbl_patients = le nom de la table contenant la correspondance BEN_IDT_ANO <-> BEN_NR_PSA (au format Oracle)						*/
/*																																				*/
/*	******************************************************************************************************************************************	*/
/*																																				*/
/*		Tables en sortie : &tbl_out. (renseign�e en param�tres de la macro)																		*/
/*		avec s�lection des codes disponibles dans le r�f�rentiel, filtr�e sur les soins de ville uniquement, les NIR "normaux" + suppression	*/
/*		des lignes pour information																												*/
/*																																				*/
/*	******************************************************************************************************************************************	*/

%macro extract_NABM_DCIR(annee_deb=, annee_fin=, tbl_out=, tbl_codes=, tbl_patients=);

	%let annee_fin_flx = %sysevalf(&annee_fin. + 1);

	* On boucle sur les ann�es de rep�rage (en flux);
	%do Annee = &annee_deb. %to &annee_fin_flx.;

		* On boucle sur les 12 mois de l ann�e;
		%do i = 1 %to 12;

			%if &i. < 10 %then %let Mois = 0&i.;
			%else %let Mois = &i.;

			%put Donn�es de &Mois./&Annee.;

			proc sql;

				%connectora;

					CREATE TABLE tmp_extract_NABM_DCIR_&Annee._&Mois. AS
						SELECT * FROM CONNECTION TO ORACLE (
							SELECT
								pop.BEN_IDT_ANO,
								prs.EXE_SOI_DTD,
								prs.EXE_SOI_DTF,
								prs.BSE_REM_MNT,
								bio.BIO_ACT_QSN,
								bio.BIO_PRS_IDE,
								ete.ETB_EXE_FIN,
								ete.ETE_MCO_DDP,
								cod.*
							FROM &tbl_patients. pop
								INNER JOIN ER_PRS_F prs
									ON	pop.BEN_NIR_PSA = prs.BEN_NIR_PSA
								INNER JOIN ER_BIO_F bio
									ON	bio.FLX_DIS_DTD = prs.FLX_DIS_DTD 
									AND	bio.FLX_TRT_DTD = prs.FLX_TRT_DTD 
									AND	bio.FLX_EMT_TYP = prs.FLX_EMT_TYP 
									AND	bio.FLX_EMT_NUM = prs.FLX_EMT_NUM 
									AND	bio.FLX_EMT_ORD = prs.FLX_EMT_ORD 
									AND	bio.ORG_CLE_NUM = prs.ORG_CLE_NUM 
									AND	bio.DCT_ORD_NUM = prs.DCT_ORD_NUM 
									AND	bio.PRS_ORD_NUM = prs.PRS_ORD_NUM
									AND	bio.REM_TYP_AFF = prs.REM_TYP_AFF
								INNER JOIN &tbl_codes. cod
									ON bio.BIO_PRS_IDE = TRIM(cod.code_NABM)
								LEFT JOIN ER_ETE_F ete
									ON	ete.FLX_DIS_DTD = prs.FLX_DIS_DTD 
									AND	ete.FLX_TRT_DTD = prs.FLX_TRT_DTD 
									AND	ete.FLX_EMT_TYP = prs.FLX_EMT_TYP 
									AND	ete.FLX_EMT_NUM = prs.FLX_EMT_NUM 
									AND	ete.FLX_EMT_ORD = prs.FLX_EMT_ORD 
									AND	ete.ORG_CLE_NUM = prs.ORG_CLE_NUM 
									AND	ete.DCT_ORD_NUM = prs.DCT_ORD_NUM 
									AND	ete.PRS_ORD_NUM = prs.PRS_ORD_NUM
									AND	ete.REM_TYP_AFF = prs.REM_TYP_AFF
							WHERE  prs.BEN_CDI_NIR = '00' AND prs.CPL_MAJ_TOP IN (0, 1)
								AND (ete.ETE_IND_TAA NOT IN (1, 2) OR ete.ETE_IND_TAA IS NULL)
								AND prs.DPN_QLF NOT IN (71, 72) AND NOT(prs.DPN_QLF = 0 AND prs.PRS_DPN_QLP = 71)
								AND prs.FLX_DIS_DTD = TO_DATE(%str(%'&Annee.&Mois.01%'), 'YYYYMMDD')
								AND prs.EXE_SOI_DTD BETWEEN TO_DATE(%str(%'&Annee_deb.0101%'), 'YYYYMMDD') AND TO_DATE(%str(%'&Annee_fin.1231%'), 'YYYYMMDD')
							);

				disconnect from oracle;

			quit;

			%arret_erreur;

			* On empile toutes les tables mensuelles;
			%if %sysfunc(exist(&tbl_out.)) = 1 %then 
				%do;

					proc append base = &tbl_out. data = tmp_extract_NABM_DCIR_&annee._&mois.;
					run;

					%arret_erreur;

					proc delete data = tmp_extract_NABM_DCIR_&annee._&mois.;
					run; quit;

				%end;

			%if %sysfunc(exist(&tbl_out.)) = 0 %then 
				%do;

					data &tbl_out.;
						set tmp_extract_NABM_DCIR_&annee._&mois.;
					run;

					%arret_erreur;

					proc delete data = tmp_extract_NABM_DCIR_&annee._&mois.;
					run; quit;

				%end;

		%end;
		* Fin de la boucle sur les 12 mois;

	%end;
	* Fin de la boucle par ann�e;

	* On applique le programme de r�gularisations pour c�er la table finale;
	proc sql undo_policy = none;

		CREATE TABLE &tbl_out. AS
			SELECT
				BEN_IDT_ANO,
				datepart(EXE_SOI_DTD) AS date_debut length = 4 format ddmmyy10.,
				CASE	WHEN datepart(EXE_SOI_DTF) IS NULL THEN datepart(EXE_SOI_DTD)
						ELSE datepart(EXE_SOI_DTF)
						END AS date_fin length = 4 format ddmmyy10.,
				"DCIR" AS type,
				BIO_PRS_IDE,
				reperage,
				ETB_EXE_FIN,
				ETE_MCO_DDP,
				SUM(BSE_REM_MNT) AS montant,
				SUM(BIO_ACT_QSN) AS quantite
			FROM &tbl_out.
			GROUP BY 1, 2, 3, 4, 5, 6, 7, 8;

		DELETE FROM &tbl_out. WHERE montant <= 0 OR quantite < 0;

	quit;

	%arret_erreur;

%mend extract_NABM_DCIR;
