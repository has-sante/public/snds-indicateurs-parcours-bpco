/*	******************************************************************************************************************************************	*/
/*																																				*/
/*		Macro pour rep�rer les p�riodes d exon�ration d ALD contenant les codes CIM souhait�s : extract_ALD_CIM									*/
/*																																				*/
/*	******************************************************************************************************************************************	*/
/*																																				*/
/*		Arguments en entr�e :																													*/
/*																																				*/
/*			- tbl_out = le nom de la table en sortie (peut contenir un nom de librairie)														*/
/*																																				*/
/*			- tbl_codes = la table contenant les codes CIM � rep�rer (au format Oracle)															*/
/*																																				*/
/*			- tbl_patients = le nom de la table contenant la correspondance BEN_IDT_ANO <-> BEN_NR_PSA (au format Oracle)						*/
/*																																				*/
/*	******************************************************************************************************************************************	*/
/*																																				*/
/*		Tables en sortie : &out. (renseign�e en param�tres de la macro)																			*/
/*																																				*/
/*	******************************************************************************************************************************************	*/

%macro extract_ALD_CIM(tbl_out=, tbl_codes=, tbl_patients=);

	proc sql;

		%connectora;

			CREATE TABLE &tbl_out. AS SELECT * FROM CONNECTION TO ORACLE (
					SELECT
						c.BEN_IDT_ANO,
						a.IMB_ETM_NAT,
						a.IMB_ALD_DTD,
						a.IMB_ALD_DTF,
						a.INS_DTE,
						a.UPD_DTE,
						a.IMB_ALD_NUM,
						a.MED_MTF_COD,
						b.*
					FROM &tbl_patients. c, IR_IMB_R a, &tbl_codes. b
					WHERE c.BEN_NIR_PSA = a.BEN_NIR_PSA AND SUBSTR(TRIM(a.MED_MTF_COD), 1, taille) = TRIM(b.code_CIM) 
						AND IMB_ETM_NAT IN (41, 43, 45) AND a.INS_DTE < TO_DATE(%str(%'&Annee_N1.0501%'), 'YYYYMMDD')
					);

		DISCONNECT FROM ORACLE;

	quit;

	proc sort data = &tbl_out.;
		by BEN_IDT_ANO IMB_ETM_NAT IMB_ALD_NUM MED_MTF_COD INS_DTE UPD_DTE IMB_ALD_DTF descending IMB_ALD_DTD;
	run;

	data &tbl_out.;
		set &tbl_out.;
		by BEN_IDT_ANO IMB_ETM_NAT IMB_ALD_NUM MED_MTF_COD INS_DTE UPD_DTE IMB_ALD_DTF descending IMB_ALD_DTD;
		if last.MED_MTF_COD then
			output;
	run;

	data &tbl_out.;
		set &tbl_out.;
		length date_debut date_fin 4.;
		type = "ALD";
		date_debut = datepart(IMB_ALD_DTD);
		date_fin = datepart(IMB_ALD_DTF);
		format date_debut date_fin ddmmyy10.;
		drop IMB_ALD_DTD IMB_ALD_DTF;
	run;

	%arret_erreur;

%mend extract_ALD_CIM;
