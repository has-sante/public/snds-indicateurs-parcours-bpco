/*	******************************************************************************************************************************************	*/
/*																																				*/
/*		Table de valeurs contenant les codes ATC n�cessaires au calcul du score de Charlson														*/
/*																																				*/
/*	******************************************************************************************************************************************	*/

* On supprime la table si elle existe;
%suppr_table(
	lib = orauser, 
	table = codes_ATC_Charlson
	);

* On cr�e la table;
data orauser.codes_ATC_Charlson;
infile datalines delimiter = "�" dsd ;
retain classe_ATC reperage;
length classe_ATC $ 7 reperage 3.;
input classe_ATC $ reperage;
datalines;
N06DX01�5
N06DA01�5
N06DA02�5
N06DA03�5
N06DA04�5
R03AB03�6
R03AC02�6
R03AC03�6
R03AC04�6
R03AC08�6
R03AC12�6
R03AC13�6
R03AC18�6
R03AC19�6
R03AK06�6
R03AK07�6
R03AK08�6
R03AK10�6
R03AK11�6
R03AL01�6
R03AL02�6
R03AL03�6
R03AL04�6
R03AL08�6
R03AL09�6
R03BA01�6
R03BA02�6
R03BA03�6
R03BA05�6
R03BA07�6
R03BA08�6
R03BB01�6
R03BB02�6
R03BB04�6
R03BB06�6
R03BB07�6
R03BB54�6
R03BC01�6
R03BC03�6
R03CC02�6
R03CC03�6
R03CC12�6
R03DA04�6
R03DA05�6
R03DA08�6
R03DC03�6
R03DX03�6
R03DX05�6
R03DX09�6
R03DX10�6
A10AB01�10
A10AB03�10
A10AB04�10
A10AB05�10
A10AB06�10
A10AC01�10
A10AC03�10
A10AC04�10
A10AD01�10
A10AD03�10
A10AD04�10
A10AD05�10
A10AE01�10
A10AE02�10
A10AE03�10
A10AE04�10
A10AE05�10
A10AE06�10
A10AE30�10
A10AE56�10
A10BA02�10
A10BB01�10
A10BB03�10
A10BB04�10
A10BB06�10
A10BB07�10
A10BB09�10
A10BB12�10
A10BD02�10
A10BD03�10
A10BD05�10
A10BD07�10
A10BD08�10
A10BD10�10
A10BF01�10
A10BF02�10
A10BG02�10
A10BG03�10
A10BH01�10
A10BH02�10
A10BH03�10
A10BJ06�10
A10BX02�10
A10BX04�10
A10BX07�10
A10BX14�10
;
run;
