/*	******************************************************************************************************************************************	*/
/*																																				*/
/*		Table de valeurs contenant les codes CCAM n�cessaires au calcul du score de Charlson													*/
/*																																				*/
/*	******************************************************************************************************************************************	*/

* On supprime la table si elle existe;
%suppr_table(
	lib = orauser, 
	table = codes_CCAM_Charlson
	);

* On cr�e la table;
data orauser.codes_CCAM_Charlson;
infile datalines delimiter = "�" dsd ;
retain reperage code_CCAM;
length code_CCAM $ 7 reperage 3.;
input reperage code_CCAM $;
datalines;
3�EAAF002
3�EAAF900
3�EBAF001
3�EBAF006
3�EBAF010
3�EBAF011
3�ECAF001
3�ECAF004
3�ECLF003
3�ECLF004
3�ECPF001
3�ECPF002
3�ECPF005
3�EDAF001
3�EDAF003
3�EDAF005
3�EDAF006
3�EDAF010
3�EDLF004
3�EDLF005
3�EDLF006
3�EDLF007
3�EDLF008
3�EDLF013
3�EDPF001
3�EDPF004
3�EDPF005
3�EDPF006
3�EDPF009
3�EEAF002
3�EEAF004
3�EEAF006
3�EELF002
3�EEPF001
3�EFAF001
3�EFLF001
3�EFPF001
3�EGAF002
3�EGAF004
3�EGPF001
3�ENAF001
3�EPPF003
3�EZAF002
3�EZJF001
3�EZNF002
3�EZPF003
3�DGLF002
3�DGLF001
3�DHAF001
3�DHPF002
3�DHAF004
12�JVJB001
12�JVJF004
12�JVJF008
12�JVRP004
12�JVRP007
12�JVRP008
12�YYYY007
;
run;
