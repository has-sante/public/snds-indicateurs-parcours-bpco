/*	******************************************************************************************************************************************	*/
/*																																				*/
/*		Table de valeurs contenant les codes CIM10 n�cessaires � l'ensemble du projet (pour le rep�rage des hospitalisations)					*/
/*																																				*/
/*	******************************************************************************************************************************************	*/

* On cr�e la table;
data diag_codes_CIM_BPCO;
infile datalines delimiter = "�" dsd ;
retain code_CIM reperage flag_pneumo;
length code_CIM $ 7 reperage 3. flag_pneumo 3.;
input code_CIM $ reperage flag_pneumo;
datalines;
J93�11�1
J942�11�0
J86�11�0
I20�12�0
I21�12�0
I22�12�0
Z515�13�0
J450�14�0
J451�14�0
J458�14�0
J459�14�0
J46�15�0
J440�16�0
J441�16�0
J448�16�0
J449�16�0
J960�17�0
J181�18�0
J09�19�0
J10�19�0
J11�19�0
I26�20�0
I130�21�0
I132�21�0
I110�21�0
I50�21�0
I501�22�0
J12�23�0
J13�23�0
J14�23�0
J15�23�0
J16�23�0
J17�23�0
J18�23�0
J12�24�0
J13�24�0
J14�24�0
J15�24�0
J16�24�0
J17�24�0
J18�24�0
J12�25�0
J13�25�0
J14�25�0
J15�25�0
J16�25�0
J17�25�0
J18�25�0
M05�26�0
M06�26�0
M070�27�0
M071�27�0
M072�27�0
M073�27�0
M076�27�0
M45�27�0
M46�27�0
Z511�58�0
Z5101�59�0
;
run;

* On supprime la table si elle existe;
%suppr_table(
	lib = orauser, 
	table = diag_codes_CIM_BPCO
	);

* On ajoute la longueur de code_CIM;
data orauser.diag_codes_CIM_BPCO;
	set diag_codes_CIM_BPCO;
	taille = length(compress(code_CIM));
run;

proc delete data = diag_codes_CIM_BPCO;
run;
