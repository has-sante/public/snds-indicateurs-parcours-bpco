/*	******************************************************************************************************************************************	*/
/*																																				*/
/*		Table de valeurs contenant les codes GHM n�cessaires � l'ensemble du projet																*/
/*																																				*/
/*	******************************************************************************************************************************************	*/

* On cr�e la table;
data codes_GHM_BPCO;
infile datalines delimiter = "�" dsd ;
retain code_GHM reperage;
length code_GHM $ 5 reperage 3.;
input code_GHM $ reperage;
datalines;
05C04�28
05C05�28
05C02�29
05C03�29
05K05�30
05K06�30
28Z07�58
28Z10�59
28Z11�59
28Z18�59
28Z23�59
28Z24�59
28Z25�59
;
run;

* On supprime la table si elle existe;
%suppr_table(
	lib = orauser, 
	table = codes_GHM_BPCO
	);

* On ajoute la longueur de code_GHM;
data orauser.codes_GHM_BPCO;
	set codes_GHM_BPCO;
	taille = length(compress(code_GHM));
run;

proc delete data = codes_GHM_BPCO;
run;
