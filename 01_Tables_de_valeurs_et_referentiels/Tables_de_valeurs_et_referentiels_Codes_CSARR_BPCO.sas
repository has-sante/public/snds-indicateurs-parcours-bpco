/*	******************************************************************************************************************************************	*/
/*																																				*/
/*		Table de valeurs contenant les codes CSARR n�cessaires � l'ensemble du projet															*/
/*																																				*/
/*	******************************************************************************************************************************************	*/

* On supprime la table si elle existe;
%suppr_table(
	lib = orauser, 
	table = codes_CSARR_BPCO
	);

* On cr�e la table;
data orauser.codes_CSARR_BPCO;
infile datalines delimiter = "�" dsd ;
retain code_CSARR reperage;
length code_CSARR $ 7 reperage 3.;
input code_CSARR $ reperage;
datalines;
DKQ+008�7
EQQ+206�7
EQR+175�7
EQR+275�7
ANR+036�7
GLR+074�7
GLR+077�7
GLR+093�7
GLR+131�7
GLR+139�7
GLR+167�7
GLR+169�7
GLR+170�7
GLR+186�7
GLR+224�7
GLR+226�7
GLR+236�7
GLR+285�7
PCQ+179�7
PCR+025�7
PEQ+266�7
NKR+059�7
NKR+085�7
PER+118�7
PER+285�7
DKR+013�7
DKR+016�7
DKR+061�7
DKR+118�7
DKR+181�7
DKR+182�7
DKR+194�7
DKR+195�7
DKR+200�7
DKR+247�7
DKR+254�7
DKR+291�7
PCM+064�7
PCM+253�7
PCM+262�7
PCM+283�7
PCR+004�7
PCR+272�7
ZZC+028�7
ZZC+255�7
ZZQ+027�7
ZZQ+261�7
ZZR+227�7
ZZR+238�7
ZZQ+112�7
ZZQ+192�7
ZZR+020�7
ZZR+293�7
GLQ+043�7
GLQ+175�7
GLR+206�7
;
run;
