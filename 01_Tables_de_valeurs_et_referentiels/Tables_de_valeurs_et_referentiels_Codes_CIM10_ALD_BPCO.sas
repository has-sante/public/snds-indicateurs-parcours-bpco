/*	******************************************************************************************************************************************	*/
/*																																				*/
/*		Table de valeurs contenant les codes CIM10 n�cessaires � l'ensemble du projet (pour le rep�rage des ALD)								*/
/*																																				*/
/*	******************************************************************************************************************************************	*/

* On cr�e la table;
data ALD_codes_CIM_BPCO;
infile datalines delimiter = "�" dsd ;
retain code_CIM reperage;
length code_CIM $ 7 reperage 3.;
input code_CIM $ reperage;
datalines;
F00�50
G30�50
F01�51
F02�51
F03�51
J44�52
J45�53
E84�54
M05�56
M06�56
M070�57
M071�57
M072�57
M073�57
M076�57
M45�57
M46�57
;
run;

* On supprime la table si elle existe;
%suppr_table(
	lib = orauser, 
	table = ALD_codes_CIM_BPCO
	);

* On ajoute la longueur de code_CIM;
data orauser.ALD_codes_CIM_BPCO;
	set ALD_codes_CIM_BPCO;
	taille = length(compress(code_CIM));
run;

proc delete data = ALD_codes_CIM_BPCO;
run;
