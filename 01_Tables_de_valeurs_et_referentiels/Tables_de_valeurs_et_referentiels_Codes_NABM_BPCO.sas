/*	******************************************************************************************************************************************	*/
/*																																				*/
/*		Table de valeurs contenant les codes NABM n�cessaires � l'ensemble du projet															*/
/*																																				*/
/*	******************************************************************************************************************************************	*/

* On supprime la table si elle existe;
%suppr_table(
	lib = orauser, 
	table = codes_NABM_BPCO
	);

* On cr�e la table;
data orauser.codes_NABM_BPCO;
infile datalines delimiter = "�" dsd ;
retain code_NABM reperage;
length code_NABM $ 4 reperage 3.;
input code_NABM $ reperage;
datalines;
0999�31
1612�31
0571�31
;
run;
