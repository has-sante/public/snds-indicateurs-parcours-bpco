/*	******************************************************************************************************************************************	*/
/*																																				*/
/*		Table de valeurs contenant les codes GHM n�cessaires au calcul du score de Charlson														*/
/*																																				*/
/*	******************************************************************************************************************************************	*/

* On cr�e la table;
data codes_GHM_Charlson;
infile datalines delimiter = "�" dsd ;
retain reperage code_GHM;
length code_GHM $ 7 reperage 3.;
input reperage code_GHM $;
datalines;
12�28Z04Z
;
run;

* On supprime la table si elle existe;
%suppr_table(
	lib = orauser, 
	table = codes_GHM_Charlson
	);

* On ajoute la longueur de code_GHM;
data orauser.codes_GHM_Charlson;
	set codes_GHM_Charlson;
	length taille 3.;
	taille = length(compress(code_GHM));
run;

proc delete data = codes_GHM_Charlson;
run;
