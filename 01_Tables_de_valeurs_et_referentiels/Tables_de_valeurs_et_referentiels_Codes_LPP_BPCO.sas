/*	******************************************************************************************************************************************	*/
/*																																				*/
/*		Table de valeurs contenant les codes LPP n�cessaires � l'ensemble du projet																*/
/*																																				*/
/*	******************************************************************************************************************************************	*/

* On supprime la table si elle existe;
%suppr_table(
	lib = orauser, 
	table = codes_LPP_BPCO
	);

* On cr�e la table;
data orauser.codes_LPP_BPCO;
infile datalines delimiter = "�" dsd ;
length code_LPP 5. reperage 3.;
input code_LPP reperage;
datalines;
1136581�5
1130220�5
1163030�6
1196270�6
;
run;
