/*	******************************************************************************************************************************************	*/
/*																																				*/
/*		Table de valeurs contenant les codes ATC n�cessaires � l'ensemble du projet																*/
/*																																				*/
/*	******************************************************************************************************************************************	*/

* On supprime la table si elle existe;
%suppr_table(
	lib = orauser, 
	table = codes_ATC_BPCO
	);

* On cr�e la table;
data orauser.codes_ATC_BPCO;
infile datalines delimiter = "�" dsd ;
retain classe_ATC reperage;
length classe_ATC $ 7 reperage 3.;
input classe_ATC $ reperage;
datalines;
N07BA01�35
N07BA03�35
N06AX12�35
J01CA04�36
J01CR02�36
J01FG01�36
J01FA07�36
J01FA02�36
J01FA10�36
J01DC02�36
J01DD13�36
J01DD04�36
J01MA12�36
J01DD08�36
J01FA09�36
J01MA02�36
J01FA15�36
R03DX05�37
R03DX09�38
R03DX08�38
R03DC03�39
J07BB01�40
J07BB02�40
J07BB03�40
J07BB02�40
J07BB02�40
R03BA01�41
R03BA02�41
R03BA05�41
R03AC02�42
R03AC03�42
R03BB01�43
R03AL01�44
R03AC13�45
R03AC18�45
R03AC19�45
R03AC12�45
R03BB06�46
R03BB04�46
R03BB07�46
R03AL04�47
R03AL06�47
R03BB54�47
R03AL03�47
R03AK08�48
R03AK06�48
R03AK07�48
R03AK10�48
R03AL09�49
R03AL08�49
;
run;
