/*	******************************************************************************************************************************************	*/
/*																																				*/
/*		Table de valeurs contenant les codes GME n�cessaires � l'ensemble du projet																*/
/*																																				*/
/*	******************************************************************************************************************************************	*/

* On supprime la table si elle existe;
%suppr_table(
	lib = orauser, 
	table = codes_GME_BPCO
	);

* On cr�e la table;
data orauser.codes_GME_BPCO;
infile datalines delimiter = "�" dsd ;
retain code_GME reperage;
length code_GME $ 2 reperage 3.;
input code_GME $ reperage;
datalines;
04�7
;
run;
