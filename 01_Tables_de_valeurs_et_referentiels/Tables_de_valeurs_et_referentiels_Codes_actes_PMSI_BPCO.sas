/*	******************************************************************************************************************************************	*/
/*																																				*/
/*		Table de valeurs contenant les codes actes n�cessaires � l ensemble du projet (pour le rep�rage dans le PMSI)							*/
/*																																				*/
/*	******************************************************************************************************************************************	*/

* On cr�e la table;
data codes_actes_BPCO;
infile datalines delimiter = "�" dsd ;
retain code_acte reperage;
length code_acte $ 3 reperage 3.;
input code_acte $ reperage;
datalines;
AMK�7
AMC�7
AMS�7
BPC�7
TTE�32
TDT�32
APU�32
APC�32
CCX�32
GS�32
G�32
C�32
CS�32
CA�32
TCP�32
TEP�32
TLC�32
TLE�32
TC�32
TCG�32
TE1�32
TE2�32
VGS�32
VG�32
V�32
VS�32
VL�32
APV�32
VA�32
VU�32
ADA�32
ADC�32
ADE�32
ATM�32
ADI�32
TTE�33
TDT�33
CCX�33
GS�33
G�33
C�33
CS�33
CA�33
TCP�33
TEP�33
TLC�33
TLE�33
TC�33
TCG�33
TE1�33
TE2�33
VGS�33
VG�33
V�33
VS�33
VL�33
VA�33
VU�33
ADA�33
ADC�33
ADE�33
ATM�33
ADI�33
;
run;

* On supprime la table si elle existe;
%suppr_table(
	lib = orauser, 
	table = codes_actes_BPCO
	);

* On ajoute la longueur de code_CIM;
data orauser.codes_actes_BPCO;
	set codes_actes_BPCO;
	taille = length(compress(code_acte));
run;

%suppr_table(
	lib = work, 
	table = codes_actes_BPCO
	);
