/*	******************************************************************************************************************************************	*/
/*																																				*/
/*		Patient CMUc																															*/
/*																																				*/
/*	******************************************************************************************************************************************	*/

*	******************************************************************************************************************************************;
*	On r�cup�re l information de la CMUc : si le patient a au moins 1 remboursement avec BEN_CMU_TOP = 1 dans l ann�e;

%macro CMUC_DCIR(annee_deb=, annee_fin=);

	%let annee_fin_flx = %sysevalf(&annee_fin. + 1);

	* On boucle sur les ann�es de rep�rage (en flux);
	%do Annee = &annee_deb. %to &annee_fin_flx.;

		%put annee_deb = &Annee_deb.;
		* On boucle sur les 12 mois de l ann�e;
		%do i = 1 %to 12;

			%if &i. < 10 %then %let Mois = 0&i.;
			%else %let Mois = &i.;

			%put Donn�es de &Mois./&Annee.;

			proc sql undo_policy = none;

				%connectora;

					CREATE TABLE patients_CMUC_&Annee._&Mois. AS
						SELECT * FROM CONNECTION TO ORACLE (
							SELECT DISTINCT
								a.BEN_IDT_ANO,
								1 AS BEN_CMU_TOP
							FROM corresp_id_patient a
								INNER JOIN ER_PRS_F b
									ON a.BEN_NIR_PSA = b.BEN_NIR_PSA
							WHERE b.BEN_CDI_NIR = '00' AND b.FLX_DIS_DTD = TO_DATE(%str(%'&Annee.&Mois.01%'), 'YYYYMMDD')							
								AND b.EXE_SOI_DTD BETWEEN TO_DATE(%str(%'&Annee_deb.0101%'), 'YYYYMMDD') 
									AND TO_DATE(%str(%'&Annee_deb.1231%'), 'YYYYMMDD')
								AND BEN_CMU_TOP = '1' 
							);

				disconnect from oracle;

			quit;

			%arret_erreur;

		%end;
		* Fin de la boucle sur les 12 mois;

	%end;
	* Fin de la boucle par ann�e;

	* On empile toutes les tables mensuelles;
	data patients_CMUC;
		set patients_CMUC_:;
	run;

	%arret_erreur;

	proc datasets library = work memtype = data nolist;
		delete patients_CMUC_:;
	run;

%mend CMUC_DCIR;

%CMUC_DCIR(
	annee_deb = &Annee_N., 
	annee_fin = &Annee_N.
	);

*	******************************************************************************************************************************************;
*	On ajoute l information de la CMUc dans la table de population;
proc sql;

	ALTER TABLE res.T_INDI_BPCO_&an_N. ADD BEN_CMU_TOP INT length = 3;

	UPDATE res.T_INDI_BPCO_&an_N. a
		SET BEN_CMU_TOP = CASE	WHEN BEN_IDT_ANO IN (SELECT BEN_IDT_ANO FROM patients_CMUC) THEN 1
								ELSE 0
								END;

quit;

*	******************************************************************************************************************************************;
*	On supprime les tables temporaires;

proc delete data = patients_CMUC;
run; quit;

*	******************************************************************************************************************************************;
*	V�rifications;

%proc_freq(
	in_tbl = res.T_INDI_BPCO_&an_N., 
	out_tbl = _CMUC, 
	list_var_in = BEN_CMU_TOP, 
	list_var_out = BEN_CMU_TOP Frequency
	);
