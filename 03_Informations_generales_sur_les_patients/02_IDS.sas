/*	******************************************************************************************************************************************	*/
/*																																				*/
/*		Indice de d�faveur social																												*/
/*																																				*/
/*	******************************************************************************************************************************************	*/

*	******************************************************************************************************************************************;
*	On ajoute l information de l IDS dans la table de population;

data patient_depcom;
	set res.T_INDI_BPCO_&an_N. (keep = BEN_IDT_ANO BEN_RES_DPT BEN_RES_COM);
	length code_postal $5;
	code_postal = SUBSTR(BEN_RES_DPT, 2, 2)||BEN_RES_COM;
run;

proc sql undo_policy = none;

	CREATE TABLE patient_depcom AS
		SELECT DISTINCT
			a.BEN_IDT_ANO,
			b.code_insee
		FROM patient_depcom a
			INNER JOIN rfcommun.T_FIN_GEO_LOC_FRANCE b
				ON a.code_postal = b.code_jointure
		ORDER BY code_insee, BEN_IDT_ANO;

quit;

proc sort data = consopat.DEFA_UU2013 out = DEFA_UU2013 (keep = depcom FDEP13 quintile_pop);
	by depcom;
run;

data patient_depcom (keep = BEN_IDT_ANO code_insee FDEP13 quintile_pop);
	merge	patient_depcom (in = a)
			DEFA_UU2013 (in = b rename = (depcom = code_insee));
	by code_insee;
	if a;
run;

*	******************************************************************************************************************************************;
*	On ajoute l information de l IDS dans la table de population;

proc sort data = patient_depcom nodupkey;
	by BEN_IDT_ANO;
run;

proc sort data = res.T_INDI_BPCO_&an_N. force;
	by BEN_IDT_ANO;
run;

data res.T_INDI_BPCO_&an_N.;
	merge	res.T_INDI_BPCO_&an_N. (in = a)
			patient_depcom (in = b);
	by BEN_IDT_ANO;
	if a;
run;

* On recr�e l index qu on a cass� avec le proc sort;
proc sql;

	CREATE INDEX BEN_IDT_ANO ON res.T_INDI_BPCO_&an_N. (BEN_IDT_ANO);

quit;

*	******************************************************************************************************************************************;
*	On supprime la table temporaire;

proc delete data = patient_depcom DEFA_UU2013;
run; quit;

*	******************************************************************************************************************************************;
*	V�rifications;

data verif;
	set res.T_INDI_BPCO_&an_N.;
	format FDEP_renseigne f_oui_non.;
	if FDEP13 = . then
		FDEP_renseigne = 0;
	else 
		FDEP_renseigne = 1;
run;
		
%proc_freq(
	in_tbl = verif, 
	out_tbl = _FDEP13, 
	list_var_in = FDEP_renseigne, 
	list_var_out = FDEP_renseigne Frequency
	);

proc delete data = verif;
run; quit;
