/*	******************************************************************************************************************************************	*/
/*																																				*/
/*		R�cup�ration des donn�es d'ALD																											*/
/*																																				*/
/*	******************************************************************************************************************************************	*/

*	******************************************************************************************************************************************;
*	On r�cup�re la ligne concernant l info maximale pour l ann�e d inclusion;

proc sql undo_policy = none;

	%connectora;

		CREATE TABLE histo_ALD_&annee_N. AS
			SELECT DISTINCT * FROM CONNECTION TO ORACLE (
				SELECT DISTINCT
					a.BEN_IDT_ANO,
					b.IMB_ETM_NAT,
					b.IMB_ALD_DTD,
					b.IMB_ALD_DTF,
					b.INS_DTE,
					b.UPD_DTE,
					b.IMB_ALD_NUM,
					b.MED_MTF_COD,
					CASE	WHEN SUBSTR(b.MED_MTF_COD, 1, 3) BETWEEN 'A00' AND 'B99' THEN 1
							WHEN SUBSTR(b.MED_MTF_COD, 1, 3) BETWEEN 'C00' AND 'D48' THEN 2
							WHEN SUBSTR(b.MED_MTF_COD, 1, 3) BETWEEN 'D50' AND 'D89' THEN 3
							WHEN SUBSTR(b.MED_MTF_COD, 1, 3) BETWEEN 'E00' AND 'E90' THEN 4
							WHEN SUBSTR(b.MED_MTF_COD, 1, 3) BETWEEN 'F00' AND 'F99' THEN 5
							WHEN SUBSTR(b.MED_MTF_COD, 1, 3) BETWEEN 'G00' AND 'G99' THEN 6
							WHEN SUBSTR(b.MED_MTF_COD, 1, 3) BETWEEN 'H00' AND 'H59' THEN 7
							WHEN SUBSTR(b.MED_MTF_COD, 1, 3) BETWEEN 'H60' AND 'H95' THEN 8
							WHEN SUBSTR(b.MED_MTF_COD, 1, 3) BETWEEN 'I00' AND 'I99' THEN 9
							WHEN SUBSTR(b.MED_MTF_COD, 1, 3) BETWEEN 'J00' AND 'J99' THEN 10
							WHEN SUBSTR(b.MED_MTF_COD, 1, 3) BETWEEN 'K00' AND 'K93' THEN 11
							WHEN SUBSTR(b.MED_MTF_COD, 1, 3) BETWEEN 'L00' AND 'L99' THEN 12
							WHEN SUBSTR(b.MED_MTF_COD, 1, 3) BETWEEN 'M00' AND 'M99' THEN 13
							WHEN SUBSTR(b.MED_MTF_COD, 1, 3) BETWEEN 'N00' AND 'N99' THEN 14
							WHEN SUBSTR(b.MED_MTF_COD, 1, 3) BETWEEN 'O00' AND 'O99' THEN 15
							WHEN SUBSTR(b.MED_MTF_COD, 1, 3) BETWEEN 'P00' AND 'P94' THEN 16
							WHEN SUBSTR(b.MED_MTF_COD, 1, 3) BETWEEN 'Q00' AND 'Q99' THEN 17
							WHEN SUBSTR(b.MED_MTF_COD, 1, 3) BETWEEN 'R00' AND 'R99' THEN 18
							WHEN SUBSTR(b.MED_MTF_COD, 1, 3) BETWEEN 'S00' AND 'T98' THEN 19
							WHEN SUBSTR(b.MED_MTF_COD, 1, 3) BETWEEN 'V01' AND 'Y98' THEN 20
							WHEN SUBSTR(b.MED_MTF_COD, 1, 3) BETWEEN 'Z00' AND 'Z99' THEN 21
							WHEN SUBSTR(b.MED_MTF_COD, 1, 3) BETWEEN 'U00' AND 'U99' THEN 22
							END AS Chapitre
				FROM corresp_id_patient a
					INNER JOIN IR_IMB_R b
						ON a.BEN_NIR_PSA = b.BEN_NIR_PSA
				WHERE IMB_ETM_NAT IN (41, 43, 45) AND b.INS_DTE < TO_DATE(%str(%'&Annee_N1.0501%'), 'YYYYMMDD')
				);

	disconnect from oracle;

quit; 

proc sort data = histo_ALD_&annee_N.;
		by BEN_IDT_ANO IMB_ETM_NAT IMB_ALD_NUM MED_MTF_COD INS_DTE UPD_DTE IMB_ALD_DTF descending IMB_ALD_DTD;
run;

data histo_ALD_&annee_N.;
	set histo_ALD_&annee_N.;
	by BEN_IDT_ANO IMB_ETM_NAT IMB_ALD_NUM MED_MTF_COD INS_DTE UPD_DTE IMB_ALD_DTF descending IMB_ALD_DTD;
	if last.MED_MTF_COD then
		output;
run;

data histo_ALD_&annee_N. (rename = (IMB_ALD_DTD2 = IMB_ALD_DTD IMB_ALD_DTF2 = IMB_ALD_DTF));
	set histo_ALD_&annee_N.;
	length IMB_ALD_DTD2 IMB_ALD_DTF2 4.;
	IMB_ALD_DTD2 = datepart(IMB_ALD_DTD);
	IMB_ALD_DTF2 = datepart(IMB_ALD_DTF);
	format IMB_ALD_DTD2 IMB_ALD_DTF2 ddmmyy10.;
	drop IMB_ALD_DTD IMB_ALD_DTF;
	if IMB_ALD_DTD2 <= "31DEC&Annee_N."d and (IMB_ALD_DTF2 = "01JAN1600"d or IMB_ALD_DTF2 >= "01JAN&Annee_N."d) then
		output;
run;

*	******************************************************************************************************************************************;
*	On r�cup�re une ligne par patient et par chapitre;

proc sort data = histo_ALD_&annee_N. out = chapitre_&annee_N. (keep = BEN_IDT_ANO Chapitre) nodupkey;
	by BEN_IDT_ANO Chapitre;
run;

* On ajoute l information dans la table de population;
%macro ajout_chapitres_ALD;

	%do ALD = 1 %to 22;

		%put Ajout de l ALD no &ALD. ;

		proc sql;

			ALTER TABLE res.T_INDI_BPCO_&an_N. ADD ALD_chapitre_&ALD. INT length = 3;

			UPDATE res.T_INDI_BPCO_&an_N.
				SET ALD_chapitre_&ALD. =
					CASE	WHEN BEN_IDT_ANO IN (SELECT BEN_IDT_ANO FROM chapitre_&annee_N. WHERE Chapitre = &ALD.) THEN 1
							ELSE 0
							END;

		quit;

	%end;

%mend ajout_chapitres_ALD;
%ajout_chapitres_ALD;

*	******************************************************************************************************************************************;
*	On ajoute l information d au moins une ALD dans l ann�e dans la table de population;

proc sql;

	ALTER TABLE res.T_INDI_BPCO_&an_N. ADD ALD INT length = 3;

	UPDATE res.T_INDI_BPCO_&an_N.
		SET ALD =
			CASE	WHEN BEN_IDT_ANO IN (SELECT BEN_IDT_ANO FROM histo_ALD_&annee_N.) THEN 1
					ELSE 0
					END;

quit;

*	******************************************************************************************************************************************;
*	On supprime les tables temporaires;

proc datasets library = work memtype = data nolist;
	delete histo_ALD_&annee_N. chapitre_&annee_N.;
run; quit;

*	******************************************************************************************************************************************;
*	V�rifications;

* On ne passe pas par la macro car trop de variables;
proc sql;

	CREATE TABLE verif._0&tmp_num_tab._ALD AS
		SELECT 
			ALD,
			ALD_Chapitre_1,
			ALD_Chapitre_2,
			ALD_Chapitre_3,
			ALD_Chapitre_4,
			ALD_Chapitre_5,
			ALD_Chapitre_6,
			ALD_Chapitre_7,
			ALD_Chapitre_8,
			ALD_Chapitre_9,
			ALD_Chapitre_10,
			ALD_Chapitre_11,
			ALD_Chapitre_12,
			ALD_Chapitre_13,
			ALD_Chapitre_14,
			ALD_Chapitre_15,
			ALD_Chapitre_16,
			ALD_Chapitre_17,
			ALD_Chapitre_18,
			ALD_Chapitre_19,
			ALD_Chapitre_20,
			ALD_Chapitre_21,
			ALD_Chapitre_22,
			COUNT(*) AS Frequency
		FROM res.T_INDI_BPCO_&an_N.
		GROUP BY ALD, ALD_Chapitre_1, ALD_Chapitre_2, ALD_Chapitre_3, ALD_Chapitre_4, ALD_Chapitre_5, ALD_Chapitre_6, ALD_Chapitre_7,
			ALD_Chapitre_8, ALD_Chapitre_9, ALD_Chapitre_10, ALD_Chapitre_11, ALD_Chapitre_12, ALD_Chapitre_13, ALD_Chapitre_14,
			ALD_Chapitre_15, ALD_Chapitre_16, ALD_Chapitre_17, ALD_Chapitre_18, ALD_Chapitre_19, ALD_Chapitre_20, ALD_Chapitre_21,
			ALD_Chapitre_22;

quit;

proc sql noprint; SELECT SUM(Frequency) INTO: nb_tot FROM verif._0&tmp_num_tab._ALD; quit;

data verif._0&tmp_num_tab._ALD;
	set verif._0&tmp_num_tab._ALD;
	length percent 4.;
	format percent nlpct7.1 Frequency commafr_0ch.;
	percent = Frequency/&nb_tot.;
run;

%let tmp_num_tab = %sysevalf(&tmp_num_tab. + 1);

%macro verif_ALD;

	%do i = 1 %to 22;
				
		%proc_freq(
			in_tbl = res.T_INDI_BPCO_&an_N., 
			out_tbl = _ALD&i., 
			list_var_in = ALD_Chapitre_&i., 
			list_var_out = ALD_Chapitre_&i. Frequency
			);

	%end;

%mend verif_ALD;
%verif_ALD;
