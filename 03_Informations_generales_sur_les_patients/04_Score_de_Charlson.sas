/*	******************************************************************************************************************************************	*/
/*																																				*/
/*		Calcul du score de Charlson																												*/
/*																																				*/
/*	******************************************************************************************************************************************	*/

*	******************************************************************************************************************************************;
*	R�cup�ration des soins - Actes CCAM;

* On appelle la macro pour le rep�rage dans le DCIR;
%suppr_table(
	lib = work, 
	table = reperage_CCAM_sdv_Charlson
	);

%extract_CCAM_DCIR(
	annee_deb = &annee_1N., 
	annee_fin = &annee_N., 
	tbl_out = reperage_CCAM_sdv_Charlson, 
	tbl_codes = codes_CCAM_Charlson,
	tbl_patients = corresp_id_patient
	);
	
* On appelle la macro pour le rep�rage dans le PMSI;
%suppr_table(
	lib = work, 
	table = reperage_CCAM_PMSI_Charlson
	);

%extract_CCAM_PMSI(
	annee_deb = &annee_1N.,
	annee_fin = &annee_N., 
	HAD = 1, 
	MCO = 1, 
	RIP = 1, 
	SSR = 1, 
	tbl_out = reperage_CCAM_PMSI_Charlson, 
	tbl_codes = codes_CCAM_Charlson,
	tbl_patients = corresp_id_patient
	);
	
* On concat�ne dans une table finale;
%suppr_table(
	lib = travail, 
	table = reperage_CCAM_Charlson
	);

data travail.reperage_CCAM_Charlson;
	length type $12.;
	set	reperage_CCAM_sdv_Charlson
		reperage_CCAM_PMSI_Charlson;
run;

proc delete data = reperage_CCAM_sdv_Charlson reperage_CCAM_PMSI_Charlson;
run; quit;

*	******************************************************************************************************************************************;
*	R�cup�ration des soins - Codes CIP & UCD;

* On appelle la macro pour le rep�rage dans le DCIR - CIP;
%suppr_table(
	lib = work, 
	table = reperage_CIP_sdv_Charlson
	);

%extract_CIP(
	annee_deb = &annee_1N.,
	annee_fin = &annee_N., 
	tbl_out = reperage_CIP_sdv_Charlson, 
	tbl_codes = codes_ATC_Charlson,
	tbl_patients = corresp_id_patient
	);

* On appelle la macro pour le rep�rage dans le DCIR - UCD;
%suppr_table(
	lib = work, 
	table = reperage_UCD_sdv_Charlson
	);

%extract_UCD_DCIR(
	annee_deb = &annee_1N.,
	annee_fin = &annee_N., 
	tbl_out = reperage_UCD_sdv_Charlson, 
	tbl_codes = codes_ATC_Charlson,
	tbl_patients = corresp_id_patient
	);

* On appelle la macro pour le rep�rage dans le PMSI;
%suppr_table(
	lib = work, 
	table = reperage_UCD_PMSI_Charlson
	);

%extract_UCD_PMSI(
	annee_deb = &annee_1N.,
	annee_fin = &annee_N., 
	HAD = 1, 
	MCO = 1, 
	RIP = 1, 
	SSR = 1, 
	tbl_out = reperage_UCD_PMSI_Charlson, 
	tbl_codes = codes_ATC_Charlson,
	tbl_patients = corresp_id_patient
	);
	
* On concat�ne dans une table finale;
%suppr_table(
	lib = travail, 
	table = reperage_ATC_Charlson
	);

data travail.reperage_ATC_Charlson;
	length type $12.;
	set	reperage_CIP_sdv_Charlson
		reperage_UCD_sdv_Charlson
		reperage_UCD_PMSI_Charlson;
run;

proc delete data = reperage_CIP_sdv_Charlson reperage_UCD_sdv_Charlson reperage_UCD_PMSI_Charlson;
run; quit;

*	**************************************************************************************************************************************;
*	R�cup�ration des soins - Codes CIM10;

* Dans les ALD;
%suppr_table(
	lib = work, 
	table = reperage_CIM10_ALD_Charlson
	);

%extract_ALD_CIM(
	tbl_out = reperage_CIM10_ALD_Charlson,
	tbl_codes = codes_CIM_Charlson,
	tbl_patients = corresp_id_patient
	);



* Dans les hospitalisations;
%suppr_table(
	lib = work, 
	table = reperage_CIM10_PMSI_Charlson
	);

%extract_CIM10_PMSI(
	annee_deb = &annee_1N., 
	annee_fin = &annee_N., 
	HAD_DP = 0,
	HAD_DAS = 0, 
	HAD_MPP = 0, 
	HAD_MPA = 0, 
	MCO_DP = 1, 
	MCO_DR = 1, 
	MCO_DAS = 1, 
	MCO_DP_UM = 1, 
	MCO_DR_UM = 1,
	SSR_FP = 0, 
	SSR_MPP = 0, 
	SSR_AE = 0, 
	SSR_DAS = 0, 
	tbl_out = reperage_CIM10_PMSI_Charlson, 
	tbl_codes = codes_CIM_Charlson,
	tbl_patients = corresp_id_patient
	);

data travail.reperage_CIM10_Charlson ;
	length type $12.;
	set	reperage_CIM10_ALD_Charlson (in = a)
		reperage_CIM10_PMSI_Charlson (in = b);
	if b then 
		source = "PMSI";
	if a then
		source = "ALD";
run;

proc delete data = reperage_CIM10_ALD_Charlson reperage_CIM10_PMSI_Charlson;
run; quit;

*	******************************************************************************************************************************************;
*	R�cup�ration des soins - GHM;

%suppr_table(
	lib = travail, 
	table = reperage_GHM_Charlson
	);

%extract_GHM_PMSI(
	annee_deb = &annee_1N., 
	annee_fin = &annee_N., 
	tbl_out = travail.reperage_GHM_Charlson, 
	tbl_codes = codes_GHM_Charlson,
	tbl_patients = corresp_id_patient
	);

*	******************************************************************************************************************************************;
*	� partir des donn�es rep�r�es, s�lectionner les lignes pour lesquels les soins ont lieu dans l ann�e pr�c�dant le 1er janvier de l ann�e 
	de rep�rage;

* Actes CCAM;
data travail.reperage_CCAM_Charlson;
	set travail.reperage_CCAM_Charlson (where = (
			intnx("year", "01JAN&Annee_N."d, -1, 'same') <= date_debut <= "01JAN&Annee_N."d
		or intnx("year", "01JAN&Annee_N."d, -1, 'same') <= date_fin <= "01JAN&Annee_N."d
		or date_debut < intnx("year", "01JAN&Annee_N."d, -1, 'same') and date_fin > "01JAN&Annee_N."d));
run;

* Codes CIM 10;
data travail.reperage_CIM10_Charlson;
	set travail.reperage_CIM10_Charlson (where = ((source = "PMSI" and
			intnx("year", "01JAN&Annee_N."d, -1, 'same') <= date_debut <= "01JAN&Annee_N."d
		or intnx("year", "01JAN&Annee_N."d, -1, 'same') <= date_fin <= "01JAN&Annee_N."d
		or date_debut < intnx("year", "01JAN&Annee_N."d, -1, 'same') and date_fin > "01JAN&Annee_N."d) or (source = "ALD" and
		date_debut <= intnx("year", "31DEC&Annee_N."d, -1, 'same') and (date_fin = "01JAN1600"d or date_fin is null
		or date_fin >= intnx("year", "01JAN&Annee_N."d, -1, 'same')))));
run;

* GHM;
data travail.reperage_GHM_Charlson;
	set travail.reperage_GHM_Charlson (where = (
			intnx("year", "01JAN&Annee_N."d, -1, 'same') <= date_debut <= "01JAN&Annee_N."d
		or intnx("year", "01JAN&Annee_N."d, -1, 'same') <= date_fin <= "01JAN&Annee_N."d
		or date_debut < intnx("year", "01JAN&Annee_N."d, -1, 'same') and date_fin > "01JAN&Annee_N."d));
run;

* Codes CIP & UCD;
data travail.reperage_ATC_Charlson;
	set travail.reperage_ATC_Charlson (where = (
			intnx("year", "01JAN&Annee_N."d, -1, 'same') <= date_debut <= "01JAN&Annee_N."d
		or intnx("year", "01JAN&Annee_N."d, -1, 'same') <= date_fin <= "01JAN&Annee_N."d
		or date_debut < intnx("year", "01JAN&Annee_N."d, -1, 'same') and date_fin > "01JAN&Annee_N."d));
run;

*	******************************************************************************************************************************************;
*	Pour les m�dicaments, on regarde les nombres de d�livrances;

* Pour les traitements de la d�mence, il faut au moins 3 d�livrances;
proc sql;

	CREATE TABLE travail.Charlson_DCIR_demence AS
		SELECT *
		FROM travail.reperage_ATC_Charlson
		WHERE reperage = 5
		GROUP BY BEN_IDT_ANO
		HAVING COUNT(DISTINCT date_debut) >= 3;

quit;

* Pour les traitements de reperage pulmonaire chronique, il faut au moins 3 d�livrances;
proc sql;

	CREATE TABLE travail.Charlson_DCIR_pulmonaire AS
		SELECT *
		FROM travail.reperage_ATC_Charlson
		WHERE reperage = 6
		GROUP BY BEN_IDT_ANO
		HAVING COUNT(DISTINCT date_debut) >= 3;

quit;

* Pour les traitements de diab�te sans complication, il faut au moins 3 d�livrances (ou 2 en cas de grand conditionnement);
proc sql;

	CREATE TABLE travail.Charlson_DCIR_diabete2 AS
		SELECT *
		FROM travail.reperage_ATC_Charlson
		WHERE PHA_CND_TOP = "GC" AND reperage = 10
		GROUP BY BEN_IDT_ANO
		HAVING COUNT(DISTINCT date_debut) >= 2;

	CREATE TABLE travail.Charlson_DCIR_diabete3 AS
		SELECT *
		FROM travail.reperage_ATC_Charlson
		WHERE PHA_CND_TOP NE "GC" AND reperage = 10
		GROUP BY BEN_IDT_ANO
		HAVING COUNT(DISTINCT date_debut) >= 3;

quit;

*	******************************************************************************************************************************************;
*	Cr�ation de la table contenant le score de Charlson;

data travail.reperages_Charlson;
	set	travail.reperage_CCAM_Charlson (keep = BEN_IDT_ANO reperage)
		travail.reperage_CIM10_Charlson (keep = BEN_IDT_ANO reperage)
		travail.reperage_GHM_Charlson (keep = BEN_IDT_ANO reperage)
		travail.Charlson_DCIR_demence (keep = BEN_IDT_ANO reperage)
		travail.Charlson_DCIR_pulmonaire (keep = BEN_IDT_ANO reperage)
		travail.Charlson_DCIR_diabete2 (keep = BEN_IDT_ANO reperage)
		travail.Charlson_DCIR_diabete3 (keep = BEN_IDT_ANO reperage);
run;

* On r�cup�re 1 info unique par patient;
proc sort data = travail.reperages_Charlson nodupkey;
	by BEN_IDT_ANO reperage;
run;

proc delete data = travail.reperage_CCAM_Charlson travail.reperage_CIM10_Charlson travail.reperage_GHM_Charlson travail.Charlson_DCIR_demence
	travail.Charlson_DCIR_pulmonaire travail.Charlson_DCIR_diabete2 travail.Charlson_DCIR_diabete3;
run; quit;

*	******************************************************************************************************************************************;
*	Correction des reperages;

* Les patients rep�r�s pour Diab�te sans complication + Patho c�r�brovasculaire ou patho r�nale mod�r�e ou s�v�re ou infarctus du myocarde;
* 	=> Diab�te avec complication;
data patients_Diabete_sans;
	set travail.reperages_Charlson;
	where reperage = 10;
run;

data patients_complications;
	set travail.reperages_Charlson;
	where reperage in (1, 4, 12);
run;

proc sql;

	DELETE FROM travail.reperages_Charlson
		WHERE BEN_IDT_ANO IN (SELECT BEN_IDT_ANO FROM patients_Diabete_sans)
			AND BEN_IDT_ANO IN (SELECT BEN_IDT_ANO FROM patients_complications)
			AND reperage = 10;

	INSERT INTO travail.reperages_Charlson
		SELECT	BEN_IDT_ANO,
				13
		FROM patients_Diabete_sans
			WHERE BEN_IDT_ANO IN (SELECT BEN_IDT_ANO FROM patients_complications);

quit;

proc delete data = patients_Diabete_sans patients_complications;
run; quit;

* Les patients rep�r�s pour Diab�te sans complication et Diab�te avec complication => Diab�te avec complication;
data patients_Diabete_sans;
	set travail.reperages_Charlson;
	where reperage = 10;
run;

data patients_Diabete_avec;
	set travail.reperages_Charlson;
	where reperage = 13;
run;

proc sql;

	DELETE FROM travail.reperages_Charlson
		WHERE BEN_IDT_ANO IN (SELECT BEN_IDT_ANO FROM patients_Diabete_sans)
			AND BEN_IDT_ANO IN (SELECT BEN_IDT_ANO FROM patients_Diabete_avec)
			AND reperage = 10;

quit;

proc delete data = patients_Diabete_sans patients_Diabete_avec;
run; quit;

* Les patients rep�r�s pour Cancer et Pathologie m�tastatique => Pathologie m�tastatique;
data patients_Cancer;
	set travail.reperages_Charlson;
	where reperage = 14;
run;

data patients_Metastatique;
	set travail.reperages_Charlson;
	where reperage = 16;
run;

proc sql;

	DELETE FROM travail.reperages_Charlson
		WHERE BEN_IDT_ANO IN (SELECT BEN_IDT_ANO FROM patients_Cancer) 
			AND BEN_IDT_ANO IN (SELECT BEN_IDT_ANO FROM patients_Metastatique)
			AND reperage = 14;

quit;

proc delete data = patients_Cancer patients_Metastatique;
run; quit;

* Les patients rep�r�s pour Pathologie h�patique l�g�re et Pathologie h�patique mod�r�e ou s�v�re => Pathologie h�patique mod�r�e ou s�v�re;
data patients_hepatique_leg;
	set travail.reperages_Charlson;
	where reperage = 9;
run;

data patients_hepatique_sev;
	set travail.reperages_Charlson;
	where reperage = 15;
run;

proc sql;

	DELETE FROM travail.reperages_Charlson
		WHERE BEN_IDT_ANO IN (SELECT BEN_IDT_ANO FROM patients_hepatique_leg)
			AND BEN_IDT_ANO IN (SELECT BEN_IDT_ANO FROM patients_hepatique_sev)
			AND reperage = 9;

quit;

proc delete data = patients_hepatique_leg patients_hepatique_sev;
run; quit;

*	******************************************************************************************************************************************;
*	Ajout des rep�rage des CMA dans la table de population;

%macro maj_Charlson_population(num_CMA=, nom_CMA=);

	proc sql;

		ALTER TABLE res.T_INDI_BPCO_&an_N. ADD &nom_CMA. INT length = 3;

		UPDATE res.T_INDI_BPCO_&an_N.
			SET &nom_CMA. =	
				CASE	WHEN BEN_IDT_ANO IN (SELECT BEN_IDT_ANO FROM travail.reperages_Charlson WHERE reperage = &num_CMA.) 
						THEN 1
						ELSE 0
						END ;

	quit;

%mend maj_Charlson_population;

%maj_Charlson_population(
	num_CMA = 1,
	nom_CMA = Charlson_IDM
	);

%maj_Charlson_population(
	num_CMA = 2,
	nom_CMA = Charlson_Insuf_Cardiaque
	);

%maj_Charlson_population(
	num_CMA = 3,
	nom_CMA = Charlson_Malad_Vasculaire
	);

%maj_Charlson_population(
	num_CMA = 4,
	nom_CMA = Charlson_Malad_CerebroVasc
	);

%maj_Charlson_population(
	num_CMA = 5,
	nom_CMA = Charlson_Demence
	);

%maj_Charlson_population(
	num_CMA = 6,
	nom_CMA = Charlson_Malad_Pulmonaires
	);

%maj_Charlson_population(
	num_CMA = 7,
	nom_CMA = Charlson_Malad_Rhumatisme
	);

%maj_Charlson_population(
	num_CMA = 8,
	nom_CMA = Charlson_Ulcere
	);

%maj_Charlson_population(
	num_CMA = 9,
	nom_CMA = Charlson_Hepatite
	);

%maj_Charlson_population(
	num_CMA = 10,
	nom_CMA = Charlson_Diabete_SansCompl
	);

%maj_Charlson_population(
	num_CMA = 11,
	nom_CMA = Charlson_Hemiplegie_Para
	);

%maj_Charlson_population(
	num_CMA = 12,
	nom_CMA = Charlson_Malad_Renale
	);

%maj_Charlson_population(
	num_CMA = 13,
	nom_CMA = Charlson_Diabete_Compl
	);

%maj_Charlson_population(
	num_CMA = 14,
	nom_CMA = Charlson_Cancer
	);

%maj_Charlson_population(
	num_CMA = 15,
	nom_CMA = Charlson_Patho_Foie
	);

%maj_Charlson_population(
	num_CMA = 16,
	nom_CMA = Charlson_Tumeur
	);

%maj_Charlson_population(
	num_CMA = 17,
	nom_CMA = Charlson_Malad_VIH
	);

*	******************************************************************************************************************************************;
*	Calcul du score de Charlson;

proc sql;

	ALTER TABLE res.T_INDI_BPCO_&an_N. ADD score_Charlson_CMA INT length = 3;

	UPDATE res.T_INDI_BPCO_&an_N.
		SET score_Charlson_CMA = SUM
			(CASE	WHEN Charlson_IDM = 1 THEN 0
					WHEN Charlson_IDM = 0 THEN 0
					ELSE .
					END,
			CASE 	WHEN Charlson_Insuf_Cardiaque = 1 THEN 2
					WHEN Charlson_Insuf_Cardiaque = 0 THEN 0
					ELSE .
					END,
			CASE 	WHEN Charlson_Malad_Vasculaire = 1 THEN 1
					WHEN Charlson_Malad_Vasculaire = 0 THEN 0
					ELSE .
					END,
			CASE	WHEN Charlson_Malad_CerebroVasc = 1 THEN 1
					WHEN Charlson_Malad_CerebroVasc = 0 THEN 0
					ELSE .
					END,
			CASE 	WHEN Charlson_Demence = 1 THEN 2
					WHEN Charlson_Demence = 0 THEN 0
					ELSE .
					END,
			CASE 	WHEN Charlson_Malad_Pulmonaires = 1 THEN 1
					WHEN Charlson_Malad_Pulmonaires = 0 THEN 0
					ELSE .
					END,
			CASE 	WHEN Charlson_Malad_Rhumatisme = 1 THEN 0
					WHEN Charlson_Malad_Rhumatisme = 0 THEN 0
					ELSE .
					END,
			CASE 	WHEN Charlson_Ulcere = 1 THEN 0
					WHEN Charlson_Ulcere = 0 THEN 0
					ELSE .
					END,
			CASE 	WHEN Charlson_Hepatite = 1 THEN 2
					WHEN Charlson_Hepatite = 0 THEN 0
					ELSE .
					END,
			CASE 	WHEN Charlson_Diabete_SansCompl = 1 THEN 0
					WHEN Charlson_Diabete_SansCompl = 0 THEN 0
					ELSE .
					END,
			CASE 	WHEN Charlson_Hemiplegie_Para = 1 THEN 2
					WHEN Charlson_Hemiplegie_Para = 0 THEN 0
					ELSE .
					END,
			CASE 	WHEN Charlson_Malad_Renale = 1 THEN 1
					WHEN Charlson_Malad_Renale = 0 THEN 0
					ELSE .
					END,
			CASE 	WHEN Charlson_Diabete_Compl = 1 THEN 0
					WHEN Charlson_Diabete_Compl = 0 THEN 0
					ELSE .
					END,
			CASE 	WHEN Charlson_Cancer = 1 THEN 2
					WHEN Charlson_Cancer = 0 THEN 0
					ELSE .
					END,
			CASE 	WHEN Charlson_Patho_Foie = 1 THEN 3
					WHEN Charlson_Patho_Foie = 0 THEN 0
					ELSE .
					END,
			CASE 	WHEN Charlson_Tumeur = 1 THEN 11
					WHEN Charlson_Tumeur = 0 THEN 0
					ELSE .
					END,
			CASE 	WHEN Charlson_Malad_VIH = 1 THEN 1
					WHEN Charlson_Malad_VIH = 0 THEN 0
					ELSE .
					END
			) ;

	* Ajout du score pond�r� par l �ge;
	ALTER TABLE res.T_INDI_BPCO_&an_N. ADD Charlson INT length = 3;

	UPDATE res.T_INDI_BPCO_&an_N.
		SET Charlson =	
			CASE	WHEN Age < 50 then score_Charlson_CMA
					WHEN Age BETWEEN 50 AND 59 THEN COALESCE(score_Charlson_CMA, 0) + 1
					WHEN Age BETWEEN 60 AND 69 THEN COALESCE(score_Charlson_CMA, 0) + 2
					WHEN Age BETWEEN 70 AND 79 THEN COALESCE(score_Charlson_CMA, 0) + 3
					WHEN Age BETWEEN 80 AND 89 THEN COALESCE(score_Charlson_CMA, 0) + 4
					WHEN Age >= 90 THEN COALESCE(score_Charlson_CMA, 0) + 5
					END ;

	* Suppression du score de Charlson non pond�r�;
	ALTER TABLE res.T_INDI_BPCO_&an_N. DROP score_Charlson_CMA;

quit;

*	******************************************************************************************************************************************;
*	V�rifications;

* On ne passe pas par la macro car trop de variables;
proc sql;

	CREATE TABLE verif._0&tmp_num_tab._Charlson AS
		SELECT 
			Charlson,
			Charlson_IDM,
			Charlson_Insuf_Cardiaque,
			Charlson_Malad_Vasculaire,
			Charlson_Malad_CerebroVasc,
			Charlson_Demence,
			Charlson_Malad_Pulmonaires,
			Charlson_Malad_Rhumatisme,
			Charlson_Ulcere,
			Charlson_Hepatite,
			Charlson_Diabete_SansCompl,
			Charlson_Hemiplegie_Para,
			Charlson_Malad_Renale,
			Charlson_Diabete_Compl,
			Charlson_Cancer,
			Charlson_Patho_Foie,
			Charlson_Tumeur,
			Charlson_Malad_VIH,
			COUNT(*) AS Frequency
		FROM res.T_INDI_BPCO_&an_N.
		GROUP BY Charlson, Charlson_IDM, Charlson_Insuf_Cardiaque, Charlson_Malad_Vasculaire, Charlson_Malad_CerebroVasc, Charlson_Demence,
			Charlson_Malad_Pulmonaires, Charlson_Malad_Rhumatisme, Charlson_Ulcere, Charlson_Hepatite, Charlson_Diabete_SansCompl, Charlson_Hemiplegie_Para,
			Charlson_Malad_Renale, Charlson_Diabete_Compl, Charlson_Cancer, Charlson_Patho_Foie, Charlson_Tumeur, Charlson_Malad_VIH;

quit;

proc sql noprint; SELECT SUM(Frequency) INTO: nb_tot FROM verif._0&tmp_num_tab._Charlson; quit;

data verif._0&tmp_num_tab._Charlson;
	set verif._0&tmp_num_tab._Charlson;
	length percent 4.;
	format percent nlpct7.1 Frequency commafr_0ch.;
	percent = Frequency/&nb_tot.;
run;

%let tmp_num_tab = %sysevalf(&tmp_num_tab. + 1);

*	******************************************************************************************************************************************;
*	On supprime les tables temporaires;

proc delete data = travail.reperages_Charlson travail.reperage_ATC_Charlson;
run; quit;
