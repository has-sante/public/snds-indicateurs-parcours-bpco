/*	******************************************************************************************************************************************	*/
/*																																				*/
/*		Table finale - Ajout de formats et de labels																							*/
/*																																				*/
/*	******************************************************************************************************************************************	*/

*	******************************************************************************************************************************************;
*	Cr�ation de la table finale;

data res.T_INDI_BPCO_&an_N.;
	set res.T_INDI_BPCO_&an_N. (keep = Ben_Idt_Ano Age Ben_Sex_Cod Ben_Dcd_Dte BEN_RES_DPT BEN_RES_COM RGM_GRG_COD ALD ALD_Chapitre_1 
			ALD_Chapitre_2 ALD_Chapitre_3 ALD_Chapitre_4 ALD_Chapitre_5 ALD_Chapitre_6 ALD_Chapitre_7 ALD_Chapitre_8 ALD_Chapitre_9 
			ALD_Chapitre_10 ALD_Chapitre_11 ALD_Chapitre_12 ALD_Chapitre_13 ALD_Chapitre_14 ALD_Chapitre_15 ALD_Chapitre_16 ALD_Chapitre_17 
			ALD_Chapitre_18 ALD_Chapitre_19 ALD_Chapitre_20 ALD_Chapitre_21 ALD_Chapitre_22 ALD_BPCO_&an_N. Nb_Broncho_LDA Charlson 
			Charlson_IDM Charlson_Insuf_Cardiaque Charlson_Malad_Vasculaire Charlson_Malad_CerebroVasc Charlson_Demence Charlson_Malad_Pulmonaires 
			Charlson_Malad_Rhumatisme Charlson_Ulcere Charlson_Hepatite Charlson_Diabete_Compl Charlson_Diabete_SansCompl Charlson_Hemiplegie_Para 
			Charlson_Malad_Renale Charlson_Cancer Charlson_Patho_Foie Charlson_Tumeur Charlson_Malad_VIH FDEP13 code_insee quintile_pop 
			Ben_Cmu_Top Nb_Sej_Exacerbation
			Indicateur_01 Indicateur_02a Indicateur_02b Indicateur_03 Indicateur_04 Indicateur_05 Indicateur_06 Indicateur_07 Indicateur_09 nb_ATB
			ALD_BPCO_Septembre ALD_BPCO_decembre nb_hospit_BPCO nb_hospit_BPCO_Ind02 nb_tabac);
	label		
		BEN_IDT_ANO = "Num�ro d'individu"
		Age = "Age du patient au 1er janvier &Annee_N."
		Ben_Sex_Cod = "Sexe du patient"
		BEN_DCD_DTE = "Date de d�c�s"
		BEN_RES_DPT = "D�partement de r�sidence du patient"
		BEN_RES_COM = "Commune de r�sidence"
		RGM_GRG_COD = "Grand r�gime d'affiliation"
		ALD = "Au moins une ALD active en &Annee_N."
		ALD_Chapitre_1 = "ALD A00-B99 : Certaines maladies infectieuses et parasitaires"
		ALD_Chapitre_2 = "ALD C00-D48 : Tumeurs"
		ALD_Chapitre_3 = "ALD D50-D89 : Maladies du sang et des organes h�matopoi�tiques et certains troubles du syst�me immunitaire"
		ALD_Chapitre_4 = "ALD E00-E90 : Maladies endocriniennes, nutritionnelles et m�taboliques"
		ALD_Chapitre_5 = "ALD F00-F99 : Troubles mentaux et du comportement"
		ALD_Chapitre_6 = "ALD G00-G99 : Maladies du syst�me nerveux"
		ALD_Chapitre_7 = "ALD H00-H59 : Maladies de l'�il et de ses annexes"
		ALD_Chapitre_8 = "ALD H60-H95 : Maladies de l'oreille et de l'apophyse masto�de"
		ALD_Chapitre_9 = "ALD I00-I99 : Maladies de l'appareil circulatoire"
		ALD_Chapitre_10 = "ALD J00-J99 : Maladies de l'appareil respiratoire"
		ALD_Chapitre_11 = "ALD K00-K93 : Maladies de l'appareil digestif"
		ALD_Chapitre_12 = "ALD L00-L99 : Maladies de la peau et du tissu cellulaire sous-cutan�"
		ALD_Chapitre_13 = "ALD M00-M99 : Maladies du syst�me ost�o-articulaire, des muscles et du tissu conjonctif"
		ALD_Chapitre_14 = "ALD N00-N99 : Maladies de l'appareil g�nito-urinaire"
		ALD_Chapitre_15 = "ALD O00-O99 : Grossesse, accouchement et puerp�ralit�"
		ALD_Chapitre_16 = "ALD P00-P96 : Certaines affections dont l'origine se situe dans la p�riode p�rinatale"
		ALD_Chapitre_17 = "ALD Q00-Q99 : Malformations cong�nitales et anomalies chromosomiques"
		ALD_Chapitre_18 = "ALD R00-R99 : Sympt�mes, signes et r�sultats anormaux d'examens cliniques et de laboratoire, non class�s ailleurs"
		ALD_Chapitre_19 = "ALD S00-T98 : L�sions traumatiques, empoisonnements et certaines autres cons�quences de causes externes"
		ALD_Chapitre_20 = "ALD V01-Y98 : Causes externes de morbidit� et de mortalit�"
		ALD_Chapitre_21 = "ALD Z00-Z99 : Facteurs influant sur l'�tat de sant� et motifs de recours aux services de sant�"
		ALD_Chapitre_22 = "ALD U00-U99 : Codes d'utilisation particuli�re"
		ALD_BPCO_&an_N. = "Patient ayant une ALD BPCO active en &Annee_N."
		Nb_Broncho_LDA = "Nombre de d�livrances rembours�es de bronchodilatateur anticholinergique ou B�ta-2 longue dur�e d'action en &Annee_N."
		Charlson = "Indice de Charlson en &Annee_N."
		Charlson_IDM = "Composante de l'indice de Charlson - Infarctus du myocarde"
		Charlson_Insuf_Cardiaque = "Composante de l'indice de Charlson - Insuffisance cardiaque congestive"
		Charlson_Malad_Vasculaire = "Composante de l'indice de Charlson - Maladie vasculaire p�riph�rique"
		Charlson_Malad_CerebroVasc = "Composante de l'indice de Charlson - Maladies c�r�brovasculaires"
		Charlson_Demence = "Composante de l'indice de Charlson - D�mence"
		Charlson_Malad_Pulmonaires = "Composante de l'indice de Charlson - Maladies pulmonaires chroniques"
		Charlson_Malad_Rhumatisme = "Composante de l'indice de Charlson - Maladies rhumatismales"
		Charlson_Ulcere = "Composante de l'indice de Charlson - Ulc�re de l'estomac"
		Charlson_Hepatite = "Composante de l'indice de Charlson - H�patite"
		Charlson_Diabete_Compl = "Composante de l'indice de Charlson - Diab�te avec complications chroniques"
		Charlson_Diabete_SansCompl = "Composante de l'indice de Charlson - Diab�te sans complications chroniques"
		Charlson_Hemiplegie_Para = "Composante de l'indice de Charlson - H�mipl�gies ou parapl�gies"
		Charlson_Malad_Renale = "Composante de l'indice de Charlson - Maladies r�nales"
		Charlson_Cancer = "Composante de l'indice de Charlson - Cancers"
		Charlson_Patho_Foie = "Composante de l'indice de Charlson - Pathologies du foie de s�v�re � mod�rer"
		Charlson_Tumeur = "Composante de l'indice de Charlson - Tumeurs malignes m�tastasique"
		Charlson_Malad_VIH = "Composante de l'indice de Charlson - Maladies dues au VIH"
		FDEP13 = "Indice de d�favorisation sociale"
		code_insee = "Code INSEE"
		quintile_pop = "R�partition en quintile des communes pour l'indice de d�favorisation 2013"
		BEN_CMU_TOP = "Patient b�n�ficiant de la CMUC en &Annee_N."
		Nb_Sej_Exacerbation = "Nombre de s�jours pour exacerbation en &Annee_N."
		;
	format BPCO_: ALD: Charlson_: BEN_CMU_TOP f_oui_non. BEN_DCD_DTE date9.;
run;

* V�rif;
proc sql;

	SELECT COUNT(*) AS nb_lignes, COUNT(DISTINCT BEN_IDT_ANO) AS nb_patients
	FROM res.T_INDI_BPCO_&an_N.;

quit;

*	******************************************************************************************************************************************;
*	Cr�ation d une table de correspondance et d'une table de population avec les indicateur;

data travail.corresp_id_patient;
	set orauser.corresp_id_patient;
run;
